import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import {AssetsController} from './assets.controller';
import {AssetsService} from './assets.service';
import {AssetSchema} from './schemas/asset.schema';
import {ProgramSchema} from '../programs/schemas/programs.schema';
import {ProjectSchema} from '../projects/schemas/projects.schema';
import {StatusSchema} from "../status/schemas/status.schema";
import {TypeSchema} from "../type/schemas/type.schema";
import {ClassificationSchema} from "../classification/schemas/classification.schema";
import {EffortSchema} from "../effort/schemas/effort.schema";
import {UserSchema} from "../users/schemas/user.schema";

@Module({
  imports: [MongooseModule.forFeature([{name: 'Type', schema: TypeSchema}, {name: 'Effort', schema: EffortSchema},
      {name: 'Classification', schema: ClassificationSchema}, { name: 'Asset', schema: AssetSchema },
      { name: 'Programs', schema: ProgramSchema }, {name: 'Status', schema: StatusSchema},
      {name: 'Projects', schema: ProjectSchema},{ name: 'Users', schema: UserSchema }])],
  controllers: [AssetsController],
  providers: [AssetsService],
})
export class AssetsModule {}
