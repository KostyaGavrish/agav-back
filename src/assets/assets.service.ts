import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

import {AssetModel} from './dto/create-asset.dto';
import {Asset} from './interfaces/asset.interface';
import * as jwt from 'jsonwebtoken';
import {Programs} from '../programs/interfaces/program.interfaces';
import {Projects} from '../projects/interfaces/project.interface';
import {Status} from '../status/interfaces/status.interface';
import {Type} from '../type/interfaces/type.interface';
import {Classification} from "../classification/interfaces/classification.interface";
import {Effort} from "../effort/interfaces/effort.interface";
import {Destination} from "../destination/interfaces/destination.interface";
import {Users} from "../users/interfaces/users.interface";
const mongoose = require('mongoose');

@Injectable()
export class AssetsService {
  constructor(@InjectModel('Asset') private readonly assetModel: Model<Asset>,
              @InjectModel('Projects') private readonly projectModel: Model<Projects>,
              @InjectModel('Programs') private readonly programModel: Model<Programs>,
              @InjectModel('Status') private readonly statusModel: Model<Status>,
              @InjectModel('Type') private readonly typeModel: Model<Type>,
              @InjectModel('Classification') private readonly classificationModel: Model<Classification>,
              @InjectModel('Effort') private readonly effortModel: Model<Effort>,
              @InjectModel('Users') private readonly userModel: Model<Users>) {}

  async create(asset: AssetModel): Promise<Asset> {
    let newID: string;
    let date = new Date();
    // console.log(await this.assetModel.findOne({}, {}, {sort: {'created_at': -1}}).exec());
    const lastAsset = await this.assetModel.findOne({}, {}, {sort: {'index': -1}}).exec();
    // console.log('lastAsset.index', lastAsset.index);
    asset.index = lastAsset ? (lastAsset.index + 1 || 1) : 1;
    let project = await this.projectModel.findOne({_id: asset.identification.projectId});
    newID = project.shortName + '-' + asset.index + '-' + date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear();
    asset.id = newID;
    const createdAsset = new this.assetModel(asset);
    const err = createdAsset.validateSync();
    return err ? err : createdAsset.save();
  }

  async updateAsset(assetID, asset){
    const createdAsset = new this.assetModel(asset);
    const err = await createdAsset.validateSync();
    console.log(err);
    if (err){
      return err;
    } else {
      console.log('asset', asset);
      return await this.assetModel.findOneAndUpdate({id: assetID}, {$set: asset}, {new: true}).exec();
    }
  }

  async findAll() {
    // const allAssets = await this.assetModel.find().exec();
    // let assets: Array<object> = [];
    // const userFromJwt = jwt.decode(token, 'secretKey');
    // const user = await this.userModel.findOne({id: userFromJwt.id});
    // console.log('asddsdssf------------------', userFromJwt);
    // console.log('asddsdssf------------------', user);
    // if (user.role === "ADMIN" || user.role === "SUPER_USER") {
    //   assets = allAssets.concat();
    // } else {
    //   for (let i = 0; i < allAssets.length; i++) {
    //     const program = await this.programModel.findOne({_id: allAssets[i].identification.programId});
    //     const project = await this.projectModel.findOne({_id: allAssets[i].identification.projectId});
    //     for (let j = 0; j < user['creationRights'].length; j++) {
    //       if (+program.id === user['creationRights'][j].programId) {
    //         if (user['creationRights'][j].projects.indexOf(+project.id !== -1)) {
    //           console.log('--------------------------------------------user program', user['creationRights'][j].programId);
    //           console.log('--------------------------------------------program', +program.id);
    //           assets.push(allAssets[i]);
    //         }
    //       }
    //     }
    //   }
    // }
    const assets = await this.assetModel.find().exec();
    if (assets.length === 0) {
      return assets;
    }
    // console.log(assets);
    for (let i = 0; i < assets.length; i++){
      let tempStatus;
      let tempType;
      let tempEffortType;
      let tempEffortUnit;
      let tempClassification;

      //1
      if (mongoose.Types.ObjectId.isValid(assets[i].status)) {
        tempStatus = await this.statusModel.findOne({_id: assets[i].status});
        if (tempStatus) assets[i].status = tempStatus.name;
      } else {
        assets[i].status = '';
      }

      //2
      if (mongoose.Types.ObjectId.isValid(assets[i].type)) {
        tempType = await this.typeModel.findOne({_id: assets[i].type}).exec();
        if (tempType) {
          assets[i].type = tempType.name;
        } else {
          assets[i].type = '';
        }
      } else {
        assets[i].type = '';
      }

      //3
      if (assets[i].efforts && mongoose.Types.ObjectId.isValid(assets[i].efforts.type)) {
        tempEffortType = await this.effortModel.findOne({_id: assets[i].efforts.type}).exec();
        assets[i].efforts.type = tempEffortType.type;
        tempEffortUnit = tempEffortType.unit.findIndex(x => x.id === +assets[i].efforts.unit);
        if(tempEffortType.unit[tempEffortUnit]) assets[i].effort.unit = tempEffortType.unit[tempEffortUnit].name;
        if (tempEffortUnit == -1) assets[i].efforts.unit = '';
      } else {
        // console.log('assets', assets);
        assets[i].efforts.type = '';
      }

      //4
      if (assets[i].classification && mongoose.Types.ObjectId.isValid(assets[i].classification.type)) {
        tempClassification = await this.classificationModel.findOne({_id: assets[i].classification.type}).exec();
        // console.log('tempClassification', tempClassification);
          if (tempClassification) {
          assets[i].classification.type = tempClassification.name;
        } else {
          assets[i].classification.type = '';
        }
      } else {
        assets[i].classification.type = '';
      }
      assets[i].identification.program = await this.programModel.findOne({_id: assets[i].identification.programId});
      assets[i].identification.project = await this.projectModel.findOne({_id: assets[i].identification.projectId});
    }
    const result = this.assetsTree(assets);
    return assets;
  }

  assetsTree(assets) {
    let keysObj = {};
    let resArr = [];

    assets.forEach(asset => {
      // console.log('keysObj[asset.id]', keysObj[asset.id]);
      if (keysObj[asset.id]) {
        let tArr = keysObj[asset.id];
        keysObj[asset.id] = asset;
        asset.children = tArr;
      } 
      else {
        keysObj[asset.id] = asset;
        asset.children = [];
      }

      if (asset.parentAssetId && asset.parentAssetId !== '0') {
        if (!keysObj[asset.parentAssetId]) {
            keysObj[asset.parentAssetId] = [asset];
        } else if (Array.isArray(keysObj[asset.parentAssetId])) {
          keysObj[asset.parentAssetId].push(asset);
        } else {
          keysObj[asset.parentAssetId].children.push(asset);
        }
      } else {
        resArr.push(asset);
      }
    });
    return resArr;
  }

  async findAllParrents(){
    return await this.assetModel.find({isPlatformAsset: true}).exec();
  }

  async findAllUnitaire(){
    return await this.assetModel.find({parentAssetId: '0', isPlatformAsset: false}).exec();
  }

  async findChilds(id){
    return await this.assetModel.find({parentAssetId: id}).exec();
  }
  async deleteAsset(assetID, headers) {
      const user = jwt.decode(headers.authorization.substring(7), 'secretKey');
      // console.log('user', user);
      const asset = await this.getAsset(assetID);
      if (asset && (asset.identification.userId == user.id || user.roles[0] === 'ADMIN')) {
        this.assetModel.updateMany({parentAssetId: assetID}, {$set: { parentAssetId: '0',  isPlatformAsset: true}}).then((arr) => {
          // console.log(arr);
        });
        return  this.assetModel.deleteOne({id: assetID}).exec();
      } 
      else {
        return false;
      }

  }

  async getAsset(assetID){
    return await this.assetModel.findOne({id: assetID}).exec();
  }

  async getNewAsset(assetID) {
    let assets = await this.assetModel.findOne({id: assetID}).exec();
    let tempStatus;
    let tempType;
    let tempEffortType;
    let tempEffortUnit;
    let tempClassification;
    //1
    if (mongoose.Types.ObjectId.isValid(assets.status)) {
      tempStatus = await this.statusModel.findOne({_id: assets.status});
      if (tempStatus) assets.status = tempStatus.name;
    } else {
      assets.status = '';
    }

    //2
    if (mongoose.Types.ObjectId.isValid(assets.type)) {
      tempType = await this.typeModel.findOne({_id: assets.type}).exec();
      if (tempType) {
        assets.type = tempType.name;
      } else {
        assets.type = '';
      }
    } else {
      assets.type = '';
    }

    //3
      if (assets.efforts && assets.efforts.efforts) {
          for (let i = 0; i < assets.efforts.efforts.length; i++) {
              if (assets.efforts.efforts[i] && mongoose.Types.ObjectId.isValid(assets.efforts.efforts[i].type)) {
                  tempEffortType = await this.effortModel.findOne({_id: assets.efforts.efforts[i].type}).exec();
                  // console.log('tempEffortType', tempEffortType);
                  // console.log('tempEffortType.type', tempEffortType.type);
                  // const test = tempEffortType.type;
                  assets.efforts.efforts[i].type = tempEffortType.type;
                  tempEffortUnit = tempEffortType.unit.findIndex(x => x.id === +assets.efforts.efforts[i].unit);
                  if (tempEffortType.unit[tempEffortUnit]) assets.efforts.efforts[i].unit = tempEffortType.unit[tempEffortUnit].name;
                  if (tempEffortUnit === -1) assets.efforts.efforts[i].unit = '';
                  // else {
                  //     assets.efforts.efforts[i].type = '';
                  // }
              }
          }
      }
      // tempEffortType = await this.effortModel.findOne({_id: assets.efforts.type}).exec();
      // assets.efforts.efforts[i].type = tempEffortType.type;
      // tempEffortUnit = tempEffortType.unit.findIndex(x => x.id === +assets.efforts.efforts[i].unit.id);
      // if (tempEffortType.unit[tempEffortUnit]) assets.efforts.unit = tempEffortType.unit[tempEffortUnit].name;
    //   if (tempEffortUnit == -1) assets.efforts.efforts[i].unit = '';
    // } else {
    //   assets.efforts.efforts[i].type = '';
    // }

    //4
      console.log('assets', assets);
    if (assets.classification && mongoose.Types.ObjectId.isValid(assets.classification.type)) {
      tempClassification = await this.classificationModel.findOne({_id: assets.classification.type}).exec();
      console.log('-------------------------', tempClassification);
      if (tempClassification) {
        assets.classification.type = tempClassification.name;
      } else {
        assets.classification.type = '';
      }
    } else {
      assets.classification.type = '';
    }
    console.log('assets.classification.type', assets.classification.type);

    return assets;
  }

  async findAllAssets(){
    return await this.assetModel.find().exec();
  }
  async findAssetsChildren(){
    return await this.assetModel.find({parentAssetId: { $not: '0' }}).exec();
  }

}
