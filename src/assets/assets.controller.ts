import {
    Controller, Get, Post, Body, Headers, Param, UseGuards, Delete, Put, Res, HttpStatus,
    ReflectMetadata,
} from '@nestjs/common';
import {AssetsService} from './assets.service';
import {AssetModel} from './dto/create-asset.dto';
import {Asset} from './interfaces/asset.interface';
import {ApiBearerAuth, ApiOAuth2Auth, ApiResponse, ApiUseTags} from '@nestjs/swagger';
import {AuthGuard} from '@nestjs/passport';
import {TokenModel} from '../models/token.model';
import {AssetIdModel} from './dto/assetId.model';
import {RolesGuard} from '../guards/roles.guard';
import {Roles} from '../guards/roles.decorator';

@ApiUseTags('Assets')
@ApiOAuth2Auth()
@Controller()
export class AssetsController {
  constructor(private readonly assetsService: AssetsService) {}

  @Post('/asset')
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({status: 200, description: 'Success'})
  @ApiResponse({status: 401, description: 'Unauthorized'})
  @ApiResponse({status: 400, description: 'Bad request'})
  async create(@Res() res, @Body() asset: AssetModel, @Headers() headers: TokenModel) {
    const answer = (await this.assetsService.create(asset));
    // console.log('answer------------------------->', answer);
    answer.errors ? res.status(HttpStatus.BAD_REQUEST).json(answer.errors) : res.status(HttpStatus.CREATED).json(answer);
  }

  @Get('/assetsList')
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  @ApiResponse({status: 400, description: 'Bad request'})
  async getAssetsList(@Headers() headers: TokenModel): Promise<Asset[]> {
    return this.assetsService.findAll();
  }

  @Get('/assetsChildren')
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  @ApiResponse({status: 400, description: 'Bad request'})
  async assetsChildren(@Headers() headers: TokenModel): Promise<Asset[]> {
    return this.assetsService.findAssetsChildren();
  }

  @Get('/assetsAll')
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  @ApiResponse({status: 400, description: 'Bad request'})
  async assetsAll(@Headers() headers: TokenModel): Promise<Asset[]> {
    return this.assetsService.findAllAssets();
  }

  @Get('/asset/:assetId')
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  @ApiResponse({status: 400, description: 'Bad request'})
  getAsset(@Headers() headers: TokenModel, @Param() params: AssetIdModel) {
    return this.assetsService.getAsset(params.assetId);
  }

  @Get('/newasset/:assetId')
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  @ApiResponse({status: 400, description: 'Bad request'})
  getNewAsset(@Headers() headers: TokenModel, @Param() params: AssetIdModel) {
    return this.assetsService.getNewAsset(params.assetId);
  }

  @Delete('/asset/:assetId')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('ADMIN')
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  @ApiResponse({status: 400, description: 'Bad request'})
  async deleteAsset(@Headers() headers: TokenModel, @Res() res, @Param() params: AssetIdModel) {
      const result = await this.assetsService.deleteAsset(params.assetId, headers);
      result ? res.status(HttpStatus.OK).send() : res.status(HttpStatus.FORBIDDEN).send();
  }

  @Put('/asset/:assetId')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('ADMIN')
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 403, description: 'Error access'})
  async updateAsset(@Headers() headers: TokenModel, @Res() res, @Param() params: AssetIdModel, @Body() body: AssetModel) {
    // return this.assetsService.updateAsset(params.assetId, body);
    // return params.assetId
    //   return await this.assetsService.updateAsset(params.assetId, body)
    const answer = (await this.assetsService.updateAsset(params.assetId, body));
    answer.errors ? res.status(HttpStatus.BAD_REQUEST).json(answer.errors) : res.status(HttpStatus.OK).json(answer);
  }
}
