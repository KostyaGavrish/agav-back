import { Document } from 'mongoose';

export interface Asset extends Document {
  readonly id: string;
  identification: {
    name: string,
    programId: string,
    projectId: string,
    userId: string,
    creationDate: number,
  };
}
