import * as mongoose from 'mongoose';
import any = jasmine.any;
import {type} from "os";

export const AssetSchema = new mongoose.Schema({
  id: String,
  index: Number,
  identification: {
    name: {
      type: String,
      required: true,
    },
    programId: {
      type: String,
      required: true,
    },
    program: {
      type: Object,
      // required: true,
    },
    project: {
      type: Object,
      // required: true,
    },
    projectId: {
      type: String,
      required: true,
    },
    userId: {
      type: String,
      // required: true,
    },
    creationDate: {
      type: Date,
      // required: true,
    },
  },
  desc: {
    type: String,
    // required: true,
  },
  status: {
    type: String,
    // enum: ['PENDING', 'WAITING_VALIDATION', 'VALIDATED'],
    // required: true,
  },
  assetStatus: {
      type: String,
      // enum: ['INITIALIZED', 'IN_PROGRESS', 'DONE', 'TRANSFERED'],
  },
  type: {
    type: String,
    // enum: ['METHOD', 'STATE_OF_ART', 'STUDY', 'DATASET', 'PATENT', 'PUBLICATION', 'SOFTWARE', 'STANDARD', 'OTHER'],
    // enum: ['METHOD', 'STATE_OF_ART', 'STUDY', 'DATASET', 'PATENT', 'SOFTWARE', 'STANDARD', 'OTHER'],
    // required: true,
  },
  deliverable: {
    exists: {
      type: Boolean,
      // required: true,
    },
    reference: {
      type: String,
      // required: true,
    },
  },
  piShared: {
    exists: {
      type: Boolean,
      // required: true,
    },
    partners: {
      type: String,
      // required: true,
    },
  },
  // effort: {
  //   type: {
  //     type: String,
  //     // enum: ['HUMAN', 'FINANCIAL'],
  //     // required: true,
  //   },
  //   ha: {
  //     type: Number,
  //     // required: true,
  //   },
  //   jh: {
  //     type: Number,
  //     // required: true,
  //   },
  //   value: {
  //     type: Number,
  //   },
  //   unit: {
  //     type: String,
  //   },
  //   hm: {
  //     type: Number,
  //     // required: true,
  //   },
  //   contributors: {
  //     type: String,
  //     // required: true,
  //   },
  // },
    efforts: {
        efforts: [
            {
                type: {
                    type: String,
                },
                value: Number,
                unit: String,
            },
        ],
        contributors: {
            type: String,
            // required: true,
        },
    },
  copyright: {
    exists: {
      type: Boolean,
      // required: true,
    },
    reference: {
      type: String,
      // required: true,
    },
  },
  patent: {
    exists: {
      type: Boolean,
      // required: true,
    },
    reference: {
      type: String,
      // required: true,
    },
  },
  demonstration: {
    type: String,
    // required: true,
  },
  repository: {
    type: String,
    // required: true,
  },
  label: {
    type: String,
    // required: true,
  },
  detailedDescription: {
    type: String,
    // required: true,
  },
  dependencies: {
    licenses: {
      type: String,
      // required: true,
    },
    hardware: {
      type: String,
      // required: true,
    },
    reusable: {
      type: Boolean,
      // required: true,
    },
    actor: {
      type: String,
      // required: true,
    },
  },
  linkedPlatforms: {
    type: String,
    // required: true,
  },
  keywords: {
    type: String,
    // required: true,
  },
  parentAssetId: String,
  isPlatformAsset: {
    type: Boolean,
    // required: true,
  },
  children: Array,
  classification: {
    type: {
      type: String,
      required: true,
      // enum: ['HIGH_POTENTIAL', 'FOLLOW', 'CONFIRM'],
    },
    perspective: {
      type: String,
      // required: true,
    },
    // comment: {
    //     type: String,
    // },
  },
});
// { timestamps: { createdAt: 'created_at' } });