import { ApiModelProperty } from '@nestjs/swagger';
export class IdentificationModel {
  @ApiModelProperty()
  name: string;
  @ApiModelProperty()
  programId: string;
  @ApiModelProperty()
  projectId: string;
  @ApiModelProperty()
  userId: string;
  @ApiModelProperty()
  creationDate: number;
}

export class PiSharedModel {
  @ApiModelProperty()
  exists: boolean;
  @ApiModelProperty()
  partners: string;
}

export class PatentModel {
  @ApiModelProperty()
  exists: boolean;
  @ApiModelProperty()
  reference: string;

}

export class CopyrightModel {
  @ApiModelProperty()
  exists: boolean;
  @ApiModelProperty()
  reference: string;

}

export class DeliverableModel {
  @ApiModelProperty()
  exists: boolean;
  @ApiModelProperty()
  reference: string;

}
export class Efforts {
  @ApiModelProperty()
  type: string;
  @ApiModelProperty()
  value: number;
  @ApiModelProperty({isArray: true})
  unit: object;
}

export class EffortModel {pm
    @ApiModelProperty()
    efforts: Efforts;
    @ApiModelProperty()
    contributors: string;
}

export class DependenciesModel {
  @ApiModelProperty()
  licenses: string;
  @ApiModelProperty()
  hardware: string;
  @ApiModelProperty()
  reusable: boolean;
  @ApiModelProperty()
  actor: string;
}

export class ClassificationModel {
    @ApiModelProperty({enum: ['HIGH_POTENTIAL', 'FOLLOW', 'CONFIRM']})
    type: string;
    @ApiModelProperty()
    perspective: string;
    @ApiModelProperty()
    comment: string;
}

export class AssetModel {
  // @ApiModelProperty()
  // readonly login: string;
  @ApiModelProperty()
  id: string;

  @ApiModelProperty()
  identification: IdentificationModel;
  @ApiModelProperty()
  desc: string;

  @ApiModelProperty({ enum: ['PENDING', 'WAITING_VALIDATION', 'VALIDATED'] })
  status: string;

  @ApiModelProperty({ enum: ['INITIALIZED', 'IN_PROGRESS', 'DONE', 'TRANSFERED'] })
  assetStatus: string;

  @ApiModelProperty({ enum: ['METHOD', 'STATE_OF_ART', 'STUDY', 'DATASET', 'PATENT', 'PUBLICATION','SOFTWARE', 'STANDARD', 'OTHER'] })
  type: string;

  @ApiModelProperty()
  deliverable: DeliverableModel;

  @ApiModelProperty()
  piShared: PiSharedModel;

  @ApiModelProperty()
  effort: EffortModel;

  @ApiModelProperty()
  copyright: CopyrightModel;

  @ApiModelProperty()
  patent: PatentModel;

  @ApiModelProperty()
  demonstration: string;

  @ApiModelProperty()
  repository: string;

  @ApiModelProperty()
  label: string;

  @ApiModelProperty()
  detailedDescription: string;

  @ApiModelProperty()
  dependencies: DependenciesModel;

  @ApiModelProperty()
  linkedPlatforms: string;

  @ApiModelProperty()
  keywords: string;

  @ApiModelProperty()
  parentAssetId: string;

  @ApiModelProperty()
  isPlatformAsset: boolean;
  children: any;

  @ApiModelProperty()
  classification: ClassificationModel;
}
