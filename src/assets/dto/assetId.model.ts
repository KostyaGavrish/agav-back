import { ApiModelProperty } from '@nestjs/swagger';

export class AssetIdModel {
  @ApiModelProperty()
  readonly assetId: string;
}