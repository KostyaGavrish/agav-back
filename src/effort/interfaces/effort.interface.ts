import { Document } from 'mongoose';

export interface Effort extends Document {
  readonly name: string;
}
