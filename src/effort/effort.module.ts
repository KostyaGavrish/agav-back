import {Module} from '@nestjs/common';
import {MongooseModule} from '@nestjs/mongoose';
import {EffortSchema} from './schemas/effort.schema';
import {EffortService} from './effort.service';
import {EffortController} from './effort.controller';


@Module({
  imports: [MongooseModule.forFeature([{name: 'Effort', schema: EffortSchema}])],
  controllers: [EffortController],
  providers: [EffortService],
})
export class EffortModule {}