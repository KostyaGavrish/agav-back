import {ApiModelProperty} from '@nestjs/swagger';

// export class UnitModel {
//     @ApiModelProperty()
//     id: string;
//     @ApiModelProperty()
//     name: string;
// }

export class EffortModel {
  @ApiModelProperty()
  name: string;
  @ApiModelProperty({isArray: true})
  unit: object;
}