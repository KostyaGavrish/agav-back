import {Model} from 'mongoose';
import {Injectable} from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import {Effort} from './interfaces/effort.interface';
import {EffortModel} from './dto/create-effort.dto';

@Injectable()
export class EffortService {
  constructor(@InjectModel('Effort') private readonly effortModel: Model<Effort>) {
  }

  async createEffort(effort: EffortModel): Promise<any> {
    const createdEffort = new this.effortModel(effort);
    const err = createdEffort.validateSync();
    return err ? err : createdEffort.save();
  }

  async findAll() {
    return await this.effortModel.find().exec();
  }

  async deleteEffort(effortID) {
    return await this.effortModel.findOneAndDelete({_id: effortID}).exec() || new Error('Can\'t find id');
  }

  async updateEffort(effortID, effort) {
    const createdEffort = new this.effortModel(effort);
    const err = await createdEffort.validateSync();
    const data = {
      type: effort.type,
      unit: effort.unit,
    };
    if (err) {
      return err;
    } else {
      return await this.effortModel.findOneAndUpdate({_id: effortID}, {$set: data}, {new: true}).exec() || new Error('Can\'t find id');
    }
  }
}
