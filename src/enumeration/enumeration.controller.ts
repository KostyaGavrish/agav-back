import {Controller, Get, Post, Body, Headers, Param, Query, UseGuards, Delete, Put, Res, HttpStatus} from '@nestjs/common';
import {ApiBearerAuth, ApiOAuth2Auth, ApiResponse, ApiUseTags} from '@nestjs/swagger';
import {AuthGuard} from '@nestjs/passport';
import {TokenModel} from '../models/token.model';
// import {TransferService} from './transfer.service';
// import {Transfer} from './interfaces/transfer.interface';
// import {TransferIdModel, TransferQueryParamsModel} from './dto/transferId.model';
import {RolesGuard} from '../guards/roles.guard';
import {Roles} from '../guards/roles.decorator';
import {EnumService} from './enumeration.service';
import {EnumIdModel} from './dto/enumId.model';
import {EnumModel} from './dto/create-enum.dto';

@ApiUseTags('Enum')
@ApiOAuth2Auth()
@Controller()
export class EnumController {
  constructor(private readonly enumService: EnumService) {}

  @Get('/enum/:enumId')
  //@UseGuards(AuthGuard('jwt'), RolesGuard)
  //@Roles('ADMIN', 'SUPER_USER')
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  async getEmun(@Param() params: EnumIdModel): Promise<any[]> {
      return this.enumService.findAll();
  }

  @Post('/enum')
  // @UseGuards(AuthGuard('jwt'), RolesGuard)
  // @Roles('ADMIN')
  @ApiResponse({status: 200, description: 'Success'})
  @ApiResponse({status: 401, description: 'Unauthorized'})
  @ApiResponse({status: 400, description: 'Bad request'})
  // async createProject(@Res() res, @Body() transfer: TransferModel, @Headers() headers: TokenModel) {
  async addEnumValue(@Res() res, @Body() emun: EnumModel, @Headers() headers: TokenModel) {
    const answer = (await this.enumService.createEnum(emun));
    answer.message ? res.status(HttpStatus.BAD_REQUEST).json(answer.message) : res.status(HttpStatus.CREATED).json(answer);
  }

  @Delete('/emun/:enumId')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('ADMIN')
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  // deleteTransfer(@Headers() headers: TokenModel, @Param() params: TransferIdModel) {
  deleteEnum(@Headers() headers: TokenModel, @Param() params: EnumIdModel) {
    // return this.transferService.deleteTransfer(params.transferId);
  }

  @Put('/enum/:enumId')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('ADMIN')
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 403, description: 'Error access'})
  // async updateTransfer(@Headers() headers: TokenModel, @Res() res, @Param() params: TransferIdModel, @Body() body: TransferModel) {
  async updateEnum(@Headers() headers: TokenModel, @Res() res, @Param() params: EnumIdModel, @Body() body: EnumModel) {
    // return this.transferService.updateTransfer(params.transferId, body);
    // const answer = (await this.transferService.updateTransfer(params.transferId, body));
    // answer.message ? res.status(HttpStatus.BAD_REQUEST).json(answer.message) : res.status(HttpStatus.OK).json(answer);

  }
}
