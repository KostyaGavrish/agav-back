import { ApiModelProperty } from '@nestjs/swagger';

export class EnumIdModel {
  @ApiModelProperty()
  readonly enumId: string;
}