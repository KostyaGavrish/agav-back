import {ApiModelProperty} from '@nestjs/swagger';

export class EnumModel {
  // @ApiModelProperty()
  // id: string;

  @ApiModelProperty()
  values: array<object>;
}