import * as mongoose from 'mongoose';

export const EnumSchema = new mongoose.Schema({
  // id: String,
  values: [{
    id: Number,
    shortName: String,
    name: String,
  }],
});