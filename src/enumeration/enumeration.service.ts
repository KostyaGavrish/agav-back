import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import {Enums} from './interfaces/enum.interface';
import {EnumModel} from './dto/create-enum.dto';

// import {Transfer} from './interfaces/transfer.interface';
// import {TransferModel} from './dto/create-transfer.dto';

// var ObjectId = mongoose.Types.ObjectId;

@Injectable()
export class EnumService {
  constructor(@InjectModel('Enums') private readonly enumModel: Model<Enums>) {}
  // constructor() {}

  async createEnum(project: EnumModel): Promise<any> {
    // var newID;
    // const lastTransfer = await this.transferModel.findOne({}, {}, {sort: {'_id': -1}}).exec();
    // const thisDate = new Date();
    //
    // transfer.index = lastTransfer ? (lastTransfer.index + 1) : 1;
    // newID = "TR-" + transfer.assets[0].identification.project + "-" + transfer.index + "-" + thisDate.getDate() + '.' + (thisDate.getMonth() + 1) + '.' + thisDate.getFullYear();
    //
    // transfer['id'] = newID;
    // const createdTransfer = new this.transferModel(transfer);
    // // return await createdTransfer.save();
    // const err = createdTransfer.validateSync();
    // return err ? err : createdTransfer.save();
  }

  async findAll() {
    return await this.enumModel.find().exec();
  }

  async getEnum(enumID){
    // return await this.transferModel.findOne({id: transferID}).exec();
  }

  async deleteEnum(enumID){
    // return await this.transferModel.deleteOne({id: transferID}).exec();
  }

  async updateEnum(enumID) {
    // const createdTransfer = new this.transferModel(transfer);
    // const err = await createdTransfer.validateSync();
    // console.log(err);
    // if (err){
    //   return err;
    // } else {
    //   return await this.transferModel.findOneAndUpdate({id: transferID}, {$set: transfer}, {new: true}).exec();
    // }
  }
}
