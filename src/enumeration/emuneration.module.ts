import {Module} from '@nestjs/common';
import {MongooseModule} from '@nestjs/mongoose';
import {EnumService} from './enumeration.service';
import {EnumController} from './enumeration.controller';
import {EnumSchema} from './schemas/enum.schema';

@Module({
  imports: [MongooseModule.forFeature([{name: 'Enums', schema: EnumSchema}])],
  controllers: [EnumController],
  providers: [EnumService],
})
export class EnumModule {
}