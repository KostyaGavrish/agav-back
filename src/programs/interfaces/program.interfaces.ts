import { Document } from 'mongoose';

export interface Programs extends Document {
  readonly id: number;
  readonly name: string;
  readonly shortName: string;
  readonly projectId: array<string>;
  readonly projects: any;
}
