import * as mongoose from 'mongoose';

export const ProgramSchema = new mongoose.Schema({
  id: Number,
  name: {
    type: String,
    // required: true,
  },
  shortName: {
    type: String,
    // required: true,
  },
  projectId: {
    type: Array,
  },
  projects: {
    type: Array,
  },
});