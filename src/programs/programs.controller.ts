import {Controller, Get, Post, Body, Headers, Param, Query, UseGuards, Delete, Put, Res, HttpStatus} from '@nestjs/common';
import {ApiBearerAuth, ApiOAuth2Auth, ApiResponse, ApiUseTags} from '@nestjs/swagger';
import {AuthGuard} from '@nestjs/passport';
import {TokenModel} from '../models/token.model';
import {RolesGuard} from '../guards/roles.guard';
import {Roles} from '../guards/roles.decorator';
import {ProgramIdModel} from './dto/programId.model';
import {ProgramModel} from './dto/create-program.dto';
import {Programs} from './interfaces/program.interfaces';
import {ProgramsService} from './programs.service';


@ApiUseTags('Program')
@ApiOAuth2Auth()
@Controller()
export class ProgramsController {
  constructor(private readonly programsService: ProgramsService) {}

  @Get('/programsList')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  async getProgramsList(@Headers() headers: TokenModel): Promise<Programs[]> {
    return this.programsService.findAll();
  }

  @Get('/userProgramsList')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  async getProgramsListUser(@Headers() headers: TokenModel): Promise<Programs[]> {
    return this.programsService.findAllProgramUser(headers.authorization.substring(7));
  }

  @Post('/program')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('ADMIN', 'SUPER_USER')
  @ApiResponse({status: 200, description: 'Success'})
  @ApiResponse({status: 401, description: 'Unauthorized'})
  @ApiResponse({status: 400, description: 'Bad request'})
  async createProgram(@Res() res, @Body() program: ProgramModel, @Headers() headers: TokenModel) {
    const answer = (await this.programsService.createProgram(program));
    answer.message ? res.status(HttpStatus.BAD_REQUEST).json(answer.message) : res.status(HttpStatus.CREATED).json(answer);
  }

  @Delete('/program/:programId')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('ADMIN', 'SUPER_USER')
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  @ApiResponse({status: 400, description: 'Bad request'})
  async deleteProgram(@Headers() headers: TokenModel, @Res() res, @Param() params: ProgramIdModel) {
    const answer = (await this.programsService.deleteProgram(params.programId));
    answer.message ? res.status(HttpStatus.BAD_REQUEST).json(answer.message) : res.status(HttpStatus.OK).json(answer);
  }

  @Put('/program/:programId')
  // @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('ADMIN', 'SUPER_USER')
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 403, description: 'Error access'})
  @ApiResponse({status: 400, description: 'Bad request'})
  async updateProgram(@Headers() headers: TokenModel, @Res() res, @Param() params: ProgramIdModel, @Body() body: ProgramModel) {
    const answer = (await this.programsService.updateProgram(params.programId, body));
    answer.message ? res.status(HttpStatus.BAD_REQUEST).json(answer.message) : res.status(HttpStatus.OK).json(answer);
  }
}
