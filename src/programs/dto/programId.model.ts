import { ApiModelProperty } from '@nestjs/swagger';

export class ProgramIdModel {
  @ApiModelProperty()
  readonly programId: number;
}