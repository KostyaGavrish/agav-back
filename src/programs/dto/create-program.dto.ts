import {ApiModelProperty} from '@nestjs/swagger';

export class ProgramModel {
  @ApiModelProperty()
  id: number;

  @ApiModelProperty()
  name: string;

  @ApiModelProperty()
  shortName: string;

}