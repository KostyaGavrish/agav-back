import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import {Programs} from './interfaces/program.interfaces';
import {ProgramModel} from './dto/create-program.dto';
import {Projects} from '../projects/interfaces/project.interface';
import * as jwt from 'jsonwebtoken';
import {Users} from '../users/interfaces/users.interface';

@Injectable()
export class ProgramsService {
  constructor(@InjectModel('Projects') private readonly projectModel: Model<Projects>,
              @InjectModel('Programs') private readonly programModel: Model<Programs>,
              @InjectModel('Users') private readonly userModel: Model<Users>,
              ) {}

  isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

  async createProgram(program: ProgramModel): Promise<any> {
    const lastProgram = await this.programModel.findOne({}, {}, {sort: {'_id': -1}}).exec();
    program.id = lastProgram ? (lastProgram.id + 1) : 1;
    const createdProgram = new this.programModel(program);
    const err = createdProgram.validateSync();
    return err ? err : createdProgram.save();
  }

  async findAll(){
    let programs;
    programs = await this.programModel.find().exec();
    for (let i = 0; i < programs.length; i++){
      programs[i].projects = await this.projectModel.find({'id':  { $in: programs[i].projectId}}).exec();
    }
    return programs;
  }

  async findAllProgramUser(token) {
    const user = jwt.decode(token, 'secretKey');
    const newUser = await this.userModel.findOne({id: user.id}).exec();
    console.log('user', newUser);
    console.log('userrrrrrr', newUser.creationRights);
    let programs;
    if (newUser.role === "ADMIN" || newUser.role === "SUPER_USER") {
      programs = await this.programModel.find().exec();
      for (let i = 0; i < programs.length; i++){
        programs[i].projects = await this.projectModel.find({'id':  { $in: programs[i].projectId}}).exec();
      }
      return programs;
    } else {
      const creationRightsProgram: Array<number> = [];
      for( let i = 0; i < newUser.creationRights.length; i++ ) {
        creationRightsProgram.push(+newUser.creationRights[i]['programId']);
      }
      console.log('creationRightsProgram', creationRightsProgram);
      programs = await this.programModel.find({'id': { $in: creationRightsProgram}}).exec();
    }
    let creationRightsProjects: Array<number>;
    for (let j = 0; j < programs.length; j++) {
      for (let i = 0; i < newUser.creationRights.length; i++) {
        if (programs[j].id === newUser.creationRights[i].programId) {
          creationRightsProjects = newUser.creationRights[i].projects;
          programs[j].projects = await this.projectModel.find({'id': {$in: creationRightsProjects}}).exec();
          console.log('programs', programs);
        }
      }
      // creationRightsProjects.push(+newUser.creationRights[j].projects);
      // programs[j].projects = await this.projectModel.find({'id':  { $in: programs[j].projectId}}).exec();
    }
    return programs;
    // return await this.programModel.find().exec();
  }

  async deleteProgram(programID){
    if (!this.isNumeric(programID)) return new Error('Id is not valid');
    return await this.programModel.findOneAndDelete({id: programID}).exec() ||  new Error('Can\'t find id') ;
  }

  async updateProgram(programID, program) {
    if (!this.isNumeric(programID)) return new Error('Id is not valid');
    const createdProgram = new this.programModel(program);
    const err = await createdProgram.validateSync();
    const data = {
      name: program.name,
      shortName: program.shortName,
      // projectId: program.projectId,
    };
    if (err){
      return err;
    } else {
      return await this.programModel.findOneAndUpdate({id: programID}, {$set: data}, {new: true}).exec() ||  new Error('Can\'t find id');
    }
  }
}
