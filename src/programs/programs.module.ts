import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
// import {TransferSchema} from './schemas/transfer.schema';
import {ProgramsController} from './programs.controller';
import {ProgramsService} from './programs.service';
import {ProgramSchema} from './schemas/programs.schema';
import {Programs} from './interfaces/program.interfaces';
import {ProjectSchema} from '../projects/schemas/projects.schema';
import {UserSchema} from '../users/schemas/user.schema';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Programs', schema: ProgramSchema }, { name: 'Users', schema: UserSchema }, {name: 'Projects', schema: ProjectSchema}])],
  controllers: [ProgramsController],
  providers: [ProgramsService],
})
export class ProgramsModule {}