import * as mongoose from 'mongoose';

export const ProjectSchema = new mongoose.Schema({
  id: Number,
  name: {
    type: String,
    // required: true,
  },
  shortName: {
    type: String,
    // required: true,
  },
});