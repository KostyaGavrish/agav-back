import {Module} from '@nestjs/common';
import {MongooseModule} from '@nestjs/mongoose';
import {ProjectsService} from './projects.service';
import {ProjectsController} from './projects.controller';
import {ProjectSchema} from './schemas/projects.schema';
import {ProjectInterface} from './interfaces/project.interface';
import {ProgramSchema} from '../programs/schemas/programs.schema';
import {Programs} from '../programs/interfaces/program.interfaces';


@Module({
  imports: [MongooseModule.forFeature([{name: 'Projects', schema: ProjectSchema}, { name: 'Programs', schema: ProgramSchema }])],
  controllers: [ProjectsController],
  providers: [ProjectsService],
})
export class ProjectsModule {
}