import {ApiModelProperty} from '@nestjs/swagger';

export class ProjectModel {
  @ApiModelProperty()
  id: number;

  @ApiModelProperty()
  name: string;

  @ApiModelProperty()
  shortName: string;

}