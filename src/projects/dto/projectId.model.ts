import { ApiModelProperty } from '@nestjs/swagger';

export class ProjectIdModel {
  @ApiModelProperty()
  readonly projectId: number;
}