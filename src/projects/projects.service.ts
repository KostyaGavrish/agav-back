import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { Projects } from './interfaces/project.interface';
import { ProjectModel } from './dto/create-project.dto';
import {Programs} from '../programs/interfaces/program.interfaces';
import {ProgramModel} from '../programs/dto/create-program.dto';

@Injectable()
export class ProjectsService {
  constructor(@InjectModel('Projects') private readonly projectModel: Model<Projects>,
              @InjectModel('Programs') private readonly programModel: Model<Programs>) {}

  isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

  async createProject(project: ProjectModel): Promise<any> {
    const lastProject = await this.projectModel.findOne({}, {}, {sort: {'_id': -1}}).exec();
    project.id = lastProject ? (lastProject.id + 1) : 1;
    const data = {
      id: project.id,
      name: project.name,
      shortName: project.shortName,
    };
    let program = await this.programModel.findOne({id: project.programId}).exec();
    program.projectId[program.projectId.length] = project.id;
    console.log('program', program);

    this.programModel.findOneAndUpdate({id: project.programId}, program).exec();
    const createdProject = new this.projectModel(data);
    // console.log('createdProject', createdProject);
    const err = createdProject.validateSync();
    return err ? err : createdProject.save().then(res => {
      // console.log(res);
      return res;
    });
  }

  async findAll() {
    return await this.projectModel.find().exec();
  }

  async deleteProject(projectID, programID){
    if (!this.isNumeric(projectID)) return new Error('Id is not valid');
    console.log('programID', programID);

    let program = await this.programModel.findOne({ id: programID });
    console.log('program', program);
    let updProjIds = program.projectId.filter((number) => {
      return number != projectID;
    });
    program.projectId = updProjIds;
    console.log('program', program);
    await this.programModel.findOneAndUpdate({ id: programID }, {$set: program}).exec();

    return await this.projectModel.findOneAndDelete({id: projectID}).exec() ||  new Error('Can\'t find id') ;
  }

  async updateProject(projectID, project) {
    if (!this.isNumeric(projectID)) return new Error('Id is not valid');
    const createdProject = new this.projectModel(project);
    const err = await createdProject.validateSync();
    const data = {
      name: project.name,
      shortName: project.shortName,
    };
    if (err){
      return err;
    } else {
      return await this.projectModel.findOneAndUpdate({id: projectID}, {$set: data}, {new: true}).exec() ||  new Error('Can\'t find id');
    }
  }
}
