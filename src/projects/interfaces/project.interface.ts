import { Document } from 'mongoose';

export interface Projects extends Document {
  readonly id: number;
  readonly name: string;
  readonly shortName: string;
}
