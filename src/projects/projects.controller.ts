import {Controller, Get, Post, Body, Headers, Param, Query, UseGuards, Delete, Put, Res, HttpStatus} from '@nestjs/common';
import {ApiBearerAuth, ApiOAuth2Auth, ApiResponse, ApiUseTags} from '@nestjs/swagger';
import {AuthGuard} from '@nestjs/passport';
import {TokenModel} from '../models/token.model';
import {RolesGuard} from '../guards/roles.guard';
import {Roles} from '../guards/roles.decorator';
import {ProjectIdModel} from './dto/projectId.model';
import {ProjectModel} from './dto/create-project.dto';
import {ProjectsService} from './projects.service';
import {Projects} from './interfaces/project.interface';

@ApiUseTags('Projects')
@ApiOAuth2Auth()
@Controller()
export class ProjectsController {
  constructor(private readonly projectService: ProjectsService) {}

  @Get('/projectsList')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  //@Roles('ADMIN', 'SUPER_USER')
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  async getProjectsList(): Promise<Projects[]> {
    return await this.projectService.findAll();
  }

  @Post('/project')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('ADMIN', 'SUPER_USER')
  @ApiResponse({status: 200, description: 'Success'})
  @ApiResponse({status: 401, description: 'Unauthorized'})
  @ApiResponse({status: 400, description: 'Bad request'})
  async createProject(@Res() res, @Body() project: ProjectModel, @Headers() headers: TokenModel) {
    const answer = (await this.projectService.createProject(project));
    answer.message ? res.status(HttpStatus.BAD_REQUEST).json(answer.message) : res.status(HttpStatus.CREATED).json(answer);
  }

  @Delete('/project/:programId/:projectId')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('ADMIN', 'SUPER_USER')
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  @ApiResponse({ status: 400, description: 'Problem with id'})
  async deleteProject(@Headers() headers: TokenModel, @Res() res, @Param() params: ProjectIdModel) {
   const answer = (await this.projectService.deleteProject(params.projectId, params.programId));
    answer.message ? res.status(HttpStatus.BAD_REQUEST).json(answer.message) : res.status(HttpStatus.OK).json(answer);
  }

  @Put('/project/:projectId')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('ADMIN', 'SUPER_USER')
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 403, description: 'Error access'})
  @ApiResponse({ status: 400, description: 'Problem with id'})
  async updateProject(@Headers() headers: TokenModel, @Res() res, @Param() params: ProjectIdModel, @Body() body: ProjectModel) {
    const answer = (await this.projectService.updateProject(params.projectId, body));
    answer.message ? res.status(HttpStatus.BAD_REQUEST).json(answer.message) : res.status(HttpStatus.OK).json(answer);
  }
}
