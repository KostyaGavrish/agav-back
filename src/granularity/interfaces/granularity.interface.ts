import { Document } from 'mongoose';

export interface Granularity extends Document {
  readonly name: string;
}
