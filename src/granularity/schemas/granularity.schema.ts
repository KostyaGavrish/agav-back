import * as mongoose from 'mongoose';

export const GranularitySchema = new mongoose.Schema({
  name: {
    type: String,
  },
});