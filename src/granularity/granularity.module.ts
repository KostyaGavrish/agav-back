import {Module} from '@nestjs/common';
import {MongooseModule} from '@nestjs/mongoose';
import {Granularity} from './interfaces/granularity.interface';
import {GranularityController} from './granularity.controller';
import {GranularitySchema} from './schemas/granularity.schema';
import {GranularityService} from './granularity.service';


@Module({
  imports: [MongooseModule.forFeature([{name: 'Granularity', schema: GranularitySchema}])],
  controllers: [GranularityController],
  providers: [GranularityService],
})
export class GranularityModule {}