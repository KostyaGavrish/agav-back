import {ApiModelProperty} from '@nestjs/swagger';

export class GranularityModel {
  @ApiModelProperty()
  name: string;
}