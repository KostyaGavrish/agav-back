import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import {Granularity} from './interfaces/granularity.interface';
import {GranularityModel} from './dto/create-granularity.dto';

@Injectable()
export class GranularityService {
  constructor(@InjectModel('Granularity') private readonly granularityModel: Model<Granularity>) {}

  async createGranularity(granularity: GranularityModel): Promise<any> {
    const createdGranularity = new this.granularityModel(granularity);
    const err = createdGranularity.validateSync();
    return err ? err : createdGranularity.save();
  }

  async findAll() {
    return await this.granularityModel.find().exec();
  }

  async deleteGranularity(granularityID){
    return await this.granularityModel.findOneAndDelete({_id: granularityID}).exec() ||  new Error('Can\'t find id') ;
  }

  async updateGranularity(granularityID, granularity) {
    const createdGranularity = new this.granularityModel(granularity);
    const err = await createdGranularity.validateSync();
    const data = {
      name: granularity.name,
    };
    if (err){
      return err;
    } else {
      return await this.granularityModel.findOneAndUpdate({_id: granularityID}, {$set: data}, {new: true}).exec() ||  new Error('Can\'t find id');
    }
  }
}
