import {Controller, Get, Post, Body, Headers, Param, Query, UseGuards, Delete, Put, Res, HttpStatus} from '@nestjs/common';
import {ApiBearerAuth, ApiOAuth2Auth, ApiResponse, ApiUseTags} from '@nestjs/swagger';
import {AuthGuard} from '@nestjs/passport';
import {TokenModel} from '../models/token.model';
import {RolesGuard} from '../guards/roles.guard';
import {Roles} from '../guards/roles.decorator';
import {GranularityService} from './granularity.service';
import {GranularityModel} from './dto/create-granularity.dto';

@ApiUseTags('granularity')
@ApiOAuth2Auth()
@Controller()
export class GranularityController {
  constructor(private readonly granularityService: GranularityService) {}

  @Get('/granularityList')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  async getGranularityList(): Promise<any> {
    return await this.granularityService.findAll();
  }

  @Post('/granularity')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('ADMIN', 'SUPER_USER')
  @ApiResponse({status: 200, description: 'Success'})
  @ApiResponse({status: 401, description: 'Unauthorized'})
  @ApiResponse({status: 400, description: 'Bad request'})
  async createGranularity(@Res() res, @Body() granularity: GranularityModel, @Headers() headers: TokenModel) {
    const answer = (await this.granularityService.createGranularity(granularity));
    answer.message ? res.status(HttpStatus.BAD_REQUEST).json(answer.message) : res.status(HttpStatus.CREATED).json(answer);
  }

  @Delete('/granularity/:granularityId')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('ADMIN', 'SUPER_USER')
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  @ApiResponse({status: 400, description: 'Bad request'})
  async deleteGranularity(@Headers() headers: TokenModel, @Res() res, @Param() params: any) {
    const answer = (await this.granularityService.deleteGranularity(params.granularityId));
    answer.message ? res.status(HttpStatus.BAD_REQUEST).json(answer.message) : res.status(HttpStatus.OK).json(answer);
  }

  @Put('/granularity/:granularityId')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('ADMIN', 'SUPER_USER')
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 403, description: 'Error access'})
  @ApiResponse({ status: 400, description: 'Problem with id'})
  // async updateTransfer(@Headers() headers: TokenModel, @Res() res, @Param() params: TransferIdModel, @Body() body: TransferModel) {
  async updateGranularity(@Headers() headers: TokenModel, @Res() res, @Param() params: any, @Body() body: GranularityModel) {
    // return this.transferService.updateTransfer(params.transferId, body);
    const answer = (await this.granularityService.updateGranularity(params.granularityId, body));
    answer.message ? res.status(HttpStatus.BAD_REQUEST).json(answer.message) : res.status(HttpStatus.OK).json(answer);
  }
}
