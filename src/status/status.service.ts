import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import {StatusModel} from './dto/create-status.dto';
import {Status} from './interfaces/status.interface';

@Injectable()
export class StatusService {
  constructor(@InjectModel('Status') private readonly statusModel: Model<Status>) {}

  async createStatus(status: StatusModel): Promise<any> {
    const createdStatus = new this.statusModel(status);
    const err = createdStatus.validateSync();
    return err ? err : createdStatus.save();
  }

  async findAll() {
    return await this.statusModel.find().exec();
  }

  async deleteStatus(statusID){
    return await this.statusModel.findOneAndDelete({_id: statusID}).exec() ||  new Error('Can\'t find id') ;
  }

  async updateStatus(statusID, status) {
    const createdStatus = new this.statusModel(status);
    const err = await createdStatus.validateSync();
    const data = {
      name: status.name,
    };
    if (err){
      return err;
    } else {
      return await this.statusModel.findOneAndUpdate({_id: statusID}, {$set: data}, {new: true}).exec() ||  new Error('Can\'t find id');
    }
  }
}
