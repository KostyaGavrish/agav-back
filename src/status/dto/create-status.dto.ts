import {ApiModelProperty} from '@nestjs/swagger';

export class StatusModel {
  @ApiModelProperty()
  name: string;
}