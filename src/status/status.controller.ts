import {Controller, Get, Post, Body, Headers, Param, Query, UseGuards, Delete, Put, Res, HttpStatus} from '@nestjs/common';
import {ApiBearerAuth, ApiOAuth2Auth, ApiResponse, ApiUseTags} from '@nestjs/swagger';
import {AuthGuard} from '@nestjs/passport';
import {TokenModel} from '../models/token.model';
import {RolesGuard} from '../guards/roles.guard';
import {Roles} from '../guards/roles.decorator';
import {StatusService} from './status.service';
import {StatusModel} from './dto/create-status.dto';

@ApiUseTags('status')
@ApiOAuth2Auth()
@Controller()
export class StatusController {
  constructor(private readonly statusService: StatusService) {}

  @Get('/statusList')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  async getStatusList(): Promise<any> {
    return await this.statusService.findAll();
  }

  @Post('/status')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('ADMIN', 'SUPER_USER')
  @ApiResponse({status: 200, description: 'Success'})
  @ApiResponse({status: 401, description: 'Unauthorized'})
  @ApiResponse({status: 400, description: 'Bad request'})
  // async createProject(@Res() res, @Body() transfer: TransferModel, @Headers() headers: TokenModel) {
  async createStatus(@Res() res, @Body() status: StatusModel, @Headers() headers: TokenModel) {
    const answer = (await this.statusService.createStatus(status));
    answer.message ? res.status(HttpStatus.BAD_REQUEST).json(answer.message) : res.status(HttpStatus.CREATED).json(answer);
  }

  @Delete('/status/:statusId')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('ADMIN', 'SUPER_USER')
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  @ApiResponse({status: 400, description: 'Bad request'})
  async deleteStatus(@Headers() headers: TokenModel, @Res() res, @Param() params: any) {
    const answer = (await this.statusService.deleteStatus(params.statusId));
    answer.message ? res.status(HttpStatus.BAD_REQUEST).json(answer.message) : res.status(HttpStatus.OK).json(answer);
  }

  @Put('/status/:statusId')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('ADMIN', 'SUPER_USER')
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 403, description: 'Error access'})
  @ApiResponse({ status: 400, description: 'Problem with id'})
  async updateStatus(@Headers() headers: TokenModel, @Res() res, @Param() params: any, @Body() body: StatusModel) {
    const answer = (await this.statusService.updateStatus(params.statusId, body));
    answer.message ? res.status(HttpStatus.BAD_REQUEST).json(answer.message) : res.status(HttpStatus.OK).json(answer);
  }
}
