import { Document } from 'mongoose';

export interface Status extends Document {
  readonly name: string;
}
