import {Model} from 'mongoose';
import {Injectable} from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import {Transfer} from './interfaces/transfer.interface';
import {TransferModel} from './dto/create-transfer.dto';
import {Projects} from "../projects/interfaces/project.interface";
import {Destination} from "../destination/interfaces/destination.interface";
import {Programs} from "../programs/interfaces/program.interfaces";

var ObjectId = mongoose.Types.ObjectId;

@Injectable()
export class TransferService {
  constructor(@InjectModel('Transfer') private readonly transferModel: Model<Transfer>,
              @InjectModel('Projects') private readonly projectModel: Model<Projects>,
              @InjectModel('Programs') private readonly programModel: Model<Programs>,
              @InjectModel('Destination') private readonly destinationModel: Model<Destination>,
  ) {
  }

  async create(transfer: TransferModel): Promise<Transfer> {
    let newID;
    const lastTransfer = await this.transferModel.findOne({}, {}, {sort: {'_id': -1}}).exec();
    const thisDate = new Date();

    transfer.index = lastTransfer ? (lastTransfer.index + 1) : 1;
    newID = "TR-" + transfer.assets[0].identification.project.shortName + "-" + transfer.index + "-" + thisDate.getDate() + '.' + (thisDate.getMonth() + 1) + '.' + thisDate.getFullYear();

    transfer['id'] = newID;
    console.log('transfer', transfer);
    const createdTransfer = new this.transferModel(transfer);
    // return await createdTransfer.save();
    const err = createdTransfer.validateSync();
    console.log('err', err);
    return err ? err : createdTransfer.save();
  }

  async findAll() {
    return await this.transferModel.find().exec();
  }

  async getTransfer(transferID) {
    const transfer = await this.transferModel.findOne({id: transferID}).exec();
    // let tempDestination;
    // if (transfer.destination && mongoose.Types.ObjectId.isValid(transfer.destination.type)) {
    //   tempDestination = await this.transferModel.findOne({_id: transfer.destination.type}).exec()
    //   if (tempDestination) {
    //
    //   } else {
    //
    //   }
    // } else {
    //
    // }
    return transfer;
  }

  async getTransfersForAsset(assetId) {
    // console.log("Asset id filter: ", assetId);
    return await this.transferModel.find({"assets": {$elemMatch: {id: assetId}}}).exec();
  }

  async deleteTransfer(transferID) {
    return await this.transferModel.deleteOne({id: transferID}).exec();
  }

  async updateTransfer(transferID, transfer) {
    const createdTransfer = new this.transferModel(transfer);
    const err = await createdTransfer.validateSync();
    if (err) {
      return err;
    } else {
      return await this.transferModel.findOneAndUpdate({id: transferID}, {$set: transfer}, {new: true}).exec();
    }
  }
}
