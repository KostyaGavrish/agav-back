import { Document } from 'mongoose';

export interface Transfer extends Document {
  readonly id: string;
  // identification: {
  //   name: string,
  //   program: string,
  //   project: string,
  //   userId: string,
  //   creationDate: number,
  // };
}
