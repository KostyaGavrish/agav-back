import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import {TransferService} from './transfer.service';
import {TransferSchema} from './schemas/transfer.schema';
import {TransferController} from './transfer.controller';
import {ProjectSchema} from "../projects/schemas/projects.schema";
import {DestinationSchema} from "../destination/schemas/destination.schema";
import {ProgramSchema} from "../programs/schemas/programs.schema";

@Module({
  imports: [MongooseModule.forFeature([{name: 'Destination', schema: DestinationSchema}, { name: 'Programs', schema: ProgramSchema }, { name: 'Transfer', schema: TransferSchema }, {name: 'Projects', schema: ProjectSchema}])],
  controllers: [TransferController],
  providers: [TransferService],
})
export class TransferModule {}
