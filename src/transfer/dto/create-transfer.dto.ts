import {ApiModelProperty} from '@nestjs/swagger';
import {AssetModel} from '../../assets/dto/create-asset.dto';

export class DestinationModel {
    @ApiModelProperty({enum: ['PROJECT', 'PARTNER']})
    type: string;
    @ApiModelProperty()
    name: string;
    @ApiModelProperty()
    contact: string;
}

export class OpenSourceModel {
    @ApiModelProperty()
    exists: boolean;
    @ApiModelProperty()
    hyperlink: string;
}

export class SciCommunityModel {
    @ApiModelProperty()
    exists: boolean;
    @ApiModelProperty()
    hyperlink: string;
}

export class OpenModel {
    @ApiModelProperty()
    exists: boolean;
    @ApiModelProperty()
    hyperlink: string;
}

export class IndicatorsModel {
    @ApiModelProperty()
    usersCount: number;
    @ApiModelProperty()
    downloadsCount: number;
    @ApiModelProperty()
    businessImpact: string;
    @ApiModelProperty()
    licenseCost: number;
    @ApiModelProperty()
    industrializedPoC: boolean;
    @ApiModelProperty()
    licenseReturn: boolean;
    @ApiModelProperty()
    experimentationsCount: number;
    @ApiModelProperty()
    awardsCount: number;
}

// export class ClassificationModel {
//     @ApiModelProperty({enum: ['HIGH_POTENTIAL', 'FOLLOW', 'CONFIRM']})
//     type: string;
//     @ApiModelProperty()
//     perspective: string;
// }

export class TransferModel {
    // @ApiModelProperty()
    id: string;

    @ApiModelProperty()
    userId: number;

    @ApiModelProperty({isArray: true})
    assets: AssetModel;

    @ApiModelProperty()
    destination: DestinationModel;

    @ApiModelProperty()
    openSource: OpenSourceModel;

    @ApiModelProperty()
    sciCommunity: SciCommunityModel;

    @ApiModelProperty()
    open: OpenModel;

    @ApiModelProperty()
    indicators: IndicatorsModel;

    @ApiModelProperty({enum: ['PENDING', 'WAITING_VALIDATION', 'VALIDATED']})
    status: string;

    // @ApiModelProperty()
    // classification: ClassificationModel;
}
