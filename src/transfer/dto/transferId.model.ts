import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export class TransferIdModel {
  @ApiModelProperty()
  readonly transferId: string;
}

export class TransferQueryParamsModel {
  @ApiModelPropertyOptional()
  readonly asset: string;
}