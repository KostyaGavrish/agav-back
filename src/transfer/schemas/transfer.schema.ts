import * as mongoose from 'mongoose';

export const TransferSchema = new mongoose.Schema({
    id: String,
    // id: Number,
    index: Number,
    userId: {
        type: Number,
        required: true,
    },
    assets: {
        type: Array,
        // required: true,
    },
    destination: {
        type: {
            type: String,
            required: true,
            // enum: ['PROJECT', 'PARTNER'],
        },
        name: {
            type: String,
            // required: true,
        },
        contact: {
            type: String,
            // required: true,
        },
    },
    openSource: {
        exists: {
            type: Boolean,
            // required: true,
        },
        hyperlink: {
            type: String,
            // required: true,
        },
    },
    sciCommunity: {
        exists: {
            type: Boolean,
            // required: true,
        },
        hyperlink: {
            type: String,
            // required: true,
        },
    },
    open: {
        exists: {
            type: Boolean,
            // required: true,
        },
        hyperlink: {
            type: String,
            // required: true,
        },
    },
    indicators: {
        usersCount: {
            type: Number,
            // required: true,
        },
        downloadsCount: {
            type: Number,
            // required: true,
        },
        businessImpact: {
            type: String,
            // required: true,
        },
        licenseCost: {
            type: Number,
            // required: true,
        },
        industrializedPoC: {
            type: Boolean,
            // required: true,
        },
        licenseReturn: {
            type: Boolean,
            // required: true,
        },
        experimentationsCount: {
            type: Number,
            // required: true,
        },
        awardsCount: {
            type: Number,
            // required: true,
        },
    },

    status: {
        type: String,
        required: true,
        enum: ['PENDING', 'WAITING_VALIDATION', 'VALIDATED'],
    },
    // classification: {
    //     type: {
    //         type: String,
    //         required: true,
    //         enum: ['HIGH_POTENTIAL', 'FOLLOW', 'CONFIRM'],
    //     },
    //     perspective: {
    //         type: String,
    //         // required: true,
    //     },
    // },
});