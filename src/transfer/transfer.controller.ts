import {Controller, Get, Post, Body, Headers, Param, Query, UseGuards, Delete, Put, Res, HttpStatus} from '@nestjs/common';
import {ApiBearerAuth, ApiOAuth2Auth, ApiResponse, ApiUseTags} from '@nestjs/swagger';
import {AuthGuard} from '@nestjs/passport';
import {TokenModel} from '../models/token.model';
import {TransferModel} from './dto/create-transfer.dto';
import {TransferService} from './transfer.service';
import {Transfer} from './interfaces/transfer.interface';
import {TransferIdModel, TransferQueryParamsModel} from './dto/transferId.model';
import {RolesGuard} from "../guards/roles.guard";
import {Roles} from "../guards/roles.decorator";

@ApiUseTags('Transfers')
@ApiOAuth2Auth()
@Controller()
export class TransferController {
  constructor(private readonly transferService: TransferService) {}

  @Post('/transfer')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('ADMIN')
  @ApiResponse({status: 200, description: 'Success'})
  @ApiResponse({status: 401, description: 'Unauthorized'})
  @ApiResponse({status: 400, description: 'Bad request'})
  async create(@Res() res, @Body() transfer: TransferModel, @Headers() headers: TokenModel) {
    const answer = (await this.transferService.create(transfer));
    answer.errors ? res.status(HttpStatus.BAD_REQUEST).json(answer.errors) : res.status(HttpStatus.CREATED).json(answer);
  }

  @Get('/transferList')
  //@UseGuards(AuthGuard('jwt'), RolesGuard)
  //@Roles('ADMIN', 'SUPER_USER')
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  async getTransferList(@Headers() headers: TokenModel, @Query() query: TransferQueryParamsModel): Promise<Transfer[]> {
    console.log("Transfer !");
    console.log(query);
    if(query.asset) {
      return this.transferService.getTransfersForAsset(query.asset);
    }
    else {
      return this.transferService.findAll();
    }
  }

  @Get('/transfer/:transferId')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('ADMIN', 'SUPER_USER')
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  @ApiResponse({status: 400, description: 'Bad request'})
  getTransfer(@Headers() headers: TokenModel, @Param() params: TransferIdModel) {
      return this.transferService.getTransfer(params.transferId);
  }

  // @Get('/transfer')
  // //@UseGuards(AuthGuard('jwt'), RolesGuard)
  // @Roles()
  // @ApiResponse({ status: 200, description: 'Success'})
  // @ApiResponse({ status: 401, description: 'Unauthorized'})
  // @ApiResponse({status: 400, description: 'Bad request'})
  // getTransferForAsset(@Headers() headers: TokenModel, @Query() query: any) {
  //   console.log("Transfer for Asset !")
  //   return this.transferService.getTransfersForAsset(query.asset);
  // }

  @Delete('/transfer/:transferId')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('ADMIN')
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  deleteTransfer(@Headers() headers: TokenModel, @Param() params: TransferIdModel) {
    return this.transferService.deleteTransfer(params.transferId);
  }

  @Put('/transfer/:transferId')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('ADMIN')
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 403, description: 'Error access'})
  async updateTransfer(@Headers() headers: TokenModel, @Res() res, @Param() params: TransferIdModel, @Body() body: TransferModel) {
    // return this.transferService.updateTransfer(params.transferId, body);
      const answer = (await this.transferService.updateTransfer(params.transferId, body));
      answer.errors ? res.status(HttpStatus.BAD_REQUEST).json(answer.errors) : res.status(HttpStatus.OK).json(answer);
  }
}
