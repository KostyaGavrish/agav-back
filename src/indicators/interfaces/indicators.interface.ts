import { Document } from 'mongoose';

export interface Indicators extends Document {
  readonly name: string;
}
