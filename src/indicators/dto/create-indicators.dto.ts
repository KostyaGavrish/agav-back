import {ApiModelProperty} from '@nestjs/swagger';

export class IndicatorsModel {
  @ApiModelProperty()
  name: string;
}