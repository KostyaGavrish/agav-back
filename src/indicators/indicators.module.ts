import {Module} from '@nestjs/common';
import {MongooseModule} from '@nestjs/mongoose';
import {IndicatorsSchema} from './schemas/indicators.schema';
import {IndicatorsService} from './indicators.service';
import {IndicatorsController} from './indicators.controller';


@Module({
  imports: [MongooseModule.forFeature([{name: 'Indicators', schema: IndicatorsSchema}])],
  controllers: [IndicatorsController],
  providers: [IndicatorsService],
})
export class IndicatorsModule {}