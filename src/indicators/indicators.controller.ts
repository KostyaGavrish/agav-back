import {Controller, Get, Post, Body, Headers, Param, Query, UseGuards, Delete, Put, Res, HttpStatus} from '@nestjs/common';
import {ApiBearerAuth, ApiOAuth2Auth, ApiResponse, ApiUseTags} from '@nestjs/swagger';
import {AuthGuard} from '@nestjs/passport';
import {TokenModel} from '../models/token.model';
import {RolesGuard} from '../guards/roles.guard';
import {Roles} from '../guards/roles.decorator';
import {IndicatorsModel} from './dto/create-indicators.dto';
import {IndicatorsService} from './indicators.service';

@ApiUseTags('indicators')
@ApiOAuth2Auth()
@Controller()
export class IndicatorsController {
  constructor(private readonly indicatorsService: IndicatorsService) {}

  @Get('/indicatorsList')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  async getIndicatorsList(): Promise<any> {
    return await this.indicatorsService.findAll();
  }

  @Post('/indicators')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('ADMIN', 'SUPER_USER')
  @ApiResponse({status: 200, description: 'Success'})
  @ApiResponse({status: 401, description: 'Unauthorized'})
  @ApiResponse({status: 400, description: 'Bad request'})
  async createIndicators(@Res() res, @Body() indicators: IndicatorsModel, @Headers() headers: TokenModel) {
    const answer = (await this.indicatorsService.createIndicators(indicators));
    answer.message ? res.status(HttpStatus.BAD_REQUEST).json(answer.message) : res.status(HttpStatus.CREATED).json(answer);
  }

  @Delete('/indicators/:indicatorsId')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('ADMIN', 'SUPER_USER')
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  @ApiResponse({status: 400, description: 'Bad request'})
  async deleteIndicators(@Headers() headers: TokenModel, @Res() res, @Param() params: any) {
    const answer = (await this.indicatorsService.deleteIndicators(params.indicatorsId));
    answer.message ? res.status(HttpStatus.BAD_REQUEST).json(answer.message) : res.status(HttpStatus.OK).json(answer);
  }

  @Put('/indicators/:indicatorsId')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('ADMIN', 'SUPER_USER')
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 403, description: 'Error access'})
  @ApiResponse({ status: 400, description: 'Problem with id'})
  // async updateTransfer(@Headers() headers: TokenModel, @Res() res, @Param() params: TransferIdModel, @Body() body: TransferModel) {
  async updateIndicators(@Headers() headers: TokenModel, @Res() res, @Param() params: any, @Body() body: IndicatorsModel) {
    // return this.transferService.updateTransfer(params.transferId, body);
    const answer = (await this.indicatorsService.updateIndicators(params.indicatorsId, body));
    answer.message ? res.status(HttpStatus.BAD_REQUEST).json(answer.message) : res.status(HttpStatus.OK).json(answer);
  }
}
