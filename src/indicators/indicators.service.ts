import {Model} from 'mongoose';
import {Injectable} from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import {Indicators} from './interfaces/indicators.interface';
import {IndicatorsModel} from './dto/create-indicators.dto';

@Injectable()
export class IndicatorsService {
  constructor(@InjectModel('Indicators') private readonly indicatorsModel: Model<Indicators>) {
  }

  async createIndicators(indicators: IndicatorsModel): Promise<any> {
    const createdIndicators = new this.indicatorsModel(indicators);
    const err = createdIndicators.validateSync();
    return err ? err : createdIndicators.save();
  }

  async findAll() {
    return await this.indicatorsModel.find().exec();
  }

  async deleteIndicators(indicatorsID) {
    return await this.indicatorsModel.findOneAndDelete({_id: indicatorsID}).exec() || new Error('Can\'t find id');
  }

  async updateIndicators(indicatorsID, indicators) {
    const createdIndicators = new this.indicatorsModel(indicators);
    const err = await createdIndicators.validateSync();
    const data = {
      name: indicators.name,
    };
    if (err) {
      return err;
    } else {
      return await this.indicatorsModel.findOneAndUpdate({_id: indicatorsID}, {$set: data}, {new: true}).exec() || new Error('Can\'t find id');
    }
  }
}
