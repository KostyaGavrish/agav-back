import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';

import { AssetsModule } from './assets/assets.module';
import { AuthModule } from './auth/auth.module';
import { TransferModule } from './transfer/transfer.module';
import { APP_GUARD } from '@nestjs/core';
import { RolesGuard } from './guards/roles.guard';
import { ToolsModule } from 'tools/tools.module';
import {ProjectsModule} from './projects/projects.module';
import {ProgramsModule} from './programs/programs.module';
import {EnumModule} from './enumeration/emuneration.module';
import {GranularityModule} from './granularity/granularity.module';
import {StatusModule} from './status/status.module';
import {TypeModule} from './type/type.module';
import {EffortModule} from './effort/effort.module';
import {IndicatorsModule} from './indicators/indicators.module';
import {ClassificationModule} from './classification/classification.module';
import {DestinationModule} from './destination/destination.module';

@Module({
  imports: [UsersModule,
    AssetsModule,
    TransferModule,
    ToolsModule,
    AuthModule,
    ProjectsModule,
    ProgramsModule,
    EnumModule,
    GranularityModule,
    StatusModule,
    TypeModule,
    EffortModule,
    IndicatorsModule,
    ClassificationModule,
    DestinationModule,
    MongooseModule.forRoot('mongodb://geeksters:geek413@ds245234.mlab.com:45234/agav-back'),
    // MongooseModule.forRoot(process.env.MONGOURL/* , {user: process.env.MONGO_USER, pass: process.env.MONGO_PASSWORD, authSource: "admin"} */)
  ],
  controllers: [
    AppController
  ],
  providers: [ 
    AppService,
    { provide: APP_GUARD, useClass: RolesGuard },
  ]
})
export class AppModule {}
