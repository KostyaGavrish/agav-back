import { Document } from 'mongoose';

export interface Type extends Document {
  readonly name: string;
}
