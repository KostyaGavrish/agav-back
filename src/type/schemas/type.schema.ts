import * as mongoose from 'mongoose';

export const TypeSchema = new mongoose.Schema({
  name: {
    type: String,
  },
});