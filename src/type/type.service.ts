import {Model} from 'mongoose';
import {Injectable} from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import {Type} from './interfaces/type.interface';
import {TypeModel} from './dto/create-type.dto';

@Injectable()
export class TypeService {
  constructor(@InjectModel('Type') private readonly typeModel: Model<Type>) {
  }

  async createType(type: TypeModel): Promise<any> {
    const createdType = new this.typeModel(type);
    const err = createdType.validateSync();
    return err ? err : createdType.save();
  }

  async findAll() {
    return await this.typeModel.find().exec();
  }

  async deleteType(typeID) {
    return await this.typeModel.findOneAndDelete({_id: typeID}).exec() || new Error('Can\'t find id');
  }

  async updateType(typeID, type) {
    const createdType = new this.typeModel(type);
    const err = await createdType.validateSync();
    const data = {
      name: type.name,
    };
    if (err) {
      return err;
    } else {
      return await this.typeModel.findOneAndUpdate({_id: typeID}, {$set: data}, {new: true}).exec() || new Error('Can\'t find id');
    }
  }
}
