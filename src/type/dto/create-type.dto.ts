import {ApiModelProperty} from '@nestjs/swagger';

export class TypeModel {
  @ApiModelProperty()
  name: string;
}