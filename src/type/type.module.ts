import {Module} from '@nestjs/common';
import {MongooseModule} from '@nestjs/mongoose';
import {TypeSchema} from './schemas/type.schema';
import {TypeService} from './type.service';
import {TypeController} from './type.controller';

@Module({
  imports: [MongooseModule.forFeature([{name: 'Type', schema: TypeSchema}])],
  controllers: [TypeController],
  providers: [TypeService],
})
export class TypeModule {}