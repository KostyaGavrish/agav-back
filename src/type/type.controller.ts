import {Controller, Get, Post, Body, Headers, Param, Query, UseGuards, Delete, Put, Res, HttpStatus} from '@nestjs/common';
import {ApiBearerAuth, ApiOAuth2Auth, ApiResponse, ApiUseTags} from '@nestjs/swagger';
import {AuthGuard} from '@nestjs/passport';
import {TokenModel} from '../models/token.model';
import {RolesGuard} from '../guards/roles.guard';
import {Roles} from '../guards/roles.decorator';
import {TypeModel} from './dto/create-type.dto';
import {TypeService} from './type.service';

@ApiUseTags('type')
@ApiOAuth2Auth()
@Controller()
export class TypeController {
  constructor(private readonly typeService: TypeService) {}

  @Get('/typeList')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  async getTypeList(): Promise<any> {
    return await this.typeService.findAll();
  }

  @Post('/type')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('ADMIN', 'SUPER_USER')
  @ApiResponse({status: 200, description: 'Success'})
  @ApiResponse({status: 401, description: 'Unauthorized'})
  @ApiResponse({status: 400, description: 'Bad request'})
  async createType(@Res() res, @Body() type: TypeModel, @Headers() headers: TokenModel) {
    const answer = (await this.typeService.createType(type));
    answer.message ? res.status(HttpStatus.BAD_REQUEST).json(answer.message) : res.status(HttpStatus.CREATED).json(answer);
  }

  @Delete('/type/:typeId')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('ADMIN', 'SUPER_USER')
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  @ApiResponse({status: 400, description: 'Bad request'})
  async deleteType(@Headers() headers: TokenModel, @Res() res, @Param() params: any) {
    const answer = (await this.typeService.deleteType(params.typeId));
    answer.message ? res.status(HttpStatus.BAD_REQUEST).json(answer.message) : res.status(HttpStatus.OK).json(answer);
  }

  @Put('/type/:typeId')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('ADMIN', 'SUPER_USER')
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 403, description: 'Error access'})
  @ApiResponse({ status: 400, description: 'Problem with id'})
  async updateType(@Headers() headers: TokenModel, @Res() res, @Param() params: any, @Body() body: TypeModel) {
    const answer = (await this.typeService.updateType(params.typeId, body));
    answer.message ? res.status(HttpStatus.BAD_REQUEST).json(answer.message) : res.status(HttpStatus.OK).json(answer);
  }
}
