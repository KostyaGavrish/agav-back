import {Injectable, CanActivate, ExecutionContext} from '@nestjs/common';
import {Observable} from 'rxjs';
import {Reflector} from '@nestjs/core';
import * as jwt from 'jsonwebtoken';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {
  }

  async canActivate(context: ExecutionContext): boolean {
    const roles = this.reflector.get<string[]>('roles', context.getHandler());
    // If no @Roles() annotation, public access
    if (!roles) {
      return true;
    }

    // Get user from AccessToken
    const request = context.switchToHttp().getRequest();
    const user = jwt.decode(request.headers.authorization.substring(7), 'secretKey');

    // Check if user has one of the roles defined in the @Roles() annotation
    const hasRole = () => user.roles.some((role) => roles.includes(role));

    // If the method is CRUD and the user has a defined role, accept
    if (hasRole() && (request.method === "PUT" || request.method === "POST" || request.method === "DELETE" || request.method === "GET")) {
      return user && user.roles && hasRole();
    }

    // If the user doesn't have a defined role

    // If the method is PUT and the user is the one who created the asset
    // Accept
    if (request.method === "PUT") {
      return user.id == request.body.identification.userId;
    }

    // If the method is DELETE
    // Accept
    if (request.method === "DELETE") {
      return true;
    }

    // If the method is POST and the user is part of the project of the asset
    // Accept
    if (request.method === "POST") {
      // console.log('Post user Works')
      const creationRightsProgram: Array<number> = [];
      for( let i = 0; i < user.creationRights.length; i++ ) {
        creationRightsProgram.push(user.creationRights[i]['programId']);
      }
      return this.checkRights(creationRightsProgram, request.body.identification);
    }
  }

  checkRights(creatorRights, identification) {
    return !!creatorRights[identification.program] &&
      creatorRights[identification.program].indexOf(identification.project) >= 0;
  }
}
