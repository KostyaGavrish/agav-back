import { ApiModelProperty } from '@nestjs/swagger';

export class TokenModel {
  @ApiModelProperty()
  readonly Authorization: string;
}