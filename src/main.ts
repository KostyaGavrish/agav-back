import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import {RolesGuard} from './guards/roles.guard';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  // app.useGlobalGuards(new RolesGuard());
  const options = new DocumentBuilder()
    .setTitle('agav api')
    .setDescription('The agav API description')
    .setVersion('0.0.1')
    // .addTag('agav')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);
  await app.listen(3000);

}
bootstrap();
