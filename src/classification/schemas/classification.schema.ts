import * as mongoose from 'mongoose';

export const ClassificationSchema = new mongoose.Schema({
  name: {
    type: String,
  },
  shortName: {
    type: String,
  },
});