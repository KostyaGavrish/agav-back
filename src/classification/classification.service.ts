import {Model} from 'mongoose';
import {Injectable} from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import {Classification} from './interfaces/classification.interface';
import {ClassificationModel} from './dto/create-classification.dto';

@Injectable()
export class ClassificationService {
  constructor(@InjectModel('Classification') private readonly classificationModel: Model<Classification>) {
  }

  async createClassification(classification: ClassificationModel): Promise<any> {

    const createdClassification = new this.classificationModel(classification);
    const err = createdClassification.validateSync();
    return err ? err : createdClassification.save();
  }

  async findAll() {
    return await this.classificationModel.find().exec();
  }

  async deleteClassification(classificationID) {
    return await this.classificationModel.findOneAndDelete({_id: classificationID}).exec() || new Error('Can\'t find id');
  }

  async updateClassification(classificationID, classification) {
    const createdClassification = new this.classificationModel(classification);
    const err = await createdClassification.validateSync();
    const data = {
      name: classification.name,
      shortName: classification.shortName,
    };
    if (err) {
      return err;
    } else {
      return await this.classificationModel
        .findOneAndUpdate({_id: classificationID}, {$set: data}, {new: true}).exec() || new Error('Can\'t find id');
    }
  }
}
