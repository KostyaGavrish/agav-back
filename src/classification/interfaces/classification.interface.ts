import { Document } from 'mongoose';

export interface Classification extends Document {
  readonly name: string;
}
