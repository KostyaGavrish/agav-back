import {ApiModelProperty} from '@nestjs/swagger';

export class ClassificationModel {
  @ApiModelProperty()
  name: string;
}