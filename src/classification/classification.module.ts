import {Module} from '@nestjs/common';
import {MongooseModule} from '@nestjs/mongoose';
import {ClassificationSchema} from './schemas/classification.schema';
import {ClassificationService} from './classification.service';
import {ClassificationController} from './classification.controller';

@Module({
  imports: [MongooseModule.forFeature([{name: 'Classification', schema: ClassificationSchema}])],
  controllers: [ClassificationController],
  providers: [ClassificationService],
})
export class ClassificationModule {}