import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Asset } from '../assets/interfaces/asset.interface';
import { Transfer } from '../transfer/interfaces/transfer.interface';
import * as jwt from 'jsonwebtoken';
import { ActionModel } from './dto/create-action.dto';
import { Action } from './interfaces/action.interface';

/** STATS
 * Nombre d'actifs par projet (distribution des actifs par unitaire/plateforme)
 * Nombre d'actifs par programme (distribution des actifs par unitaire/plateforme)
 * Nombre de tranferts par projet (distribution des transferts /actif)
 * Nombre de tranferts par programme (distribution des transferts /actif)
 * Nombre total d'actifs
 * Nombre total de transferts
 * Top 10 des actifs les plus transférés (distribution des transferts /actif)
 * Répartition des actifs en fonction du type (potentiel, ..., ...)
 * Répartition des actifs en fonction du statut (en cours, attente de validation, ...)
 * Actifs plateforme sans enfants et unitaires sans parents
*/

@Injectable()
export class ToolsService {
  constructor(
    @InjectModel('Asset') private readonly assetModel: Model<Asset>,
    @InjectModel('Transfer') private readonly transferModel: Model<Transfer>,
    @InjectModel('Action') private readonly actionModel: Model<Action>
  ) {}

  convertArrayOfObjectsToCSV(args){  
    var result, ctr, keys, columnDelimiter, lineDelimiter, data;

    data = args.data || null;
    if (data == null || !data.length) {
        return null;
    }

    columnDelimiter = args.columnDelimiter || ';';
    lineDelimiter = args.lineDelimiter || '\n';

    keys = Object.keys(data[0]);

    result = '';
    result += keys.join(columnDelimiter);
    result += lineDelimiter;

    data.forEach(function(item) {
        ctr = 0;
        keys.forEach(function(key) {
            if (ctr > 0) result += columnDelimiter;
            result += item[key];
            ctr++;
        });
        result += lineDelimiter;
    });

    return result;
  }

  async getAssetsStats() {
    /**
     * Nombre par projet
     * Par Programme
     * Total
     * Top 10 plus transférés (transfert/actif)
     * Répartition par type (potentiel, ...)
     * Répartition par statut (en cours, ...)
     * Plateforme sans enfant / Unitaire sans parent
    */
    
    let assets = await this.assetModel.find().exec();
    assets = assets.filter(item => item["status"] == "VALIDATED");

    let stats = {
      assetsNumber: assets.length,
      assetsPerProject: [],
      assetsPerProgram: [],
      assetsPerType: [],
      assetsPerStatus: [],
      mostTransferedAssets: []
    }
    let projects: Array<any> = [];
    let programs: Array<any> = [];
    let types: Array<any> = [];
    let status: Array<any> = [];

    for(let asset of assets){
      if(projects.filter(item => item == asset.identification.project).length == 0){
        projects.push(asset.identification.project);
      }
      if(programs.filter(item => item == asset.identification.program).length == 0){
        programs.push(asset.identification.program);
      }
      if(types.filter(item => item == asset["type"]).length == 0){
        types.push(asset["type"]);
      }
      if(status.filter(item => item == asset["status"]).length == 0){
        status.push(asset["status"]);
      }
      asset["transfers"] = await this.transferModel.find({"assets": {$elemMatch: {id: asset.id}}}).exec();
    }
    
    projects.forEach((item) => {
      stats.assetsPerProject.push({
        project: item.toUpperCase(),
        assets: assets.filter(it => it.identification.project == item).length
      })
    });
    programs.forEach((item) => {
      stats.assetsPerProgram.push({
        program: item.toUpperCase(),
        assets: assets.filter(it => it.identification.program == item).length
      })
    });
    types.forEach((item) => {
      stats.assetsPerType.push({
        type: item.toUpperCase(),
        assets: assets.filter(it => it["type"] == item).length
      })
    });
    status.forEach((item) => {
      stats.assetsPerStatus.push({
        status: item.toUpperCase(),
        assets: assets.filter(it => it["status"] == item).length
      })
    });

    assets.sort((objA, objB) => {
      if(objA["transfers"].length > objB["transfers"].length) return -1;
      else if(objB["transfers"].length > objA["transfers"].length) return 1;
      else return 0;
    }).slice(0, Math.max(10, assets.length-1)).forEach(item => stats.mostTransferedAssets.push({
      name: item.id,
      desc: item["desc"].replace(/\t/g, "").replace(/\n/g, ""),
      transfers: item["transfers"].length
    }))

    let csvText = "";
    let delimiter = ";";
    let lineDelimiter = "\n";

    csvText += "assetsNumber" + delimiter + stats["assetsNumber"] + lineDelimiter + lineDelimiter;

    for(let k in stats) {
      if(k != "assetsNumber"){
        csvText += k + lineDelimiter;
        csvText += this.convertArrayOfObjectsToCSV({
          data: stats[k],
          columnDelimiter: delimiter,
          lineDelimiter: lineDelimiter
        });
        csvText += lineDelimiter;
      }
    }
    return {text: csvText}
  }
  
  async getTransfersStats() {
    /**
     * Nombre de tranferts par projet (distribution des transferts /actif)
     * Nombre de tranferts par programme (distribution des transferts /actif)
     * Total transferts
    */
   
    let transfers = await this.transferModel.find().exec();
    let stats = {
      transfersNumber: transfers.length,
      transfersPerProject: [],
      transfersPerProgram: []
    }
    let projects: Array<any> = [];
    let programs: Array<any> = [];

    for(let transfer of transfers){
      if(projects.filter(item => item == transfer["assets"][0].identification.project).length == 0){
        projects.push(transfer["assets"][0].identification.project);
      }
      if(programs.filter(item => item == transfer["assets"][0].identification.program).length == 0){
        programs.push(transfer["assets"][0].identification.program);
      }
    }
    
    projects.forEach((item) => {
      stats.transfersPerProject.push({
        project: item.toUpperCase(),
        transfers: transfers.filter(it => it["assets"][0].identification.project == item).length
      })
    });
    programs.forEach((item) => {
      stats.transfersPerProgram.push({
        program: item.toUpperCase(),
        transfers: transfers.filter(it => it["assets"][0].identification.program == item).length
      })
    });

    let csvText = "";
    let delimiter = ";";
    let lineDelimiter = "\n";

    csvText += "transfersNumber" + delimiter + stats["transfersNumber"] + lineDelimiter + lineDelimiter;

    for(let k in stats) {
      if(k != "transfersNumber"){
        csvText += k + lineDelimiter;
        csvText += this.convertArrayOfObjectsToCSV({
          data: stats[k],
          columnDelimiter: delimiter,
          lineDelimiter: lineDelimiter
        });
        csvText += lineDelimiter;
      }
    }
    return {text: csvText}
  }

  async getAllStats() {
    let assetsStats = await this.getAssetsStats();
    let transfersStats = await this.getTransfersStats();

    return {
      text: assetsStats.text + '\n\n\n\n' + transfersStats.text
    }
  }

  async getActions() {
    let actions = await this.actionModel.find().exec();
    actions.forEach(item => item.id = String(item._id));
    return actions;
  }

  async getActionById(actionId: string) {
    let action = await this.actionModel.findOne({"_id": actionId}).exec();
    action.id = action._id;
    return action;
  }
  
  async getActionsByProject(project: string) {
    let actions = await this.actionModel.find({"assignees": project}).exec();
    actions.forEach(item => item.id = String(item._id));
    return actions;
  }

  async getActionsByAsset(assetId: string) {
    let actions = await this.actionModel.find({"relatedResource": {"type": "ASSET", "id": assetId}}).exec();
    actions.forEach(item => item.id = String(item._id));
    return actions;
  }

  async getActionsByTransfer(transferId: string) {
    let actions = await this.actionModel.find({"relatedResource": {"type": "TRANSFER", "id": transferId}}).exec();
    actions.forEach(item => item.id = String(item._id));
    return actions;
  }
  
  async createAction(action: ActionModel): Promise<Action | Error> {
    const createdAction = new this.actionModel(action);
    const err = createdAction.validateSync();
    return err ? err : createdAction.save();
  }

  async updateAction(actionId: string, action): Promise<Action | Error> {
    const createdAction = new this.actionModel(action);
    console.log(createdAction);
    const err = await createdAction.validateSync();
    console.log(err);
    return err ? err : await this.actionModel.findOneAndUpdate({"_id": actionId}, {$set: action}, {new: true}).exec();
  }

  async deleteAction(actionId: string) {
    return await this.actionModel.deleteOne({"_id": actionId}).exec();
  }
}
