import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AssetSchema } from '../assets/schemas/asset.schema';
import { TransferSchema } from '../transfer/schemas/transfer.schema';
import { ToolsService } from './tools.service';
import { ToolsController } from './tools.controller';
import { ActionSchema } from './schemas/action.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'Asset', schema: AssetSchema },
      { name: 'Transfer', schema: TransferSchema },
      { name: 'Action', schema: ActionSchema }
    ])
  ],
  controllers: [
    ToolsController
  ],
  providers: [
    ToolsService
  ],
})
export class ToolsModule {}
