import { Controller, Get, Post, Body, Headers, Param, UseGuards, Delete, Put, Res, HttpStatus, ReflectMetadata } from '@nestjs/common';
import { ApiOAuth2Auth, ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from '../guards/roles.decorator';
import { ToolsService } from './tools.service';
import { TokenModel } from 'models/token.model';
import { ActionModel } from './dto/create-action.dto'
import { RolesGuard } from 'guards/roles.guard';

@ApiUseTags('Tools')
@ApiOAuth2Auth()
@Controller()
export class ToolsController {
  constructor(
    private readonly toolsService: ToolsService
  ) {}

  @Get('/stats/assets')
  @Roles('ADMIN')
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({status: 200, description: 'Success'})
  @ApiResponse({status: 401, description: 'Unauthorized'})
  @ApiResponse({status: 400, description: 'Bad request'})
  async assetsStats(): Promise<any> {
    return this.toolsService.getAssetsStats();
  }

  @Get('/stats/transfers')
  @Roles('ADMIN')
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({status: 200, description: 'Success'})
  @ApiResponse({status: 401, description: 'Unauthorized'})
  @ApiResponse({status: 400, description: 'Bad request'})
  async transfersStats(): Promise<any> {
    return this.toolsService.getTransfersStats();
  }

  @Get('/stats/all')
  @Roles('ADMIN')
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({status: 200, description: 'Success'})
  @ApiResponse({status: 401, description: 'Unauthorized'})
  @ApiResponse({status: 400, description: 'Bad request'})
  async allStats(): Promise<any> {
    return this.toolsService.getAllStats();
  }

  @Get('/actions')
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({status: 200, description: 'Success'})
  @ApiResponse({status: 401, description: 'Unauthorized'})
  @ApiResponse({status: 400, description: 'Bad request'})
  async getActions(): Promise<any> {
    return this.toolsService.getActions();
  }

  @Get('/actions/:actionId')
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({status: 200, description: 'Success'})
  @ApiResponse({status: 401, description: 'Unauthorized'})
  @ApiResponse({status: 400, description: 'Bad request'})
  async getActionById(@Param() params:any): Promise<any> {
    return this.toolsService.getActionById(params.actionId);
  }

  @Get('/actions/project/:projectId')
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({status: 200, description: 'Success'})
  @ApiResponse({status: 401, description: 'Unauthorized'})
  @ApiResponse({status: 400, description: 'Bad request'})
  async getActionsByUser(@Param() params:any): Promise<any> {
    return this.toolsService.getActionsByProject(params.projectId);
  }

  @Get('/actions/asset/:assetId')
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({status: 200, description: 'Success'})
  @ApiResponse({status: 401, description: 'Unauthorized'})
  @ApiResponse({status: 400, description: 'Bad request'})
  async getActionsByAsset(@Param() params:any): Promise<any> {
    return this.toolsService.getActionsByAsset(params.assetId);
  }

  @Get('/actions/transfer/:transferId')
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({status: 200, description: 'Success'})
  @ApiResponse({status: 401, description: 'Unauthorized'})
  @ApiResponse({status: 400, description: 'Bad request'})
  async getActionsByTransfer(@Param() params:any): Promise<any> {
    return this.toolsService.getActionsByTransfer(params.transferId);
  }

  @Delete('/actions/:actionId')
  @Roles('ADMIN')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @ApiResponse({status: 200, description: 'Success'})
  @ApiResponse({status: 401, description: 'Unauthorized'})
  @ApiResponse({status: 400, description: 'Bad request'})
  async deleteAction(@Param() params:any): Promise<any> {
    return this.toolsService.deleteAction(params.actionId);
  }

  @Post('/actions')
  @Roles('ADMIN')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @ApiResponse({status: 200, description: 'Success'})
  @ApiResponse({status: 401, description: 'Unauthorized'})
  @ApiResponse({status: 400, description: 'Bad request'})
  async createAction(@Res() res, @Body() action: ActionModel, @Headers() headers: TokenModel) {
    const answer = (await this.toolsService.createAction(action));
    answer.errors ? res.status(HttpStatus.BAD_REQUEST).json(answer.errors) : res.status(HttpStatus.CREATED).json(answer);
  }

  @Put('/actions/:actionId')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  @ApiResponse({ status: 403, description: 'Error access'})
  async updateAsset(@Headers() headers: TokenModel, @Res() res, @Param() params: any, @Body() body: ActionModel) {
    const answer = (await this.toolsService.updateAction(params.actionId, body));
    answer.errors ? res.status(HttpStatus.BAD_REQUEST).json(answer.errors) : res.status(HttpStatus.OK).json(answer);
  }
}
