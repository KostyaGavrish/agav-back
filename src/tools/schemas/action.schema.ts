import * as mongoose from 'mongoose';

export const ActionSchema = new mongoose.Schema({
  id: String,
  type: {
    type: String,
    enum: [
      'ADMIN_ACTION', 
      'USER_ACTION'
    ]
  },
  relatedResource: {
    type: {
      type: String,
      enum: [
        'ASSET',
        'TRANSFER'
      ]
    },
    id: String
  },
  description: String,
  comment: [{
    date: Date,
    author: String,
    text: String
  }],
  creator: String,
  assignees: [String],
  createdAt: Date,
  dueDate: Date,
  done: {
    type: Boolean,
    default: false
  },
  archived: {
    type: Boolean,
    default: false
  }
});