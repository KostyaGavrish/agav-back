import { ApiModelProperty } from '@nestjs/swagger';

export class RelatedResourceModel {
  @ApiModelProperty({enum: ['ASSET', 'TRANSFER']})
  type: string;

  @ApiModelProperty()
  id: string;
}

export class Comment {
  @ApiModelProperty()
  date: Date;

  @ApiModelProperty()
  author: string;

  @ApiModelProperty()
  text: string;
}

export class ActionModel {
  // No @ApiModelProperty(), id is set by the backend
  id: string;

  @ApiModelProperty({enum: ['ADMIN_ACTION', 'USER_ACTION']})
  type: string;

  @ApiModelProperty()
  relatedResource: RelatedResourceModel;

  @ApiModelProperty()
  description: string;

  @ApiModelProperty()
  comment: Array<Comment>;

  @ApiModelProperty()
  creator: string;

  @ApiModelProperty()
  assignees: Array<string>;

  @ApiModelProperty()
  createdAt: Date;

  @ApiModelProperty()
  dueDate: Date;

  @ApiModelProperty()
  done: boolean;

  @ApiModelProperty()
  archived: boolean;
}