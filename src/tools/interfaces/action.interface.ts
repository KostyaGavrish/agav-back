import { Document } from 'mongoose';

export interface Comment {
  date: Date,
  author: string,
  text: string
}

export interface Action extends Document {
  id?: string,
  type: string,
  relatedResource: {
    type: string,
    id: string
  },
  description: string,
  comment: Array<Comment>,
  creator: string,
  assignees: Array<string>,
  createdAt: Date,
  dueDate: Date,
  done: boolean,
  archived: boolean
}
