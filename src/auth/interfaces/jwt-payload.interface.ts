export interface JwtPayload {
    email: string;
    password: string;
    roles: string;
    creationRights: any;
    id: any;
}