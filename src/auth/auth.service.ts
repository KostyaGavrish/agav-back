import * as jwt from 'jsonwebtoken';
import { Injectable } from '@nestjs/common';
import { JwtPayload } from './interfaces/jwt-payload.interface';

@Injectable()
export class AuthService {
    async createToken(userData?) {
        const user: JwtPayload = { email: userData.login, password: userData.password, roles: [userData.roles], creationRights: userData.creationRights, id: userData.id };
        const expiresIn = 15778800;
        const accessToken = jwt.sign(user, 'secretKey', { expiresIn });
        // console.log('accessToken:', accessToken);
        return {
            accessToken,
            expiresIn,
        };
    }

    async validateUser(payload: JwtPayload): Promise<any> {
        // await console.log('payload-->', payload);
        // put some validation logic here
        // for example query user by id/email/username
        return payload;
    }
}