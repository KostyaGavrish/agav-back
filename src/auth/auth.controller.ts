import { Controller, Get, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService) {}
    //
    // @Get('token')
    // @UseGuards(AuthGuard('jwt'))
    // async createToken(): Promise<any> {
    //     // return await this.authService.validateUser();
    // }
    //
    // @Get('data')
    // @UseGuards(AuthGuard('jwt'))
    // findAll() {
    //     return 'ok';
    // }
}