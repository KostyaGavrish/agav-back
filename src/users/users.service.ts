import {Model} from 'mongoose';
import {Injectable} from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import {Users} from './interfaces/users.interface';
import {JwtPayload} from '../auth/interfaces/jwt-payload.interface';
import { AuthService } from "../auth/auth.service";
import * as crypto from 'crypto';

@Injectable()
export class UsersService {
  algorithm = 'aes-256-ctr';
  password = 'ertydfrtyfvtyugbytudfi';

  constructor(@InjectModel('Users') private readonly userModel: Model<Users>) {
  }

  encrypt(text){
    const cipher = crypto.createCipher(this.algorithm, this.password);
    let crypted = cipher.update(text,'utf8','hex');
    crypted += cipher.final('hex');
    return crypted;
  }

  decrypt(text){
    const decipher = crypto.createDecipher(this.algorithm, this.password);
    let dec = decipher.update(text, 'hex', 'utf8');
    dec += decipher.final('utf8');
    return dec;
  }

  isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

  async create(user: any): Promise<any> {
    console.log('user==================>', user);
    console.log('user111111111111==================>', user.creationRigths);
    const lastUser = await this.userModel.findOne({}, {}, {sort: {'_id': -1}}).exec();
    user.id = lastUser ? (lastUser.id + 1) : 1;
    user.password = this.encrypt(user.password);
    const createdUser = new this.userModel(user);
    const err = createdUser.validateSync();
    return err ? err : createdUser.save();
  }

  async findAll() {
    return await this.userModel.find().then( res => {
      for (let i = 0; i < res.length; i++){
        res[i].password='';
      }
      // console.log('resqwer', res);
      return res;
    });
  }

  async getUser(userId) {
    if (!this.isNumeric(userId)) return new Error('Id is not valid');
    return await this.userModel.findOne({id: userId}).then(res => {
      if (res){
        res.password='';
        return res;
      }
    }).catch (err => {
      return err;
    });
  }

  async deleteUser(userID) {
    if (!this.isNumeric(userID)) return new Error('Id is not valid');
    return await this.userModel.deleteOne({id: userID}).exec();
  }

  async updateUser(userID, user) {
    if (!this.isNumeric(userID)) return new Error('Id is not valid');
    const createdUser = new this.userModel(user);
    const err = await createdUser.validateSync();
    const data = {
      firstName: user.firstName,
      lastName: user.lastName,
      role: user.role,
      login: user.login,
      creationRights: user.creationRights,
    };
    user.password ? data.password = this.encrypt(user.password) : null;
    if (err) {
      return err;
    } else {
      return await this.userModel.findOneAndUpdate({id: userID}, {$set: data}, {new: true}).exec() || new Error('Can\'t find id');
    }
  }

  async login(user) {
    console.log('user', user);
    if (!(user.login && user.password)) {
      return new Error('Need password and email');
    } else {
      return await this.userModel.findOne({login: user.login}).then(res => {
        console.log('res', res);
        console.log(this.decrypt(res.password))
        if (res.password === this.encrypt(user.password)) {
          console.log('4')
          return res;
        } else {
          console.log('5')
          return new Error('Not registered user');
        }
      }).catch((err) => {
        return err;
      });
    }
  }
}
