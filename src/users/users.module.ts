import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import {AuthService} from '../auth/auth.service';
import {UsersService} from './users.service';
import {UserSchema} from './schemas/user.schema';
import {MongooseModule} from '@nestjs/mongoose';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Users', schema: UserSchema }])],
  controllers: [UsersController],
  providers: [AuthService, UsersService],
})
export class UsersModule {}