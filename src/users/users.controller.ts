import {
  Controller,
  Get,
  Post,
  Res,
  Delete,
  Body,
  UseGuards,
  UseInterceptors,
  Param, Headers, Query, HttpStatus, Put,
} from '@nestjs/common';

import {ApiBearerAuth, ApiResponse, ApiUseTags} from '@nestjs/swagger';

import {LoginUserModel} from './models/login-user.model';
import {PasswordUserModel} from './models/login-user.model';
import {TokenModel} from '../models/token.model';
import {AuthService} from '../auth/auth.service';
import {AuthGuard} from '@nestjs/passport';
import {RolesGuard} from '../guards/roles.guard';
import {Roles} from '../guards/roles.decorator';
import {UsersService} from './users.service';
import {UserModel} from './dto/create-user.dto';
import {UserIdModel} from './dto/userId.model';

@ApiBearerAuth()
@ApiUseTags('User')
@Controller()

export class UsersController {
  constructor(private authService: AuthService, private userService: UsersService) {
  }

  @Get('/login/:login')
  @ApiResponse({status: 200, description: 'Login success.'})
  @ApiResponse({status: 403, description: 'Invalid Data.'})
   async login(@Headers() headers: PasswordUserModel, @Res() res, @Param() params: LoginUserModel) {
    console.log('headers', headers);
    const user = {
      login: params.login || '',
      password: headers.authorization || '',
    };
    const answer = await this.userService.login(user);
    if (!answer.message) {
      const userData = {
        email: answer.login,
        password: answer.password,
        roles: answer.role,
        creationRights: answer.creationRights,
        id: answer.id,
      };
      const userForAnswer = {
        email: answer.login,
        roles: answer.role,
        creationRights: answer.creationRights,
        id: answer.id,
        firstName : answer.firstName,
        lastName : answer.lastName,
      };
      this.authService.createToken(userData).then(resp => {
        const token = resp.accessToken;
        userForAnswer.token = token;
        // console.log('TOKEN->', token);
        // console.log('answer', userForAnswer);
        res.status(HttpStatus.OK).json(userForAnswer);
      });
    }
    // answer.message ? res.status(HttpStatus.BAD_REQUEST).json(answer.message) : res.status(HttpStatus.CREATED).json(answer);
  //   const users = [
  //     {
  //       login: 'Dir-TI',
  //       password: 'XQOE2o3h',
  //       data: {
  //         id: 1,
  //         firstName: 'Direction TI',
  //         lastName: 'Territoires Intelligents',
  //         role: 'ADMIN',
  //         creationRights: {},
  //         uid: 'srt3hfg4yfytsd5fguuyfitydrys'
  //       }
  //     },
  //     {
  //       login: 'Dir-TA',
  //       password: '6*tlHaeA',
  //       data: {
  //         id: 2,
  //         firstName: 'Direction TA',
  //         lastName: 'Transport Autonome',
  //         role: 'ADMIN',
  //         creationRights: {},
  //         uid: 'srt3hfg4yfytsd5fg56fitydrys'
  //       }
  //     },
  //     {
  //       login: 'Dir-IA',
  //       password: 'ET733pEu',
  //       data: {
  //         id: 3,
  //         firstName: 'Direction IA',
  //         lastName: 'Industrie Agile',
  //         role: 'ADMIN',
  //         creationRights: {},
  //         uid: 'srt3hfg4yfytsazfg56fitydrys'
  //       }
  //     },
  //     {
  //       login: 'Dir-IC',
  //       password: 'rAapnjTd',
  //       data: {
  //         id: 4,
  //         firstName: 'Direction IC',
  //         lastName: 'Internet de Confiance',
  //         role: 'ADMIN',
  //         creationRights: {},
  //         uid: 'srt3hfg4yfaasd5fg56fitydrys'
  //       }
  //     },
  //     {
  //       login: 'Dir-DT',
  //       password: 'H5h3Orp*',
  //       data: {
  //         id: 5,
  //         firstName: 'Direction DT',
  //         lastName: 'Direction Technique',
  //         role: 'ADMIN',
  //         creationRights: {},
  //         uid: 'str3hfg4yfytsd5fg56fitydrys'
  //       }
  //     },
  //     {
  //       login: 'DirecteurGeneral',
  //       password: '0oEkuM5g',
  //       data: {
  //         id: 6,
  //         firstName: 'DG',
  //         lastName: 'Directeur Général',
  //         role: 'ADMIN',
  //         creationRights: {},
  //         uid: 'srt3hfg4yhgtsd5fg56fitydrys'
  //       }
  //     },
  //     {
  //       login: 'SecretaireGeneral',
  //       password: '0Ip!GEjf',
  //       data: {
  //         id: 7,
  //         firstName: 'SG',
  //         lastName: 'Secretaire Général',
  //         role: 'ADMIN',
  //         creationRights: {},
  //         uid: 'srt3hfg4yfytsd5f8y6fitydrys'
  //       }
  //     },
  //     {
  //       login: 'Operations-DT',
  //       password: 'mG*bXKrJ',
  //       data: {
  //         id: 8,
  //         firstName: 'Opérations DT',
  //         lastName: 'Ops. Direction Technique',
  //         role: 'ADMIN',
  //         creationRights: {},
  //         uid: 'srt3hfg4ythtsd5f8y6fitydrys'
  //       }
  //     },
  //     {
  //       login: 'Admin-DT',
  //       password: '$vJXw6s0',
  //       data: {
  //         id: 9,
  //         firstName: 'Admin DT',
  //         lastName: 'Adm. Direction Technique',
  //         role: 'ADMIN',
  //         creationRights: {},
  //         uid: 'srt3hfg4yfytsd5khy6fitydrys'
  //       }
  //     },
  //     {
  //       login: 'Transverse-DT',
  //       password: 'f4ikUJwG',
  //       data: {
  //         id: 10,
  //         firstName: 'Transverse DT',
  //         lastName: 'Direction Technique',
  //         role: 'SUPER_USER',
  //         creationRights: {
  //           'DT': ['Transverse']
  //         },
  //         uid: 'fvt3hfg4yfytsd5f8y6fitydrys'
  //       }
  //     },
  //     {
  //       login: 'Projet-AMC',
  //       password: 'SPoEvbQa',
  //       data: {
  //         id: 11,
  //         firstName: 'AMC',
  //         lastName: 'Amélioration des Marges de Conception',
  //         role: 'SUPER_USER',
  //         creationRights: {
  //           'IA': ['AMC']
  //         },
  //         uid: 'fvt3hnb4yfytsd5f8y6fitydrys'
  //       }
  //     },
  //     {
  //       login: 'Projet-CDF',
  //       password: 'bI7ZcFLq',
  //       data: {
  //         id: 12,
  //         firstName: 'CDF',
  //         lastName: 'Amélioration des Marges de Conception',
  //         role: 'SUPER_USER',
  //         creationRights: {
  //           'IA': ['CDF']
  //         },
  //         uid: 'fvt3hn546fytsd5f8y6fitydrys'
  //       }
  //     },
  //     {
  //       login: 'Projet-DSL',
  //       password: 'XC05u!Yv',
  //       data: {
  //         id: 13,
  //         firstName: 'DSL',
  //         lastName: 'DSL',
  //         role: 'SUPER_USER',
  //         creationRights: {
  //           'IA': ['DSL']
  //         },
  //         uid: 'fvt3hngf8fytsd5f8y6fitydrys'
  //       }
  //     },
  //     {
  //       login: 'Projet-ISC',
  //       password: 'dUmDB2Xz',
  //       data: {
  //         id: 14,
  //         firstName: 'ISC',
  //         lastName: 'ISC',
  //         role: 'SUPER_USER',
  //         creationRights: {
  //           'IA': ['ISC']
  //         },
  //         uid: 'fvt3hngf83ktsd5f8y6fitydrys'
  //       }
  //     },
  //     {
  //       login: 'Projet-OAR',
  //       password: 'RXtOMZmu',
  //       data: {
  //         id: 15,
  //         firstName: 'OAR',
  //         lastName: 'OAR',
  //         role: 'SUPER_USER',
  //         creationRights: {
  //           'IA': ['OAR']
  //         },
  //         uid: 'fvt3hngx73ktsd5f8y6fitydrys'
  //       }
  //     },
  //     {
  //       login: 'Projet-TOP',
  //       password: 'Q1aa2pz!',
  //       data: {
  //         id: 16,
  //         firstName: 'TOP',
  //         lastName: 'TOP',
  //         role: 'SUPER_USER',
  //         creationRights: {
  //           'IA': ['TOP']
  //         },
  //         uid: 'fvt3hngf83gvsd5f8y6fitydrys'
  //       }
  //     },
  //     {
  //       login: 'Projet-EIC',
  //       password: 'z7HVae9h',
  //       data: {
  //         id: 17,
  //         firstName: 'EIC',
  //         lastName: 'EIC',
  //         role: 'SUPER_USER',
  //         creationRights: {
  //           'IC': ['EIC']
  //         },
  //         uid: 'fvt3hngf83gvs99f8y6fitydrys'
  //       }
  //     },
  //     {
  //       login: 'Projet-Io4',
  //       password: 'RF39oN$f',
  //       data: {
  //         id: 18,
  //         firstName: 'Io4',
  //         lastName: 'Io4',
  //         role: 'SUPER_USER',
  //         creationRights: {
  //           'IC': ['Io4']
  //         },
  //         uid: 'fvt3hngf83gfd49f8y6fitydrys'
  //       }
  //     },
  //     {
  //       login: 'Projet-STC',
  //       password: '9LupRwYf',
  //       data: {
  //         id: 19,
  //         firstName: 'STC',
  //         lastName: 'STC',
  //         role: 'SUPER_USER',
  //         creationRights: {
  //           'IC': ['STC']
  //         },
  //         uid: 'fvt3gd6f83gfd49f8y6fitydrys'
  //       }
  //     },
  //     {
  //       login: 'Projet-API',
  //       password: 'pGf!U1!g',
  //       data: {
  //         id: 20,
  //         firstName: 'API',
  //         lastName: 'API',
  //         role: 'SUPER_USER',
  //         creationRights: {
  //           'TA': ['API']
  //         },
  //         uid: 'fvt3vv6f83gfd49f8y6fitydrys'
  //       }
  //     },
  //     {
  //       login: 'Projet-CMI',
  //       password: 's0HRI2L8',
  //       data: {
  //         id: 21,
  //         firstName: 'CMI',
  //         lastName: 'CMI',
  //         role: 'SUPER_USER',
  //         creationRights: {
  //           'TA': ['CMI']
  //         },
  //         uid: 'fvt3vg4483gfd49f8y6fitydrys'
  //       }
  //     },
  //     {
  //       login: 'Projet-CTI',
  //       password: 'W3O6hcIQ',
  //       data: {
  //         id: 22,
  //         firstName: 'CTI',
  //         lastName: 'CTI',
  //         role: 'SUPER_USER',
  //         creationRights: {
  //           'TA': ['CTI']
  //         },
  //         uid: 'fvt3vv6frr7fd49f8y6fitydrys'
  //       }
  //     },
  //     {
  //       login: 'Projet-EVA',
  //       password: 'ASDUdmdR',
  //       data: {
  //         id: 23,
  //         firstName: 'EVA',
  //         lastName: 'EVA',
  //         role: 'SUPER_USER',
  //         creationRights: {
  //           'TA': ['EVA']
  //         },
  //         uid: 'fvt3vv6f83gfd5gg8y6fitydrys'
  //       }
  //     },
  //     {
  //       login: 'Projet-PST',
  //       password: 'v*DSuN*E',
  //       data: {
  //         id: 24,
  //         firstName: 'PST',
  //         lastName: 'PST',
  //         role: 'SUPER_USER',
  //         creationRights: {
  //           'TA': ['PST']
  //         },
  //         uid: 'fvt3vv6fhhgfd49f8y6fitydrys'
  //       }
  //     },
  //     {
  //       login: 'Projet-SCA',
  //       password: 'LZzzo7Y4',
  //       data: {
  //         id: 25,
  //         firstName: 'SCA',
  //         lastName: 'SCA',
  //         role: 'SUPER_USER',
  //         creationRights: {
  //           'TA': ['SCA']
  //         },
  //         uid: 'fvt3vv6213gfd49f8y6fitydrys'
  //       }
  //     },
  //     {
  //       login: 'Projet-SVA',
  //       password: 'z0QNqBZs',
  //       data: {
  //         id: 26,
  //         firstName: 'SVA',
  //         lastName: 'SVA',
  //         role: 'SUPER_USER',
  //         creationRights: {
  //           'TA': ['SVA']
  //         },
  //         uid: 'fwx3vv6f83gfd49f8y6fitydrys'
  //       }
  //     },
  //     {
  //       login: 'Projet-TAS',
  //       password: 'Q2lmHxGr',
  //       data: {
  //         id: 27,
  //         firstName: 'TAS',
  //         lastName: 'TAS',
  //         role: 'SUPER_USER',
  //         creationRights: {
  //           'TA': ['TAS']
  //         },
  //         uid: 'fvt3vv6fwxpfd49f8y6fitydrys'
  //       }
  //     },
  //     {
  //       login: 'Projet-BFI',
  //       password: '45rbeZLm',
  //       data: {
  //         id: 28,
  //         firstName: 'BFI',
  //         lastName: 'BFI',
  //         role: 'SUPER_USER',
  //         creationRights: {
  //           'TI': ['BFI']
  //         },
  //         uid: 'fvt3vv6f83gfd49f8y6fitydrys'
  //       }
  //     },
  //     {
  //       login: 'Projet-BST',
  //       password: 'bu*7FRq!',
  //       data: {
  //         id: 29,
  //         firstName: 'BST',
  //         lastName: 'BST',
  //         role: 'SUPER_USER',
  //         creationRights: {
  //           'TI': ['BST']
  //         },
  //         uid: 'fvt3vv6f86yfd49f8y6fitydrys'
  //       }
  //     },
  //     {
  //       login: 'Projet-HCU',
  //       password: '4ifiiVTx',
  //       data: {
  //         id: 30,
  //         firstName: 'HCU',
  //         lastName: 'HCU',
  //         role: 'SUPER_USER',
  //         creationRights: {
  //           'TI': ['HCU']
  //         },
  //         uid: 'fvt3vpkf83gfd49f8y6fitydrys'
  //       }
  //     },
  //     {
  //       login: 'Projet-IVA',
  //       password: 'mx$src7G',
  //       data: {
  //         id: 31,
  //         firstName: 'IVA',
  //         lastName: 'IVA',
  //         role: 'SUPER_USER',
  //         creationRights: {
  //           'TI': ['IVA']
  //         },
  //         uid: 'fvt3vv6f83gfd49f8y6fitydrxx'
  //       }
  //     },
  //     {
  //       login: 'Projet-LCE',
  //       password: 'Dl5*Zg2V',
  //       data: {
  //         id: 32,
  //         firstName: 'LCE',
  //         lastName: 'LCE',
  //         role: 'SUPER_USER',
  //         creationRights: {
  //           'TI': ['LCE']
  //         },
  //         uid: 'jit3vv6f83gfd49f8y6fitydrys'
  //       }
  //     },
  //     {
  //       login: 'Projet-MSM',
  //       password: 'nzyk!EJ0',
  //       data: {
  //         id: 33,
  //         firstName: 'MSM',
  //         lastName: 'MSM',
  //         role: 'SUPER_USER',
  //         creationRights: {
  //           'TI': ['MSM']
  //         },
  //         uid: 'fvt3vg0f83gfd49f8y6fitydrys'
  //       }
  //     },
  //     {
  //       login: 'Projet-SCE',
  //       password: 'EsQ!Vt1f',
  //       data: {
  //         id: 34,
  //         firstName: 'SCE',
  //         lastName: 'SCE',
  //         role: 'SUPER_USER',
  //         creationRights: {
  //           'TI': ['SCE']
  //         },
  //         uid: 'fvt3vg0f83gfd562ty6fitydrys'
  //       }
  //     },
  //     {
  //       login: 'Projet-LRA',
  //       password: '1zPQVgLu',
  //       data: {
  //         id: 35,
  //         firstName: 'LRA',
  //         lastName: 'LRA',
  //         role: 'SUPER_USER',
  //         creationRights: {
  //           'TA': ['LRA']
  //         },
  //         uid: 'fvt3vg0f83gfd556yy6fitydrys'
  //       }
  //     },
  //     {
  //       login: 'Projet-ELA',
  //       password: 'Sr!oq2Iz',
  //       data: {
  //         id: 36,
  //         firstName: 'ELA',
  //         lastName: 'ELA',
  //         role: 'SUPER_USER',
  //         creationRights: {
  //           'TA': ['ELA']
  //         },
  //         uid: 'fvt3vg0003gfd562ty6fitydrys'
  //       }
  //     },
  //     {
  //       login: 'Projet-FSF',
  //       password: 'PQeXfdHP',
  //       data: {
  //         id: 37,
  //         firstName: 'FSF',
  //         lastName: 'FSF',
  //         role: 'SUPER_USER',
  //         creationRights: {
  //           'TA': ['FSF']
  //         },
  //         uid: 'fvt3vg0fhj9fd562ty6fitydrys'
  //       }
  //     },
  //     {
  //       login: 'Projet-ISE',
  //       password: 'xp$FLy1*',
  //       data: {
  //         id: 38,
  //         firstName: 'ISE',
  //         lastName: 'ISE',
  //         role: 'SUPER_USER',
  //         creationRights: {
  //           'TA': ['ISE']
  //         },
  //         uid: 'fvt3vg0f8v7bd562ty6fitydrys'
  //       }
  //     },
  //     {
  //       login: 'Projet-SIM',
  //       password: 'lr8$iX81',
  //       data: {
  //         id: 39,
  //         firstName: 'SIM',
  //         lastName: 'SIM',
  //         role: 'SUPER_USER',
  //         creationRights: {
  //           'IA': ['SIM']
  //         },
  //         uid: 'f99kvg0f83gfd562ty6fitydrys'
  //       }
  //     },
  //     {
  //       login: 'Projet-ROM',
  //       password: 'dH68Xy$3',
  //       data: {
  //         id: 40,
  //         firstName: 'ROM',
  //         lastName: 'ROM',
  //         role: 'SUPER_USER',
  //         creationRights: {
  //           'IA': ['ROM']
  //         },
  //         uid: 'f12kvg0f8378jpo2ty6fitysryd'
  //       }
  //     },
  //     {
  //       login: 'Resp-Comm',
  //       password: '56vFXy3$',
  //       data: {
  //         id: 41,
  //         firstName: 'Resp. Communication',
  //         lastName: 'Resp. Communication',
  //         role: 'ADMIN',
  //         creationRights: {},
  //         uid: 'f12kvg0fvb5ujpo2ty6fitysryd'
  //       }
  //     },
  //     {
  //       login: 'Dir-DS',
  //       password: '16aFTs3$',
  //       data: {
  //         id: 42,
  //         firstName: 'Direction DS',
  //         lastName: 'Direction Scientifique',
  //         role: 'ADMIN',
  //         creationRights: {},
  //         uid: 'f12kvg0fvb5ujpo2ty6fitysrye'
  //       }
  //     },
  //     {
  //       login: 'Coord-DS',
  //       password: '26vRTs2$',
  //       data: {
  //         id: 43,
  //         firstName: 'Coord. DS',
  //         lastName: 'Coord. Direction Scientifique',
  //         role: 'ADMIN',
  //         creationRights: {},
  //         uid: 'f12kvg0fvb5ujpo2ty6fitysryf'
  //       }
  //     },
  //     {
  //       login: 'Coord-TA',
  //       password: 'R7vRTs6$',
  //       data: {
  //         id: 44,
  //         firstName: 'Coord. TA',
  //         lastName: 'Coord. Transport Autonome',
  //         role: 'ADMIN',
  //         creationRights: {},
  //         uid: 'f12kvg0fvb5ujpo2ty6fitysryg'
  //       }
  //     },
  //     {
  //       login: 'Coord-DT',
  //       password: '16vGSs2$',
  //       data: {
  //         id: 45,
  //         firstName: 'Coord. DT',
  //         lastName: 'Coord. Direction Technique',
  //         role: 'ADMIN',
  //         creationRights: {},
  //         uid: 'f12kvg0fvb5ujpo2ty6fitysryh'
  //       }
  //     },
  //     {
  //       login: 'Projet-MIC',
  //       password: 'tV4*i8X1',
  //       data: {
  //         id: 46,
  //         firstName: 'MIC',
  //         lastName: 'MIC',
  //         role: 'SUPER_USER',
  //         creationRights: {
  //           'TI': ['MIC']
  //         },
  //         uid: 'f99kvg0f8356kjl2ty6fitydrys'
  //       }
  //     },
  //     {
  //       login: 'Projet-IMM',
  //       password: 'hJ$4f6WO',
  //       data: {
  //         id: 47,
  //         firstName: 'IMM',
  //         lastName: 'IMM',
  //         role: 'SUPER_USER',
  //         creationRights: {
  //           'TI': ['IMM']
  //         },
  //         uid: '83y6fit56kjl2t9kf9dryg0fvys'
  //       }
  //     },
  //     {
  //       login: 'juriste',
  //       password: 'hf6WOJ$4',
  //       data: {
  //         id: 48,
  //         firstName: 'Grégory',
  //         lastName: 'PESCINI',
  //         role: 'ADMIN',
  //         creationRights: {},
  //         uid: '83y6fit56kj0zZ9kf9dryg0fvys'
  //       }
  //     },
  //     {
  //       login: 'singapour',
  //       password: '2h0J6!JW',
  //       data: {
  //         id: 49,
  //         firstName: 'François-Xavier',
  //         lastName: 'LANNUZEL',
  //         role: 'ADMIN',
  //         creationRights: {},
  //         uid: '83y4gH0zkj0zZyg0fvy9kf9drs'
  //       }
  //     }
  //   ]
  //   /*
  //   const users = [
  //       {
  //           login: 'admin',
  //           password: 'admin123',
  //           data: {
  //               id: 1,
  //               firstName: 'AdminName',
  //               lastName: 'AdminLastName',
  //               creationRights: {
  //                   'IC': ['EIC'],
  //               },
  //               role: 'ADMIN',
  //               uid: 'srt3hfg4yfytsd5fguuyfitydrys',
  //           },
  //       },
  //       {
  //           login: 'user',
  //           password: 'user123',
  //           data: {
  //               id: 2,
  //               firstName: 'User',
  //               lastName: 'Test',
  //               creationRights: {
  //        {
  //           login: 'Projet-ROM',
  //           password: 'dH68Xy$3',
  //           data: {
  //               id: 39,
  //               firstName: 'ROM',
  //               lastName: 'ROM',
  //               role: 'SUPER_USER',
  //               creationRights: {
  //                   'IA': ['ROM']
  //               },
  //               uid: 'f12kvg0f8378jpo2ty6fitysryd'
  //           }
  //       }A', 'PST', 'SCA', 'SVA', 'TAS', 'Autre'],
  //        {
  //           login: 'Projet-ROM',
  //           password: 'dH68Xy$3',
  //           data: {
  //               id: 39,
  //               firstName: 'ROM',
  //               lastName: 'ROM',
  //               role: 'SUPER_USER',
  //               creationRights: {
  //                   'IA': ['ROM']
  //               },
  //               uid: 'f12kvg0f8378jpo2ty6fitysryd'
  //           }
  //       }
  //        {
  //           login: 'Projet-ROM',
  //           password: 'dH68Xy$3',
  //           data: {
  //               id: 39,
  //               firstName: 'ROM',
  //               lastName: 'ROM',
  //               role: 'SUPER_USER',
  //               creationRights: {
  //                   'IA': ['ROM']
  //               },
  //               uid: 'f12kvg0f8378jpo2ty6fitysryd'
  //           }
  //       }
  //        {
  //           login: 'Projet-ROM',
  //           password: 'dH68Xy$3',
  //           data: {
  //               id: 39,
  //               firstName: 'ROM',
  //               lastName: 'ROM',
  //               role: 'SUPER_USER',
  //               creationRights: {
  //                   'IA': ['ROM']
  //               },
  //               uid: 'f12kvg0f8378jpo2ty6fitysryd'
  //           }
  //       }
  //        {
  //           login: 'Projet-ROM',
  //           password: 'dH68Xy$3',
  //           data: {
  //               id: 39,
  //               firstName: 'ROM',
  //               lastName: 'ROM',
  //               role: 'SUPER_USER',
  //               creationRights: {
  //                   'IA': ['ROM']
  //               },
  //               uid: 'f12kvg0f8378jpo2ty6fitysryd'
  //           }
  //       }
  //       }{
  //           login: 'Projet-ROM',
  //           password: 'dH68Xy$3',
  //           data: {
  //               id: 39,
  //               firstName: 'ROM',
  //               lastName: 'ROM',
  //               role: 'SUPER_USER',
  //               creationRights: {
  //                   'IA': ['ROM']
  //               },
  //               uid: 'f12kvg0f8378jpo2ty6fitysryd'
  //           }
  //       }
  //       {{
  //           login: 'Projet-ROM',
  //           password: 'dH68Xy$3',
  //           data: {
  //               id: 39,
  //               firstName: 'ROM',
  //               lastName: 'ROM',
  //               role: 'SUPER_USER',
  //               creationRights: {
  //                   'IA': ['ROM']
  //               },
  //               uid: 'f12kvg0f8378jpo2ty6fitysryd'
  //           }
  //       }
  //           login: 'SuperUser',
  //           password: 'SuperUser123',
  //           data: {
  //               id: 3,
  //               firstName: 'SuperUser',
  //               lastName: 'Test',
  //               creationRights: {
  //                   'IA': ['TOP', 'Autre'],
  //                   'DT': ['Transverse'],
  //               },
  //               role: 'SUPER_USER',
  //               uid: 'vugbrfygbvgvujbufyirbfygvhbj',
  //           },
  //       },
  //       {
  //           login: 'user2',
  //           password: 'user2',
  //           data: {
  //               id: 4,
  //               firstName: 'User2',
  //               lastName: 'Test2',
  //               creationRights: {
  //                   "IC": ['EIC', 'STC', 'Autre'],
  //                   'IA': ['AMC', 'CDF', 'ISC', 'OAR', 'TOP', 'Autre'],
  //                   'DT': ['Transverse'],
  //               },
  //               role: 'SUPER_USER',
  //               uid: 'vugbrfygdsfghfsagretuy',
  //           },
  //       },
  //       {
  //           login: 'user3',
  //           password: 'user3',
  //           data: {
  //               id: 5,
  //               firstName: 'User3',
  //               lastName: 'Test3',
  //               creationRights: {
  //                   "IC": ['EIC', 'STC'],
  //                   'IA': ['AMC', 'CDF', 'TOP', 'Autre'],
  //                   'DT': ['Transverse'],
  //               },
  //               role: 'SUPER_USER',
  //               uid: 'vugbrfhjfgdfdszfjhbjgvbuy',
  //           },
  //       },
  //
  //   ];
  //   */
  //   let user = {};
  //   let token = '';
  //   let errorData = true;
  //   console.log('login ', headers);
  //   console.log('login ', headers.authorization);
  //   let userData = {
  //     login: params.login,
  //     password: headers.authorization,
  //   };
  //   // console.log('userTOKEN => ', this.authService.createToken(userData));
  //   users.forEach((element) => {
  //     if (element.login.toLowerCase() === userData.login.toLowerCase()) {
  //       if (element.password === userData.password) {
  //         errorData = false;
  //         user = element.data;
  //         userData.roles = user.role;
  //         userData.creationRights = user.creationRights;
  //         userData.id = user.id;
  //         this.authService.createToken(userData).then(resp => {
  //           token = resp.accessToken;
  //           user.token = token;
  //           // console.log('TOKEN->', token);
  //           res.status(HttpStatus.OK).json(user);
  //         });
  //       }
  //     }
  //   });
  //   if (errorData) {
  //     res.status(HttpStatus.FORBIDDEN).send();
  //   }
  }

  @Delete('/logout')
  // @UseGuards(AuthGuard('jwt'))
  @ApiResponse({status: 200, description: 'Logout success.'})
  @ApiResponse({status: 403, description: 'Forbidden.'})
  logout(@Res() res, @Headers() headers: TokenModel) {
    res.status(HttpStatus.OK).send();
  }


  @Get('/usersList')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  async getTransferList(@Headers() headers: TokenModel): Promise<any> {
      return this.userService.findAll();
  }

  @Get('/user/:userId')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  async getUserList(@Headers() headers: TokenModel, @Param() params: UserIdModel): Promise<any> {
    return this.userService.getUser(params.userId);
  }

  @Post('/user')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('ADMIN')
  @ApiResponse({status: 200, description: 'Success'})
  @ApiResponse({status: 401, description: 'Unauthorized'})
  @ApiResponse({status: 400, description: 'Bad request'})
  async create(@Res() res, @Body() user: UserModel, @Headers() headers: TokenModel) {
    const answer = (await this.userService.create(user));
    answer.message ? res.status(HttpStatus.BAD_REQUEST).json(answer.message) : res.status(HttpStatus.CREATED).json(answer);
  }

  @Delete('/users/:userId')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('ADMIN')
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  @ApiResponse({ status: 400, description: 'Problem with id'})
  async deleteUser(@Headers() headers: TokenModel, @Res() res, @Param() params: UserIdModel) {
    const answer = (await this.userService.deleteUser(params.userId));
    answer.message ? res.status(HttpStatus.BAD_REQUEST).json(answer.message) : res.status(HttpStatus.OK).json(answer);
  }

  @Put('/users/:userId')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('ADMIN')
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 403, description: 'Error access'})
  @ApiResponse({ status: 400, description: 'Problem with id'})
  async updateUser(@Headers() headers: TokenModel, @Res() res, @Param() params: UserIdModel, @Body() body: UserModel) {
    const answer = (await this.userService.updateUser(params.userId, body));
    answer.message ? res.status(HttpStatus.BAD_REQUEST).json(answer.message) : res.status(HttpStatus.OK).json(answer);
  }
  // @Get('/user/:userId')
  // @ApiResponse({status: 200, description: 'Success'})
  // @ApiResponse({status: 403, description: 'Forbidden.'})
  // @UseGuards(AuthGuard('jwt'))
  // getUser(@Param() params, @Headers() headers: TokenModel) {
  //     const user = {
  //         1: {
  //
  //             id: 1,
  //             firstName: 'Direction TI',
  //             lastName: 'Territoires Intelligents',
  //             role: 'ADMIN',
  //             creationRights: {}
  //         },
  //         2: {
  //
  //             id: 2,
  //             firstName: 'Direction TA',
  //             lastName: 'Transport Autonome',
  //             role: 'ADMIN',
  //             creationRights: {}
  //         },
  //         3: {
  //
  //             id: 3,
  //             firstName: 'Direction IA',
  //             lastName: 'Industrie Agile',
  //             role: 'ADMIN',
  //             creationRights: {}
  //         },
  //         4: {
  //
  //             id: 4,
  //             firstName: 'Direction IC',
  //             lastName: 'Internet de Confiance',
  //             role: 'ADMIN',
  //             creationRights: {}
  //         },
  //         5: {
  //
  //             id: 5,
  //             firstName: 'Direction DT',
  //             lastName: 'Direction Technique',
  //             role: 'ADMIN',
  //             creationRights: {}
  //         },
  //         6: {
  //
  //             id: 6,
  //             firstName: 'DG',
  //             lastName: 'Directeur Général',
  //             role: 'ADMIN',
  //             creationRights: {}
  //         },
  //         7: {
  //
  //             id: 7,
  //             firstName: 'SG',
  //             lastName: 'Secretaire Général',
  //             role: 'ADMIN',
  //             creationRights: {}
  //         },
  //         8: {
  //
  //             id: 8,
  //             firstName: 'Opérations DT',
  //             lastName: 'Ops. Direction Technique',
  //             role: 'ADMIN',
  //             creationRights: {}
  //         },
  //         9: {
  //
  //             id: 9,
  //             firstName: 'Admin DT',
  //             lastName: 'Adm. Direction Technique',
  //             role: 'ADMIN',
  //             creationRights: {}
  //         },
  //         10: {
  //             id: 10,
  //             firstName: 'Transverse DT',
  //             lastName: 'Direction Technique',
  //             role: 'SUPER_USER',
  //             creationRights: {
  //                 'DT': ['Transverse']
  //             }
  //         },
  //         11: {
  //             id: 11,
  //             firstName: 'AMC',
  //             lastName: 'Amélioration des Marges de Conception',
  //             role: 'SUPER_USER',
  //             creationRights: {
  //                 'IA': ['AMC']
  //             }
  //         },
  //         12: {
  //             id: 12,
  //             firstName: 'CDF',
  //             lastName: 'Amélioration des Marges de Conception',
  //             role: 'SUPER_USER',
  //             creationRights: {
  //                 'IA': ['CDF']
  //             }
  //         },
  //         13: {
  //             id: 13,
  //             firstName: 'DSL',
  //             lastName: 'DSL',
  //             role: 'SUPER_USER',
  //             creationRights: {
  //                 'IA': ['DSL']
  //             }
  //         },
  //         14: {
  //             id: 14,
  //             firstName: 'ISC',
  //             lastName: 'ISC',
  //             role: 'SUPER_USER',
  //             creationRights: {
  //                 'IA': ['ISC']
  //             }
  //         },
  //         15: {
  //             id: 15,
  //             firstName: 'OAR',
  //             lastName: 'OAR',
  //             role: 'SUPER_USER',
  //             creationRights: {
  //                 'IA': ['OAR']
  //             }
  //         },
  //         16: {
  //             id: 16,
  //             firstName: 'TOP',
  //             lastName: 'TOP',
  //             role: 'SUPER_USER',
  //             creationRights: {
  //                 'IA': ['TOP']
  //             }
  //         },
  //         17: {
  //             id: 17,
  //             firstName: 'EIC',
  //             lastName: 'EIC',
  //             role: 'SUPER_USER',
  //             creationRights: {
  //                 'IC': ['EIC']
  //             }
  //         },
  //         18: {
  //             id: 18,
  //             firstName: 'Io4',
  //             lastName: 'Io4',
  //             role: 'SUPER_USER',
  //             creationRights: {
  //                 'IC': ['Io4']
  //             }
  //         },
  //         19: {
  //             id: 19,
  //             firstName: 'STC',
  //             lastName: 'STC',
  //             role: 'SUPER_USER',
  //             creationRights: {
  //                 'IC': ['STC']
  //             }
  //         },
  //         20: {
  //             id: 20,
  //             firstName: 'API',
  //             lastName: 'API',
  //             role: 'SUPER_USER',
  //             creationRights: {
  //                 'TA': ['API']
  //             }
  //         },
  //         21: {
  //             id: 21,
  //             firstName: 'CMI',
  //             lastName: 'CMI',
  //             role: 'SUPER_USER',
  //             creationRights: {
  //                 'TA': ['CMI']
  //             }
  //         },
  //         22: {
  //             id: 22,
  //             firstName: 'CTI',
  //             lastName: 'CTI',
  //             role: 'SUPER_USER',
  //             creationRights: {
  //                 'TA': ['CTI']
  //             }
  //         },
  //         23: {
  //             id: 23,
  //             firstName: 'EVA',
  //             lastName: 'EVA',
  //             role: 'SUPER_USER',
  //             creationRights: {
  //                 'TA': ['EVA']
  //             }
  //         },
  //         24: {
  //             id: 24,
  //             firstName: 'PST',
  //             lastName: 'PST',
  //             role: 'SUPER_USER',
  //             creationRights: {
  //                 'TA': ['PST']
  //             }
  //         },
  //         25: {
  //             id: 25,
  //             firstName: 'SCA',
  //             lastName: 'SCA',
  //             role: 'SUPER_USER',
  //             creationRights: {
  //                 'TA': ['SCA']
  //             }
  //         },
  //         26: {
  //             id: 26,
  //             firstName: 'SVA',
  //             lastName: 'SVA',
  //             role: 'SUPER_USER',
  //             creationRights: {
  //                 'TA': ['SVA']
  //             }
  //         },
  //         27: {
  //             id: 27,
  //             firstName: 'TAS',
  //             lastName: 'TAS',
  //             role: 'SUPER_USER',
  //             creationRights: {
  //                 'TA': ['TAS']
  //             }
  //         },
  //         28: {
  //             id: 28,
  //             firstName: 'BFI',
  //             lastName: 'BFI',
  //             role: 'SUPER_USER',
  //             creationRights: {
  //                 'TI': ['BFI']
  //             }
  //         },
  //         29: {
  //             id: 29,
  //             firstName: 'BST',
  //             lastName: 'BST',
  //             role: 'SUPER_USER',
  //             creationRights: {
  //                 'TI': ['BST']
  //             }
  //         },
  //         30: {
  //             id: 30,
  //             firstName: 'HCU',
  //             lastName: 'HCU',
  //             role: 'SUPER_USER',
  //             creationRights: {
  //                 'TI': ['HCU']
  //             }
  //         },
  //         31: {
  //             id: 31,
  //             firstName: 'IVA',
  //             lastName: 'IVA',
  //             role: 'SUPER_USER',
  //             creationRights: {
  //                 'TI': ['IVA']
  //             }
  //         },
  //         32: {
  //             id: 32,
  //             firstName: 'LCE',
  //             lastName: 'LCE',
  //             role: 'SUPER_USER',
  //             creationRights: {
  //                 'TI': ['LCE']
  //             }
  //         },
  //         33: {
  //             id: 33,
  //             firstName: 'MSM',
  //             lastName: 'MSM',
  //             role: 'SUPER_USER',
  //             creationRights: {
  //                 'TI': ['MSM']
  //             }
  //         },
  //         34: {
  //             id: 34,
  //             firstName: 'SCE',
  //             lastName: 'SCE',
  //             role: 'SUPER_USER',
  //             creationRights: {
  //                 'TI': ['SCE']
  //             }
  //         },
  //         35: {
  //             id: 35,
  //             firstName: 'LRA',
  //             lastName: 'LRA',
  //             role: 'SUPER_USER',
  //             creationRights: {
  //                 'TA': ['LRA']
  //             }
  //         },
  //         36: {
  //             id: 36,
  //             firstName: 'ELA',
  //             lastName: 'ELA',
  //             role: 'SUPER_USER',
  //             creationRights: {
  //                 'TA': ['ELA']
  //             }
  //         },
  //         37: {
  //             id: 37,
  //             firstName: 'FSF',
  //             lastName: 'FSF',
  //             role: 'SUPER_USER',
  //             creationRights: {
  //                 'TA': ['FSF']
  //             }
  //         },
  //         38: {
  //             id: 38,
  //             firstName: 'ISE',
  //             lastName: 'ISE',
  //             role: 'SUPER_USER',
  //             creationRights: {
  //                 'TA': ['ISE']
  //             }
  //         },
  //         39: {
  //             id: 39,
  //             firstName: 'SIM',
  //             lastName: 'SIM',
  //             role: 'SUPER_USER',
  //             creationRights: {
  //                 'IA': ['SIM']
  //             }
  //         },
  //         40: {
  //             id: 40,
  //             firstName: 'ROM',
  //             lastName: 'ROM',
  //             role: 'SUPER_USER',
  //             creationRights: {
  //                 'IA': ['ROM']
  //             }
  //         },
  //         41: {
  //             id: 41,
  //             firstName: 'Resp. Communication',
  //             lastName: 'Resp. Communication',
  //             role: 'ADMIN',
  //             creationRights: {}
  //         },
  //         42: {
  //             id: 42,
  //             firstName: 'Direction DS',
  //             lastName: 'Direction Scientifique',
  //             role: 'ADMIN',
  //             creationRights: {}
  //         },
  //         43: {
  //             id: 43,
  //             firstName: 'Coord. DS',
  //             lastName: 'Coord. Direction Scientifique',
  //             role: 'ADMIN',
  //             creationRights: {}
  //         },
  //         44: {
  //             id: 44,
  //             firstName: 'Coord. TA',
  //             lastName: 'Coord. Transport Autonome',
  //             role: 'ADMIN',
  //             creationRights: {}
  //         },
  //         45: {
  //             id: 45,
  //             firstName: 'Coord. DT',
  //             lastName: 'Coord. Direction Technique',
  //             role: 'ADMIN',
  //             creationRights: {}
  //         },
  //         46: {
  //             id: 46,
  //             firstName: 'MIC',
  //             lastName: 'MIC',
  //             role: 'SUPER_USER',
  //             creationRights: {}
  //         },
  //         47: {
  //             id: 47,
  //             firstName: 'IMM',
  //             lastName: 'IMM',
  //             role: 'SUPER_USER',
  //             creationRights: {}
  //         },
  //         48: {
  //             id: 48,
  //             firstName: 'Grégory',
  //             lastName: 'PESCINI',
  //             role: 'ADMIN',
  //             creationRights: {}
  //         },
  //         49: {
  //             id: 49,
  //             firstName: 'François-Xavier',
  //             lastName: 'LANNUZEL',
  //             role: 'ADMIN',
  //             creationRights: {}
  //         }
  //     }
  //     /* const user = {
  //         1: {
  //             id: 1,
  //             firstName: 'AdminName',
  //             lastName: 'AdminLastName',
  //             role: 'ADMIN',
  //             creationRights: {
  //                 'IC': ['EIC'],
  //             },
  //         },
  //         2: {
  //             id: 2,
  //             firstName: 'User',
  //             lastName: 'Test',
  //             role: 'SUPER_USER',
  //             creationRights: {
  //                 "TA": ['API', 'CMI', 'CTI', 'EVA', 'PST', 'SCA', 'SVA', 'TAS', 'Autre'],
  //             },
  //         },
  //         3: {
  //             id: 3,
  //             firstName: 'SuperUser',
  //             lastName: 'Test',
  //             role: 'SUPER_USER',
  //             creationRights: {
  //                 'IA': ['TOP', 'Autre'],
  //                 'DT': ['Transverse'],
  //             },
  //         },
  //         4: {
  //             id: 4,
  //             firstName: 'User2',
  //             lastName: 'Test2',
  //             role: 'SUPER_USER',
  //             creationRights: {
  //                 "IC": ['EIC', 'STC', 'Autre'],
  //                 'IA': ['AMC', 'CDF', 'ISC', 'OAR', 'TOP', 'Autre'],
  //                 'DT': ['Transverse'],
  //             },
  //         },
  //         5: {
  //             id: 5,
  //             firstName: 'User3',
  //             lastName: 'Test3',
  //             role: 'SUPER_USER',
  //             creationRights: {
  //                 "IC": ['EIC', 'STC'],
  //                 'IA': ['AMC', 'CDF', 'TOP', 'Autre'],
  //                 'DT': ['Transverse'],
  //             },
  //         },
  //     }; */
  //     return user[params.userId];
  // }
}