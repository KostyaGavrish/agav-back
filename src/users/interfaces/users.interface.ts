import {Document} from 'mongoose';


export interface Users extends Document {
  readonly id: number;
  readonly firstName: string;
  readonly lastName: string;
  readonly role: string;
  readonly login: string;
  readonly password: string;
  readonly creationRights: array<string>;
}