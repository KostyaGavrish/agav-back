import { ApiModelProperty } from '@nestjs/swagger';

export class UserIdModel {
  @ApiModelProperty()
  readonly userId: number;
}