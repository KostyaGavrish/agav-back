import {ApiModelProperty} from '@nestjs/swagger';

export class UserModel {
  // @ApiModelProperty()
  id: number;

  @ApiModelProperty()
  login: string;

  @ApiModelProperty()
  password: string;

  @ApiModelProperty()
  firstName: string;

  @ApiModelProperty()
  lastName: string;

  @ApiModelProperty({enum: ['ADMIN', 'SUPER_USER', 'USER']})
  role: string;

  @ApiModelProperty({isArray: true})
  creationRights: string;
}