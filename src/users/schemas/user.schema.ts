import * as mongoose from 'mongoose';

export const UserSchema = new mongoose.Schema({
  id: Number,
  firstName: String,
  lastName: String,
  role: {
    type: String,
    enum: ['ADMIN', 'SUPER_USER', 'USER'],
  },
  login: String,
  password: String,
  creationRights: Array,
});