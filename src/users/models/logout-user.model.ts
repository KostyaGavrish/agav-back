import { ApiModelProperty } from '@nestjs/swagger';

export class LogoutUserModel {
  @ApiModelProperty()
  readonly token: string;
}
