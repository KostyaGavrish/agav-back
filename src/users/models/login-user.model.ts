import { ApiModelProperty } from '@nestjs/swagger';

export class LoginUserModel {
  @ApiModelProperty()
  readonly login: string;
  // @ApiModelProperty()
  // readonly password: string;
}

export class PasswordUserModel {
  // @ApiModelProperty()
  // readonly login: string;
  @ApiModelProperty()
  readonly password: string;
}