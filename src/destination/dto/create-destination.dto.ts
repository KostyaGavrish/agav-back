import {ApiModelProperty} from '@nestjs/swagger';

export class DestinationModel {
  @ApiModelProperty()
  name: string;
}