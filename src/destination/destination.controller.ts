import {Controller, Get, Post, Body, Headers, Param, Query, UseGuards, Delete, Put, Res, HttpStatus} from '@nestjs/common';
import {ApiBearerAuth, ApiOAuth2Auth, ApiResponse, ApiUseTags} from '@nestjs/swagger';
import {AuthGuard} from '@nestjs/passport';
import {TokenModel} from '../models/token.model';
import {RolesGuard} from '../guards/roles.guard';
import {Roles} from '../guards/roles.decorator';
import {DestinationModel} from './dto/create-destination.dto';
import {DestinationService} from './destination.service';

@ApiUseTags('destination')
@ApiOAuth2Auth()
@Controller()
export class DestinationController {
  constructor(private readonly destinationService: DestinationService) {}

  @Get('/destinationList')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  async getDestinationList(): Promise<any> {
    return await this.destinationService.findAll();
  }

  @Post('/destination')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('ADMIN', 'SUPER_USER')
  @ApiResponse({status: 200, description: 'Success'})
  @ApiResponse({status: 401, description: 'Unauthorized'})
  @ApiResponse({status: 400, description: 'Bad request'})
  async createDestination(@Res() res, @Body() destination: DestinationModel, @Headers() headers: TokenModel) {
    const answer = (await this.destinationService.createDestination(destination));
    answer.message ? res.status(HttpStatus.BAD_REQUEST).json(answer.message) : res.status(HttpStatus.CREATED).json(answer);
  }

  @Delete('/destination/:destinationId')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('ADMIN', 'SUPER_USER')
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 401, description: 'Unauthorized'})
  @ApiResponse({status: 400, description: 'Bad request'})
  async deleteDestination(@Headers() headers: TokenModel, @Res() res, @Param() params: any) {
    const answer = (await this.destinationService.deleteDestination(params.destinationId));
    answer.message ? res.status(HttpStatus.BAD_REQUEST).json(answer.message) : res.status(HttpStatus.OK).json(answer);
  }

  @Put('/destination/:destinationId')
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Roles('ADMIN', 'SUPER_USER')
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 403, description: 'Error access'})
  @ApiResponse({ status: 400, description: 'Problem with id'})
  async updateDestination(@Headers() headers: TokenModel, @Res() res, @Param() params: any, @Body() body: DestinationModel) {
    const answer = (await this.destinationService.updateDestination(params.destinationId, body));
    answer.message ? res.status(HttpStatus.BAD_REQUEST).json(answer.message) : res.status(HttpStatus.OK).json(answer);
  }
}
