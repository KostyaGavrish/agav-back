import {Model} from 'mongoose';
import {Injectable} from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import {Destination} from './interfaces/destination.interface';
import {DestinationModel} from './dto/create-destination.dto';

@Injectable()
export class DestinationService {
    constructor(@InjectModel('Destination') private readonly destinationModel: Model<Destination>) {
    }

    async createDestination(destination: DestinationModel): Promise<any> {
        const createdDestination = new this.destinationModel(destination);
        const err = createdDestination.validateSync();
        return err ? err : createdDestination.save();
    }

    async findAll() {
        return await this.destinationModel.find().exec();
    }

    async deleteDestination(destinationID) {
        return await this.destinationModel.findOneAndDelete({_id: destinationID}).exec() || new Error('Can\'t find id');
    }

    async updateDestination(destinationID, destination) {
        const createdDestination = new this.destinationModel(destination);
        const err = await createdDestination.validateSync();
        const data = {
            name: destination.name,
        };
        if (err) {
            return err;
        } else {
            return await this.destinationModel
                .findOneAndUpdate({_id: destinationID}, {$set: data}, {new: true}).exec() || new Error('Can\'t find id');
        }
    }
}
