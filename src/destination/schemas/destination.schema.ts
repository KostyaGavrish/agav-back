import * as mongoose from 'mongoose';

export const DestinationSchema = new mongoose.Schema({
  name: {
    type: String,
  },
});