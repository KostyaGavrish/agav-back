import {Module} from '@nestjs/common';
import {MongooseModule} from '@nestjs/mongoose';
import {DestinationSchema} from './schemas/destination.schema';
import {DestinationService} from './destination.service';
import {DestinationController} from './destination.controller';

@Module({
  imports: [MongooseModule.forFeature([{name: 'Destination', schema: DestinationSchema}])],
  controllers: [DestinationController],
  providers: [DestinationService],
})
export class DestinationModule {}