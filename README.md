# Running the app

```bash
# start mongodb in docker
$ cd ./mongodbdocker
$ docker build -t agavmongodb .
$ docker run -p 27017:27017 agavmongodb
# in new termimal start server 
$ docker build -t agavnestjs .
$ docker run -p 3000:3000 -e MONGOURL='mongodb://192.168.31.35:27017/agav' agavnestjs
```

