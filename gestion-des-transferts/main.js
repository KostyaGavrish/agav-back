(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/actions-center/actions-center.component.html":
/*!**************************************************************!*\
  !*** ./src/app/actions-center/actions-center.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"actions-center\">\n  <div class=\"row\">\n    <div class=\"col-md-2 col-lg-2\"></div>\n    <div class=\"col-md-8 col-lg-8\">\n      <div class=\"panel panel-primary table-wrapper\">\n        <h4 class=\"panel-heading\">\n          Liste des actions\n        </h4>\n        <span title=\"Montrer/Cacher les archives\" (click)=\"toggleArchives($event)\" class=\"archives\">\n          <fa *ngIf=\"isAdmin()\" class=\"user-icon\" name=\"archive\"></fa>\n        </span>\n        <span *ngIf=\"isAdmin()\" (click)=\"purgeDoneActions()\" class=\"purge\">Purger</span>\n        <p-treeTable [value]=\"filteredActions\">\n          <!-- <ng-template pTemplate=\"header\">\n            <tr>\n              <th>Resource</th>\n              <th>Description</th>\n              <th>Assignee</th>\n              <th>Due date</th>\n              <th>Status</th>\n            </tr>\n          </ng-template> -->\n\n          <p-column [style]=\"{'width':'15%'}\">\n            <ng-template let-col let-node=\"rowData\">\n              <div (click)=\"goToAssetEdition(node.relatedResource.id)\" style=\"cursor: pointer\" class=\"td-flex-wrapper\">\n                <div style=\"text-transform: capitalize\">{{node.relatedResource.type.toLowerCase() + \" \"}}</div>\n                <div>{{node.relatedResource.id}}</div>\n              </div>\n            </ng-template>\n          </p-column>\n          \n          <p-column [style]=\"{'width':'25vw'}\">\n            <ng-template let-col let-node=\"rowData\">\n              <div class=\"td-flex-wrapper left-aligned-content\">\n                <div class=\"asset-description\">{{node.description}}</div>\n              </div>\n            </ng-template>\n          </p-column>\n          \n          <p-column>\n            <ng-template let-col let-node=\"rowData\">\n              <div class=\"td-flex-wrapper\">\n                <div>{{node.assignees.join(\", \")}}</div>\n              </div>\n            </ng-template>\n          </p-column>\n          \n          <p-column>\n            <ng-template let-col let-node=\"rowData\">\n              <div class=\"td-flex-wrapper\">\n                <div>{{getDate(node.dueDate)}}</div>\n              </div>\n            </ng-template>\n          </p-column>\n          \n          <p-column>\n            <ng-template let-col let-node=\"rowData\">\n              <div class=\"td-flex-wrapper inline-content tools\">\n                <div class=\"status sticker\" [ngClass]=\"{done: node.done}\"></div>\n                <div *ngIf=\"node.comment && node.comment.length > 0\" title=\"{{node.comment[node.comment.length -1].text}}\">\n                  <fa class=\"user-icon\" name=\"file-text\" style=\"cursor: default\"></fa>\n                </div>\n                <div *ngIf=\"isAdmin()\" (click)=\"openActionModal($event, node.id)\" title=\"Editer\">\n                  <fa class=\"user-icon\" name=\"pencil\"></fa>\n                </div>\n                <fa *ngIf=\"isAdmin()\" class=\"user-icon\" name=\"trash\" (click)=\"deleteAction(node.id)\" [ngClass]=\"{'disabled': !isAdmin()}\"></fa>\n              </div>\n            </ng-template>\n          </p-column>\n        </p-treeTable>\n        <div *ngIf=\"filteredActions.length == 0\" style=\"text-align: center\">Pas d'action en attente !</div>\n      </div>\n    </div>\n    <div class=\"col-md-2 col-lg-2\"></div>\n  </div>\n</div>\n\n\n<smite-modal id=\"action_edit\">\n  <div class=\"smite-modal\">\n    <div class=\"smite-modal-body\">\n      <div class=\"smite-modal-closer\" (click)=\"modals.close('action_edit')\">X</div>\n      <div class=\"smite-modal-header\">\n        <div class=\"title\">Editer l'action</div>\n        <div class=\"col desc\">\n          <p>Editer l'action \"{{actionToEdit?.description}}\"</p>\n        </div>\n      </div>\n      <div class=\"content\">\n        <form [formGroup]=\"editActionForm\" class=\"asset-actions-wrapper\">\n          <div class=\"flex-col\">\n            <div class=\"flex-row\">\n              <!-- Select user/admin action type -->\n              <div class=\"action-field\">\n                <label for=\"actionType\">Type d'action</label>\n                <select name=\"actionType\" formControlName=\"actionType\" style=\"display: block\">\n                  <option value=\"USER_ACTION\">Action technique (sur l'actif ou sa fiche)</option>\n                  <option value=\"ADMIN_ACTION\">Action business (contact partenaire ou autre)</option>\n                </select>\n              </div>\n              <!-- Due Date datepicker -->\n              <div class=\"action-field\">\n                <label for=\"actionDate\">Date limite</label>\n                <input class=\"action-deadline\" type=\"date\" name=\"actionDate\" formControlName=\"actionDate\" useValueAsDate #actionDateInput/>\n              </div>\n            </div>\n            <div class=\"flex-row\">\n              <!-- Input assignee(s) -->\n              <div class=\"action-field\">\n                <label for=\"actionAssignee\">Acteur</label>\n                <input type=\"text\" formControlName=\"actionAssignee\" name=\"actionAssignee\" placeholder=\"Nom de la personne responsable...\">\n              </div>\n              <!-- Checkbox isDone -->\n              <div class=\"action-field\">\n                  <label for=\"actionDone\">Effectuée</label>\n                  <input type=\"checkbox\" formControlName=\"actionDone\" name=\"actionDone\">\n                </div>\n            </div>\n            <!-- TextArea description -->\n            <div class=\"action-field\">\n              <label for=\"actionDesc\">Description</label>\n              <textarea name=\"actionDesc\" formControlName=\"actionDesc\" placeholder=\"Nature de l'action à accomplir...\"></textarea>\n            </div>\n          </div>\n          <div class=\"flex-col\">\n            <div class=\"action-field\">\n              <label for=\"actionComment\">Commentaires</label>\n              <div class=\"flex-row\">\n                <textarea name=\"actionComment\" formControlName=\"actionComment\" placeholder=\"Rajouter un commentaire...\" style=\"margin-right: 10px; flex: 1; min-width: 300px;\"></textarea>\n                <div (click)=\"addComment()\" class=\"comment-button button\">Ajouter</div>\n              </div>\n            </div>\n            <div class=\"comments\">\n              <div *ngFor=\"let comment of actionToEdit?.comment?.slice().reverse()\">\n                <span style=\"font-weight: bolder;\">{{comment.author}} @ {{comment.date | date}} : </span>\n                <span>{{comment.text}}</span>\n              </div>\n            </div>\n          </div>\n        </form>\n        \n      </div>\n      <div class=\"smite-modal-controls\">\n        <div (click)=\"updateAction($event)\" class=\"validate button\">Valider</div>\n      </div>\n    </div>\n  </div>\n  <div class=\"smite-modal-background\"></div>\n</smite-modal>"

/***/ }),

/***/ "./src/app/actions-center/actions-center.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/actions-center/actions-center.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host .actions-center {\n  padding-top: 90px; }\n  :host .actions-center .panel {\n    border-radius: 0;\n    background-color: #fbfbfb; }\n  :host .actions-center .panel .panel-heading {\n      border-radius: 0; }\n  :host .archives {\n  cursor: pointer;\n  position: absolute;\n  right: 30px;\n  top: 8px;\n  font-size: .8em;\n  font-weight: bolder;\n  padding: 5px 10px;\n  background: white;\n  border-radius: 3px; }\n  :host .archives.active {\n    background-color: #d4af6b; }\n  :host .purge {\n  cursor: pointer;\n  position: absolute;\n  right: 70px;\n  top: 8px;\n  font-size: .8em;\n  font-weight: bolder;\n  color: #2c7fb7;\n  padding: 5px 10px;\n  background: white;\n  border-radius: 3px; }\n  :host ::ng-deep.ui-treetable thead {\n  display: none; }\n  :host ::ng-deep.ui-treetable .ui-treetable-toggler {\n  display: none; }\n  :host ::ng-deep.ui-treetable tbody .ui-treetable-row td {\n  border: 1px solid lightgrey;\n  white-space: normal; }\n  :host ::ng-deep.ui-treetable .td-flex-wrapper {\n  display: flex;\n  flex-flow: column nowrap;\n  justify-content: center;\n  align-items: center; }\n  :host ::ng-deep.ui-treetable .td-flex-wrapper.inline-content {\n    flex-flow: row nowrap; }\n  :host ::ng-deep.ui-treetable .td-flex-wrapper.left-aligned-content {\n    align-items: flex-start; }\n  :host ::ng-deep.ui-treetable .td-flex-wrapper > *:not(:last-child) {\n    margin-right: 20px; }\n  :host .panel-heading {\n  margin: 0; }\n  :host .tools .status.sticker {\n  height: 12px;\n  width: 12px;\n  border-radius: 50%;\n  background-color: orangered; }\n  :host .tools .status.sticker.done {\n    background-color: #10aa10; }\n  :host .tools .user-icon {\n  cursor: pointer; }\n  smite-modal .content form {\n  width: 100%;\n  display: flex;\n  flex-flow: row nowrap; }\n  smite-modal .content form > *:not(:last-child) {\n    margin-right: 20px; }\n  smite-modal .content form .flex-col {\n    flex: 1;\n    display: flex;\n    flex-flow: column nowrap;\n    justify-content: flex-start;\n    align-items: flex-start; }\n  smite-modal .content form .flex-col > *:not(:last-child) {\n      margin-bottom: 20px; }\n  smite-modal .content form .flex-col .flex-row {\n      display: flex;\n      flex-flow: row wrap;\n      justify-content: flex-start;\n      align-items: flex-start; }\n  smite-modal .content form .flex-col .flex-row > *:not(:last-child) {\n        margin-right: 30px;\n        margin-bottom: 10px; }\n  smite-modal .content form .flex-col > .action-field {\n      width: 100%; }\n  smite-modal .content form .flex-col .action-field > label, smite-modal .content form .flex-col .action-field > input, smite-modal .content form .flex-col .action-field > select, smite-modal .content form .flex-col .action-field > textarea {\n      display: block; }\n  smite-modal .content form .flex-col input, smite-modal .content form .flex-col select, smite-modal .content form .flex-col textarea {\n      height: 30px;\n      padding: 3px 10px;\n      border-radius: 3px;\n      background: white;\n      border: 1px solid #0062cc; }\n  smite-modal .content form .flex-col textarea {\n      height: 60px;\n      width: 100%;\n      resize: none; }\n  smite-modal .content form .comments {\n    overflow: auto; }\n  smite-modal .content form .comments > *:not(:last-child) {\n      margin-bottom: 15px; }\n  smite-modal .content form .button {\n    padding: 3px 10px;\n    border-radius: 3px;\n    cursor: pointer; }\n  smite-modal .content form .button.comment-button {\n      background: #10aa10;\n      color: whitesmoke; }\n"

/***/ }),

/***/ "./src/app/actions-center/actions-center.component.ts":
/*!************************************************************!*\
  !*** ./src/app/actions-center/actions-center.component.ts ***!
  \************************************************************/
/*! exports provided: ActionsCenterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActionsCenterComponent", function() { return ActionsCenterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_requests_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/requests.service */ "./src/app/services/requests.service.ts");
/* harmony import */ var _services_pass_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/pass.service */ "./src/app/services/pass.service.ts");
/* harmony import */ var _services_local_storage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/local-storage.service */ "./src/app/services/local-storage.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _smite_modals__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../smite-modals */ "./src/app/smite-modals/index.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ActionsCenterComponent = /** @class */ (function () {
    function ActionsCenterComponent(formBuilder, pass, requests, storage, modals, router) {
        this.formBuilder = formBuilder;
        this.pass = pass;
        this.requests = requests;
        this.storage = storage;
        this.modals = modals;
        this.router = router;
        this.actions = [];
        this.showArchives = false;
        this.actionToEdit = {};
    }
    ActionsCenterComponent.prototype.getDate = function (date) {
        return new Date(date).toLocaleDateString();
    };
    ActionsCenterComponent.prototype.isAdmin = function () {
        return this.pass.isAdmin();
    };
    ActionsCenterComponent.prototype.goToAssetEdition = function (resourceId) {
        this.router.navigate(["edit_asset", resourceId]);
    };
    ActionsCenterComponent.prototype.openActionModal = function ($event, actionId) {
        this.actionToEdit = this.actions.find(function (item) { return item.id == actionId; });
        var form = this.editActionForm.controls;
        form.actionType.setValue(this.actionToEdit.type),
            form.actionDate.setValue(new Date(this.actionToEdit.dueDate)),
            form.actionAssignee.setValue(this.actionToEdit.assignees[0]),
            form.actionDesc.setValue(this.actionToEdit.description);
        form.actionDone.setValue(this.actionToEdit.done);
        this.modals.open("action_edit");
    };
    ActionsCenterComponent.prototype.addComment = function () {
        if (this.editActionForm.controls.actionComment.value.trim() != "") {
            this.actionToEdit.comment.push({
                date: new Date(),
                author: this.storage.getUser().firstName,
                text: this.editActionForm.controls.actionComment.value.trim()
            });
            this.requests.updateAction(this.actionToEdit.id, this.actionToEdit);
            this.editActionForm.controls.actionComment.setValue("");
        }
    };
    ActionsCenterComponent.prototype.updateAction = function ($event) {
        var form = this.editActionForm.controls;
        this.actionToEdit.type = form.actionType.value;
        this.actionToEdit.dueDate = form.actionDate.value;
        this.actionToEdit.assignees[0] = form.actionAssignee.value;
        this.actionToEdit.description = form.actionDesc.value;
        this.actionToEdit.done = form.actionDone.value;
        this.requests.updateAction(this.actionToEdit.id, this.actionToEdit);
        this.modals.close("action_edit");
    };
    ActionsCenterComponent.prototype.deleteAction = function (actionId) {
        var i = this.actions.findIndex(function (item) { return item.id = actionId; });
        this.actions.splice(i, 1);
        this.requests.deleteAction(actionId);
    };
    ActionsCenterComponent.prototype.purgeDoneActions = function () {
        var _this = this;
        this.actions.filter(function (item) { return item.done; }).forEach(function (item) {
            item.archived = true;
            _this.requests.updateAction(item.id, item);
            _this.fetchActions();
        });
    };
    ActionsCenterComponent.prototype.toggleArchives = function ($event) {
        this.showArchives = !this.showArchives;
        $event.target.closest('.archives').classList.toggle('active');
        this.fetchActions();
    };
    ActionsCenterComponent.prototype.fetchActions = function () {
        var _this = this;
        /** Need to find a way to get the user's main project
         */
        // if(this.isAdmin()) {
        this.requests.getActions().subscribe(function (data) {
            _this.actions = data;
        });
        // }
        // else {
        //   this.requests.getActionsForProject("toto").subscribe((data:Array<any>) => {
        //     this.actions = data.sort((objA, objB) => {
        //       return (new Date(objA).getTime() - new Date(objB).getTime());
        //     });
        //   });
        // }
    };
    Object.defineProperty(ActionsCenterComponent.prototype, "filteredActions", {
        get: function () {
            var _this = this;
            return this.actions.filter(function (item) { return _this.showArchives ? true : !item.archived; }).sort(function (objA, objB) {
                return (new Date(objA).getTime() - new Date(objB).getTime());
            });
        },
        enumerable: true,
        configurable: true
    });
    ActionsCenterComponent.prototype.ngOnInit = function () {
        this.fetchActions();
        this.editActionForm = this.formBuilder.group({
            actionType: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControl"]('USER_ACTION', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required),
            actionDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required),
            actionAssignee: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required),
            actionDesc: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required),
            actionComment: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required),
            actionDone: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required)
        });
    };
    ActionsCenterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-actions-center',
            template: __webpack_require__(/*! ./actions-center.component.html */ "./src/app/actions-center/actions-center.component.html"),
            styles: [__webpack_require__(/*! ./actions-center.component.scss */ "./src/app/actions-center/actions-center.component.scss")],
            providers: [
                _smite_modals__WEBPACK_IMPORTED_MODULE_5__["ModalsService"]
            ]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"],
            _services_pass_service__WEBPACK_IMPORTED_MODULE_2__["PassService"],
            _services_requests_service__WEBPACK_IMPORTED_MODULE_1__["RequestsService"],
            _services_local_storage_service__WEBPACK_IMPORTED_MODULE_3__["LocalStorageService"],
            _smite_modals__WEBPACK_IMPORTED_MODULE_5__["ModalsService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], ActionsCenterComponent);
    return ActionsCenterComponent;
}());



/***/ }),

/***/ "./src/app/administration/administration.component.html":
/*!**************************************************************!*\
  !*** ./src/app/administration/administration.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n  <div class=\"col-md-1 col-lg-1\"></div>\n  <div class=\"col-md-10 col-lg-10 reference-header-wrapper\">\n      <div class=\"panel panel-primary\">\n        <div class=\"panel-heading\">\n          <span class=\"panel-title\">Administration - Gestion des listes de références</span>\n        </div>\n      </div>\n  </div>\n  <div class=\"col-md-1 col-lg-1\"></div>\n</div>\n\n<div class=\"row\">\n  <div class=\"col-md-1 col-lg-1\"></div>\n  <div class=\"col-md-10 col-lg-10\">\n    <div class=\"reference-wrapper row\">\n      <div class=\"panel panel-primary col-md-4 col-lg-4\" (click)=\"goToReferenceEdit('Statut')\">\n        <div class=\"panel-heading\">\n          <span class=\"panel-title\">Statut</span>\n        </div>\n        <div class=\"panel-body\"></div>\n      </div>\n      <div class=\"panel panel-primary col-md-4 col-lg-4\" (click)=\"goToReferenceEdit('Destination')\">\n        <div class=\"panel-heading\">\n          <span class=\"panel-title\">Destination</span>\n        </div>\n        <div class=\"panel-body\"></div>\n      </div>\n      <div class=\"panel panel-primary col-md-4 col-lg-4\" (click)=\"goToReferenceEdit('Type')\">\n        <div class=\"panel-heading\">\n          <span class=\"panel-title\">Type</span>\n        </div>\n        <div class=\"panel-body\"></div>\n      </div>\n    </div>\n\n    <div class=\"reference-wrapper row\">\n      <div class=\"panel panel-primary col-md-4 col-lg-4\" (click)=\"goToReferenceEdit('Effort')\">\n        <div class=\"panel-heading\">\n          <span class=\"panel-title\">Effort</span>\n        </div>\n        <div class=\"panel-body\"></div>\n      </div>\n      <div class=\"panel panel-primary col-md-4 col-lg-4\" (click)=\"goToReferenceEdit('Indicateurs')\">\n        <div class=\"panel-heading\">\n          <span class=\"panel-title\">Indicateurs</span>\n        </div>\n        <div class=\"panel-body\"></div>\n      </div>\n      <div class=\"panel panel-primary col-md-4 col-lg-4\" (click)=\"goToReferenceEdit('Classification')\">\n        <div class=\"panel-heading\">\n          <span class=\"panel-title\">Classification</span>\n        </div>\n        <div class=\"panel-body\"></div>\n      </div>\n    </div>\n\n    <div class=\"reference-wrapper row\">\n      <div class=\"panel panel-primary col-md-4 col-lg-4\" (click)=\"goToReferenceEdit('Programmes')\">\n        <div class=\"panel-heading\">\n          <span class=\"panel-title\">Programmes</span>\n        </div>\n        <div class=\"panel-body\"></div>\n      </div>\n      <div class=\"panel panel-primary col-md-4 col-lg-4\" (click)=\"goToReferenceEdit('Utilisateurs')\">\n        <div class=\"panel-heading\">\n          <span class=\"panel-title\">Utilisateurs</span>\n        </div>\n        <div class=\"panel-body\"></div>\n      </div>\n      <div class=\"panel panel-primary col-md-4 col-lg-4\" (click)=\"goToReferenceEdit('Projets')\">\n        <div class=\"panel-heading\">\n          <span class=\"panel-title\">Projets</span>\n        </div>\n        <div class=\"panel-body\"></div>\n      </div>\n    </div>\n  </div>\n  <div class=\"col-md-1 col-lg-1\"></div>\n</div>\n"

/***/ }),

/***/ "./src/app/administration/administration.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/administration/administration.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host > .row {\n  padding-top: 90px; }\n\n:host .panel {\n  border-radius: 0;\n  background-color: #fbfbfb; }\n\n:host .panel .panel-heading {\n    border-radius: 0; }\n\n:host .reference-header-wrapper {\n  padding: 0;\n  border-radius: 3px; }\n\n:host .reference-header-wrapper .panel-body {\n    display: inline-block;\n    float: left; }\n\n:host .reference-header-wrapper .panel-body .list-group-item {\n      padding: 0;\n      padding-bottom: 10px;\n      margin-bottom: 10px; }\n\n:host .reference-header-wrapper .panel-body .list-group-item:last-of-type {\n        margin-bottom: 0; }\n\n:host .reference-header-wrapper .panel-body .list-group-item div {\n        padding: 10px 10px 0 10px; }\n\n:host .reference-header-wrapper .panel-body .list-group-item .sub-panel {\n        padding: 10px;\n        color: #333;\n        background-color: #f5f5f5; }\n\n:host .reference-header-wrapper .panel-body .list-group-item .caption {\n        font-weight: bold;\n        font-size: 17px; }\n\n:host .reference-wrapper {\n  display: flex;\n  justify-content: space-between; }\n\n:host .reference-wrapper .col-md-4, :host .reference-wrapper .col-lg-4 {\n    cursor: pointer;\n    padding-right: 0;\n    padding-left: 0;\n    margin-right: 15px;\n    margin-left: 15px;\n    height: 120px; }\n\n:host .reference-wrapper .col-md-4:first-of-type, :host .reference-wrapper .col-lg-4:first-of-type {\n      margin-left: 0; }\n\n:host .reference-wrapper .col-md-4:last-of-type, :host .reference-wrapper .col-lg-4:last-of-type {\n      margin-right: 0; }\n"

/***/ }),

/***/ "./src/app/administration/administration.component.ts":
/*!************************************************************!*\
  !*** ./src/app/administration/administration.component.ts ***!
  \************************************************************/
/*! exports provided: AdministrationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdministrationComponent", function() { return AdministrationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_requests_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/requests.service */ "./src/app/services/requests.service.ts");
/* harmony import */ var _services_local_storage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/local-storage.service */ "./src/app/services/local-storage.service.ts");
/* harmony import */ var _services_pass_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/pass.service */ "./src/app/services/pass.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AdministrationComponent = /** @class */ (function () {
    function AdministrationComponent(router, aRouter, requests, storageService, passService) {
        var _this = this;
        this.router = router;
        this.aRouter = aRouter;
        this.requests = requests;
        this.storageService = storageService;
        this.passService = passService;
        this.objectKeys = Object.keys;
        this.userSubscription = storageService.getUserObservable().subscribe(function (user) {
            _this.user = user;
            if (!_this.user) {
                _this.router.navigate(['login']);
            }
        });
    }
    AdministrationComponent.prototype.isAdmin = function () {
        return this.passService.isAdmin();
    };
    AdministrationComponent.prototype.isPriviledged = function () {
        return this.passService.isPriviledged();
    };
    AdministrationComponent.prototype.goToReferenceEdit = function (type) {
        this.router.navigate(['reference-edit', { type: type }]);
    };
    AdministrationComponent.prototype.ngOnInit = function () {
        this.user = this.storageService.getUser();
        if (!this.isPriviledged()) {
            alert('You are not allowed to see this page');
            this.router.navigate(['main']);
        }
    };
    AdministrationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-administration',
            template: __webpack_require__(/*! ./administration.component.html */ "./src/app/administration/administration.component.html"),
            styles: [__webpack_require__(/*! ./administration.component.scss */ "./src/app/administration/administration.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _services_requests_service__WEBPACK_IMPORTED_MODULE_2__["RequestsService"],
            _services_local_storage_service__WEBPACK_IMPORTED_MODULE_3__["LocalStorageService"],
            _services_pass_service__WEBPACK_IMPORTED_MODULE_4__["PassService"]])
    ], AdministrationComponent);
    return AdministrationComponent;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n<app-header></app-header>\n<router-outlet></router-outlet>\n\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var angular_font_awesome__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angular-font-awesome */ "./node_modules/angular-font-awesome/dist/angular-font-awesome.es5.js");
/* harmony import */ var primeng_primeng__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! primeng/primeng */ "./node_modules/primeng/primeng.js");
/* harmony import */ var primeng_primeng__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(primeng_primeng__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _projects_list_projects_list_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./projects-list/projects-list.component */ "./src/app/projects-list/projects-list.component.ts");
/* harmony import */ var _services_requests_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./services/requests.service */ "./src/app/services/requests.service.ts");
/* harmony import */ var _services_local_storage_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./services/local-storage.service */ "./src/app/services/local-storage.service.ts");
/* harmony import */ var _services_auth_guard_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./services/auth-guard.service */ "./src/app/services/auth-guard.service.ts");
/* harmony import */ var _services_filters_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./services/filters.service */ "./src/app/services/filters.service.ts");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./header/header.component */ "./src/app/header/header.component.ts");
/* harmony import */ var _new_asset_new_asset_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./new-asset/new-asset.component */ "./src/app/new-asset/new-asset.component.ts");
/* harmony import */ var angular_date_value_accessor__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! angular-date-value-accessor */ "./node_modules/angular-date-value-accessor/index.js");
/* harmony import */ var angular_date_value_accessor__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(angular_date_value_accessor__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var _new_transfer_new_transfer_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./new-transfer/new-transfer.component */ "./src/app/new-transfer/new-transfer.component.ts");
/* harmony import */ var _project_body_project_body_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./project-body/project-body.component */ "./src/app/project-body/project-body.component.ts");
/* harmony import */ var _asset_info_asset_info_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./asset-info/asset-info.component */ "./src/app/asset-info/asset-info.component.ts");
/* harmony import */ var _not_found_not_found_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./not-found/not-found.component */ "./src/app/not-found/not-found.component.ts");
/* harmony import */ var ngx_chips__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ngx-chips */ "./node_modules/ngx-chips/esm5/ngx-chips.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _transfers_list_transfers_list_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./transfers-list/transfers-list.component */ "./src/app/transfers-list/transfers-list.component.ts");
/* harmony import */ var _transfer_info_transfer_info_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./transfer-info/transfer-info.component */ "./src/app/transfer-info/transfer-info.component.ts");
/* harmony import */ var _administration_administration_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./administration/administration.component */ "./src/app/administration/administration.component.ts");
/* harmony import */ var _actions_center_actions_center_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./actions-center/actions-center.component */ "./src/app/actions-center/actions-center.component.ts");
/* harmony import */ var _smite_modals__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./smite-modals */ "./src/app/smite-modals/index.ts");
/* harmony import */ var _reference_edit_reference_edit_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./reference-edit/reference-edit.component */ "./src/app/reference-edit/reference-edit.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

























 // this is needed!



var appRoutes = [
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    { path: 'login', component: _login_login_component__WEBPACK_IMPORTED_MODULE_8__["LoginComponent"] },
    { path: 'main', component: _project_body_project_body_component__WEBPACK_IMPORTED_MODULE_18__["ProjectBodyComponent"], canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_12__["AuthGuardService"]] },
    { path: 'actions', component: _actions_center_actions_center_component__WEBPACK_IMPORTED_MODULE_26__["ActionsCenterComponent"], canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_12__["AuthGuardService"]] },
    { path: 'transferts', component: _transfers_list_transfers_list_component__WEBPACK_IMPORTED_MODULE_23__["TransfersListComponent"], canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_12__["AuthGuardService"]] },
    { path: 'new_transfer', component: _new_transfer_new_transfer_component__WEBPACK_IMPORTED_MODULE_17__["NewTransferComponent"], canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_12__["AuthGuardService"]] },
    { path: 'transfer_info/:id', component: _transfer_info_transfer_info_component__WEBPACK_IMPORTED_MODULE_24__["TransferInfoComponent"], canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_12__["AuthGuardService"]] },
    { path: 'edit_transfer/:id', component: _new_transfer_new_transfer_component__WEBPACK_IMPORTED_MODULE_17__["NewTransferComponent"], canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_12__["AuthGuardService"]] },
    { path: 'projects', component: _projects_list_projects_list_component__WEBPACK_IMPORTED_MODULE_9__["ProjectsListComponent"], canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_12__["AuthGuardService"]] },
    { path: 'new_asset', component: _new_asset_new_asset_component__WEBPACK_IMPORTED_MODULE_15__["NewAssetComponent"], canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_12__["AuthGuardService"]] },
    { path: 'asset_info', component: _asset_info_asset_info_component__WEBPACK_IMPORTED_MODULE_19__["AssetInfoComponent"], canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_12__["AuthGuardService"]] },
    { path: 'asset_info/:id', component: _asset_info_asset_info_component__WEBPACK_IMPORTED_MODULE_19__["AssetInfoComponent"], canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_12__["AuthGuardService"]] },
    { path: 'edit_asset/:id', component: _new_asset_new_asset_component__WEBPACK_IMPORTED_MODULE_15__["NewAssetComponent"], canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_12__["AuthGuardService"]] },
    { path: 'administration', component: _administration_administration_component__WEBPACK_IMPORTED_MODULE_25__["AdministrationComponent"], canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_12__["AuthGuardService"]] },
    { path: 'reference-edit', component: _reference_edit_reference_edit_component__WEBPACK_IMPORTED_MODULE_28__["ReferenceEditComponent"], canActivate: [_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_12__["AuthGuardService"]] },
    { path: '404', component: _not_found_not_found_component__WEBPACK_IMPORTED_MODULE_20__["NotFoundComponent"] },
    { path: '**', redirectTo: '/404' }
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_8__["LoginComponent"],
                _projects_list_projects_list_component__WEBPACK_IMPORTED_MODULE_9__["ProjectsListComponent"],
                _header_header_component__WEBPACK_IMPORTED_MODULE_14__["HeaderComponent"],
                _new_asset_new_asset_component__WEBPACK_IMPORTED_MODULE_15__["NewAssetComponent"],
                _new_transfer_new_transfer_component__WEBPACK_IMPORTED_MODULE_17__["NewTransferComponent"],
                _project_body_project_body_component__WEBPACK_IMPORTED_MODULE_18__["ProjectBodyComponent"],
                _asset_info_asset_info_component__WEBPACK_IMPORTED_MODULE_19__["AssetInfoComponent"],
                _not_found_not_found_component__WEBPACK_IMPORTED_MODULE_20__["NotFoundComponent"],
                _transfers_list_transfers_list_component__WEBPACK_IMPORTED_MODULE_23__["TransfersListComponent"],
                _transfer_info_transfer_info_component__WEBPACK_IMPORTED_MODULE_24__["TransferInfoComponent"],
                _administration_administration_component__WEBPACK_IMPORTED_MODULE_25__["AdministrationComponent"],
                _actions_center_actions_center_component__WEBPACK_IMPORTED_MODULE_26__["ActionsCenterComponent"],
                _reference_edit_reference_edit_component__WEBPACK_IMPORTED_MODULE_28__["ReferenceEditComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forRoot(appRoutes),
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                angular_font_awesome__WEBPACK_IMPORTED_MODULE_5__["AngularFontAwesomeModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClientModule"],
                primeng_primeng__WEBPACK_IMPORTED_MODULE_6__["TreeTableModule"],
                primeng_primeng__WEBPACK_IMPORTED_MODULE_6__["DataTableModule"],
                primeng_primeng__WEBPACK_IMPORTED_MODULE_6__["TreeModule"],
                primeng_primeng__WEBPACK_IMPORTED_MODULE_6__["SharedModule"],
                angular_date_value_accessor__WEBPACK_IMPORTED_MODULE_16__["DateValueAccessorModule"],
                ngx_chips__WEBPACK_IMPORTED_MODULE_21__["TagInputModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_22__["BrowserAnimationsModule"],
                _smite_modals__WEBPACK_IMPORTED_MODULE_27__["SmiteModalsModule"]
            ],
            providers: [
                _services_requests_service__WEBPACK_IMPORTED_MODULE_10__["RequestsService"],
                _services_local_storage_service__WEBPACK_IMPORTED_MODULE_11__["LocalStorageService"],
                _services_auth_guard_service__WEBPACK_IMPORTED_MODULE_12__["AuthGuardService"],
                _services_filters_service__WEBPACK_IMPORTED_MODULE_13__["FiltersService"],
                _services_local_storage_service__WEBPACK_IMPORTED_MODULE_11__["LocalStorageService"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/asset-info/asset-info.component.html":
/*!******************************************************!*\
  !*** ./src/app/asset-info/asset-info.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n  <div class=\"col-md-2 col-lg-2\"></div>\n  <div class=\"col-md-8 col-lg-8\">\n    <div class=\"return-btn-wrapper\">\n      <a routerLink=\"/main\" routerLinkActive=\"active\">\n        <fa name=\"angle-left\"></fa>\n        <span>Retour à la liste des actifs valorisables</span>\n      </a>\n    </div>\n  </div>\n  <div class=\"col-md-2 col-lg-2\"></div>\n</div>\n\n<div class=\"row\">\n  <div class=\"col-md-2 col-lg-2\"></div>\n  <div class=\"col-md-8 col-lg-8 main-body-wrapper\">\n    <div class=\"asset-wrapper\">\n      <div class=\"panel panel-primary\">\n        <div class=\"panel-heading\">\n          <span class=\"panel-title\">Actif valorisable \"{{currAsset?.identification?.name}}\"</span>\n        </div>\n      </div>\n      <div class=\"asset-main-body-wrapper\">\n        <div class=\"asset-info-wrapper\">\n          <div class=\"list-group\">\n            <div class=\"actif\">\n              <div class=\"list-group-item\">\n                    <span>ID:\n                      <span class=\"asset-description\">{{currAsset?.id || '-'}}</span>\n                    </span>\n              </div>\n              <div class=\"list-group-item\">\n                    <span>Date de création:\n                      <span class=\"asset-description\">{{getDate(currAsset) | date}}</span>\n                    </span>\n              </div>\n              <div class=\"list-group-item\">\n                    <span>Créé par:\n                      <span class=\"asset-description\" *ngIf=\"creator\">{{creator?.firstName}} {{creator?.lastName}}</span>\n                      <span class=\"asset-description\" *ngIf=\"!creator\">-</span>\n                    </span>\n              </div>\n              <div class=\"list-group-item\">\n                    <span>Descriptif:\n                      <span class=\"asset-description\">{{currAsset?.desc || '-'}}</span>\n                    </span>\n              </div>\n              <div class=\"list-group-item\" *ngIf=\"currAsset?.assetStatus !== 'VALIDATED'\">\n                    <span>Statut:\n                      <span class=\"asset-description\">{{getAssetStatus(currAsset)}}</span>\n                    </span>\n              </div>\n              <div class=\"list-group-item\">\n                    <span>Classification:\n                      <span class=\"asset-description\">{{getAssetClassification(currAsset)}}</span>\n                    </span>\n              </div>\n              <div class=\"list-group-item\">\n                    <span>Type:\n                      <span class=\"asset-description\">{{getAssetType(currAsset)}}</span>\n                    </span>\n              </div>\n              <!--<div *ngIf=\"isAdmin() || isAssetAuthor(currAsset) || (isUserHasAssetRights(currAsset) && currAsset.assetStatus === 'VALIDATED')\" class=\"list-group-item\">-->\n              <div class=\"list-group-item\">\n                    <span>Livrable Projet:\n                      <span class=\"asset-description\">{{currAsset?.deliverable?.exists ? currAsset.deliverable.reference : 'Non'}}</span>\n                    </span>\n              </div>\n              <!--<div *ngIf=\"isAdmin() || isAssetAuthor(currAsset) || (isUserHasAssetRights(currAsset) && currAsset.assetStatus === 'VALIDATED')\" class=\"list-group-item\">-->\n              <div class=\"list-group-item\">\n                    <span>PI Partagée:\n                      <span class=\"asset-description\">{{currAsset?.piShared?.exists ? currAsset.piShared.partners : 'Non'}}</span>\n                    </span>\n              </div>\n              <!--<div *ngIf=\"isAdmin() || isAssetAuthor(currAsset) || (isUserHasAssetRights(currAsset) && currAsset.assetStatus === 'VALIDATED')\" class=\"list-group-item\">-->\n              <div class=\"list-group-item\">\n                    <span>Efforts:\n                      <ng-container *ngIf=\"!this.effortList.length\">\n                        <span class=\"asset-description\">Non</span>\n                      </ng-container>\n                      <ng-container *ngIf=\"this.effortList.length\">\n                        <div *ngFor=\"let effort of effortList\">\n                        <span class=\"asset-description\">\n                          {{effortView(effort)}}\n                        </span>\n                      </div>\n                      </ng-container>\n                    </span>\n              </div>\n              <!--<div *ngIf=\"isAdmin() || isAssetAuthor(currAsset) || (isUserHasAssetRights(currAsset) && currAsset.assetStatus === 'VALIDATED')\" class=\"list-group-item\">-->\n              <div class=\"list-group-item\">\n                    <span>Effort contributeurs: <span class=\"asset-description\">{{currAsset?.efforts?.contributors || '-'}}</span>\n                  </span>\n              </div>\n              <!--<div *ngIf=\"isAdmin() || isAssetAuthor(currAsset) || (isUserHasAssetRights(currAsset) && currAsset.assetStatus === 'VALIDATED')\" class=\"list-group-item\">-->\n              <div class=\"list-group-item\">\n                  <span>Dépôt APP:\n                    <span class=\"asset-description\">{{currAsset?.copyright?.exists ? currAsset.copyright.reference : 'Non'}}</span>\n                  </span>\n              </div>\n              <!--<div *ngIf=\"isAdmin() || isAssetAuthor(currAsset) || (isUserHasAssetRights(currAsset) && currAsset.assetStatus === 'VALIDATED')\" class=\"list-group-item\">-->\n              <div class=\"list-group-item\">\n                  <span>Brevet:\n                    <span class=\"asset-description\">{{currAsset?.patent?.exists ? currAsset.patent.reference : 'Non'}}</span>\n                  </span>\n              </div>\n              <!--<div *ngIf=\"isAdmin() || isAssetAuthor(currAsset) || (isUserHasAssetRights(currAsset) && currAsset.assetStatus === 'VALIDATED')\" class=\"list-group-item\">-->\n              <div class=\"list-group-item\">\n                  <span>Demonstration:\n                    <span class=\"asset-description\">{{currAsset?.demonstration || '-'}}</span>\n                  </span>\n              </div>\n              <!--<div *ngIf=\"isAdmin() || isAssetAuthor(currAsset) || (isUserHasAssetRights(currAsset) && currAsset.assetStatus === 'VALIDATED')\" class=\"list-group-item\">-->\n              <div class=\"list-group-item\">\n                  <span>Actif démonstrateur:\n                    <span class=\"asset-description\">{{currAsset?.isPlatformAsset ? 'Oui' : 'Non'}}</span>\n                  </span>\n              </div>\n              <!--<div *ngIf=\"isAdmin() || isAssetAuthor(currAsset) || (isUserHasAssetRights(currAsset) && currAsset.assetStatus === 'VALIDATED')\" class=\"list-group-item\">-->\n              <div class=\"list-group-item\">\n                  <span>Actif parent:\n                    <span class=\"asset-description\">{{currAsset?.parentAssetId ? '' : 'Non'}}</span>\n                  </span>\n                <a *ngIf=\"currAsset?.parentAssetId\" routerLink=\"/asset_info/{{currAsset.parentAssetId}}\" routerLinkActive=\"active\">{{currAsset?.parentAssetId}}</a>\n              </div>\n            </div>\n\n            <!--<div class=\"caracreristaion\" *ngIf=\"isAdmin() || isAssetAuthor(currAsset) || (isUserHasAssetRights(currAsset) && currAsset.assetStatus === 'VALIDATED')\">-->\n            <div class=\"caracreristaion\">\n              <div class=\"type-caption list-group-item\"></div>\n              <div class=\"list-group-item\">\n                  <span>Ftopia / dépôt GIT:\n                    <span class=\"asset-description\">{{currAsset?.repository || '-'}}</span>\n                  </span>\n              </div>\n              <div class=\"list-group-item\">\n                  <span>Label/version:\n                    <span class=\"asset-description\">{{currAsset?.label || '-'}}</span>\n                  </span>\n              </div>\n              <div class=\"list-group-item\">\n                  <span>Description détaillée:\n                    <span class=\"asset-description\">{{currAsset?.detailedDescription || '-'}}</span>\n                  </span>\n              </div>\n              <div class=\"list-group-item\">\n                  <span>Licences dépendantes:\n                    <span class=\"asset-description\">{{currAsset?.dependencies?.licenses || '-'}}</span>\n                  </span>\n              </div>\n              <div class=\"list-group-item\">\n                  <span>Matériel dépendant:\n                    <span class=\"asset-description\">{{currAsset?.dependencies?.hardware || '-'}}</span>\n                  </span>\n              </div>\n              <div class=\"list-group-item\">\n                  <span>Réutilisables:\n                    <span class=\"asset-description\">{{currAsset?.dependencies?.reusable ? 'Oui' : 'Non'}}</span>\n                  </span>\n              </div>\n              <div class=\"list-group-item\">\n                  <span>Acteur/porteur:\n                    <span class=\"asset-description\">{{currAsset?.dependencies?.actor || '-'}}</span>\n                  </span>\n              </div>\n              <div class=\"list-group-item\">\n                  <span>Plateformes liées:\n                    <span class=\"asset-description\">{{currAsset?.linkedPlatforms || '-'}}</span>\n                  </span>\n              </div>\n              <div class=\"list-group-item\">\n                  <span>Mots-clés:\n                    <span class=\"asset-description\">{{currAsset?.keywords || '-'}}</span>\n                  </span>\n              </div>\n\n              <div *ngIf=\"currAsset.assetStatus === 'VALIDATED'\">\n                <div class=\"list-group-item\">\n                    <span>Classification type:\n                      <span class=\"asset-description\">{{currAsset?.classification?.type || '-'}}</span>\n                    </span>\n                </div>\n                <div class=\"list-group-item\">\n                    <span>Classification perspective:\n                      <span class=\"asset-description\">{{currAsset?.classification?.perspective || '-'}}</span>\n                    </span>\n                </div>\n              </div>\n            </div>\n\n          </div>\n\n        </div>\n        <div class=\"controls-wrapper\">\n          <div class=\"icons-wrapper\" *ngIf=\"!transferAsset\">\n            <fa class=\"user-icon\" name=\"pencil\" (click)=\"toEditAsset(currAsset)\" [ngClass]=\"{'disabled': !isAdmin() && user.id != currAsset?.identification?.userId || currAsset?.assetStatus === 'VALIDATED'}\"></fa>\n            <fa class=\"user-icon\" name=\"trash\" (click)=\"deleteAsset(currAsset)\" [ngClass]=\"{'disabled': !isAdmin() && !isAssetAuthor(currAsset)}\"></fa>\n          </div>\n\n          <div class=\"asset\" [ngClass]=\"{'validated': currAsset?.assetStatus === 'VALIDATED', 'for-validation': currAsset?.assetStatus === 'WAITING_VALIDATION', 'pending': currAsset?.assetStatus === 'PENDING'}\">\n            <p *ngIf=\"currAsset?.assetStatus === 'VALIDATED'\">Actif validé par le CODIR</p>\n            <p *ngIf=\"currAsset?.assetStatus === 'WAITING_VALIDATION'\">En attente de validation</p>\n            <p *ngIf=\"currAsset?.assetStatus === 'PENDING'\">En cours de rédaction</p>\n          </div>\n\n          <div *ngIf=\"assetActions.length > 0\" class=\"actions\">\n            <fa class=\"user-icon\" name=\"warning\" style=\"margin-right: 15px\"></fa>\n            <p>Actions à mener</p>\n          </div>\n\n\n          <div>\n            <span>{{project['shortName'] || ''}}</span>\n          </div>\n          <div>\n            <span>{{program['shortName'] || ''}}</span>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"col-md-2 col-lg-2\"></div>\n</div>\n\n<div *ngIf=\"currAsset?.transfersNumber > 0\" id=\"related-transfers\" class=\"row\">\n  <div class=\"col-md-2 col-lg-2\"></div>\n  <div class=\"col-md-8 col-lg-8 transfer-list-wrapper\">\n    <div class=\"table-wrapper\">\n      <div class=\"table-header\">Liste des transferts de l'actif</div>\n\n      <!-- ASSET TRANSFERS !! -->\n      <p-treeTable [value]=\"currAsset?.transfers\">\n        <p-column>\n          <ng-template let-col let-node=\"rowData\">\n            <div class=\"td-wrapper\" [ngStyle]=\"{'top': '-10px'}\">\n              <p>{{node.destination.name}}</p>\n              <p>{{node.id}}</p>\n            </div>\n          </ng-template>\n        </p-column>\n\n        <p-column [style]=\"{'width':'50%'}\">\n          <ng-template let-col let-node=\"rowData\">\n            <div class=\"td-wrapper\">\n              <p>{{node.destination.type}}</p>\n            </div>\n          </ng-template>\n        </p-column>\n\n        <p-column>\n          <ng-template let-col let-node=\"rowData\">\n            <div class=\"td-wrapper\">\n              <p>{{getTransfertStatus(node)}}</p>\n            </div>\n          </ng-template>\n        </p-column>\n\n        <p-column [style]=\"{'width':'15%'}\">\n          <ng-template let-col let-node=\"rowData\">\n            <div class=\"icons-wrapper\">\n              <fa class=\"user-icon\" name=\"eye\" (click)=\"viewTransferInfo(node)\"></fa>\n              <fa class=\"user-icon\" name=\"pencil\" (click)=\"toEditTransfer(node)\"></fa>\n            </div>\n          </ng-template>\n        </p-column>\n      </p-treeTable>\n    </div>\n  </div>\n  <div class=\"col-md-2 col-lg-2\"></div>\n</div>\n  \n  \n"

/***/ }),

/***/ "./src/app/asset-info/asset-info.component.scss":
/*!******************************************************!*\
  !*** ./src/app/asset-info/asset-info.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".row {\n  width: 100%; }\n\n.return-btn-wrapper {\n  height: 120px;\n  padding-top: 52px; }\n\n.return-btn-wrapper fa {\n    font-size: 16px;\n    margin-right: 0.5vw; }\n\n.asset-description {\n  font-weight: 100 !important; }\n\n.main-body-wrapper {\n  border: 1px solid #ddd;\n  margin-bottom: 3vw; }\n\n.col-md-8, .clo-lg-8 {\n  padding: 0; }\n\n.type-caption {\n  color: #fff;\n  height: 8px;\n  padding: 0;\n  border: none;\n  border-right: 1px solid #ddd; }\n\n.asset-wrapper {\n  border-radius: 3px; }\n\n.asset-wrapper .panel-primary {\n    border-radius: 0;\n    border-top-left-radius: 3px;\n    border-top-right-radius: 3px; }\n\n.asset-wrapper .list-group-item {\n    border-radius: 0;\n    border: none; }\n\n.asset-wrapper .list-group {\n    margin: 0; }\n\n.asset-wrapper .asset-main-body-wrapper {\n    width: 100%;\n    display: flex; }\n\n.asset-wrapper .asset-main-body-wrapper .statut-de-la-fiche-wrapper {\n      padding: 13px;\n      width: 100%;\n      height: 155px;\n      border: none;\n      border-radius: 0;\n      margin-bottom: 0; }\n\n.asset-wrapper .asset-main-body-wrapper .statut-de-la-fiche-wrapper.disabled {\n        background-color: #ebebe4;\n        color: #a9a9a9; }\n\n.asset-wrapper .asset-main-body-wrapper .statut-de-la-fiche-wrapper.disabled * {\n          cursor: default; }\n\n.asset-wrapper .asset-main-body-wrapper .statut-de-la-fiche-wrapper label:hover {\n        cursor: pointer; }\n\n.asset-wrapper .asset-main-body-wrapper .statut-de-la-fiche-wrapper div {\n        display: block;\n        margin: 24px 0;\n        text-align: center; }\n\n.asset-wrapper .asset-main-body-wrapper .statut-de-la-fiche-wrapper .btn-wrapper {\n        padding: 0;\n        margin: 0;\n        height: 40px; }\n\n.asset-wrapper .asset-main-body-wrapper .statut-de-la-fiche-wrapper .btn-wrapper button {\n          cursor: pointer;\n          margin: 0 auto;\n          display: block; }\n\n.asset-wrapper .asset-main-body-wrapper .statut-de-la-fiche-wrapper span {\n        text-transform: uppercase;\n        font-weight: 600; }\n\n.asset-wrapper .asset-main-body-wrapper .asset-info-wrapper {\n      width: 80%;\n      display: inline-block; }\n\n.asset-wrapper .asset-main-body-wrapper .asset-info-wrapper span {\n        font-size: 15px; }\n\n.asset-wrapper .asset-main-body-wrapper .asset-info-wrapper div span {\n        font-size: 15px;\n        font-weight: 600; }\n\n.asset-wrapper .asset-main-body-wrapper .controls-wrapper {\n      width: 18%;\n      display: inline-block;\n      margin-left: 2%; }\n\n.asset-wrapper .asset-main-body-wrapper .controls-wrapper .danger {\n        color: #a94442;\n        background-color: #f2dede;\n        border-color: #ebccd1; }\n\n.asset-wrapper .asset-main-body-wrapper .controls-wrapper .danger span {\n          position: relative;\n          bottom: 7px; }\n\n.asset-wrapper .asset-main-body-wrapper .controls-wrapper div {\n        width: 90%;\n        height: 30px;\n        min-width: 150px;\n        text-align: center;\n        margin-bottom: 15px;\n        border-radius: 3px;\n        background-color: #d9edf7;\n        border-color: #bce8f1;\n        color: #000;\n        display: flex;\n        justify-content: center;\n        align-items: center; }\n\n.asset-wrapper .asset-main-body-wrapper .controls-wrapper div span {\n          font-size: 15px; }\n\n.asset-wrapper .asset-main-body-wrapper .controls-wrapper .asset, .asset-wrapper .asset-main-body-wrapper .controls-wrapper .actions {\n        height: 30px;\n        display: flex;\n        flex-flow: row;\n        justify-content: center;\n        align-items: center;\n        color: whitesmoke;\n        background-color: #a94442; }\n\n.asset-wrapper .asset-main-body-wrapper .controls-wrapper .asset p, .asset-wrapper .asset-main-body-wrapper .controls-wrapper .actions p {\n          margin: 0; }\n\n.asset-wrapper .asset-main-body-wrapper .controls-wrapper .asset.validated, .asset-wrapper .asset-main-body-wrapper .controls-wrapper .actions.validated {\n          background-color: #19b319;\n          font-weight: bolder; }\n\n.asset-wrapper .asset-main-body-wrapper .controls-wrapper .asset.for-validation, .asset-wrapper .asset-main-body-wrapper .controls-wrapper .actions.for-validation {\n          background-color: #cdae2a; }\n\n.asset-wrapper .asset-main-body-wrapper .controls-wrapper .asset.pending, .asset-wrapper .asset-main-body-wrapper .controls-wrapper .actions.pending {\n          background-color: #c2bc93; }\n\n.asset-wrapper .asset-main-body-wrapper .controls-wrapper .icons-wrapper {\n        border: none;\n        display: flex;\n        justify-content: center;\n        align-items: center; }\n\n.asset-wrapper .asset-main-body-wrapper .controls-wrapper .icons-wrapper fa {\n          cursor: pointer;\n          font-size: 1.1vw;\n          width: 33.3%;\n          display: inline-block; }\n\n.asset-wrapper .asset-main-body-wrapper .controls-wrapper .icons-wrapper .user-icon:hover {\n          cursor: pointer; }\n\n.asset-wrapper .asset-main-body-wrapper .controls-wrapper .icons-wrapper .user-icon.disabled {\n          opacity: 0.75;\n          color: rgba(0, 0, 0, 0.75);\n          -webkit-filter: blur(0.5px);\n          -moz-filter: blur(0.5px);\n          filter: blur(0.5px); }\n\n.asset-wrapper .asset-main-body-wrapper .controls-wrapper .icons-wrapper .user-icon.disabled:hover {\n            cursor: default; }\n\n.transfer-list-wrapper {\n  margin: 30px auto;\n  position: relative; }\n\n.transfer-list-wrapper .results-number {\n    position: absolute;\n    right: 20px;\n    top: 10px;\n    font-size: 1.2em;\n    font-weight: bolder;\n    color: white; }\n\n.transfer-list-wrapper .table-header {\n    padding: 10px;\n    padding-right: 0;\n    border-top-left-radius: 3px;\n    border-top-right-radius: 3px;\n    font-size: 15px;\n    text-align: left;\n    background-color: #337ab7;\n    border-color: #337ab7;\n    color: #fff;\n    font-weight: 600;\n    width: 100%;\n    height: 42px; }\n\n.transfer-list-wrapper ::ng-deep.ui-treetable .ui-treetable-row {\n    width: 100%;\n    height: 90px; }\n\n.transfer-list-wrapper ::ng-deep.ui-treetable .ui-treetable-row .ui-treetable-child-table-container {\n      background-color: #F5F5F5; }\n\n.transfer-list-wrapper ::ng-deep.ui-treetable table {\n    table-layout: fixed !important; }\n\n.transfer-list-wrapper ::ng-deep.ui-treetable ::ng-deep td {\n    padding: 0 !important;\n    font-size: 15px;\n    border-right: 1px solid #ddd; }\n\n.transfer-list-wrapper ::ng-deep.ui-treetable ::ng-deep td p {\n      text-align: center; }\n\n.transfer-list-wrapper ::ng-deep.ui-treetable thead {\n    display: none; }\n\n.transfer-list-wrapper ::ng-deep.ui-treetable table {\n    table-layout: auto; }\n\n.transfer-list-wrapper ::ng-deep.ui-treetable ::ng-deep td {\n    font-size: 15px; }\n\n.transfer-list-wrapper ::ng-deep.ui-treetable .second-td {\n    padding-left: 12px; }\n\n.transfer-list-wrapper ::ng-deep.ui-treetable .second-td p {\n      text-align: left; }\n\n.transfer-list-wrapper ::ng-deep.ui-treetable .td-wrapper {\n    position: relative;\n    top: 35px; }\n\n.transfer-list-wrapper ::ng-deep.ui-treetable .icons-wrapper {\n    position: relative;\n    top: 40px;\n    display: flex;\n    justify-content: center; }\n\n.transfer-list-wrapper ::ng-deep.ui-treetable .icons-wrapper fa {\n      width: 33.3%;\n      display: block;\n      float: left;\n      text-align: center; }\n\n.transfer-list-wrapper ::ng-deep.ui-treetable .icons-wrapper .user-icon:hover {\n      cursor: pointer; }\n\n.transfer-list-wrapper ::ng-deep.ui-treetable .icons-wrapper .user-icon.disabled {\n      opacity: 0.75;\n      /* Opacity (Transparency) */\n      color: rgba(0, 0, 0, 0.75);\n      /* RGBA Color (Alternative Transparency) */\n      -webkit-filter: blur(0.5px);\n      /* Blur */\n      -moz-filter: blur(0.5px);\n      filter: blur(0.5px); }\n\n.transfer-list-wrapper ::ng-deep.ui-treetable .icons-wrapper .user-icon.disabled:hover {\n        cursor: default; }\n\n.transfer-list-wrapper .panel-heading {\n    padding: 15px;\n    margin: 0; }\n"

/***/ }),

/***/ "./src/app/asset-info/asset-info.component.ts":
/*!****************************************************!*\
  !*** ./src/app/asset-info/asset-info.component.ts ***!
  \****************************************************/
/*! exports provided: AssetInfoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AssetInfoComponent", function() { return AssetInfoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_requests_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/requests.service */ "./src/app/services/requests.service.ts");
/* harmony import */ var _services_local_storage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/local-storage.service */ "./src/app/services/local-storage.service.ts");
/* harmony import */ var _services_pass_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/pass.service */ "./src/app/services/pass.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AssetInfoComponent = /** @class */ (function () {
    function AssetInfoComponent(router, aRouter, requests, storageService, passService) {
        var _this = this;
        this.router = router;
        this.aRouter = aRouter;
        this.requests = requests;
        this.storageService = storageService;
        this.passService = passService;
        this.currAsset = {};
        this.effortList = [];
        this.user = {};
        this.creator = {};
        this.typeTouched = true;
        this.assetType = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('PENDING', []);
        this.assetActions = [];
        this.transferAsset = false;
        this.program = {};
        this.project = {};
        this.userSubscription = storageService.getUserObservable().subscribe(function (user) {
            _this.user = user;
            if (!_this.user) {
                _this.router.navigate(['login']);
            }
        });
    }
    AssetInfoComponent.prototype.isAdmin = function () {
        return this.passService.isAdmin();
    };
    AssetInfoComponent.prototype.isAssetAuthor = function (asset) {
        return this.passService.isAssetAuthor(asset);
    };
    AssetInfoComponent.prototype.isAssetStatusEditable = function (asset) {
        return this.passService.isAssetStatusEditable(asset);
    };
    AssetInfoComponent.prototype.isUserHasAssetRights = function (asset) {
        return this.passService.isUserHasAssetRights(asset);
    };
    AssetInfoComponent.prototype.getDate = function (asset) {
        if (Object.keys(asset).length > 0) {
            return asset.identification.creationDate || '';
        }
        else {
            return '';
        }
    };
    AssetInfoComponent.prototype.getAssetStatus = function (asset) {
        if (!asset || Object.keys(asset).length === 0) {
            return '';
        }
        else if (asset.assetStatus === '') {
            return 'Non';
        }
        else {
            return asset.assetStatus;
        }
    };
    AssetInfoComponent.prototype.getAssetClassification = function (asset) {
        if (!asset || Object.keys(asset).length === 0) {
            return '';
        }
        else if (asset.classification.type === '') {
            return 'Non';
        }
        else {
            return asset.classification.type;
        }
    };
    AssetInfoComponent.prototype.getAssetType = function (asset) {
        if (!asset || Object.keys(asset).length === 0) {
            return '';
        }
        else if (asset.type === '') {
            return 'Non';
        }
        else {
            return asset.type;
        }
    };
    AssetInfoComponent.prototype.deleteAsset = function (asset) {
        var _this = this;
        if (!this.isAdmin() && !this.isAssetAuthor(asset)) {
            return;
        }
        var deleteAsset = confirm("Delete asset \"" + asset.identification.name + "\"");
        if (!deleteAsset) {
            return;
        }
        this.requests.deleteAsset(asset.id).then(function (res) {
            console.log(res);
            _this.router.navigate(['/main']);
        }).catch(function (err) {
            console.error(err);
        });
    };
    AssetInfoComponent.prototype.onValiderBtnClick = function () {
        var _this = this;
        this.currAsset.status = 'VALIDATED';
        this.requests.editAsset(this.currAsset).then(function (res) {
            console.log(res);
            _this.ngOnInit();
        }).catch(function (err) {
            alert(err.message);
        });
    };
    AssetInfoComponent.prototype.editAssetStatus = function (status) {
        var _this = this;
        if (!this.isAdmin() && !this.isAssetAuthor(this.currAsset)) {
            console.log('first check');
            return;
        }
        if (status === 'WAITING_VALIDATION' && !this.isAssetStatusEditable(this.currAsset)) {
            console.log('second check');
            return;
        }
        this.currAsset.status = status;
        this.requests.editAsset(this.currAsset).then(function (res) {
            console.log(res);
            _this.currAsset = res;
        }).catch(function (err) {
            alert(err.message);
        });
    };
    AssetInfoComponent.prototype.toEditAsset = function (nodeData) {
        if (!this.isAdmin() && this.user.id != this.currAsset.identification.userId || nodeData.status === 'VALIDATED') {
            return;
        }
        this.storageService.setAsset(nodeData);
        this.router.navigate(["edit_asset/" + nodeData.id]);
    };
    AssetInfoComponent.prototype.getTransfertStatus = function (transferData) {
        return this.passService.getTransfertStatus(transferData);
    };
    AssetInfoComponent.prototype.getTransfertType = function (type) {
        return this.passService.getTransfertType(type);
    };
    AssetInfoComponent.prototype.toEditTransfer = function (transferData) {
        console.log('editing transfer => ', transferData);
        this.router.navigate(["edit_transfer/" + transferData.id]);
    };
    AssetInfoComponent.prototype.viewTransferInfo = function (transferData) {
        this.router.navigate(["transfer_info/" + transferData.id]);
    };
    AssetInfoComponent.prototype.findProgram = function () {
        var _this = this;
        this.requests.getProgramsList().then(function (res) {
            _this.program = res.find(function (i) {
                if (i._id === _this.currAsset.identification.programId) {
                    return i;
                }
            });
            _this.project = _this.program.projects.find(function (i) {
                if (i._id === _this.currAsset.identification.projectId) {
                    return i;
                }
            });
            console.log(_this.program, _this.project);
        }).catch(function (err) {
            console.log(err);
        });
    };
    AssetInfoComponent.prototype.effortView = function (effort) {
        var type = effort.type, value = effort.value, unit = effort.unit;
        if (type && unit) {
            return type + ': ' + value + ' ' + unit;
        }
        else if (!type && !unit) {
            return 'Non';
        }
        else if (!unit) {
            return type + ' ' + value;
        }
        else {
            return 'Non';
        }
    };
    AssetInfoComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.findProgram();
        this.assetType.valueChanges.subscribe(function (res) {
            if (!_this.isAssetStatusEditable(_this.currAsset))
                return;
            _this.typeTouched = _this.currAsset.status === _this.assetType.value;
        });
        this.user = this.storageService.getUser();
        this.aRouter.params.subscribe(function (params) {
            var currAssetId = params['id'];
            if (!currAssetId) {
                var storageAsset = _this.storageService.getAsset();
                if (storageAsset && Object.keys(storageAsset).length > 0) {
                    _this.transferAsset = true;
                    _this.currAsset = storageAsset;
                }
                else {
                    _this.router.navigate(['main']);
                }
            }
            else {
                _this.requests.newAsset(currAssetId).then(function (res) {
                    _this.currAsset = res;
                    if (Object.keys(res.efforts).length) {
                        _this.effortList = res.efforts.efforts;
                    }
                    _this.requests.getTransfersForAsset(currAssetId).subscribe(function (transfers) {
                        res.transfersNumber = transfers.length;
                        res.transfers = transfers;
                        console.log(res);
                    });
                    _this.requests.getActionsForAsset(_this.currAsset.id).subscribe(function (data) {
                        _this.assetActions = data.filter(function (item) { return !item.done; });
                    });
                    _this.requests.getUser(_this.currAsset.identification.userId).then(function (res) {
                        _this.creator = res;
                    }).catch(function (err) {
                        console.error(err);
                    });
                    console.log('currAsset ', _this.currAsset, _this.isAssetStatusEditable(_this.currAsset));
                }).catch(function (err) {
                    console.error(err);
                });
            }
        });
    };
    AssetInfoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-asset-info',
            template: __webpack_require__(/*! ./asset-info.component.html */ "./src/app/asset-info/asset-info.component.html"),
            styles: [__webpack_require__(/*! ./asset-info.component.scss */ "./src/app/asset-info/asset-info.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _services_requests_service__WEBPACK_IMPORTED_MODULE_3__["RequestsService"],
            _services_local_storage_service__WEBPACK_IMPORTED_MODULE_4__["LocalStorageService"],
            _services_pass_service__WEBPACK_IMPORTED_MODULE_5__["PassService"]])
    ], AssetInfoComponent);
    return AssetInfoComponent;
}());



/***/ }),

/***/ "./src/app/header/header.component.html":
/*!**********************************************!*\
  !*** ./src/app/header/header.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"nav-bar-wrapper\">\n  <div class=\"row\">\n    <div class=\"logo-wrapper col-md-3 col-lg-3\">\n      <div class=\"dropdown\">\n        <button *ngIf=\"user\" class=\"btn navbar-btn dropdown-toggle\" type=\"button\" id=\"dropdownMenuButton\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n          <fa name=\"bars\"></fa>\n        </button>\n        <ul class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuButton\">\n          <li (click)=\"goToAssetsList()\">\n            <!--<span class=\"drop-btn\">Consultation des actifs valorisables existans</span>-->\n            <fa class=\"user-icon\" name=\"id-card-o\"></fa>\n            <span class=\"drop-btn\">Consultation des actifs valorisables existants</span>\n          </li>\n          <li (click)=\"goToNewAsset()\">\n            <!--<span class=\"drop-btn\">Nouveau actif valorisables</span>-->\n            <fa class=\"user-icon\" name=\"plus-square-o\"></fa>\n            <span class=\"drop-btn\">Création d'un nouvel actif</span>\n          </li>\n          <li (click)=\"goToTransfertList()\" *ngIf=\"isPriviledged()\">\n            <!--<span class=\"drop-btn\">Transferts list</span>-->\n            <fa class=\"user-icon\" name=\"id-card\"></fa>\n            <span class=\"drop-btn\">Consultation des transferts</span>\n          </li>\n          <li (click)=\"goToActionsCenter()\" *ngIf=\"isAdmin()\">\n            <fa class=\"user-icon\" name=\"list-ul\"></fa>\n            <span class=\"drop-btn\">Centre d'actions</span>\n          </li>\n          <!-- <li (click)=\"goToAdministration()\" *ngIf=\"isAdmin()\">\n            <fa class=\"user-icon\" name=\"user\"></fa>\n            <span class=\"drop-btn\">Administration</span>\n          </li> -->\n          <li (click)=\"getStats()\" *ngIf=\"isAdmin()\">\n            <fa class=\"user-icon\" name=\"bar-chart\"></fa>\n            <span class=\"drop-btn\">Statistiques</span>\n          </li>\n          <li (click)=\"goToAdministration()\" *ngIf=\"isPriviledged()\">\n            <fa class=\"user-icon\" name=\"user\"></fa>\n            <span class=\"drop-btn\">Administration</span>\n          </li>\n        </ul>\n      </div>\n      <img src=\"../../assets/logo/systemx-logo.png\" alt=\"\">\n    </div>\n    <div class=\"title-wrapper  col-md-6 col-lg-6\">\n      <span class=\"capital letter\">A</span>\n      <span>pplication de </span>\n      <span class=\"capital letter\">G</span>\n      <span>estion des </span>\n      <span class=\"capital letter\">A</span>\n      <span>ctifs </span>\n      <span class=\"capital letter\">V</span>\n      <span>alorisables</span>\n    </div>\n    <div class=\"user-info-wrapper col-md-3 col-lg-3\" *ngIf=\"user\">\n      <div class=\"info-wrapper\">\n        <div class=\"info-sub-wrapper\">\n          <div class=\"user-info\">\n            <div>{{user?.firstName + \" \" + user?.lastName}}</div>\n            <div class=\"user-role\">{{user?.roles}}</div>\n          </div>\n          <button type=\"button\" class=\"btn btn-default dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n            <fa class=\"user-icon\" name=\"user\"></fa>\n          </button>\n          <ul class=\"dropdown-menu\">\n            <li> <span>{{user?.firstName}}</span></li>\n            <li> <span>{{user?.lastName}}</span></li>\n            <li class=\"logout-wrapper\">\n              <button type=\"button\" class=\"btn btn-primary logout\" (click)=\"logout()\">\n                <span>Logout</span>\n              </button>\n            </li>\n          </ul>\n\n        </div>\n      </div>\n    </div>\n    <div class=\"no-user-wrapper  col-md-3 col-lg-3\" *ngIf=\"!user\"></div>\n  </div>\n</div>\n\n"

/***/ }),

/***/ "./src/app/header/header.component.scss":
/*!**********************************************!*\
  !*** ./src/app/header/header.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  position: fixed;\n  width: 100%;\n  z-index: 2; }\n\n.nav-bar-wrapper {\n  cursor: default;\n  padding: 0;\n  width: 100%;\n  border-bottom: 1px solid #8c8c8c;\n  height: 70px;\n  background-color: #292929;\n  color: whitesmoke; }\n\n.nav-bar-wrapper .row {\n    height: 100%;\n    width: 100%;\n    margin: 0 !important; }\n\n.nav-bar-wrapper .row .logo-wrapper {\n      height: 100%;\n      position: relative;\n      display: flex;\n      flex-flow: row nowrap;\n      align-items: center;\n      padding: 0; }\n\n.nav-bar-wrapper .row .logo-wrapper img {\n        max-height: 60px; }\n\n.nav-bar-wrapper .row .logo-wrapper .dropdown {\n        position: unset;\n        width: 50px;\n        margin-left: 20px; }\n\n.nav-bar-wrapper .row .logo-wrapper .dropdown .dropdown-menu {\n          left: 5%;\n          top: 70px;\n          padding: 0;\n          width: -webkit-fit-content;\n          width: -moz-fit-content;\n          width: fit-content;\n          max-width: 30vw;\n          color: #292929; }\n\n.nav-bar-wrapper .row .logo-wrapper .dropdown .dropdown-menu li {\n            cursor: pointer;\n            border-bottom: 1px solid #d3d3d3;\n            padding: 0.4vw;\n            width: 100%; }\n\n.nav-bar-wrapper .row .logo-wrapper .dropdown .dropdown-menu li fa {\n              display: inline-flex;\n              justify-content: center;\n              align-items: center;\n              width: 20px;\n              margin-right: 10px; }\n\n.nav-bar-wrapper .row .logo-wrapper .dropdown .dropdown-menu li fa[name=\"plus-square-o\"] {\n                font-size: 18px; }\n\n.nav-bar-wrapper .row .logo-wrapper .dropdown .dropdown-menu li span {\n              font-size: 15px; }\n\n.nav-bar-wrapper .row .logo-wrapper .dropdown .dropdown-toggle {\n          z-index: 1;\n          background-color: transparent;\n          border: 2px solid #0062cc; }\n\n.nav-bar-wrapper .row .logo-wrapper .dropdown .dropdown-toggle fa {\n            color: whitesmoke; }\n\n.nav-bar-wrapper .row .logo-wrapper .dropdown .dropdown-toggle:hover {\n            outline: transparent;\n            background-color: #0069d9; }\n\n.nav-bar-wrapper .row .logo-wrapper .dropdown .dropdown-toggle:hover fa {\n              color: #fff; }\n\n.nav-bar-wrapper .row .title-wrapper {\n      padding: 16px 0;\n      text-align: center;\n      height: 100%; }\n\n.nav-bar-wrapper .row .title-wrapper span {\n        font-size: 20px;\n        color: whitesmoke;\n        text-transform: uppercase; }\n\n.nav-bar-wrapper .row .title-wrapper span.capital.letter {\n          font-size: 28px;\n          color: whitesmoke;\n          font-weight: bolder; }\n\n.nav-bar-wrapper .row .user-info-wrapper {\n      height: 100%;\n      padding-right: 0; }\n\n.nav-bar-wrapper .row .user-info-wrapper .info-wrapper {\n        width: 100%; }\n\n.nav-bar-wrapper .row .user-info-wrapper .info-wrapper .info-sub-wrapper {\n          float: right;\n          display: flex;\n          flex-flow: row;\n          justify-content: center;\n          align-items: center;\n          margin: 0; }\n\n.nav-bar-wrapper .row .user-info-wrapper .info-wrapper .info-sub-wrapper .user-info {\n            display: flex;\n            flex-flow: column;\n            justify-content: center;\n            align-items: flex-start; }\n\n.nav-bar-wrapper .row .user-info-wrapper .info-wrapper .info-sub-wrapper .user-info .user-role {\n              align-self: flex-end; }\n\n.nav-bar-wrapper .row .user-info-wrapper .info-wrapper .info-sub-wrapper button {\n            color: whitesmoke;\n            padding: 6px 20px 6px 12px; }\n\n.nav-bar-wrapper .row .user-info-wrapper .info-wrapper .dropdown-menu {\n          right: 0;\n          left: auto;\n          height: 110px;\n          padding: 5px; }\n\n.nav-bar-wrapper .row .user-info-wrapper .info-wrapper .dropdown-menu ul {\n            text-align: center; }\n\n.nav-bar-wrapper .row .user-info-wrapper .info-wrapper .dropdown-menu li {\n            margin-bottom: 10px; }\n\n.nav-bar-wrapper .row .user-info-wrapper .info-wrapper .dropdown-menu .logout-wrapper {\n            display: flex;\n            flex-flow: row;\n            align-items: center;\n            justify-content: center; }\n\n.nav-bar-wrapper .row .user-info-wrapper .info-wrapper .dropdown-toggle {\n          border: transparent;\n          background: transparent;\n          outline: none; }\n\n.nav-bar-wrapper .row .user-info-wrapper .info-wrapper .dropdown-toggle:active {\n            border: transparent;\n            background: transparent;\n            outline: none;\n            box-shadow: none; }\n\n.nav-bar-wrapper .row .user-info-wrapper .info-wrapper div {\n          display: inline-block;\n          float: left;\n          padding: 0;\n          text-align: right;\n          margin-right: 7px; }\n\n.nav-bar-wrapper .row .user-info-wrapper .info-wrapper div span {\n            text-overflow: ellipsis;\n            overflow: hidden;\n            display: block;\n            word-wrap: normal;\n            text-align: center; }\n\n.nav-bar-wrapper .row .user-info-wrapper .info-wrapper .logout {\n          background-color: #fff;\n          border-color: #0062cc;\n          color: #0069d9;\n          margin: 0 auto;\n          position: relative;\n          bottom: 3px; }\n\n.nav-bar-wrapper .row .user-info-wrapper .info-wrapper .logout:hover {\n          background-color: #0069d9;\n          border-color: #0062cc;\n          color: #fff; }\n\n.nav-bar-wrapper .row .user-info-wrapper .info-wrapper fa {\n          font-size: 40px; }\n\n.nav-bar-wrapper .row .user-info-wrapper .info-wrapper button span {\n          font-size: 15px; }\n\n.nav-bar-wrapper-2 {\n  width: 100%;\n  margin: 0 auto; }\n\n.nav-bar-wrapper-2 .menu-wrapper {\n    width: 80%; }\n"

/***/ }),

/***/ "./src/app/header/header.component.ts":
/*!********************************************!*\
  !*** ./src/app/header/header.component.ts ***!
  \********************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_requests_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/requests.service */ "./src/app/services/requests.service.ts");
/* harmony import */ var _services_local_storage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/local-storage.service */ "./src/app/services/local-storage.service.ts");
/* harmony import */ var _services_pass_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/pass.service */ "./src/app/services/pass.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(router, requests, storageService, passService) {
        var _this = this;
        this.router = router;
        this.requests = requests;
        this.storageService = storageService;
        this.passService = passService;
        this.userSubscription = storageService.getUserObservable().subscribe(function (user) {
            // console.log('h user updated => ', user);
            _this.user = user;
        });
    }
    HeaderComponent.prototype.isAdmin = function () {
        return this.passService.isAdmin();
    };
    HeaderComponent.prototype.isPriviledged = function () {
        return this.passService.isPriviledged();
    };
    HeaderComponent.prototype.goToNewAsset = function () {
        this.router.navigate(['new_asset']);
    };
    HeaderComponent.prototype.goToAssetsList = function () {
        this.router.navigate(['main']);
    };
    HeaderComponent.prototype.goToTransfertList = function () {
        this.router.navigate(['transferts']);
    };
    HeaderComponent.prototype.goToAdministration = function () {
        this.router.navigate(['administration']);
    };
    HeaderComponent.prototype.goToActionsCenter = function () {
        this.router.navigate(['actions']);
    };
    HeaderComponent.prototype.getStats = function () {
        this.requests.getStats();
    };
    HeaderComponent.prototype.logout = function () {
        this.storageService.deleteUser();
        this.router.navigate(['login']);
    };
    HeaderComponent.prototype.ngOnInit = function () {
        this.user = this.storageService.getUser();
        // console.log('=======> ', this.user);
    };
    HeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.scss */ "./src/app/header/header.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _services_requests_service__WEBPACK_IMPORTED_MODULE_2__["RequestsService"],
            _services_local_storage_service__WEBPACK_IMPORTED_MODULE_3__["LocalStorageService"],
            _services_pass_service__WEBPACK_IMPORTED_MODULE_4__["PassService"]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/login/login.component.html":
/*!********************************************!*\
  !*** ./src/app/login/login.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"login-body container\">\n  <div class=\"row\">\n    <div class=\"col-md-4 col-lg-4\"></div>\n    <div class=\"col-md-4 col-lg-4\">\n      <div class=\"login-form-wrapper text-center\">\n        <form class=\"form-signin\" [formGroup]=\"loginForm\" (ngSubmit)=\"onLoginFormSubmit()\" (keyup.enter)=\"onLoginFormSubmit()\">\n          <div class=\"enter-form-wrapper row\">\n            <div class=\"col-md-3 col-lg-3\"></div>\n            <div class=\"col-md-3 col-lg-3\">\n              <fa name=\"user\" size=\"4x\"></fa>\n            </div>\n            <div class=\"col-md-3 col-lg-3\">\n              <span>Connexion</span>\n            </div>\n            <div class=\"col-md-3 col-lg-3\"></div>\n\n          </div>\n          <div class=\"message-input-wrapper\">\n            <label for=\"lname\" class=\"sr-only\">Email address</label>\n            <input  class=\"form-control\" type=\"text\" placeholder=\"Enter Username\" id=\"lname\" name=\"lname\" formControlName=\"login\"\n                    [(ngModel)]=\"loginStr\" (keyup)=\"removeSpaces('loginStr')\" required/>\n            <div *ngIf=\"!login.valid && login.touched\" class=\"login-error error\">Incorrect login</div>\n          </div>\n          <div class=\"message-input-wrapper\">\n            <label for=\"lpsw\" class=\"sr-only\">Password</label>\n            <input class=\"form-control\" type=\"password\" placeholder=\"Enter Password\" id=\"lpsw\" name=\"lpsw\" formControlName=\"password\"\n                   [(ngModel)]=\"passwordStr\" (keyup)=\"removeSpaces('passwordStr')\" required>\n            <div *ngIf=\"!password.valid && password.touched\" class=\"password-error error\">Incorrect password</div>\n          </div>\n\n          <button type=\"submit\" class=\"btn btn-lg btn-primary btn-block\" [disabled]=\"!loginForm.valid\">\n            <span>Se connecter</span>\n          </button>\n          <div *ngIf=\"loginErrorMessage\" class=\"form-error error\">{{loginErrorMessage}}</div>\n        </form>\n      </div>\n    </div>\n    <div class=\"col-md-4 col-lg-4\"></div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/login/login.component.scss":
/*!********************************************!*\
  !*** ./src/app/login/login.component.scss ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".nav-bar-wrapper {\n  margin: 0;\n  padding: 0;\n  background: #DCDCDC;\n  border: 1px solid lightgrey;\n  height: 145px; }\n  .nav-bar-wrapper .logo-wrapper {\n    height: 100%; }\n  .nav-bar-wrapper .logo-wrapper img {\n      max-width: 100%;\n      max-height: 100%; }\n  .nav-bar-wrapper .title-wrapper span {\n    font-size: 26px;\n    color: #fff;\n    text-transform: uppercase; }\n  .nav-bar-wrapper .user-info-wrapper {\n    height: 100%; }\n  .row {\n  height: 100%;\n  width: 100%;\n  margin: 0; }\n  .login-body {\n  height: calc(100vh - 145px); }\n  .login-body .col-md-4, .login-body .col-lg-4 {\n    position: relative; }\n  .login-body .login-form-wrapper {\n    width: 100%;\n    top: 190px;\n    left: 115px;\n    padding: 200px 0;\n    border-radius: 0.3vw; }\n  .login-body .login-form-wrapper .row {\n      height: auto;\n      margin: 0; }\n  .login-body .login-form-wrapper .error {\n      color: #FF0000;\n      font-size: 15px; }\n  .login-body .login-form-wrapper .password-error {\n      top: 15.7vw;\n      left: 9.5vw; }\n  .login-body .login-form-wrapper .login-error {\n      top: 10vw;\n      left: 10.5vw; }\n  .login-body .login-form-wrapper .form-error {\n      bottom: -17px;\n      left: 101px;\n      text-align: center;\n      line-height: 1.2vw; }\n  .login-body .login-form-wrapper form {\n      width: 100%; }\n  .login-body .login-form-wrapper form .input {\n        align-items: center;\n        width: 100%;\n        font-size: 15px; }\n  .login-body .login-form-wrapper form .message-input-wrapper {\n        height: 100px; }\n  .login-body .login-form-wrapper form .message-input-wrapper input {\n          padding: 0.4vw;\n          margin-bottom: 20px;\n          height: 48px; }\n  .login-body .login-form-wrapper form .message-input-wrapper label {\n          margin-bottom: 0; }\n  .login-body .login-form-wrapper form .user-name-wrapper {\n        position: relative;\n        margin-bottom: 10px; }\n  .login-body .login-form-wrapper form .hint-wrapper {\n        position: absolute;\n        right: 2px; }\n  .login-body .login-form-wrapper form .hint-wrapper .hint {\n          position: absolute;\n          bottom: 2vw;\n          left: 1vw;\n          color: #000;\n          background-color: lightgrey;\n          padding: 0 2px;\n          visibility: hidden;\n          border: 1px solid #000;\n          border-radius: 0.3vw;\n          width: 190px;\n          font-size: 15px;\n          text-align: center; }\n  .login-body .login-form-wrapper form .hint-wrapper .question-wrapper:hover + .hint {\n          visibility: visible; }\n  .login-body .login-form-wrapper form .hint-wrapper .question-wrapper:hover {\n          cursor: pointer; }\n  .login-body .login-form-wrapper form .hint-wrapper .question-wrapper fa {\n          font-size: 20px; }\n  .login-body .login-form-wrapper form .enter-form-wrapper {\n        margin-bottom: 25px; }\n  .login-body .login-form-wrapper form .enter-form-wrapper span {\n          font-size: 15px;\n          margin: 17px 0;\n          display: block; }\n  .login-body .login-form-wrapper form .btn-wrapper {\n        position: relative;\n        margin-top: 40px; }\n  .login-body .login-form-wrapper form .btn-wrapper button {\n          position: relative;\n          bottom: 20px;\n          background-color: crimson;\n          border: none;\n          border-radius: 0.3vw; }\n  .login-body .login-form-wrapper form .btn-wrapper button:disabled {\n            opacity: 0.3; }\n  .login-body .login-form-wrapper form .btn-wrapper button span {\n            font-size: 15px; }\n"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_requests_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/requests.service */ "./src/app/services/requests.service.ts");
/* harmony import */ var _services_local_storage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/local-storage.service */ "./src/app/services/local-storage.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LoginComponent = /** @class */ (function () {
    function LoginComponent(formBuilder, router, requests, storageService) {
        var _this = this;
        this.formBuilder = formBuilder;
        this.router = router;
        this.requests = requests;
        this.storageService = storageService;
        this.loginStr = '';
        this.passwordStr = '';
        this.userSubscription = storageService.getUserObservable().subscribe(function (user) {
            _this.user = user;
            if (_this.user) {
                _this.router.navigate(['main']);
            }
        });
    }
    LoginComponent.prototype.onLoginFormSubmit = function () {
        var _this = this;
        this.loginErrorMessage = '';
        if (!this.loginForm.valid) {
            return;
        }
        this.requests.login(this.loginForm.value).then(function (res) {
            _this.storageService.updateUser(res);
            // localStorage.setItem('access_token', res.uid);
        }).catch(function (err) {
            console.error(err);
            console.log(err.status);
            if (err.status === 403) {
                _this.loginErrorMessage = 'Invalid login or password';
            }
            else if (err.status == 0) {
                _this.loginErrorMessage = 'Login error. Please, check your internet connection';
            }
        });
    };
    Object.defineProperty(LoginComponent.prototype, "login", {
        get: function () {
            return this.loginForm.get('login');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LoginComponent.prototype, "password", {
        get: function () {
            return this.loginForm.get('password');
        },
        enumerable: true,
        configurable: true
    });
    LoginComponent.prototype.removeSpaces = function (value) {
        this[value] = this[value].replace(/\s/gi, '');
    };
    LoginComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.user = this.storageService.getUser();
        // console.log(this.user);
        if (this.user) {
            this.router.navigate(['main']);
        }
        this.loginForm = this.formBuilder.group({
            login: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required])
        });
        this.loginForm.valueChanges.subscribe(function (e) {
            // console.log(e);
            _this.loginErrorMessage = '';
        });
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/login/login.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _services_requests_service__WEBPACK_IMPORTED_MODULE_3__["RequestsService"],
            _services_local_storage_service__WEBPACK_IMPORTED_MODULE_4__["LocalStorageService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/new-asset/new-asset.component.html":
/*!****************************************************!*\
  !*** ./src/app/new-asset/new-asset.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"modal fade\" id=\"modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"modalTitle\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h5 class=\"modal-title\" id=\"modalTitle\">Ajouter</h5>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\">\n        <ng-container *ngFor=\"let effort of effortList; let i = index\">\n          <!--<div *ngIf=\"effort.status === 'not_added'\" (click)=\"addEffort(i)\" data-dismiss=\"modal\">-->\n          <div (click)=\"addEffort(i)\" data-dismiss=\"modal\">\n            <span>{{effort.type}}</span>\n          </div>\n        </ng-container>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-danger\" data-dismiss=\"modal\">Annuler</button>\n      </div>\n    </div>\n  </div>\n</div>\n\n<div class=\"row\">\n  <div class=\"col-md-3 col-lg-3\"></div>\n  <div class=\"col-md-6 col-lg-6\">\n    <div class=\"actif-modal-wrapper\">\n      <div class=\"panel panel-primary\">\n        <div class=\"panel-heading\">\n          <span class=\"panel-title\">{{currWrapper <= 3 ? \"type de l'actif valorisable\" : currWrapper === 4 ? \"caracterisation technique\" : \"Classification (DO suite revue)\"}}</span>\n          <div>\n            <button *ngIf=\"currWrapper < 6\" class=\"btn\" (click)=\"onChancelBtnClick()\">\n              <span>Annuler</span>\n            </button>\n            <button *ngIf=\"editingAsset && currWrapper < 6\" class=\"btn\" (click)=\"onAssetFormSubmit()\" [disabled]=\"!newAssetForm.valid\">\n              <span>Enregistrer</span>\n            </button>\n          </div>\n        </div>\n\n        <form [formGroup]=\"newAssetForm\" (ngSubmit)=\"onAssetFormSubmit()\" class=\"actif-modal-body\">\n          <div *ngIf=\"currWrapper == 1 || editingAsset\" class=\"first-wrapper\">\n            <div class=\"modal-content-header\">\n              <span class=\"subtitle\">Actif valorisable</span>\n              <div class=\"hint-wrapper\">\n                <div class=\"question-wrapper\">\n                  <fa name=\"question-circle\"></fa>\n                </div>\n                <div class=\"hint\">\n                  <ul>\n                    <li>\n                      <span>- Unitaire : sans lien avec un autre actif</span>\n                    </li>\n                    <li>\n                <span>- A partir d'un actif existant : dépendant d'un actif existant.\n                Ce dernier devient \"actif valoris. démonstrateur\"</span>\n                    </li>\n                    <li>\n                      <span>- Démonstrateur : d'autres actifs seront liés à celui-ci</span>\n                    </li>\n                  </ul>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-check-wrapper\">\n              <div class=\"form-check\">\n                <label class=\"custom-control custom-radio\" (change)=\"firsSelectChange($event)\">\n                  <input id=\"platformRadio1\" name=\"isPlatformAsset\" [value]=\"'Unitaire'\" type=\"radio\" formControlName=\"isPlatformAsset\" class=\"custom-control-input\">\n                  <span class=\"custom-control-indicator\"></span>\n                  <span class=\"custom-control-description\">Unitaire</span>\n                </label>\n                <label class=\"custom-control custom-radio\" (change)=\"firsSelectChange($event)\">\n                  <input id=\"platformRadio2\" name=\"isPlatformAsset\" [value]=\"'Démonstrateur'\" type=\"radio\" formControlName=\"isPlatformAsset\" class=\"custom-control-input\">\n                  <span class=\"custom-control-indicator\"></span>\n                  <span class=\"custom-control-description\">Démonstrateur</span>\n                </label>\n              </div>\n              <div class=\"form-check\">\n                <label class=\"custom-control custom-radio\" (change)=\"firsSelectChange($event)\">\n                  <input id=\"platformRadio3\" name=\"isPlatformAsset\" [value]=\"'A partir d\\'un actif valorisable existant'\" type=\"radio\" formControlName=\"isPlatformAsset\" class=\"custom-control-input\">\n                  <span class=\"custom-control-indicator\"></span>\n                  <span class=\"custom-control-description\">Composant du démonstrateur</span>\n                </label>\n                <select class=\"first-wrapper-select asset-info-select\" formControlName=\"parentAssetId\" ([ngModel])=\"parentAssets\" [attr.disabled]=\"isPlatformAsset === 'A partir d\\'un actif valorisable existant' ? '' : null\">\n                  <option *ngFor=\"let pAsset of parentAssets\" value=\"{{pAsset.id}}\">{{pAsset.name}}</option>\n                </select>\n              </div>\n            </div>\n          </div>\n          <div *ngIf=\"currWrapper == 2 || editingAsset\" class=\"second-wrapper\">\n            <div class=\"header-content-wrapper\">\n              <div class=\"header-input-content-wrapper\">\n                <div class=\"header-content-left\">\n                  <div>\n                    <span>Nom de l'actif valorisable:<span *ngIf=\"!assetName.valid\" class=\"error\">*</span></span>\n                    <input type=\"text\" formControlName=\"assetName\">\n                  </div>\n                  <div>\n                    <span>Nom du programme:<span *ngIf=\"isProgramValueEmpty()\" class=\"error\">*</span></span>\n                    <select class=\"asset-info-select\" name=\"programName\" formControlName=\"programName\"\n                            [(ngModel)]=\"programValue\" (change)=\"selectDefaultProgramm(programValue)\">\n                      <option *ngFor=\"let programmValue of programSelectValues\" [ngValue]=\"programmValue\" [selected]=\"programmValue._id === programValue._id\">{{programmValue.shortName}}</option>\n                    </select>\n                  </div>\n                  <div>\n                    <span>Nom du project:<span *ngIf=\"isProgramValueEmpty()\" class=\"error\">*</span></span>\n                    <select class=\"asset-info-select\" name=\"projectName\" formControlName=\"projectName\" [(ngModel)]=\"projectValue\">\n                      <option *ngFor=\"let project of programValue.projects\" [ngValue]=\"project\" [selected]=\"getSelectedProject(project)\">{{project.shortName}}</option>\n                    </select>\n                  </div>\n                </div>\n                <div class=\"header-content-right\">\n                  <div>\n                    <span>Auteur:</span>\n                    <span class=\"user-name\">{{user?.firstName}} {{user?.lastName}}</span>\n                  </div>\n                  <div>\n                    <span class=\"date-text\">Date de creation de la fiche:</span>\n                    <div *ngIf=\"!editingAsset\" class=\"auther-calendar\">\n                      <input class=\"auther-date\" type=\"date\" name=\"selected_date\" [attr.disabled]=\"editingAsset\" formControlName=\"creationDate\" useValueAsDate #dateInput/>\n                    </div>\n                    <div *ngIf=\"editingAsset\" class=\"auther-calendar\">\n                      <!--<input class=\"auther-date\" type=\"date\" name=\"selected_date\" [(ngModel)]=\"currentDate\" formControlName=\"creationDate\" #dateInput/>-->\n                      <input class=\"auther-date\" type=\"date\" [ngModel]=\"currentDate | date:'yyyy-MM-dd'\" (ngModelChange)=\"currentDate = $event\" [ngModelOptions]=\"{standalone: true}\"/>\n                    </div>\n\n\n                  </div>\n                </div>\n              </div>\n\n            </div>\n\n            <div class=\"descriptif-wrapper\">\n              <span>Descriptif</span>\n              <textarea formControlName=\"desc\"></textarea>\n            </div>\n\n            <div class=\"asset-status-wrapper\">\n              <span>Statut de l'actif:</span>\n              <select class=\"asset-status-select\" name=\"status\" formControlName=\"status\" [(ngModel)]=\"statusType\">\n                <option *ngFor=\"let status of assetStatusType\" [ngValue]=\"status\">\n                  <span>{{status.name}}</span>\n                </option>\n              </select>\n            </div>\n\n            <div class=\"type-wrapper\">\n              <span>Type:</span>\n              <select formControlName=\"type\" [(ngModel)]=\"activityType\">\n                <option *ngFor=\"let status of assetTypes\" [ngValue]=\"status\">\n                  <span>{{status.name}}</span>\n                </option>\n              </select>\n            </div>\n          </div>\n          <div *ngIf=\"currWrapper == 3 || editingAsset\" class=\"third-wrapper\">\n            <div class=\"content-wrapper\">\n              <div class=\"left-side-content\">\n                <span class=\"caption-text\">Livrable projet:</span>\n                <div class=\"checkbox-wrapper\">\n                  <label class=\"custom-control custom-radio\">\n                    <input id=\"deliverableRadio1\" name=\"deliverableExists\" [value]=\"true\" (change)=\"mandatoryRadioValueChanged()\" type=\"radio\" formControlName=\"deliverableExists\" class=\"custom-control-input\">\n                    <span class=\"custom-control-description\">Oui</span>\n                  </label>\n                  <label class=\"custom-control custom-radio\">\n                    <input id=\"deliverableRadio2\" name=\"deliverableExists\" [value]=\"false\" (change)=\"mandatoryRadioValueChanged()\" type=\"radio\" formControlName=\"deliverableExists\" class=\"custom-control-input\">\n                    <span class=\"custom-control-description\">Non</span>\n                  </label>\n                </div>\n              </div>\n              <div class=\"right-side-content\">\n                <span>Si oui, référence:<span *ngIf=\"deliverableExists.value && deliverableReference.value.length == 0\" class=\"error\">*</span></span>\n                <div>\n                  <input type=\"text\" class=\"deliverableReference\" (change)=\"mandatoryRadioValueChanged()\" formControlName=\"deliverableReference\" [attr.disabled]=\"!newAssetForm.value.deliverableExists ? '' : null\">\n                </div>\n              </div>\n            </div>\n            <div class=\"content-wrapper\">\n              <div class=\"left-side-content\">\n                <span class=\"caption-text\">PI partagée:</span>\n                <div class=\"checkbox-wrapper\">\n                  <label class=\"custom-control custom-radio\">\n                    <input id=\"piRadio1\" name=\"piSharedExists\" type=\"radio\" (change)=\"mandatoryRadioValueChanged()\" [value]=\"true\" formControlName=\"piSharedExists\" class=\"custom-control-input\">\n                    <span class=\"custom-control-description\">Oui</span>\n                  </label>\n                  <label class=\"custom-control custom-radio\">\n                    <input id=\"piRadio2\" name=\"piSharedExists\" type=\"radio\" (change)=\"mandatoryRadioValueChanged()\" [value]=\"false\" formControlName=\"piSharedExists\" class=\"custom-control-input\">\n                    <span class=\"custom-control-description\">Non</span>\n                  </label>\n                </div>\n              </div>\n              <div class=\"right-side-content\">\n                <span>Si oui, partenaire(s) copro:<span *ngIf=\"piSharedExists.value && piSharedPartners.value.length == 0\" class=\"error\">*</span></span>\n                <div>\n                  <textarea id=\"partner-textarea\" rows=\"3\" (change)=\"mandatoryRadioValueChanged()\" formControlName=\"piSharedPartners\" [attr.disabled]=\"!newAssetForm.value.piSharedExists ? '' : null\"></textarea>\n                </div>\n              </div>\n            </div>\n            <div class=\"content-wrapper\" style=\"display: flex\">\n              <div class=\"left-side-content efforts\">\n                <span class=\"caption-text\">Efforts:<span *ngIf=\"!availableEffortList.length\" class=\"error\">*</span></span>\n                <button type=\"button\" class=\"btn btn-success\" data-toggle=\"modal\" data-target=\"#modal\" [disabled]=\"!effortList.length\">Ajouter</button>\n              </div>\n              <div class=\"right-side-content contributeur\" id=\"complex-right-content\">\n                <div class=\"contributeur-wrapper\">\n                  <div class=\"efforts-list-wrapper\" *ngFor=\"let effort of availableEffortList; let i = index;\">\n                    <span>{{effort['type']}}</span>\n                    <select class=\"effort-name-select\" (change)=\"addEffort(-1)\" *ngIf=\"!isCurrentAsset()\">\n                      <option *ngFor=\"let unit of availableUnitsList[effort['type']]\" [value]=\"effort['type'] + '/' + unit.id + '/' + effort['_id']\">\n                        {{unit['name']}}\n                      </option>\n                    </select>\n                    <select class=\"effort-name-select\" (change)=\"addEffort(-1)\" *ngIf=\"isCurrentAsset()\">\n                      <ng-container *ngIf=\"efforts['efforts'].length\">\n                        <option *ngFor=\"let unit of availableUnitsList[effort['type']]\" [value]=\"effort['type'] + '/' + unit.id + '/' + effort['_id']\" [selected]=\"isSelected(unit.id, efforts, i)\">\n                          {{unit['name']}}\n                        </option>\n                      </ng-container>\n                      <ng-container *ngIf=\"!efforts['efforts'].length\">\n                        <option  *ngFor=\"let unit of availableUnitsList[effort['type']]\" [value]=\"effort['type'] + '/' + unit.id\"></option>\n                      </ng-container>\n                    </select>\n                    <input type=\"number\" class=\"effort-number\" (change)=\"addEffort('number')\" [value]=\"effort['value'] ? effort['value'] : 0\">\n                    <button type=\"button\" class=\"btn btn-danger\" (click)=\"deleteEffort(i)\">\n                      <i class=\"fas fa-trash-alt\"></i>\n                    </button>\n                  </div>\n                  <span>Contributeur(s):</span>\n                  <textarea id=\"contributors-textarea\" rows=\"3\" formControlName=\"effortContributors\" (change)=\"addEffort()\"></textarea>\n                </div>\n              </div>\n            </div>\n            <div class=\"content-wrapper\">\n              <div class=\"left-side-content\">\n                <span class=\"caption-text\">Dépôt APP:</span>\n                <label class=\"custom-control custom-radio\">\n                  <input id=\"depotRadio1\" name=\"copyrightExists\" type=\"radio\" (change)=\"mandatoryRadioValueChanged()\" [value]=\"true\" formControlName=\"copyrightExists\"\n                         class=\"custom-control-input\" [style.width]=\"'auto'\">\n                  <span class=\"custom-control-indicator\"></span>\n                  <span class=\"custom-control-description\">Oui</span>\n                </label>\n                <label class=\"custom-control custom-radio\">\n                  <input id=\"depotRadio2\" name=\"copyrightExists\" type=\"radio\" (change)=\"mandatoryRadioValueChanged()\" [value]=\"false\" formControlName=\"copyrightExists\"\n                         class=\"custom-control-input\" [style.width]=\"'auto'\">\n                  <span class=\"custom-control-indicator\"></span>\n                  <span class=\"custom-control-description\">Non</span>\n                </label>\n              </div>\n              <div class=\"right-side-content\">\n                <span>Si oui, référence:<span *ngIf=\"copyrightExists.value && copyrightReference.value.length == 0\" class=\"error\">*</span></span>\n                <div>\n                  <input type=\"text\" class=\"deliverableReference\" (change)=\"mandatoryRadioValueChanged()\" formControlName=\"copyrightReference\" [attr.disabled]=\"!newAssetForm.value.copyrightExists ? '' : null\">\n                </div>\n              </div>\n            </div>\n            <div class=\"content-wrapper\">\n              <div class=\"left-side-content\">\n                <span class=\"caption-text\">Brevet:</span>\n                <label class=\"custom-control custom-radio\">\n                  <input id=\"brevetRadio1\" name=\"patentExists\" type=\"radio\" (change)=\"mandatoryRadioValueChanged()\" [value]=\"true\" formControlName=\"patentExists\"\n                         class=\"custom-control-input\" [style.width]=\"'auto'\">\n                  <span class=\"custom-control-description\">Oui</span>\n                </label>\n                <label class=\"custom-control custom-radio\">\n                  <input id=\"brevetRadio2\" name=\"patentExists\" type=\"radio\" (change)=\"mandatoryRadioValueChanged()\" [value]=\"false\" formControlName=\"patentExists\"\n                         class=\"custom-control-input\" [style.width]=\"'auto'\">\n                  <span class=\"custom-control-description\">Non</span>\n                </label>\n              </div>\n              <div class=\"right-side-content\">\n                <span>Si oui, référence:<span *ngIf=\"patentExists.value && patentReference.value.length == 0\" class=\"error\">*</span></span>\n                <div>\n                  <input type=\"text\" class=\"deliverableReference\" (change)=\"mandatoryRadioValueChanged()\" formControlName=\"patentReference\" [attr.disabled]=\"!newAssetForm.value.patentExists ? '' : null\">\n                </div>\n              </div>\n            </div>\n\n            <div class=\"content-wrapper\">\n              <span>Démonstration</span>\n              <textarea formControlName=\"demonstration\"></textarea>\n            </div>\n\n          </div>\n          <div *ngIf=\"currWrapper == 4 || editingAsset\" class=\"fourth-wrapper\">\n            <div class=\"fields-wrapper\">\n              <div class=\"second-main-wrapper\">\n                <span class=\"dependances-text subtitle\">Références</span>\n                <div class=\"references\">\n                  <div>\n                    <span>Ftopia, Dépôt GIT:<span *ngIf=\"repository.value.length == 0\" class=\"error\">*</span></span>\n                    <input type=\"text\" formControlName=\"repository\">\n                  </div>\n                  <div>\n                    <span>Label/Version:</span>\n                    <input type=\"text\" formControlName=\"label\">\n                  </div>\n                </div>\n              </div>\n              <div class=\"second-main-wrapper\">\n                <div class=\"descriptif-detaille\">\n                  <div>\n                  </div>\n                  <span class=\"dependances-text subtitle\">Descriptif detaillé</span>\n                  <textarea formControlName=\"detailedDescription\"></textarea>\n                </div>\n              </div>\n              <div class=\"second-main-wrapper\">\n                <span class=\"dependances-text subtitle\">Dépendances</span>\n                <div class=\"dependances\">\n                  <div>\n                    <span>Licence(s): </span>\n                    <input type=\"text\" formControlName=\"dependenciesLicenses\">\n                  </div>\n                  <div>\n                    <span>Matériel(s)</span>\n                    <input type=\"text\" formControlName=\"dependenciesHardware\">\n                  </div>\n                </div>\n\n                <div class=\"reutilisables\">\n                  <span>Réutilisables:</span>\n                  <div class=\"form-check\">\n                    <label class=\"custom-control custom-radio\">\n                      <input id=\"dependenciesReusable1\" name=\"dependenciesReusable\" [value]=\"true\" type=\"radio\"\n                             formControlName=\"dependenciesReusable\" class=\"custom-control-input\">\n                      <span class=\"custom-control-indicator\"></span>\n                      <span class=\"custom-control-description\">Oui</span>\n                    </label>\n                    <label class=\"custom-control custom-radio\">\n                      <input id=\"dependenciesReusable2\" name=\"dependenciesReusable\" [value]=\"false\" type=\"radio\"\n                             formControlName=\"dependenciesReusable\" class=\"custom-control-input\">\n                      <span class=\"custom-control-indicator\"></span>\n                      <span class=\"custom-control-description\">Non</span>\n                    </label>\n                  </div>\n                </div>\n\n                <div class=\"acteur-porteur\">\n                  <div class=\"form-check\">\n                    <label class=\"form-check-label\">\n                      <input class=\"form-check-input\" type=\"checkbox\" (change)=\"isActeurOrPorteur($event)\">\n                      Acteur/porteur\n                    </label>\n                  </div>\n                  <input type=\"text\" formControlName=\"dependenciesActor\"  [attr.disabled]=\"!isActeurOrPorteurExists ? '' : null\">\n                </div>\n\n              </div>\n              <div class=\"second-main-wrapper\">\n                <div class=\"plateforme-liee\">\n                  <div>\n                    <span class=\"dependances-text subtitble\">Plateforme(s) liée(s)</span>\n                    <textarea rows=\"2\" formControlName=\"linkedPlatforms\"></textarea>\n                  </div>\n                </div>\n              </div>\n\n              <div class=\"second-main-wrapper\">\n                <div class=\"mots-cles\">\n                  <div>\n                    <span class=\"dependances-text subtitle\">Mots clés</span>\n                    <textarea rows=\"3\" formControlName=\"keywords\"></textarea>\n                  </div>\n                </div>\n              </div>\n\n              <div class=\"second-main-wrapper\">\n                <div class=\"asset-status\">\n                  <div [hidden]=\"isAssetValidated\">\n                    <span class=\"dependances-text subtitle\" style=\"margin-bottom: 20px;\">Statut de la fiche</span>\n                    <div class=\"row\" style=\"margin-left: 20px;\">\n                      <div class=\"form-check\" *ngIf=\"isCurrentAsset()\">\n                        <label for=\"assetCardStatus1\" class=\"custom-control custom-radio\">\n                          <input style=\"width: unset\" id=\"assetCardStatus1\" name=\"assetCardStatus\" [value]=\"'PENDING'\" type=\"radio\"\n                                 formControlName=\"assetCardStatus\" class=\"custom-control-input\" (change)=\"isEditingAssetValid()\" [checked]=\"this.currAsset.assetStatus === 'PENDING'\">\n                          <span class=\"custom-control-indicator\"></span>\n                          <span class=\"custom-control-description\">En cours de rédaction</span>\n                        </label>\n                        <label for=\"assetCardStatus2\" class=\"custom-control custom-radio\">\n                          <input style=\"width: unset; margin-left: 20px;\" id=\"assetCardStatus2\" name=\"assetCardStatus\" [value]=\"'WAITING_VALIDATION'\" type=\"radio\"\n                                 formControlName=\"assetCardStatus\" class=\"custom-control-input\" (change)=\"isEditingAssetValid()\" [checked]=\"this.currAsset.assetStatus === 'WAITING_VALIDATION'\">\n                          <span class=\"custom-control-indicator\"></span>\n                          <span class=\"custom-control-description\">En attente de validation Codir</span>\n                        </label>\n                      </div>\n\n                      <div class=\"form-check\" *ngIf=\"!isCurrentAsset()\">\n                        <label for=\"assetCardStatus1\" class=\"custom-control custom-radio\">\n                          <input style=\"width: unset\" name=\"assetCardStatus\" [value]=\"'PENDING'\" type=\"radio\"\n                                 formControlName=\"assetCardStatus\" class=\"custom-control-input\" (change)=\"isEditingAssetValid()\">\n                          <span class=\"custom-control-indicator\"></span>\n                          <span class=\"custom-control-description\">En cours de rédaction</span>\n                        </label>\n                        <label for=\"assetCardStatus2\" class=\"custom-control custom-radio\">\n                          <input style=\"width: unset; margin-left: 20px;\" name=\"assetCardStatus\" [value]=\"'WAITING_VALIDATION'\" type=\"radio\"\n                                 formControlName=\"assetCardStatus\" class=\"custom-control-input\" (change)=\"isEditingAssetValid()\">\n                          <span class=\"custom-control-indicator\"></span>\n                          <span class=\"custom-control-description\">En attente de validation Codir</span>\n                        </label>\n                      </div>\n\n                      <span *ngIf=\"isAssetValid\" class=\"error-message\">Attention, les champs suivants sont nécessaires à la validation: Effort, Label / version, Description détaillée et Acteur / porteur.</span>\n                    </div>\n                  </div>\n                </div>\n              </div>\n\n              <div *ngIf=\"notDoneAssetActions.length > 0\" class=\"second-main-wrapper\">\n                <div class=\"asset-actions\">\n                  <div>\n                    <span class=\"dependances-text subtitle\" style=\"margin-bottom: 20px;\">Actions à mener</span>\n\n                    <p-treeTable [value]=\"notDoneAssetActions\">\n\n                      <p-column [style]=\"{'width':'50%'}\">\n                        <ng-template let-col let-node=\"rowData\">\n                          <div class=\"td-flex-wrapper left-aligned-content\">\n                            <div class=\"asset-description\">{{node.description}}</div>\n                          </div>\n                        </ng-template>\n                      </p-column>\n\n                      <p-column>\n                        <ng-template let-col let-node=\"rowData\">\n                          <div class=\"td-flex-wrapper\">\n                            <div>{{getDate(node.dueDate)}}</div>\n                          </div>\n                        </ng-template>\n                      </p-column>\n\n                      <p-column [style]=\"{'width': '30px'}\">\n                        <ng-template let-col let-node=\"rowData\">\n                          <div *ngIf=\"node.comment && node.comment.length > 0\"  title=\"{{node.comment[node.comment.length -1].text}}\">\n                            <fa class=\"user-icon\" name=\"file-text\" style=\"cursor: default\"></fa>\n                          </div>\n                        </ng-template>\n                      </p-column>\n\n                      <p-column [style]=\"{'width': '80px'}\">\n                        <ng-template let-col let-node=\"rowData\">\n                          <div class=\"td-flex-wrapper right-aligned-content tools\">\n                            <div class=\"validate-button\" (click)=\"validateAction(node.id)\">Valider</div>\n                          </div>\n                        </ng-template>\n                      </p-column>\n                    </p-treeTable>\n                  </div>\n                </div>\n              </div>\n\n            </div>\n          </div>\n          <div *ngIf=\"(currWrapper == 5 || editingAsset) && isAdmin()\" class=\"fifth-wrapper\">\n            <div class=\"classification-wrapper\">\n              <div class=\"subtitle\" style=\"margin-bottom: 20px; margin-left: 5px; font-size: 1.15em\">Classification D.O.</div>\n              <div class=\"custom-control-wrapper\">\n                <select formControlName=\"classificationType\" [(ngModel)]=\"classificationType\">\n                  <option *ngFor=\"let type of classificationTypes\" [ngValue]=\"type\">\n                    <span>{{type.name}}</span>\n                  </option>\n                </select>\n              </div>\n\n              <div class=\"perspective-wrapper\">\n                <span>Perspective:</span>\n                <textarea placeholder=\"Descriptif\" formControlName=\"classificationPerspective\"></textarea>\n              </div>\n\n              <div class=\"comment-wrapper\">\n                <span>Commentaire:</span>\n                <textarea placeholder=\"Commentaire\" formControlName=\"assetComment\"></textarea>\n              </div>\n\n              <div>\n                <span>Statut de la fiche:</span>\n                <span class=\"asset-card-status\" [ngClass]=\"{'validated': assetCardStatus.value === 'VALIDATED'}\">{{getStatusDisplayString(assetCardStatus.value)}}</span>\n                <span *ngIf=\"assetCardStatus.value === 'WAITING_VALIDATION'\" style=\"margin-left: 30px\">\n                  <button class=\"btn btn-success\" (click)=\"validateAssetCard()\">\n                    <span>Valider la fiche</span>\n                  </button>\n                </span>\n              </div>\n\n              <form [formGroup]=\"newActionForm\" class=\"asset-actions-wrapper\">\n                <div class=\"subtitle\">Ajouter une action</div>\n                <div class=\"flex-row\">\n                  <!-- Select user/admin action type -->\n                  <div class=\"action-field\">\n                    <label for=\"actionType\">Type d'action</label>\n                    <select name=\"actionType\" formControlName=\"actionType\" style=\"display: block\">\n                      <option value=\"USER_ACTION\">Action technique (sur l'actif ou sa fiche)</option>\n                      <option value=\"ADMIN_ACTION\">Action business (contact partenaire ou autre)</option>\n                    </select>\n                  </div>\n                  <!-- Due Date datepicker -->\n                  <div class=\"action-field\">\n                    <label for=\"actionDate\">Date limite</label>\n                    <input class=\"action-deadline\" type=\"date\" name=\"actionDate\" formControlName=\"actionDate\" useValueAsDate #actionDateInput/>\n                  </div>\n                </div>\n                <!-- Input assignee(s) -->\n                <div class=\"action-field\">\n                  <label for=\"actionAssignee\">Acteur</label>\n                  <input type=\"text\" formControlName=\"actionAssignee\" name=\"actionAssignee\" placeholder=\"Nom de la personne responsable...\">\n                </div>\n                <!-- TextArea description -->\n                <div class=\"action-field\">\n                  <label for=\"actionDesc\">Description</label>\n                  <textarea name=\"actionDesc\" formControlName=\"actionDesc\" placeholder=\"Nature de l'action à accomplir...\"></textarea>\n                </div>\n                <div class=\"action-field\">\n                  <div (click)=\"createAction()\" class=\"action-validate-button\">Créer l'action</div>\n                </div>\n              </form>\n\n            </div>\n          </div>\n          <div *ngIf=\"currWrapper == 6\" class=\"sixth-wrapper\">\n            <p>Fiche actif valorisable enregistrée:</p>\n            <p>\"{{resAsset?.identification?.name}}\"</p>\n            <p>Ref. \"{{resAsset?.id}}\"</p>\n            <a routerLink=\"/main\" routerLinkActive=\"active\">\n              <fa name=\"angle-left\"></fa>\n              <span> Retour à la liste des actifs valorisables</span>\n            </a>\n          </div>\n        </form>\n\n        <div class=\"suivant-btn\">\n          <div>\n            <button class=\"left-aligned btn btn-primary\" *ngIf=\"currWrapper > 1 && currWrapper !== 6\" (click)=\"prevWrapper()\">\n              <span>Retour</span>\n            </button>\n          </div>\n          <div *ngIf=\"assetErrorMessage\" class=\"asset-error error\">{{assetErrorMessage}}</div>\n          <div>\n            <button class=\"right-aligned btn btn-primary\" *ngIf=\"currWrapper < 6 && currWrapper !== maxWrapper && !editingAsset\" (click)=\"nextWrapper()\" [disabled]=\"areFieldsInvalidForNextPage()\">\n              <span>Suivant</span>\n            </button>\n            <button class=\"right-aligned btn btn-primary\" *ngIf=\"currWrapper === maxWrapper || editingAsset\" (click)=\"onAssetFormSubmit()\" [disabled]=\"!newAssetForm.valid\">\n              <span>Enregistrer</span>\n            </button>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"col-md-3 col-lg-3\"></div>\n</div>\n\n<smite-modal id=\"create-update-action\">\n  <div class=\"smite-modal\">\n    <div class=\"smite-modal-body\">\n      <div class=\"smite-modal-closer\" (click)=\"modals.close('create-update-action')\">X</div>\n      <div class=\"smite-modal-header\">\n        <div class=\"title\">Valider l'action</div>\n        <div class=\"col desc\">\n          <p>Valider l'action \"{{actionToValidate?.description}}\"</p>\n        </div>\n      </div>\n      <div class=\"content\">\n        <div class=\"flex-col\">\n          <div class=\"label\">Ecrire un commentaire:</div>\n          <textarea></textarea>\n        </div>\n      </div>\n      <div class=\"smite-modal-controls\">\n        <div (click)=\"updateAction($event)\" class=\"validate button\">Valider</div>\n      </div>\n    </div>\n  </div>\n  <div class=\"smite-modal-background\"></div>\n</smite-modal>\n"

/***/ }),

/***/ "./src/app/new-asset/new-asset.component.scss":
/*!****************************************************!*\
  !*** ./src/app/new-asset/new-asset.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host > .row {\n  padding-top: 90px; }\n\n:host .panel {\n  border-radius: 0;\n  background-color: #fbfbfb; }\n\n:host .panel .panel-heading {\n    border-radius: 0; }\n\n:host input, :host select, :host textarea {\n  background-color: white; }\n\n.modal-body span {\n  cursor: pointer; }\n\n.modal .modal-header {\n  display: flex;\n  align-items: center;\n  justify-content: space-between;\n  border-top-left-radius: 6px;\n  border-top-right-radius: 6px;\n  color: #fff;\n  background-color: #337ab7;\n  border-color: #337ab7; }\n\n.modal .modal-header:before, .modal .modal-header:after {\n    display: none; }\n\n.modal .modal-body {\n  display: flex;\n  flex-direction: column; }\n\n.modal .modal-body h5 {\n    font-weight: bold; }\n\n.modal .modal-body > div {\n    padding: 0.2vw 0.5vw;\n    border: 1px solid #0062cc;\n    color: #0062cc;\n    background-color: #fff;\n    border-radius: 5px;\n    cursor: pointer;\n    margin-bottom: 1.5vw; }\n\n.modal .modal-body > div:hover {\n      background: #0062cc;\n      color: #fff; }\n\n.modal .modal-body .creation-rights-wrapper {\n    display: flex;\n    flex-direction: column; }\n\n.modal .modal-body .creation-rights-wrapper label {\n      margin: 0 10px; }\n\n.error-message {\n  color: red; }\n\n* {\n  font-size: 15px; }\n\n.first-wrapper select {\n  width: 100%; }\n\n.subtitle {\n  font-size: 1.15em;\n  font-weight: bolder;\n  margin-top: 30px;\n  margin-bottom: 10px;\n  text-transform: uppercase; }\n\ninput {\n  padding: 0 0.5vw;\n  margin-right: 0.5vw;\n  background: transparent;\n  border: 1px solid #0062cc;\n  border-radius: 3px;\n  width: 100%; }\n\nspan {\n  font-weight: bold; }\n\nselect {\n  background: transparent;\n  border: 1px solid #0062cc;\n  border-radius: 3px; }\n\nselect:hover {\n    outline: transparent;\n    background-color: #0069d9;\n    color: #fff; }\n\nselect:active {\n    background-color: inherit;\n    color: #000; }\n\ntextarea {\n  border: 1px solid #0062cc;\n  border-radius: 3px;\n  resize: none;\n  width: 100%; }\n\n.suivant-btn {\n  display: flex;\n  margin: 10px; }\n\n.suivant-btn > * {\n    flex: 1; }\n\n.suivant-btn span {\n    font-size: 15px; }\n\n.suivant-btn .asset-error {\n    position: absolute;\n    color: #ff0000;\n    left: 43%;\n    bottom: 13px; }\n\n.suivant-btn button.left-aligned {\n    float: left; }\n\n.suivant-btn button.right-aligned {\n    float: right; }\n\n.actif-modal-wrapper {\n  position: relative; }\n\n.actif-modal-wrapper .panel-primary {\n    width: 100%; }\n\n.actif-modal-wrapper .panel-primary .panel-heading {\n      display: flex;\n      justify-content: space-between;\n      align-items: center;\n      text-transform: uppercase;\n      text-decoration: underline;\n      font-weight: bold;\n      font-size: 15px; }\n\n.actif-modal-wrapper .panel-primary .panel-heading .btn {\n        text-transform: none;\n        text-decoration: none;\n        color: #337ab7;\n        background-color: #ffffff;\n        margin: 0 2px; }\n\n.actif-modal-wrapper .panel-primary .panel-heading .btn:hover {\n          background-color: #ebebeb; }\n\n.actif-modal-wrapper form {\n    width: 100%; }\n\n.actif-modal-wrapper .actif-modal-body {\n    width: 100%;\n    position: relative;\n    /* > *:not(:last-child) {\n      margin-bottom: 30px;\n    } */ }\n\n.actif-modal-wrapper .actif-modal-body .error {\n      color: #FF0000;\n      position: absolute; }\n\n.actif-modal-wrapper .actif-modal-body .asset-info-select:disabled {\n      opacity: 0.3;\n      cursor: default; }\n\n.actif-modal-wrapper .actif-modal-body .first-wrapper {\n      padding: 0.5vw 1vw; }\n\n.actif-modal-wrapper .actif-modal-body .first-wrapper input {\n        width: auto; }\n\n.actif-modal-wrapper .actif-modal-body .first-wrapper .form-check-wrapper {\n        margin-top: 15px; }\n\n.actif-modal-wrapper .actif-modal-body .first-wrapper .hint-wrapper {\n        display: inline;\n        float: right; }\n\n.actif-modal-wrapper .actif-modal-body .first-wrapper .hint-wrapper .hint {\n          position: absolute;\n          bottom: -4.7vw;\n          left: 2.7vw;\n          background-color: #428bca;\n          border: 2px solid #fff;\n          visibility: hidden;\n          width: 12vw;\n          color: #fff;\n          height: 10.5vw;\n          font-size: 15px;\n          text-align: left;\n          padding: 0.2vw;\n          border-radius: 0.3vw; }\n\n.actif-modal-wrapper .actif-modal-body .first-wrapper .hint-wrapper .hint ul {\n            padding: 0;\n            margin: 0;\n            list-style: none;\n            position: relative;\n            top: 0.7vw; }\n\n.actif-modal-wrapper .actif-modal-body .first-wrapper .hint-wrapper .hint ul li {\n              margin-bottom: 0.4vw;\n              line-height: 1; }\n\n.actif-modal-wrapper .actif-modal-body .first-wrapper .hint-wrapper .hint ul li span {\n                font-size: 0.75vw;\n                letter-spacing: 0.07vw;\n                margin-bottom: 0.2vw; }\n\n.actif-modal-wrapper .actif-modal-body .first-wrapper .hint-wrapper .hint:before {\n          content: \"\";\n          position: absolute;\n          top: 4.1vw;\n          left: -2.1vw;\n          -webkit-transform: rotate(90deg);\n                  transform: rotate(90deg);\n          border-style: solid;\n          border-width: 1.5vw 1.5vw 0;\n          border-color: #428bca transparent;\n          display: block;\n          width: 0;\n          z-index: 0; }\n\n.actif-modal-wrapper .actif-modal-body .first-wrapper .hint-wrapper .question-wrapper:hover + .hint {\n          visibility: visible; }\n\n.actif-modal-wrapper .actif-modal-body .first-wrapper .hint-wrapper .question-wrapper:hover {\n          cursor: pointer; }\n\n.actif-modal-wrapper .actif-modal-body .first-wrapper .hint-wrapper .question-wrapper fa {\n          font-size: 1.2vw; }\n\n.actif-modal-wrapper .actif-modal-body .first-wrapper .modal-content-header {\n        width: 100%; }\n\n.actif-modal-wrapper .actif-modal-body .first-wrapper .modal-content-header fa {\n          font-size: 1.4vw; }\n\n.actif-modal-wrapper .actif-modal-body .first-wrapper label {\n        font-size: 15px;\n        margin-bottom: 0.5vw;\n        display: block; }\n\n.actif-modal-wrapper .actif-modal-body .first-wrapper label:last-of-type {\n          display: inline-block; }\n\n.actif-modal-wrapper .actif-modal-body .first-wrapper input {\n        margin-right: 0.5vw; }\n\n.actif-modal-wrapper .actif-modal-body .first-wrapper select {\n        font-size: 15px;\n        margin-left: 0.5vw; }\n\n.actif-modal-wrapper .actif-modal-body .second-wrapper {\n      width: 100%; }\n\n.actif-modal-wrapper .actif-modal-body .second-wrapper textarea {\n        resize: none; }\n\n.actif-modal-wrapper .actif-modal-body .second-wrapper .header-content-wrapper {\n        width: 100%;\n        padding: 0.5vw; }\n\n.actif-modal-wrapper .actif-modal-body .second-wrapper .header-content-wrapper .header-input-content-wrapper {\n          margin-top: 1vw;\n          min-width: 98%; }\n\n.actif-modal-wrapper .actif-modal-body .second-wrapper .header-content-wrapper .header-input-content-wrapper .header-content-left div {\n            display: inline-block;\n            margin-bottom: 20px; }\n\n.actif-modal-wrapper .actif-modal-body .second-wrapper .header-content-wrapper .header-input-content-wrapper .header-content-left div:nth-child(2) {\n              width: 50%; }\n\n.actif-modal-wrapper .actif-modal-body .second-wrapper .header-content-wrapper .header-input-content-wrapper .header-content-left div:last-of-type {\n              margin-bottom: 0; }\n\n.actif-modal-wrapper .actif-modal-body .second-wrapper .header-content-wrapper .header-input-content-wrapper .header-content-left div span {\n              margin-right: 1vw;\n              font-size: 15px; }\n\n.actif-modal-wrapper .actif-modal-body .second-wrapper .header-content-wrapper .header-input-content-wrapper .header-content-left div input {\n              width: 18vw; }\n\n.actif-modal-wrapper .actif-modal-body .second-wrapper .header-content-wrapper .header-input-content-wrapper .header-content-right .auther-input {\n            width: 18vw;\n            text-align: center;\n            margin-left: 1vw; }\n\n.actif-modal-wrapper .actif-modal-body .second-wrapper .header-content-wrapper .header-input-content-wrapper .header-content-right .auther-input::-webkit-input-placeholder {\n              color: #ff0000; }\n\n.actif-modal-wrapper .actif-modal-body .second-wrapper .header-content-wrapper .header-input-content-wrapper .header-content-right .auther-input:-ms-input-placeholder {\n              color: #ff0000; }\n\n.actif-modal-wrapper .actif-modal-body .second-wrapper .header-content-wrapper .header-input-content-wrapper .header-content-right .auther-input::-ms-input-placeholder {\n              color: #ff0000; }\n\n.actif-modal-wrapper .actif-modal-body .second-wrapper .header-content-wrapper .header-input-content-wrapper .header-content-right .auther-input::placeholder {\n              color: #ff0000; }\n\n.actif-modal-wrapper .actif-modal-body .second-wrapper .header-content-wrapper .header-input-content-wrapper .header-content-right div {\n            margin-bottom: 20px;\n            height: 21px; }\n\n.actif-modal-wrapper .actif-modal-body .second-wrapper .header-content-wrapper .header-input-content-wrapper .header-content-right div span:first-of-type {\n              display: inline-block;\n              float: left;\n              margin-right: 0.5vw; }\n\n.actif-modal-wrapper .actif-modal-body .second-wrapper .header-content-wrapper .header-input-content-wrapper .header-content-right div .date-text {\n              position: relative;\n              top: 5px; }\n\n.actif-modal-wrapper .actif-modal-body .second-wrapper .header-content-wrapper .header-input-content-wrapper .header-content-right div .auther-calendar {\n              position: relative;\n              float: left; }\n\n.actif-modal-wrapper .actif-modal-body .second-wrapper .header-content-wrapper .header-input-content-wrapper .header-content-right div .auther-calendar input {\n                border: none;\n                background-color: transparent;\n                padding: 5px 10px;\n                margin: 0;\n                margin-left: 0.5vw;\n                cursor: pointer;\n                pointer-events: visiblePainted !important; }\n\n.actif-modal-wrapper .actif-modal-body .second-wrapper .header-content-wrapper .header-input-content-wrapper .header-content-right div .auther-calendar input[type=\"date\"]::-webkit-calendar-picker-indicator {\n                opacity: 1;\n                display: block;\n                background: url(\"data:image/svg+xml,%3Csvg xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22 viewBox%3D%220 0 448 512%22%3E%3Cpath d%3D%22M436 160H12c-6.6 0-12-5.4-12-12v-36c0-26.5 21.5-48 48-48h48V12c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v52h128V12c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v52h48c26.5 0 48 21.5 48 48v36c0 6.6-5.4 12-12 12zM12 192h424c6.6 0 12 5.4 12 12v260c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V204c0-6.6 5.4-12 12-12zm116 204c0-6.6-5.4-12-12-12H76c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12v-40zm0-128c0-6.6-5.4-12-12-12H76c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12v-40zm128 128c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12v-40zm0-128c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12v-40zm128 128c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12v-40zm0-128c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12v-40z%22%2F%3E%3C%2Fsvg%3E\") no-repeat;\n                color: transparent;\n                width: 20px;\n                height: 20px;\n                position: relative;\n                left: 5px;\n                margin-left: 3px;\n                border-width: thin; }\n\n.actif-modal-wrapper .actif-modal-body .second-wrapper .header-content-wrapper .header-input-content-wrapper .header-content-right div .auther-calendar input[type=\"date\"]::-webkit-calendar-picker-indicator:hover {\n                  cursor: pointer; }\n\n.actif-modal-wrapper .actif-modal-body .second-wrapper .header-content-wrapper .header-input-content-wrapper .header-content-right div .auther-calendar .open-button {\n                position: absolute;\n                top: 3px;\n                right: 0.6vw;\n                width: 25px;\n                height: 25px;\n                background: #fff;\n                pointer-events: none; }\n\n.actif-modal-wrapper .actif-modal-body .second-wrapper .header-content-wrapper .header-input-content-wrapper .header-content-right div .auther-calendar .open-button:hover {\n                  cursor: pointer; }\n\n.actif-modal-wrapper .actif-modal-body .second-wrapper .header-content-wrapper .header-input-content-wrapper .header-content-right div .auther-calendar .open-button button {\n                border: none;\n                background: transparent; }\n\n.actif-modal-wrapper .actif-modal-body .second-wrapper .header-content-wrapper .header-input-content-wrapper .header-content-right div .auther-calendar .open-button button:hover {\n                  cursor: pointer; }\n\n.actif-modal-wrapper .actif-modal-body .second-wrapper .header-content-wrapper .header-input-content-wrapper .header-content-right div .auther-calendar .auther-input {\n                width: 100%; }\n\n.actif-modal-wrapper .actif-modal-body .second-wrapper .descriptif-wrapper {\n        width: 100%;\n        padding: 0 0.5vw; }\n\n.actif-modal-wrapper .actif-modal-body .second-wrapper .descriptif-wrapper span {\n          font-size: 15px; }\n\n.actif-modal-wrapper .actif-modal-body .second-wrapper .descriptif-wrapper textarea {\n          height: 130px;\n          width: 100%;\n          padding: 0 0.5vw; }\n\n.actif-modal-wrapper .actif-modal-body .second-wrapper .statut-wrapper, .actif-modal-wrapper .actif-modal-body .second-wrapper .type-wrapper {\n        padding: 0.5vw;\n        width: 100%; }\n\n.actif-modal-wrapper .actif-modal-body .second-wrapper .statut-wrapper select, .actif-modal-wrapper .actif-modal-body .second-wrapper .type-wrapper select {\n          margin-right: 10px;\n          height: 35px;\n          width: 100%; }\n\n.actif-modal-wrapper .actif-modal-body .second-wrapper .statut-wrapper span, .actif-modal-wrapper .actif-modal-body .second-wrapper .type-wrapper span {\n          font-size: 15px;\n          width: 50px;\n          display: inline-block; }\n\n.actif-modal-wrapper .actif-modal-body .third-wrapper {\n      width: 100%; }\n\n.actif-modal-wrapper .actif-modal-body .third-wrapper #partner-textarea, .actif-modal-wrapper .actif-modal-body .third-wrapper #contributors-textarea {\n        padding-left: 5px;\n        margin: 0; }\n\n.actif-modal-wrapper .actif-modal-body .third-wrapper #partner-textarea, .actif-modal-wrapper .actif-modal-body .third-wrapper #contributors-textarea {\n        resize: none; }\n\n.actif-modal-wrapper .actif-modal-body .third-wrapper #contributors-textarea {\n        resize: none; }\n\n.actif-modal-wrapper .actif-modal-body .third-wrapper .content-wrapper {\n        width: 100%; }\n\n.actif-modal-wrapper .actif-modal-body .third-wrapper .content-wrapper:nth-child(1), .actif-modal-wrapper .actif-modal-body .third-wrapper .content-wrapper:nth-child(4), .actif-modal-wrapper .actif-modal-body .third-wrapper .content-wrapper:nth-child(5), .actif-modal-wrapper .actif-modal-body .third-wrapper .content-wrapper:nth-child(6) {\n          height: 60px; }\n\n.actif-modal-wrapper .actif-modal-body .third-wrapper .content-wrapper:nth-child(2) {\n          height: 90px; }\n\n.actif-modal-wrapper .actif-modal-body .third-wrapper .content-wrapper:last-child {\n          height: auto; }\n\n.actif-modal-wrapper .actif-modal-body .third-wrapper .content-wrapper span {\n          display: inline-block;\n          float: left;\n          margin-right: 10px; }\n\n.actif-modal-wrapper .actif-modal-body .third-wrapper .content-wrapper .left-side-content {\n          width: 50%;\n          display: inline-block;\n          float: left; }\n\n.actif-modal-wrapper .actif-modal-body .third-wrapper .content-wrapper .left-side-content .caption-text {\n            width: 110px; }\n\n.actif-modal-wrapper .actif-modal-body .third-wrapper .content-wrapper .left-side-content .checkbox-wrapper input {\n            height: auto;\n            width: auto;\n            margin-left: 0; }\n\n.actif-modal-wrapper .actif-modal-body .third-wrapper .content-wrapper .left-side-content.efforts {\n            display: flex;\n            flex-direction: column;\n            float: none; }\n\n.actif-modal-wrapper .actif-modal-body .third-wrapper .content-wrapper .left-side-content.efforts button {\n              width: 50%; }\n\n.actif-modal-wrapper .actif-modal-body .third-wrapper .content-wrapper .left-side-content.efforts input.effort-number {\n              width: 20%; }\n\n.actif-modal-wrapper .actif-modal-body .third-wrapper .content-wrapper .right-side-content {\n          width: 50%;\n          position: relative;\n          display: inline-block;\n          float: right; }\n\n.actif-modal-wrapper .actif-modal-body .third-wrapper .content-wrapper #complex-right-content input[type='number'] {\n          width: 25%;\n          margin-left: 10px; }\n\n.actif-modal-wrapper .actif-modal-body .third-wrapper .content-wrapper #complex-right-content div {\n          width: 100%; }\n\n.actif-modal-wrapper .actif-modal-body .third-wrapper .deliverableReference:disabled {\n        background-color: #ebebe4; }\n\n.actif-modal-wrapper .actif-modal-body .fourth-wrapper {\n      width: 100%; }\n\n.actif-modal-wrapper .actif-modal-body .fourth-wrapper h2 {\n        text-decoration: underline;\n        text-transform: uppercase;\n        font-weight: bold;\n        margin-left: 15px; }\n\n.actif-modal-wrapper .actif-modal-body .fourth-wrapper textarea {\n        resize: none; }\n\n.actif-modal-wrapper .actif-modal-body .fourth-wrapper .suivant-btn {\n        margin: 0; }\n\n.actif-modal-wrapper .actif-modal-body .fourth-wrapper .fields-wrapper {\n        margin: 0 auto;\n        padding: 0.4vw;\n        width: 100%; }\n\n.actif-modal-wrapper .actif-modal-body .fourth-wrapper .fields-wrapper .second-main-wrapper {\n          padding: 0.4vw; }\n\n.actif-modal-wrapper .actif-modal-body .fourth-wrapper .fields-wrapper .second-main-wrapper .dependances-text {\n            display: block;\n            font-weight: bold; }\n\n.actif-modal-wrapper .actif-modal-body .fourth-wrapper .fields-wrapper .second-main-wrapper textarea {\n            padding: 8px 10px; }\n\n.actif-modal-wrapper .actif-modal-body .fourth-wrapper .fields-wrapper .second-main-wrapper .mots-cles div {\n            margin-bottom: 0.6vw; }\n\n.actif-modal-wrapper .actif-modal-body .fourth-wrapper .fields-wrapper .second-main-wrapper .acteur-porteur label input {\n            width: auto;\n            margin-right: 1vw;\n            display: inline-block;\n            float: left; }\n\n.actif-modal-wrapper .actif-modal-body .fourth-wrapper .fields-wrapper .second-main-wrapper .reutilisables span {\n            display: inline-block;\n            float: left;\n            margin-right: 1vw; }\n\n.actif-modal-wrapper .actif-modal-body .fourth-wrapper .fields-wrapper .second-main-wrapper .reutilisables label {\n            margin-right: 0.5vw; }\n\n.actif-modal-wrapper .actif-modal-body .fourth-wrapper .fields-wrapper .second-main-wrapper .reutilisables label input {\n              width: auto;\n              margin: 0; }\n\n.actif-modal-wrapper .actif-modal-body .fourth-wrapper .fields-wrapper .second-main-wrapper .dependances span {\n            margin-bottom: 15px; }\n\n.actif-modal-wrapper .actif-modal-body .fourth-wrapper .fields-wrapper .second-main-wrapper .dependances div {\n            margin-bottom: 0.4vw; }\n\n.actif-modal-wrapper .actif-modal-body .fourth-wrapper .fields-wrapper .second-main-wrapper .dependances div input {\n              line-height: initial; }\n\n.actif-modal-wrapper .actif-modal-body .fourth-wrapper .fields-wrapper .second-main-wrapper .references {\n            margin-top: 1vw; }\n\n.actif-modal-wrapper .actif-modal-body .fourth-wrapper .fields-wrapper .second-main-wrapper .references div input {\n              line-height: initial; }\n\n.actif-modal-wrapper .actif-modal-body .fourth-wrapper .fields-wrapper .second-main-wrapper .descriptif-detaille {\n            position: relative; }\n\n.actif-modal-wrapper .actif-modal-body .fourth-wrapper .fields-wrapper .second-main-wrapper .descriptif-detaille div {\n              position: relative; }\n\n.actif-modal-wrapper .actif-modal-body .fourth-wrapper .fields-wrapper .second-main-wrapper .descriptif-detaille div span .error {\n                width: auto;\n                position: absolute;\n                right: -0.5vw;\n                top: 0;\n                margin: 0; }\n\n.actif-modal-wrapper .actif-modal-body .fourth-wrapper .fields-wrapper .second-main-wrapper .descriptif-detaille textarea {\n              height: 100px; }\n\n.actif-modal-wrapper .actif-modal-body .fourth-wrapper .btn-wrapper button {\n        margin-bottom: 0.5vw; }\n\n.actif-modal-wrapper .actif-modal-body .fifth-wrapper .classification-wrapper {\n      width: 100%;\n      padding: 0.5vw; }\n\n.actif-modal-wrapper .actif-modal-body .fifth-wrapper .classification-wrapper .custom-control-wrapper {\n        display: inline-block;\n        width: 100%; }\n\n.actif-modal-wrapper .actif-modal-body .fifth-wrapper .classification-wrapper .custom-control-wrapper label {\n          text-align: center;\n          padding: 0; }\n\n.actif-modal-wrapper .actif-modal-body .fifth-wrapper .classification-wrapper .custom-control-wrapper label input {\n            margin: 0;\n            margin-right: 0.5vw;\n            width: auto; }\n\n.actif-modal-wrapper .actif-modal-body .fifth-wrapper .classification-wrapper .perspective-wrapper, .actif-modal-wrapper .actif-modal-body .fifth-wrapper .classification-wrapper .comment-wrapper {\n        margin-top: 10px; }\n\n.actif-modal-wrapper .actif-modal-body .fifth-wrapper .classification-wrapper .asset-card-status {\n        margin-left: 20px;\n        font-weight: normal; }\n\n.actif-modal-wrapper .actif-modal-body .fifth-wrapper .classification-wrapper .asset-card-status.validated {\n          color: green;\n          font-weight: bolder; }\n\n.actif-modal-wrapper .actif-modal-body .sixth-wrapper {\n      text-align: center;\n      padding-top: 50px; }\n\n.actif-modal-wrapper .header-content-wrapper {\n    width: 100%;\n    padding: 0.5vw; }\n\n.actif-modal-wrapper .header-content-wrapper .header-input-content-wrapper {\n      margin-top: 1vw;\n      width: 90%; }\n\n.actif-modal-wrapper .header-content-wrapper .header-input-content-wrapper .header-content-left div {\n        margin-bottom: 0.4vw; }\n\n.actif-modal-wrapper .header-content-wrapper .header-input-content-wrapper .header-content-left div:last-of-type {\n          margin-bottom: 0; }\n\n.actif-modal-wrapper .header-content-wrapper .header-input-content-wrapper .header-content-left div span {\n          margin-right: 1vw;\n          font-size: 15px; }\n\n.actif-modal-wrapper .header-content-wrapper .header-input-content-wrapper .header-content-left div input {\n          position: relative; }\n\n.actif-modal-wrapper .header-content-wrapper .header-input-content-wrapper .header-content-right .auther-input {\n        text-align: center;\n        margin-left: 1vw; }\n\n.actif-modal-wrapper .header-content-wrapper .header-input-content-wrapper .header-content-right .auther-input::-webkit-input-placeholder {\n          color: #ff0000; }\n\n.actif-modal-wrapper .header-content-wrapper .header-input-content-wrapper .header-content-right .auther-input:-ms-input-placeholder {\n          color: #ff0000; }\n\n.actif-modal-wrapper .header-content-wrapper .header-input-content-wrapper .header-content-right .auther-input::-ms-input-placeholder {\n          color: #ff0000; }\n\n.actif-modal-wrapper .header-content-wrapper .header-input-content-wrapper .header-content-right .auther-input::placeholder {\n          color: #ff0000; }\n\n.actif-modal-wrapper .header-content-wrapper .header-input-content-wrapper .header-content-right .user-name {\n        -webkit-user-select: none;\n           -moz-user-select: none;\n            -ms-user-select: none;\n                user-select: none;\n        display: inline-block;\n        overflow: hidden;\n        white-space: nowrap;\n        text-overflow: ellipsis;\n        font-weight: normal; }\n\n.actif-modal-wrapper .header-content-wrapper .header-input-content-wrapper .header-content-right div .auther-calendar .auther-input {\n        width: 100%; }\n\n.actif-modal-wrapper .header-content-wrapper .header-input-content-wrapper .header-content-right div .auther-calendar fa {\n        font-size: 1vw;\n        margin-left: 0.7vw; }\n\n.actif-modal-wrapper .descriptif-wrapper {\n    width: 100%;\n    padding: 0 0.5vw; }\n\n.actif-modal-wrapper .descriptif-wrapper span {\n      font-size: 15px; }\n\n.actif-modal-wrapper .descriptif-wrapper textarea {\n      width: 100%;\n      padding: 0 0.5vw; }\n\n.actif-modal-wrapper .asset-status-wrapper {\n    width: 100%;\n    padding: 0 .5vw;\n    margin: 10px 0; }\n\n.actif-modal-wrapper .asset-status-wrapper select {\n      margin-left: 20px; }\n\n.actif-modal-wrapper .statut-wrapper, .actif-modal-wrapper .actif-modal-body .second-wrapper .type-wrapper, .actif-modal-wrapper .type-wrapper {\n    padding: 0.5vw;\n    width: 100%;\n    margin-bottom: 10px; }\n\n.actif-modal-wrapper .statut-wrapper select, .actif-modal-wrapper .actif-modal-body .second-wrapper .type-wrapper select, .actif-modal-wrapper .type-wrapper select {\n      height: 35px; }\n\n.actif-modal-wrapper .statut-wrapper span, .actif-modal-wrapper .actif-modal-body .second-wrapper .type-wrapper span, .actif-modal-wrapper .type-wrapper span {\n      margin-right: 10px;\n      font-size: 15px; }\n\n.actif-modal-wrapper .type-wrapper {\n    margin-bottom: 0; }\n\n.actif-modal-wrapper .type-wrapper select {\n      height: 35px; }\n\n.actif-modal-wrapper .content-wrapper {\n    padding: 0.5vw;\n    width: 100%; }\n\n.actif-modal-wrapper .content-wrapper span {\n      width: auto;\n      font-size: 15px; }\n\n.actif-modal-wrapper .content-wrapper .checkbox-wrapper input {\n      height: 1.2vw;\n      width: 1.2vw;\n      margin-left: 1vw; }\n\n.actif-modal-wrapper .content-wrapper textarea {\n      padding: 0 0.5vw; }\n\n.actif-modal-wrapper .content-wrapper .right-side-content {\n      width: 40%; }\n\n.actif-modal-wrapper .content-wrapper .right-side-content.contributeur {\n        float: none !important; }\n\n.actif-modal-wrapper .content-wrapper .right-side-content .contributeur-wrapper .efforts-list-wrapper {\n        display: flex;\n        align-items: center;\n        justify-content: space-between;\n        margin: 0.5vw;\n        padding: 0.5vw;\n        border: 1px solid #0062cc;\n        background-color: #fff;\n        border-radius: 5px;\n        width: calc(100% - 1vw) !important; }\n\n.actif-modal-wrapper .content-wrapper .right-side-content .contributeur-wrapper .efforts-list-wrapper:last-of-type {\n          margin-bottom: 1.5vw; }\n\n.actif-modal-wrapper .content-wrapper .right-side-content .contributeur-wrapper .efforts-list-wrapper span {\n          width: 40%; }\n\n.asset-actions ::ng-deep.ui-treetable thead {\n  display: none; }\n\n.asset-actions ::ng-deep.ui-treetable tbody {\n  border: none;\n  padding: 3px; }\n\n.asset-actions ::ng-deep.ui-treetable tbody:not(:last-child) {\n    border-bottom: 1px solid lightgray; }\n\n.asset-actions ::ng-deep.ui-treetable .ui-treetable-toggler {\n  display: none; }\n\n.asset-actions ::ng-deep.ui-treetable .ui-treetable-tablewrapper {\n  border: 1px solid lightgray;\n  border-radius: 0; }\n\n.asset-actions ::ng-deep.ui-treetable tbody .ui-treetable-row td {\n  white-space: normal; }\n\n.asset-actions ::ng-deep.ui-treetable .td-flex-wrapper {\n  display: flex;\n  flex-flow: column nowrap;\n  justify-content: center;\n  align-items: center; }\n\n.asset-actions ::ng-deep.ui-treetable .td-flex-wrapper > *:not(:last-child) {\n    margin-bottom: 10px; }\n\n.asset-actions ::ng-deep.ui-treetable .td-flex-wrapper.inline-content {\n    flex-flow: row nowrap; }\n\n.asset-actions ::ng-deep.ui-treetable .td-flex-wrapper.inline-content > *:not(:last-child) {\n      margin-right: 20px; }\n\n.asset-actions ::ng-deep.ui-treetable .td-flex-wrapper.left-aligned-content {\n    align-items: flex-start; }\n\n.asset-actions ::ng-deep.ui-treetable .td-flex-wrapper.right-aligned-content {\n    align-items: flex-end; }\n\n.asset-actions .panel-heading {\n  margin: 0; }\n\n.asset-actions .tools .status.sticker {\n  height: 12px;\n  width: 12px;\n  border-radius: 50%;\n  background-color: orangered; }\n\n.asset-actions .tools .status.sticker.done {\n    background-color: #10aa10; }\n\n.asset-actions .tools .user-icon {\n  cursor: pointer; }\n\n.asset-actions .tools .validate-button {\n  cursor: pointer;\n  padding: 2px 10px;\n  border-radius: 3px;\n  color: white;\n  background-color: #10aa10; }\n\n.asset-actions-wrapper .flex-row {\n  display: flex;\n  flex-flow: row nowrap; }\n\n.asset-actions-wrapper .action-field {\n  padding: 0 20px; }\n\n.asset-actions-wrapper .action-field:not(:last-child) {\n    margin-bottom: 10px; }\n\n.asset-actions-wrapper select {\n  padding: 5px; }\n\n.asset-actions-wrapper .action-deadline {\n  padding: 3px; }\n\n.asset-actions-wrapper .action-validate-button {\n  padding: 5px 10px;\n  background: #10aa10;\n  color: whitesmoke;\n  border-radius: 3px;\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content; }\n\nsmite-modal .title {\n  text-transform: uppercase;\n  font-weight: 600;\n  font-size: 2.3rem; }\n\nsmite-modal .content {\n  color: #252525;\n  margin-top: 20px; }\n\nsmite-modal .content .flex-col {\n    flex: 1;\n    display: flex;\n    flex-flow: column nowrap;\n    justify-content: flex-start;\n    align-items: flex-start; }\n\nsmite-modal .content .label {\n    color: #252525;\n    padding: none; }\n\nsmite-modal .content textarea {\n    height: 100px; }\n\nsmite-modal .smite-modal-controls {\n  flex: 1;\n  display: flex;\n  flex-flow: column nowrap;\n  justify-content: flex-end;\n  align-items: flex-end; }\n\nsmite-modal .smite-modal-controls .validate {\n    padding: 3px 10px;\n    background-color: #10aa10;\n    width: -webkit-fit-content;\n    width: -moz-fit-content;\n    width: fit-content;\n    color: whitesmoke;\n    border-radius: 3px;\n    cursor: pointer; }\n"

/***/ }),

/***/ "./src/app/new-asset/new-asset.component.ts":
/*!**************************************************!*\
  !*** ./src/app/new-asset/new-asset.component.ts ***!
  \**************************************************/
/*! exports provided: NewAssetComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewAssetComponent", function() { return NewAssetComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_local_storage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/local-storage.service */ "./src/app/services/local-storage.service.ts");
/* harmony import */ var _services_requests_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/requests.service */ "./src/app/services/requests.service.ts");
/* harmony import */ var _services_pass_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/pass.service */ "./src/app/services/pass.service.ts");
/* harmony import */ var _smite_modals__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../smite-modals */ "./src/app/smite-modals/index.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var NewAssetComponent = /** @class */ (function () {
    function NewAssetComponent(formBuilder, requests, storageService, router, modals, passService, datePipe) {
        this.formBuilder = formBuilder;
        this.requests = requests;
        this.storageService = storageService;
        this.router = router;
        this.modals = modals;
        this.passService = passService;
        this.datePipe = datePipe;
        this.currAsset = {};
        this.assetTypes = [];
        this.classificationTypes = [];
        this.activityType = {};
        this.classificationType = {
            _id: ''
        };
        this.isAssetValidated = false;
        this.currWrapper = 1;
        this.user = {};
        this.parentAssets = [];
        this.resAsset = {};
        this.currentProject = {};
        this.assetActions = [];
        this.programsValues = ['IC', 'IA', 'TI', 'TA', 'DT'];
        this.initProjectValues = {
            'IC': ['EIC', 'Io4', 'STC', 'Autre'],
            'IA': ['AMC', 'CDF', 'DSL', 'ISC', 'OAR', 'TOP', 'SIM', 'Autre'],
            'TI': ['BFI', 'BST', 'HCU', 'IVA', 'LCE', 'MSM', 'SCE', 'Autre'],
            'TA': ['API', 'CMI', 'CTI', 'ELA', 'EVA', 'FSF', 'ISE', 'LRA', 'PST', 'SCA', 'SVA', 'TAS', 'Autre'],
            'DT': ['Transverse']
        };
        this.projectValues = this.initProjectValues[this.programsValues[0]].slice();
        this.isActeurOrPorteurExists = false;
        this.firstChangeName = true;
        this.maxWrapper = 5;
        this.editingAsset = false;
        this.programSelectValues = [];
        this.statusType = {
            _id: ''
        };
        this.assetStatusType = [];
        this.programValue = {};
        this.projectValue = {};
        this.effortList = [];
        this.availableEffortList = [];
        this.availableUnitsList = {};
        this.efforts = {
            efforts: [],
            contributors: ''
        };
        this.maxWrapper = this.isAdmin() ? 5 : 4;
    }
    NewAssetComponent.prototype.noWhitespaceValidator = function (control) {
        var isWhitespace = (control.value || '').trim().length === 0;
        var isValid = !isWhitespace;
        return isValid ? null : { 'whitespace': true };
    };
    NewAssetComponent.prototype.deleteEffort = function (index) {
        if (confirm('Etes-vous sûr de vouloir supprimer cet élément?')) {
            this.availableEffortList.splice(index, 1);
            this.addEffort(-1);
        }
    };
    NewAssetComponent.prototype.isSelected = function (unitId, effortUnit, index) {
        if (unitId && effortUnit['efforts'] && this.efforts['efforts'][index] && this.efforts['efforts'][index].unit.id) {
            return unitId === effortUnit['efforts'][index].unit.id;
        }
    };
    NewAssetComponent.prototype.addEffort = function (val) {
        var _this = this;
        var effortsArray = [];
        if (typeof val === 'number') {
            if (val !== -1) {
                this.availableUnitsList[this.effortList[val].type] = this.effortList[val].unit;
                this.availableEffortList.push(this.effortList[val]);
            }
            setTimeout(function () {
                var typeUnit = document.getElementsByClassName('effort-name-select');
                var values = document.getElementsByClassName('effort-number');
                var arr = Array.from(typeUnit);
                var effort = {};
                var unit;
                var _loop_1 = function (i) {
                    var temp = typeUnit[i]['value'].split('/');
                    _this.availableUnitsList[temp[0]].forEach(function (item) {
                        if (item.id === +temp[1]) {
                            unit = item.id;
                        }
                    });
                    if (temp && _this.availableUnitsList[temp[0]]) {
                        effort = {
                            type: temp[2],
                            unit: unit,
                            value: +values[i]['value'] || 0
                        };
                        console.log('effort', effort);
                        effortsArray.push(effort);
                    }
                };
                for (var i = 0; i < arr.length; i++) {
                    _loop_1(i);
                }
                _this.efforts['efforts'] = effortsArray.slice();
            }, 0);
        }
        else if (val === 'number') {
            var typeUnit = document.getElementsByClassName('effort-name-select');
            var arr = Array.from(typeUnit);
            var values = document.getElementsByClassName('effort-number');
            for (var i = 0; i < arr.length; i++) {
                if (this.efforts['efforts'].length) {
                    this.efforts['efforts'][i]['value'] = +values[i]['value'];
                }
            }
        }
        else {
            this.efforts['contributors'] = document.getElementById('contributors-textarea')['value'];
        }
    };
    NewAssetComponent.prototype.prevWrapper = function () {
        if (this.currWrapper > 1) {
            this.currWrapper--;
        }
    };
    NewAssetComponent.prototype.nextWrapper = function () {
        if (this.currWrapper < this.maxWrapper) {
            this.currWrapper++;
        }
    };
    NewAssetComponent.prototype.onChancelBtnClick = function () {
        this.router.navigate(['main']);
    };
    NewAssetComponent.prototype.isProgramValueEmpty = function () {
        return !Object.keys(this.programValue).length || !this.programValue.projects.length;
    };
    NewAssetComponent.prototype.isCurrentAsset = function () {
        return Object.keys(this.currAsset).length;
    };
    NewAssetComponent.prototype.onAssetFormSubmit = function () {
        var _this = this;
        this.assetErrorMessage = '';
        var inpAsset = this.newAssetForm.value;
        var newAsset = {};
        var currAssUserId;
        if (Object.keys(this.currAsset).length > 0) {
            newAsset.id = this.currAsset.id;
            currAssUserId = this.currAsset.identification.userId;
            newAsset.children = this.currAsset.children;
        }
        this.user = this.storageService.getUser();
        if (!this.user) {
            this.assetErrorMessage = 'Login error. Please, log in';
        }
        newAsset.identification = {
            name: inpAsset.assetName,
            programId: inpAsset.programName._id,
            projectId: this.projectValue._id,
            userId: currAssUserId ? currAssUserId : this.user.id,
            creationDate: this.currentDate ? this.currentDate : new Date()
        };
        newAsset.desc = inpAsset.desc;
        // Get asset status from form
        newAsset.status = this.statusType._id;
        newAsset.assetStatus = inpAsset.assetCardStatus;
        if (this.activityType['name'] === '- Tous -') {
            this.activityType['name'] = '';
        }
        newAsset.type = this.activityType._id;
        newAsset.deliverable = {
            exists: inpAsset.deliverableExists,
            reference: inpAsset.deliverableReference
        };
        newAsset.piShared = {
            exists: inpAsset.piSharedExists,
            partners: inpAsset.piSharedPartners
        };
        if (this.statusType['name'] === '- Tous -') {
            this.statusType['name'] = '';
        }
        newAsset.efforts = this.efforts;
        newAsset.copyright = {
            exists: inpAsset.copyrightExists,
            reference: inpAsset.copyrightReference
        };
        newAsset.patent = {
            exists: inpAsset.patentExists,
            reference: inpAsset.patentReference
        };
        newAsset.demonstration = inpAsset.demonstration;
        newAsset.repository = inpAsset.repository;
        newAsset.label = inpAsset.label;
        newAsset.detailedDescription = inpAsset.detailedDescription;
        newAsset.dependencies = {
            licenses: inpAsset.dependenciesLicenses,
            hardware: inpAsset.dependenciesHardware,
            reusable: inpAsset.dependenciesReusable,
            actor: inpAsset.dependenciesActor,
        };
        newAsset.linkedPlatforms = inpAsset.linkedPlatforms;
        newAsset.keywords = inpAsset.keywords;
        newAsset.isPlatformAsset = inpAsset.isPlatformAsset === 'Démonstrateur';
        if (newAsset.isPlatformAsset) {
            newAsset.parentAssetId = '0';
        }
        else {
            newAsset.parentAssetId = inpAsset.parentAssetId || '0';
        }
        newAsset.classification = {
            type: this.classificationType._id,
            perspective: inpAsset.classificationPerspective,
            comment: inpAsset.assetComment
        };
        console.log('=========>', newAsset);
        if (Object.keys(this.currAsset).length === 0) {
            this.requests.addAsset(newAsset).then(function (res) {
                console.log(newAsset.identification);
                console.log('new asset res => ', res);
                _this.resAsset = res;
                _this.currWrapper = 6;
            }).catch(function (err) {
                console.error('new asset error => ', err);
                if (err.status === 403) {
                    _this.assetErrorMessage = 'Invalid fields';
                }
                else if (err.status == 0) {
                    _this.assetErrorMessage = 'Connection error. Please, check your internet connection';
                }
                else if (err.status === 401) {
                    _this.assetErrorMessage = 'Login error. Please, log in';
                }
            });
        }
        else {
            this.requests.editAsset(newAsset).then(function (res) {
                console.log('new asset res => ', res);
                _this.resAsset = res;
                _this.editingAsset = false;
                _this.currWrapper = 6;
            }).catch(function (err) {
                console.error('new asset error => ', err);
                if (err.status === 403) {
                    _this.assetErrorMessage = 'Invalid fields';
                }
                else if (err.status == 0) {
                    _this.assetErrorMessage = 'Connection error. Please, check your internet connection';
                }
                else if (err.status === 401) {
                    _this.assetErrorMessage = 'Login error. Please, log in';
                }
            });
        }
    };
    NewAssetComponent.prototype.firsSelectChange = function () {
        if (this.newAssetForm.value.isPlatformAsset !== 'A partir d\'un actif valorisable existant') {
            this.newAssetForm.controls['parentAssetId'].disable();
        }
        else {
            this.newAssetForm.controls['parentAssetId'].enable();
        }
    };
    NewAssetComponent.prototype.areFieldsInvalidForNextPage = function () {
        if (this.currWrapper === 2 && this.isProgramValueEmpty()) {
            return true;
        }
        if (this.currWrapper === 3 && !this.availableEffortList.length) {
            return true;
        }
        return this.currWrapper === 2 && !this.assetName.valid || this.currWrapper === 3 && this.newAssetForm['mandatoryFieldsInvalid'];
    };
    NewAssetComponent.prototype.mandatoryRadioValueChanged = function () {
        var controls = this.newAssetForm;
        var errors = {};
        if (this.deliverableExists.value === true && (!this.deliverableReference.value || this.deliverableReference.value == '')) {
            errors['deliverableMandatory'] = true;
        }
        if (this.piSharedExists.value === true && (!this.piSharedPartners.value || this.piSharedPartners.value == '')) {
            errors['piSharedMandatory'] = true;
        }
        if (this.copyrightExists.value === true && (!this.copyrightReference.value || this.copyrightReference.value == '')) {
            errors['copyrightMandatory'] = true;
        }
        if (this.patentExists.value === true && (!this.patentReference.value || this.patentReference.value == '')) {
            errors['patentMandatory'] = true;
        }
        if (Object.keys(errors).length > 0) {
            controls['mandatoryFieldsInvalid'] = true;
            return errors;
        }
        else {
            controls['mandatoryFieldsInvalid'] = false;
            return null;
        }
    };
    NewAssetComponent.prototype.getStatusDisplayString = function () {
        var value = this.newAssetForm.controls.assetCardStatus.value;
        if (value === 'PENDING')
            return 'En cours de rédaction';
        if (value === 'WAITING_VALIDATION')
            return 'En attente de validation Codir';
        if (value === 'VALIDATED')
            return 'Validée';
    };
    NewAssetComponent.prototype.validateAssetCard = function () {
        this.newAssetForm.patchValue({ assetCardStatus: 'VALIDATED' });
        this.isAssetValidated = true;
    };
    Object.defineProperty(NewAssetComponent.prototype, "desc", {
        get: function () {
            return this.newAssetForm.get('desc');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NewAssetComponent.prototype, "assetName", {
        get: function () {
            return this.newAssetForm.get('assetName');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NewAssetComponent.prototype, "programName", {
        get: function () {
            return this.newAssetForm.get('programName');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NewAssetComponent.prototype, "projectName", {
        get: function () {
            return this.newAssetForm.get('projectName');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NewAssetComponent.prototype, "creationDate", {
        get: function () {
            return this.newAssetForm.get('creationDate');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NewAssetComponent.prototype, "effortHa", {
        get: function () {
            return this.newAssetForm.get('effortHa');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NewAssetComponent.prototype, "effortJh", {
        get: function () {
            return this.newAssetForm.get('effortJh');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NewAssetComponent.prototype, "effortHm", {
        get: function () {
            return this.newAssetForm.get('effortHm');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NewAssetComponent.prototype, "effortContributors", {
        get: function () {
            return this.newAssetForm.get('effortContributors');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NewAssetComponent.prototype, "piSharedPartners", {
        get: function () {
            return this.newAssetForm.get('piSharedPartners');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NewAssetComponent.prototype, "piSharedExists", {
        get: function () {
            return this.newAssetForm.get('piSharedExists');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NewAssetComponent.prototype, "deliverableExists", {
        get: function () {
            return this.newAssetForm.get('deliverableExists');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NewAssetComponent.prototype, "deliverableReference", {
        get: function () {
            return this.newAssetForm.get('deliverableReference');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NewAssetComponent.prototype, "copyrightReference", {
        get: function () {
            return this.newAssetForm.get('copyrightReference');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NewAssetComponent.prototype, "copyrightExists", {
        get: function () {
            return this.newAssetForm.get('copyrightExists');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NewAssetComponent.prototype, "patentReference", {
        get: function () {
            return this.newAssetForm.get('patentReference');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NewAssetComponent.prototype, "patentExists", {
        get: function () {
            return this.newAssetForm.get('patentExists');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NewAssetComponent.prototype, "demonstration", {
        get: function () {
            return this.newAssetForm.get('demonstration');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NewAssetComponent.prototype, "repository", {
        get: function () {
            return this.newAssetForm.get("repository");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NewAssetComponent.prototype, "detailedDescription", {
        get: function () {
            return this.newAssetForm.get('detailedDescription');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NewAssetComponent.prototype, "assetCardStatus", {
        get: function () {
            return this.newAssetForm.get("assetCardStatus");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NewAssetComponent.prototype, "assetStatus", {
        get: function () {
            return this.newAssetForm.get("assetStatus");
        },
        enumerable: true,
        configurable: true
    });
    NewAssetComponent.prototype.getDate = function (date) {
        return new Date(date).toLocaleDateString();
    };
    NewAssetComponent.prototype.isAdmin = function () {
        return this.passService.isAdmin();
    };
    NewAssetComponent.prototype.isActeurOrPorteur = function (event) {
        this.isActeurOrPorteurExists = event.target.checked;
    };
    Object.defineProperty(NewAssetComponent.prototype, "notDoneAssetActions", {
        get: function () {
            return this.assetActions.filter(function (item) { return !item.done; });
        },
        enumerable: true,
        configurable: true
    });
    NewAssetComponent.prototype.validateAction = function (actionId) {
        this.actionToValidate = this.assetActions.find(function (item) { return item.id == actionId; });
        this.modals.open('create-update-action');
    };
    NewAssetComponent.prototype.createAction = function () {
        var _this = this;
        // Get form values and create
        var actionForm = this.newActionForm.controls;
        var action = {
            type: actionForm.actionType.value,
            relatedResource: {
                type: 'ASSET',
                id: this.currAsset.id
            },
            description: actionForm.actionDesc.value,
            creator: this.storageService.getUser().firstname,
            assignees: actionForm.actionAssignee.value,
            createdAt: new Date(),
            dueDate: actionForm.actionDate.value,
            done: false,
            archived: false
        };
        this.requests.createAction(action).subscribe(function (data) {
            _this.newActionForm.reset();
        });
    };
    NewAssetComponent.prototype.updateAction = function ($event) {
        var text = document.querySelector('#create-update-action .content textarea');
        this.actionToValidate.comment.push({
            date: new Date(),
            author: this.storageService.getUser().firstName,
            text: text.value
        });
        this.actionToValidate.done = true;
        this.requests.updateAction(this.actionToValidate.id, this.actionToValidate).then(function (data) {
            console.log(data);
        });
        this.modals.close('create-update-action');
    };
    NewAssetComponent.prototype.findSelectedAssetStatus = function () {
        var _this = this;
        return this.assetStatusType.find(function (i) {
            if (i._id === _this.currAsset.status) {
                return i;
            }
        });
    };
    NewAssetComponent.prototype.findSelectedAssetType = function () {
        var _this = this;
        return this.assetTypes.find(function (i) {
            if (i._id === _this.currAsset.type) {
                return i;
            }
        });
    };
    NewAssetComponent.prototype.getData = function () {
        var _this = this;
        this.requests.getUserProgramsList().then(function (res) {
            _this.programSelectValues = res;
            if (Object.keys(_this.currAsset).length) {
                _this.newAssetForm.patchValue({ programName: _this.getSelectedProgram(_this.currAsset.identification.programId) });
                _this.newAssetForm.patchValue({ projectName: _this.getSelectedProject() });
                _this.newAssetForm.patchValue({ creationDate: _this.datePipe.transform(_this.currAsset.identification.creationDate) });
                _this.currentDate = _this.currAsset.identification.creationDate;
                _this.newAssetForm.patchValue({ assetCardStatus: _this.currAsset.assetStatus });
                if (_this.currAsset.assetStatus === 'VALIDATED') {
                    _this.isAssetValidated = true;
                }
            }
        }).catch(function (err) {
            console.log(err);
        });
        this.requests.getItems('statusList').then(function (res) {
            _this.assetStatusType = Object.values(res).map(function (i) { return i; });
            _this.assetStatusType.unshift({ name: '- Tous -', _id: '' });
            if (Object.keys(_this.currAsset).length > 0) {
                if (_this.findSelectedAssetStatus() === undefined) {
                    _this.statusType = _this.assetStatusType[0];
                }
                else {
                    _this.newAssetForm.patchValue({ status: _this.findSelectedAssetStatus() });
                }
            }
            else {
                _this.statusType = _this.assetStatusType[0];
            }
        }).catch(function (err) {
            console.log(err);
        });
        this.requests.getItems('typeList').then(function (res) {
            _this.assetTypes = Object.values(res).map(function (i) { return i; });
            _this.assetTypes.unshift({ name: '- Tous -', _id: '' });
            if (Object.keys(_this.currAsset).length > 0) {
                if (_this.findSelectedAssetType() === undefined) {
                    _this.newAssetForm.patchValue({ type: _this.assetTypes[0] });
                }
                else {
                    _this.newAssetForm.patchValue({ type: _this.findSelectedAssetType() });
                }
            }
            else {
                _this.activityType = _this.assetTypes[0];
            }
        }).catch(function (err) {
            console.log(err);
        });
        this.requests.getItems('effortList').then(function (res) {
            _this.effortList = Object.values(res).map(function (i) {
                return i;
            });
            if (!Object.keys(_this.currAsset).length) {
                _this.availableEffortList.push(_this.effortList[0]);
                _this.availableUnitsList[_this.effortList[0].type] = _this.effortList[0].unit;
                _this.efforts['efforts'].push({ type: _this.effortList[0]._id, value: 0, unit: _this.effortList[0].unit[0].id });
            }
            else {
                _this.availableEffortList = _this.currAsset.efforts.efforts;
                _this.effortList.forEach(function (i) {
                    _this.availableUnitsList[i.type] = i.unit;
                });
                _this.efforts['efforts'] = _this.currAsset.efforts.efforts.map(function (i) { return i; });
                _this.efforts['contributors'] = _this.currAsset.efforts.contributors;
                _this.effortList.forEach(function (obj) {
                    for (var i = 0; i < _this.availableEffortList.length; i++) {
                        if (obj._id === _this.availableEffortList[i]['type']) {
                            _this.availableEffortList[i]['type'] = obj.type;
                            _this.availableEffortList[i]['_id'] = obj._id;
                        }
                    }
                });
            }
        }).catch(function (err) {
            console.log(err);
        });
        this.requests.getItems('classificationList').then(function (res) {
            _this.classificationTypes = Object.values(res).map(function (i) { return i; });
            if (Object.keys(_this.currAsset).length > 0) {
                var temp = Object.values(res).map(function (i) { return i; });
                _this.classificationType = temp.find(function (obj) {
                    if (obj._id === _this.currAsset.classification.type) {
                        return obj;
                    }
                });
                if (_this.classificationType === undefined) {
                    _this.classificationType = _this.classificationTypes[0];
                }
            }
            else {
                _this.classificationType = _this.classificationTypes[_this.classificationTypes.length - 1];
            }
        }).catch(function (err) {
            console.log(err);
        });
    };
    NewAssetComponent.prototype.getSelectedProgram = function (id) {
        return this.programSelectValues.find(function (obj) {
            if (obj._id === id) {
                return obj;
            }
        });
    };
    NewAssetComponent.prototype.getSelectedProject = function (project) {
        var _this = this;
        if (project) {
            this.currentProject = project;
        }
        var route = this.router.url.split('/');
        if (route[1] === 'edit_asset') {
            var temp = this.programSelectValues.find(function (i) {
                if (i._id === _this.currAsset.identification.programId) {
                    return i;
                }
            });
            temp = temp.projects.filter(function (i) {
                if (i._id === _this.currAsset.identification.projectId) {
                    return i;
                }
            });
            if (project) {
                return project._id === temp[0]._id;
            }
            else {
                return temp[0];
            }
        }
    };
    NewAssetComponent.prototype.selectDefaultProgramm = function (prog) {
        this.newAssetForm.patchValue({ projectName: prog.projects[0] });
    };
    NewAssetComponent.prototype.isEditingAssetValid = function () {
        this.isAssetValid = true;
        var c = this.newAssetForm.controls;
        if (c.label.value && this.efforts['efforts'].length && (this.efforts['efforts'][0]['value'] == '0' || this.efforts['efforts'][0]['value'] > 0) && this.efforts['efforts'][0]['unit']
            && c.detailedDescription.value && c.dependenciesActor.value) {
            this.isAssetValid = false;
            this.newAssetForm.patchValue({ assetCardStatus: this.newAssetForm.value.assetCardStatus });
        }
        else {
            this.newAssetForm.patchValue({ assetCardStatus: 'PENDING' });
        }
    };
    NewAssetComponent.prototype.ngOnInit = function () {
        var _this = this;
        // this.currWrapper = 3;
        this.currWrapper = 1;
        this.parentAssets = [];
        this.user = {};
        this.assetErrorMessage = '';
        var user = this.storageService.getUser();
        if (!this.isAdmin()) {
            this.programsValues = Object.keys(user.creationRights);
            this.initProjectValues = user.creationRights;
        }
        var route = this.router.url.split('/');
        this.newActionForm = this.formBuilder.group({
            actionType: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('USER_ACTION', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            actionDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            actionAssignee: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            actionDesc: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required)
        });
        this.newAssetForm = this.formBuilder.group({
            isPlatformAsset: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('Unitaire', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            parentAssetId: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', []),
            assetName: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, this.noWhitespaceValidator]),
            programName: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.programsValues[0], []),
            projectName: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](this.initProjectValues[this.programsValues[0]][0], []),
            creationDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](new Date(), []),
            desc: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', []),
            status: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('PENDING', []),
            type: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('METHOD', []),
            deliverableExists: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](false, []),
            deliverableReference: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', []),
            piSharedExists: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](false, []),
            piSharedPartners: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', []),
            effortType: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('HUMAN', []),
            effortName: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('ha', []),
            effortNameNumber: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](0, []),
            effortContributors: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', []),
            copyrightExists: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](false, []),
            copyrightReference: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', []),
            patentExists: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](false, []),
            patentReference: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', []),
            demonstration: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', []),
            repository: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            label: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', []),
            detailedDescription: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', []),
            dependenciesLicenses: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', []),
            dependenciesHardware: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', []),
            dependenciesReusable: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](false, []),
            dependenciesActor: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', []),
            linkedPlatforms: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', []),
            keywords: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', []),
            assetCardStatus: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('PENDING', []),
            assetStatus: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', []),
            classificationType: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', []),
            classificationPerspective: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', []),
            assetComment: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [])
        });
        if (route[1] === 'edit_asset') {
            this.editingAsset = true;
            this.requests.getAsset(route[2]).then(function (res) {
                _this.currAsset = res;
                console.log('RESS => ', res);
                _this.requests.getActionsForAsset(_this.currAsset.id).subscribe(function (data) {
                    _this.assetActions = data.sort(function (objA, objB) {
                        return new Date(objA).getTime() - new Date(objB).getTime();
                    });
                });
                if (!_this.isAdmin() && _this.currAsset.identification.userId != user.id) {
                    alert('You are not allowed to see this page');
                    _this.router.navigate(['main']);
                }
                if (!_this.isAdmin()) {
                    _this.programsValues = Object.keys(user.creationRights);
                    _this.initProjectValues = user.creationRights;
                    if (_this.programsValues.indexOf(_this.currAsset.identification.program) === -1) {
                        _this.programsValues.unshift(_this.currAsset.identification.program);
                        if (_this.initProjectValues[_this.currAsset.identification.program]) {
                            _this.projectValues.unshift(_this.currAsset.identification.project);
                        }
                        else {
                            _this.projectValues = [_this.currAsset.identification.project];
                        }
                    }
                }
                _this.requests.getUser(_this.currAsset.identification.userId).then(function (res) {
                    // console.log('user1 => ', res);
                    _this.user = res;
                }).catch(function (err) {
                    // console.log('user1 => ', err);
                    console.error(err);
                });
                // const effortName = Object.keys(this.currAsset.effort).filter((key: string) => {
                //     return key !== 'contributors' && key !== 'type';
                // })[0];
                _this.newAssetForm = _this.formBuilder.group({
                    isPlatformAsset: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](!_this.currAsset.isPlatformAsset ? _this.currAsset.parentAssetId != 0 ? 'A partir d\'un actif valorisable existant' : 'Unitaire' : 'Démonstrateur', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
                    // parentAssetId: new FormControl({value: this.currAsset.parentAssetId ? this.currAsset.parentAssetId : '', disabled: false}, []),
                    parentAssetId: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currAsset.parentAssetId ? _this.currAsset.parentAssetId : '0', []),
                    // parentAssetId: new FormControl({value: '', disabled: !this.currAsset.isPlatformAsset}, []),
                    assetName: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currAsset.identification.name, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _this.noWhitespaceValidator]),
                    programName: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.getSelectedProgram(_this.currAsset.identification.programId), []),
                    projectName: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currAsset.identification.project, []),
                    creationDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]({ value: _this.datePipe.transform(_this.currAsset.identification.creationDate), disabled: true }, []),
                    desc: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currAsset.desc, []),
                    status: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currAsset.status === 'VALIDATED' ? 'WAITING_VALIDATION' : _this.currAsset.status, []),
                    type: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currAsset.type, []),
                    deliverableExists: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currAsset.deliverable.exists, []),
                    deliverableReference: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currAsset.deliverable.reference, []),
                    piSharedExists: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currAsset.piShared.exists, []),
                    piSharedPartners: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currAsset.piShared.partners, []),
                    // effortType: new FormControl(this.currAsset.effort.type, []),
                    // effortName: new FormControl(this.currAsset.effort.unit, []),
                    // effortNameNumber: new FormControl(this.currAsset.effort.value, []),
                    effortContributors: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currAsset.efforts.contributors, []),
                    copyrightExists: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currAsset.copyright.exists, []),
                    copyrightReference: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currAsset.copyright.reference, []),
                    patentExists: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currAsset.patent.exists, []),
                    patentReference: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currAsset.patent.reference, []),
                    demonstration: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currAsset.demonstration, []),
                    repository: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currAsset.repository, []),
                    label: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currAsset.label, []),
                    detailedDescription: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currAsset.detailedDescription, []),
                    dependenciesLicenses: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currAsset.dependencies.licenses, []),
                    dependenciesHardware: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currAsset.dependencies.hardware, []),
                    dependenciesReusable: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currAsset.dependencies.reusable, []),
                    dependenciesActor: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currAsset.dependencies.actor, []),
                    linkedPlatforms: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currAsset.linkedPlatforms, []),
                    keywords: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currAsset.keywords, []),
                    assetCardStatus: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currAsset.assetStatus, []),
                    assetStatus: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currAsset.assetStatus, []),
                    classificationType: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currAsset.classification.type, []),
                    classificationPerspective: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currAsset.classification.perspective, []),
                    assetComment: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currAsset.classification.comment, [])
                });
                _this.getData();
                _this.newAssetForm.get('parentAssetId').disable();
                _this.requests.getAssets().then(function (res) {
                    res.forEach(function (asset) {
                        if (_this.currAsset && _this.currAsset.id === asset.id || !asset.isPlatformAsset) {
                            return;
                        }
                        _this.parentAssets.push({
                            id: asset.id,
                            name: asset.identification.name
                        });
                    });
                    if (_this.parentAssets.length >= 1) {
                        _this.newAssetForm.patchValue({
                            parentAssetId: _this.parentAssets[0].id,
                        });
                    }
                    if (_this.newAssetForm.get('isPlatformAsset').value === 'A partir d\'un actif valorisable existant') {
                        _this.newAssetForm.get('parentAssetId').enable();
                    }
                    else {
                        _this.newAssetForm.get('parentAssetId').disable();
                    }
                }).catch(function (err) {
                    console.error(err);
                });
                if (_this.initProjectValues[_this.newAssetForm.value.programName]) {
                    _this.projectValues = _this.initProjectValues[_this.newAssetForm.value.programName].slice();
                }
                _this.firstChangeName = true;
                _this.newAssetForm.get('programName').valueChanges.subscribe(function (val) {
                    if (_this.firstChangeName && !_this.isAdmin()) {
                        _this.programsValues = Object.keys(user.creationRights);
                        _this.initProjectValues = user.creationRights;
                        _this.firstChangeName = false;
                    }
                    // this.projectValues = [...this.initProjectValues[val]];
                    _this.newAssetForm.patchValue({
                        'projectName': _this.projectValues[0]
                    });
                });
            }).catch(function (err) {
                console.error(err);
            });
        }
        else {
            this.editingAsset = false;
            this.user = this.storageService.getUser();
            this.newAssetForm.get('parentAssetId').disable();
            this.requests.getAssets().then(function (res) {
                _this.getData();
                res.forEach(function (asset) {
                    if (_this.currAsset && _this.currAsset.id === asset.id || !asset.isPlatformAsset) {
                        return;
                    }
                    _this.parentAssets.push({
                        id: asset.id,
                        name: asset.identification.name
                    });
                });
                if (_this.parentAssets.length >= 1) {
                    _this.newAssetForm.patchValue({
                        parentAssetId: _this.parentAssets[0].id,
                    });
                }
                if (_this.newAssetForm.get('isPlatformAsset').value === 'A partir d\'un actif valorisable existant') {
                    _this.newAssetForm.get('parentAssetId').enable();
                }
                else {
                    _this.newAssetForm.get('parentAssetId').disable();
                }
            }).catch(function (err) {
                console.error(err);
            });
            this.newAssetForm.get('programName').valueChanges.subscribe(function (val) {
                // this.projectSelectValues = [...this.initProjectValues[val]];
                _this.newAssetForm.patchValue({
                    'projectName': _this.projectValues[0]
                });
            });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('dateInput'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], NewAssetComponent.prototype, "dateInput", void 0);
    NewAssetComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-new-asset',
            template: __webpack_require__(/*! ./new-asset.component.html */ "./src/app/new-asset/new-asset.component.html"),
            styles: [__webpack_require__(/*! ./new-asset.component.scss */ "./src/app/new-asset/new-asset.component.scss")],
            providers: [
                _smite_modals__WEBPACK_IMPORTED_MODULE_6__["ModalsService"],
                _angular_common__WEBPACK_IMPORTED_MODULE_7__["DatePipe"]
            ]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _services_requests_service__WEBPACK_IMPORTED_MODULE_4__["RequestsService"],
            _services_local_storage_service__WEBPACK_IMPORTED_MODULE_3__["LocalStorageService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _smite_modals__WEBPACK_IMPORTED_MODULE_6__["ModalsService"],
            _services_pass_service__WEBPACK_IMPORTED_MODULE_5__["PassService"],
            _angular_common__WEBPACK_IMPORTED_MODULE_7__["DatePipe"]])
    ], NewAssetComponent);
    return NewAssetComponent;
}());



/***/ }),

/***/ "./src/app/new-transfer/new-transfer.component.html":
/*!**********************************************************!*\
  !*** ./src/app/new-transfer/new-transfer.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n  <div class=\"col-md-3 col-lg-3\"></div>\n  <div class=\"col-md-6 col-lg-6\">\n    <div [formGroup]=\"newTransferForm\">\n      <div class=\"transfer-form-wrapper\">\n        <div class=\"panel panel-primary first-wrapper\" *ngIf=\"currWrapper == 1\">\n          <div class=\"panel-heading\">\n            <h2>caracterisation du transfert</h2>\n          </div>\n          <div   class=\"panel-body\">\n            <div class=\"first-wrapper-body\">\n              <div class=\"top-wrapper\">\n                <div class=\"first-part-wrapper\">\n                  <div class=\"top-part-wrapper row\">\n                    <span class=\"subtitle\">Cible</span>\n                    <label class=\"form-check-label\">\n                      <span>Nom (projet ou partenaire):</span>\n                      <input class=\"form-control\" type=\"text\" formControlName=\"destinationName\">\n                    </label>\n                  </div>\n                  <div class=\"bottom-part-wrapper row\">\n                    <div class=\"first-part-select\">\n                      <select class=\"form-control\" name=\"destinationType\" formControlName=\"destinationType\" [(ngModel)]=\"transferType\">\n                        <option *ngFor=\"let type of transferTypes\" [ngValue]=\"type\">\n                          <span>{{type.name}}</span>\n                        </option>\n                      </select>\n                    </div>\n                    <label class=\"form-check-label\">\n                      <span>Contact:</span>\n                      <input class=\"form-control\" type=\"text\" formControlName=\"destinationContact\">\n                    </label>\n                  </div>\n                </div>\n                <div class=\"second-part-wrapper\">\n                  <div class=\"form-check row\">\n                    <label class=\"form-check-label\">\n                      <input class=\"form-check-input\" type=\"checkbox\" formControlName=\"openSourceExists\">\n                      Opensource\n                    </label>\n                    <div>\n                      <label class=\"form-check-label\">\n                        <span>Hyperlien:</span>\n                        <input class=\"form-control\" type=\"text\" formControlName=\"openSourceHyperlink\"\n                               [attr.disabled]=\"!newTransferForm.value.openSourceExists ? '' : null\">\n                      </label>\n                    </div>\n                  </div>\n                  <div class=\"form-check row\">\n                    <label class=\"form-check-label\">\n                      <input class=\"form-check-input\" type=\"checkbox\" formControlName=\"sciCommunityExists\">\n                      Communauté scientifique\n                    </label>\n                    <div>\n                      <label class=\"form-check-label\">\n                        <span>Hyperlien:</span>\n                        <input class=\"form-control\" type=\"text\" formControlName=\"sciCommunityHyperlink\"\n                               [attr.disabled]=\"!newTransferForm.value.sciCommunityExists ? '' : null\">\n                      </label>\n                    </div>\n                  </div>\n\n                  <div class=\"form-check row\">\n                    <label class=\"form-check-label\">\n                      <input class=\"form-check-input\" type=\"checkbox\" formControlName=\"openExists\">\n                      Ouverte\n                    </label>\n                    <div>\n                      <label class=\"form-check-label\">\n                        <span>Hyperlien:</span>\n                        <input class=\"form-control\" type=\"text\" formControlName=\"openHyperlink\"\n                               [attr.disabled]=\"!newTransferForm.value.openExists ? '' : null\">\n                      </label>\n                    </div>\n                  </div>\n                </div>\n              </div>\n\n              <div class=\"bottom-wrapper\">\n                <span class=\"subtitle\">Indicateurs</span>\n\n                <div class=\"indicateurs-check-wrapper\">\n                  <div class=\"form-indicateurs-check\">\n                    <label class=\"form-check-label\">\n                      <input class=\"form-check-input\" type=\"checkbox\" [checked]=\"usersCountExists\" (change)=\"isUsersCountExists($event)\">\n                      Nombre d'utilisateurs\n                    </label>\n                    <input type=\"number\" class=\"indicateurs-inputs form-control\" formControlName=\"indicatorsUsersCount\" [attr.disabled]=\"!usersCountExists ? '' : null\" min=\"0\" oninput=\"this.value = Math.abs(this.value)\">\n                  </div>\n                  <div  class=\"form-indicateurs-check\">\n                    <label class=\"form-check-label\">\n                      <input class=\"form-check-input\" type=\"checkbox\" [checked]=\"downloadsCountExists\" (change)=\"isDownloadsCountExists($event)\" >\n                      Nombre de téléchargements\n                    </label>\n                    <input type=\"number\" class=\"indicateurs-inputs form-control\" formControlName=\"indicatorsDownloadsCount\" [attr.disabled]=\"!downloadsCountExists ? '' : null\" min=\"0\" oninput=\"this.value = Math.abs(this.value)\">\n                  </div>\n                </div>\n                <div class=\"bottom-textarea-wrapper\">\n                  <span>Impact business:</span>\n                  <textarea placeholder=\"Descriptif\" class=\"form-control\" formControlName=\"indicatorsBusinessImpact\"></textarea>\n                </div>\n                <div class=\"licence-wrapper\">\n                  <div class=\"licence-first-wrapper\">\n                    <span>Licence:</span>\n                    <div class=\"form-check\">\n                      <label class=\"custom-control custom-radio\">\n                        <input id=\"indicatorsIndustrializedPoC1\" name=\"indicatorsIndustrializedPoC\" [value]=\"true\" type=\"radio\"\n                               formControlName=\"indicatorsIndustrializedPoC\" class=\"custom-control-input\">\n                        <span class=\"custom-control-indicator\"></span>\n                        <span class=\"custom-control-description\">Oui</span>\n                      </label>\n                      <label class=\"custom-control custom-radio\">\n                        <input id=\"indicatorsIndustrializedPoC2\" name=\"indicatorsIndustrializedPoC\" [value]=\"false\" type=\"radio\"\n                               formControlName=\"indicatorsIndustrializedPoC\" class=\"custom-control-input\">\n                        <span class=\"custom-control-indicator\"></span>\n                        <span class=\"custom-control-description\">Non</span>\n                      </label>\n                    </div>\n                  </div>\n\n\n\n                  <div class=\"licence-second-wrapper\">\n                    <label class=\"form-check-label\">\n                      <span class=\"licence-second-title\">Si oui, quel coût?</span>\n                      <input type=\"number\" class=\"form-control license-submit-btn\" formControlName=\"indicatorsLicenseCost\" min=\"0\" oninput=\"this.value = Math.abs(this.value)\">\n                    </label>\n                    <!--<span>Si oui, quel cout?</span>-->\n                    <!--<input type=\"number\" class=\"form-control license-submit-btn\" formControlName=\"indicatorsLicenseCost\" min=\"0\" oninput=\"this.value = Math.abs(this.value)\">-->\n                  </div>\n                </div>\n\n\n                <div class=\"last-bottom-wrapper\">\n                  <div class=\"form-bottom-check\">\n                    <label class=\"form-check-label\">\n                      <input class=\"form-check-input\" type=\"checkbox\" formControlName=\"indicatorsIndustrializedPoC\">\n                      POC industrialisé\n                    </label>\n                    <label class=\"form-check-label\">\n                      <input class=\"form-check-input\" type=\"checkbox\" formControlName=\"indicatorsLicenseReturn\">\n                      Retour de licence du produit industrialisé\n                    </label>\n                    <div class=\"bottom-input-wrapper\">\n                      <div class=\"bottom-input\">\n                        <label class=\"form-check-label\">\n                          <input class=\"form-check-input\" type=\"checkbox\" [checked]=\"indicatorsExperimentationsCountExists\" (change)=\"isIndicatorsExperimentationsCountExists($event)\">\n                          Nombre d'expérimentations\n                        </label>\n                        <div>\n                          <input type=\"number\" class=\"form-control\" formControlName=\"indicatorsExperimentationsCount\" [attr.disabled]=\"!indicatorsExperimentationsCountExists ? '' : null\" min=\"0\" oninput=\"this.value = Math.abs(this.value)\">\n                        </div>\n                      </div>\n\n                      <div class=\"bottom-input\">\n                        <label class=\"form-check-label\">\n                          <input class=\"form-check-input\" type=\"checkbox\" [checked]=\"indicatorsAwardsCountExists\" (change)=\"isIndicatorsAwardsCountExists($event)\">\n                          Nombre d'Awards\n                        </label>\n                        <div>\n                          <input type=\"number\" class=\"form-control\" formControlName=\"indicatorsAwardsCount\" [attr.disabled]=\"!indicatorsAwardsCountExists ? '' : null\" min=\"0\" oninput=\"this.value = Math.abs(this.value)\">\n                        </div>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n\n          <div class=\"panel-footer\">\n            <div class=\"suivant-btn\">\n              <!--<button class=\"btn btn-primary\" *ngIf=\"currWrapper > 1 && currWrapper < 3\" (click)=\"prevWrapper()\">-->\n                <!--<span>Retour</span>-->\n              <!--</button>-->\n              <!--&lt;!&ndash;<button class=\"btn btn-primary\" *ngIf=\"currWrapper < 3\" (click)=\"nextWrapper()\" [disabled]=\"!detailedDescription.valid && currWrapper === 2\">&ndash;&gt;-->\n              <!--<button class=\"btn btn-primary\" *ngIf=\"currWrapper < 2\" (click)=\"nextWrapper()\">-->\n                <!--<span>{{currWrapper === 1 ? 'Valider ajout transfert' : 'Suivant'}}</span>-->\n              <!--</button>-->\n              <!--<div *ngIf=\"transferErrorMessage\" class=\"transfer-error\">{{transferErrorMessage}}</div>-->\n              <!--<button class=\"btn btn-primary\" *ngIf=\"currWrapper === 2\" (click)=\"onTransferFormSubmit()\" [disabled]=\"!newTransferForm.valid\">-->\n                <!--<span>Fiche transfert enregistrée</span>-->\n              <!--</button>-->\n              <div class=\"col-md-3 col-lg-3\"></div>\n              <div class=\"col-md-6 col-lg-6\">\n                <button class=\"btn btn-primary\" (click)=\"onTransferFormSubmit()\">\n                  <span>Fiche transfert enregistrée</span>\n                </button>\n              </div>\n              <div class=\"col-md-3 col-lg-3\"></div>\n            </div>\n          </div>\n        </div>\n        <!--<div *ngIf=\"currWrapper == 2\" class=\"third-wrapper\">-->\n          <!--<div class=\"fields-wrapper\">-->\n            <!--<div class=\"statut-de-la-fiche-wrapper  alert alert-danger\">-->\n              <!--<span>statut de la fiche</span>-->\n              <!--&lt;!&ndash;PENDING,WAITING_VALIDATION,VALIDATED&ndash;&gt;-->\n              <!--<div class=\"row\">-->\n              <!--<div class=\"form-check\">-->\n                <!--<label class=\"form-check-label col-md-4 col-lg-4\">-->\n                  <!--<input class=\"form-check-input\" formControlName=\"status\" type=\"radio\" value=\"PENDING\">-->\n                  <!--&lt;!&ndash;En cours de redaction (possibilite d'apporter de nouvelles modifications)&ndash;&gt;-->\n                  <!--PENDING-->\n                <!--</label>-->\n                <!--<label class=\"form-check-label col-md-4 col-lg-4\">-->\n                  <!--<input class=\"form-check-input\" formControlName=\"status\" type=\"radio\" value=\"WAITING_VALIDATION\">-->\n                  <!--&lt;!&ndash;Pour validation CODIR&ndash;&gt;-->\n                  <!--WAITING_VALIDATION-->\n                <!--</label>-->\n                <!--<label class=\"form-check-label col-md-4 col-lg-4\">-->\n                  <!--<input class=\"form-check-input\" formControlName=\"status\" type=\"radio\" value=\"VALIDATED\">-->\n                  <!--&lt;!&ndash;Pour validation CODIR&ndash;&gt;-->\n                  <!--VALIDATED-->\n                <!--</label>-->\n              <!--</div>-->\n              <!--</div>-->\n            <!--</div>-->\n            <!--<div class=\"classification-wrapper\">-->\n              <!--<span class=\"classification-text\">Classification (DO suite revue)</span>-->\n              <!--<div class=\"custom-control-wrapper\">-->\n                <!--<label class=\"custom-control custom-radio col-md-4 col-lg-4\">-->\n                  <!--<input id=\"radio1\" name=\"classificationType\" type=\"radio\" class=\"custom-control-input\"-->\n                         <!--[value]=\"'HIGH_POTENTIAL'\" formControlName=\"classificationType\">-->\n                  <!--<span class=\"custom-control-indicator\"></span>-->\n                  <!--<span class=\"custom-control-description\">Fort potentiel</span>-->\n                <!--</label>-->\n                <!--<label class=\"custom-control custom-radio col-md-4 col-lg-4\">-->\n                  <!--<input id=\"radio2\" name=\"classificationType\" type=\"radio\" class=\"custom-control-input\"-->\n                         <!--[value]=\"'FOLLOW'\" formControlName=\"classificationType\">-->\n                  <!--<span class=\"custom-control-indicator\"></span>-->\n                  <!--<span class=\"custom-control-description\">Transfert a suivre</span>-->\n                <!--</label>-->\n                <!--<label class=\"custom-control custom-radio col-md-4 col-lg-4\">-->\n                  <!--<input id=\"radio3\" name=\"classificationType\" type=\"radio\" class=\"custom-control-input\"-->\n                         <!--[value]=\"'CONFIRM'\" formControlName=\"classificationType\">-->\n                  <!--<span class=\"custom-control-indicator\"></span>-->\n                  <!--<span class=\"custom-control-description\">A confirmer</span>-->\n                <!--</label>-->\n              <!--</div>-->\n\n              <!--<div class=\"perspective-wrapper\">-->\n                <!--<span>Perspective:</span>-->\n                <!--<textarea placeholder=\"Descriptif\" formControlName=\"classificationPerspective\"></textarea>-->\n              <!--</div>-->\n\n            <!--</div>-->\n          <!--</div>-->\n\n          <!--<div class=\"suivant-btn\">-->\n            <!--<div class=\"col-md-6 col-lg-6\">-->\n              <!--<button class=\"btn btn-primary\" *ngIf=\"currWrapper > 1 && currWrapper < 3\" (click)=\"prevWrapper()\">-->\n                <!--<span>Retour</span>-->\n              <!--</button>-->\n              <!--<button class=\"btn btn-primary\" *ngIf=\"currWrapper < 2\" (click)=\"nextWrapper()\">-->\n                <!--<span>{{currWrapper === 1 ? 'Valider ajout transfert' : 'Suivant'}}</span>-->\n              <!--</button>-->\n            <!--</div>-->\n            <!--<div *ngIf=\"transferErrorMessage\" class=\"transfer-error\">{{transferErrorMessage}}</div>-->\n            <!--<div class=\"col-md-6 col-lg-6\">-->\n              <!--<button class=\"btn btn-primary\" *ngIf=\"currWrapper === 2\" (click)=\"onTransferFormSubmit()\" [disabled]=\"!newTransferForm.valid\">-->\n                <!--<span>Fiche transfert enregistrée</span>-->\n              <!--</button>-->\n            <!--</div>-->\n\n          <!--</div>-->\n        <!--</div>-->\n        <div *ngIf=\"currWrapper == 2\">\n          <div class=\"last-wrapper\">\n            <p>Fiche transfert enregistrée</p>\n            <!--<p>\"{{resStr}}\"</p>-->\n            <p>Ref. \"{{resTransfer?.id}}\"</p>\n            <a routerLink=\"/transferts\" routerLinkActive=\"active\">\n              <fa name=\"angle-left\"></fa>\n              <span>Retour à la liste des transferts</span>\n            </a>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"col-md-3 col-lg-3\"></div>\n</div>\n"

/***/ }),

/***/ "./src/app/new-transfer/new-transfer.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/new-transfer/new-transfer.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host > .row {\n  padding-top: 90px; }\n\n:host .panel {\n  border-radius: 0;\n  background-color: #fbfbfb; }\n\n:host .panel .panel-heading {\n    border-radius: 0; }\n\n* {\n  font-size: 15px; }\n\ninput {\n  outline: transparent;\n  padding-left: 0.4vw; }\n\ntextarea {\n  padding-left: 0.4vw;\n  resize: none; }\n\n.panel-footer {\n  background: transparent;\n  border: 0; }\n\nspan.error {\n  color: #ff0000; }\n\n.transfer-form-wrapper .first-wrapper {\n  width: 100%;\n  height: 100%;\n  border: 1px solid #0062cc;\n  background-color: #f8f9fa;\n  margin-bottom: 0; }\n\n.transfer-form-wrapper .first-wrapper .panel-heading h2 {\n    margin: 10px;\n    font-size: 15px;\n    text-decoration: underline;\n    text-transform: uppercase;\n    font-weight: 500; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .top-wrapper {\n    padding: 1vw;\n    height: 235px; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .top-wrapper h4 {\n      text-align: left;\n      margin: 0; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .top-wrapper .first-part-wrapper .top-part-wrapper {\n      height: 100%;\n      margin-bottom: 10px;\n      display: flex;\n      justify-content: space-between;\n      align-items: center; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .top-wrapper .first-part-wrapper .top-part-wrapper h4 {\n        float: left; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .top-wrapper .first-part-wrapper .top-part-wrapper .form-check-label {\n        display: block;\n        float: right; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .top-wrapper .first-part-wrapper .top-part-wrapper .form-check-label input {\n          width: 189px;\n          float: right; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .top-wrapper .first-part-wrapper .top-part-wrapper .form-check-label span {\n          display: inline-block;\n          margin-right: 35px;\n          font-weight: 400;\n          margin-top: 7px; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .top-wrapper .first-part-wrapper .top-part-wrapper div {\n        display: block;\n        float: right; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .top-wrapper .first-part-wrapper .top-part-wrapper div input {\n          width: 189px;\n          float: right; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .top-wrapper .first-part-wrapper .top-part-wrapper div span {\n          margin-right: 35px; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .top-wrapper .first-part-wrapper .bottom-part-wrapper {\n      height: 100%; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .top-wrapper .first-part-wrapper .bottom-part-wrapper .form-check-label {\n        float: right; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .top-wrapper .first-part-wrapper .bottom-part-wrapper .form-check-label span {\n          display: inline-block;\n          margin-right: 35px;\n          font-weight: 400;\n          margin-top: 7px; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .top-wrapper .first-part-wrapper .bottom-part-wrapper .form-check-label input {\n          width: 189px;\n          float: right; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .top-wrapper .first-part-wrapper .bottom-part-wrapper .first-part-select {\n        float: left; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .top-wrapper .first-part-wrapper .bottom-part-wrapper .first-part-select select {\n          background-color: #fff; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .top-wrapper .first-part-wrapper .bottom-part-wrapper .first-input-wrapper {\n        float: right; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .top-wrapper .first-part-wrapper .bottom-part-wrapper .first-input-wrapper span {\n          display: inline-block;\n          margin-right: 35px;\n          font-weight: 400;\n          margin-top: 7px; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .top-wrapper .first-part-wrapper .bottom-part-wrapper .first-input-wrapper input {\n          width: 189px;\n          float: right; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .top-wrapper .second-part-wrapper {\n      margin-top: 1vw; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .top-wrapper .second-part-wrapper .form-check {\n        height: 100%; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .top-wrapper .second-part-wrapper .form-check .form-check-label {\n          float: left; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .top-wrapper .second-part-wrapper .form-check div {\n          float: right;\n          margin-bottom: 10px; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .top-wrapper .second-part-wrapper .form-check div label span {\n            display: inline-block;\n            margin-right: 35px;\n            font-weight: 400;\n            margin-top: 7px; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .top-wrapper .second-part-wrapper .form-check div label input {\n            width: 189px;\n            float: right; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .bottom-wrapper {\n    margin: 50px 0 50px 0; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .bottom-wrapper .bottom-input-wrapper .bottom-input {\n      margin: 0; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .bottom-wrapper .bottom-input-wrapper .bottom-input div {\n        margin: 0; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .bottom-wrapper .bottom-input-wrapper .bottom-input div input {\n          outline: transparent;\n          padding-left: 0.4vw; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .bottom-wrapper .bottom-input-wrapper .bottom-input label {\n        width: 18vw;\n        margin: 0; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .bottom-wrapper h4 {\n      text-align: left;\n      margin: 0 0 2vw 0; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .bottom-wrapper .indicateurs-check-wrapper {\n      height: 70px; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .bottom-wrapper .indicateurs-check-wrapper .form-indicateurs-check {\n        width: 450px;\n        margin-right: 20px;\n        height: 50px; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .bottom-wrapper .indicateurs-check-wrapper .form-indicateurs-check .form-check-label {\n          float: left; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .bottom-wrapper .indicateurs-check-wrapper .form-indicateurs-check .indicateurs-inputs {\n          float: right;\n          width: 189px; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .bottom-wrapper .bottom-textarea-wrapper {\n      margin: 2vw 0;\n      height: 50px; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .bottom-wrapper .bottom-textarea-wrapper textarea {\n        float: right;\n        width: 80%;\n        resize: none; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .bottom-wrapper .bottom-textarea-wrapper span {\n        float: left; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .bottom-wrapper .licence-wrapper {\n      height: 30px;\n      margin-bottom: 3vw; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .bottom-wrapper .licence-wrapper .licence-second-wrapper {\n        float: right; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .bottom-wrapper .licence-wrapper .licence-first-wrapper {\n        position: relative;\n        top: 7px; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .bottom-wrapper .licence-wrapper div .form-check {\n        display: initial; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .bottom-wrapper .licence-wrapper div span {\n        float: left; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .bottom-wrapper .licence-wrapper div:first-of-type {\n        float: left; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .bottom-wrapper .licence-wrapper div:first-of-type label {\n          margin: 0 10px;\n          float: left; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .bottom-wrapper .licence-wrapper div:first-of-type label input {\n            float: left;\n            margin-right: 10px; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .bottom-wrapper .licence-wrapper div .licence-second-title {\n        margin-top: 7px;\n        font-weight: 400;\n        margin-right: 0.5vw;\n        float: left; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .bottom-wrapper .licence-wrapper div .license-submit-btn {\n        width: 189px;\n        float: right;\n        font-weight: 400; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .bottom-wrapper .last-bottom-wrapper .form-bottom-check .form-check-label {\n      display: block;\n      margin-bottom: 10px; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .bottom-wrapper .last-bottom-wrapper .form-bottom-check .bottom-input-wrapper .bottom-input .form-check-label {\n      display: inline-block; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .first-wrapper-body .bottom-wrapper .last-bottom-wrapper .form-bottom-check .bottom-input-wrapper .bottom-input div {\n      display: inline-block; }\n\n.transfer-form-wrapper .first-wrapper .panel-body label {\n    font-weight: 400; }\n\n.transfer-form-wrapper .first-wrapper .panel-body .subtitle {\n    flex: 1;\n    text-transform: uppercase;\n    font-size: 1.15em;\n    font-weight: bolder; }\n\n.transfer-form-wrapper .first-wrapper .panel-footer .suivant-btn {\n    margin: 10px; }\n\n.transfer-form-wrapper .first-wrapper .panel-footer .suivant-btn .transfer-error {\n      position: absolute;\n      color: #ff0000; }\n\n.transfer-form-wrapper .first-wrapper .panel-footer .suivant-btn button {\n      display: block;\n      margin: 0 auto;\n      position: relative;\n      bottom: 50px; }\n\n.transfer-form-wrapper .third-wrapper {\n  height: 400px;\n  margin-top: 2vw;\n  border: 1px solid #0062cc;\n  border-radius: 3px; }\n\n.transfer-form-wrapper .third-wrapper .suivant-btn {\n    position: relative;\n    bottom: 10px; }\n\n.transfer-form-wrapper .third-wrapper .suivant-btn button {\n      margin: 0 auto;\n      display: block; }\n\n.transfer-form-wrapper .third-wrapper label input {\n    margin-right: 1vw; }\n\n.transfer-form-wrapper .third-wrapper .fields-wrapper {\n    width: 100%; }\n\n.transfer-form-wrapper .third-wrapper .fields-wrapper .statut-de-la-fiche-wrapper {\n      padding: 13px;\n      width: 100%;\n      height: 155px;\n      border: none;\n      margin-bottom: 0; }\n\n.transfer-form-wrapper .third-wrapper .fields-wrapper .statut-de-la-fiche-wrapper label {\n        padding: 0; }\n\n.transfer-form-wrapper .third-wrapper .fields-wrapper .statut-de-la-fiche-wrapper label:hover {\n          cursor: pointer; }\n\n.transfer-form-wrapper .third-wrapper .fields-wrapper .statut-de-la-fiche-wrapper div {\n        display: block;\n        margin: 1vw 0;\n        text-align: center; }\n\n.transfer-form-wrapper .third-wrapper .fields-wrapper .statut-de-la-fiche-wrapper .btn-wrapper {\n        padding: 0;\n        margin: 0;\n        height: 40px; }\n\n.transfer-form-wrapper .third-wrapper .fields-wrapper .statut-de-la-fiche-wrapper .btn-wrapper button {\n          cursor: pointer;\n          margin: 0 auto;\n          display: block; }\n\n.transfer-form-wrapper .third-wrapper .fields-wrapper .statut-de-la-fiche-wrapper span {\n        text-transform: uppercase;\n        font-weight: 600; }\n\n.transfer-form-wrapper .last-wrapper {\n  border: 1px solid #337ab7;\n  text-align: center;\n  border-radius: 3px;\n  width: 100%;\n  margin: 0 auto;\n  padding: 1vw 2vw;\n  position: relative;\n  margin-top: 2vw;\n  font-weight: 500; }\n\n.transfer-form-wrapper .last-wrapper fa {\n    margin-right: 5px; }\n"

/***/ }),

/***/ "./src/app/new-transfer/new-transfer.component.ts":
/*!********************************************************!*\
  !*** ./src/app/new-transfer/new-transfer.component.ts ***!
  \********************************************************/
/*! exports provided: NewTransferComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewTransferComponent", function() { return NewTransferComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_local_storage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/local-storage.service */ "./src/app/services/local-storage.service.ts");
/* harmony import */ var _services_requests_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/requests.service */ "./src/app/services/requests.service.ts");
/* harmony import */ var _services_pass_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/pass.service */ "./src/app/services/pass.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var NewTransferComponent = /** @class */ (function () {
    function NewTransferComponent(formBuilder, requests, storageService, router, passService) {
        this.formBuilder = formBuilder;
        this.requests = requests;
        this.storageService = storageService;
        this.router = router;
        this.passService = passService;
        this.usersCountExists = false;
        this.downloadsCountExists = false;
        this.indicatorsExperimentationsCountExists = false;
        this.isActeurOrPorteurExists = false;
        this.indicatorsAwardsCountExists = false;
        this.transferErrorMessage = '';
        this.user = {};
        this.transferTypes = [];
        this.transferType = {};
        this.currAsset = {};
        this.currTransfert = {};
        this.resTransfer = {};
        this.resStr = '';
    }
    NewTransferComponent.prototype.noWhitespaceValidator = function (control) {
        var isWhitespace = (control.value || '').trim().length === 0;
        var isValid = !isWhitespace;
        return isValid ? null : { 'whitespace': true };
    };
    NewTransferComponent.prototype.prevWrapper = function () {
        if (this.currWrapper > 1) {
            this.currWrapper--;
        }
    };
    NewTransferComponent.prototype.nextWrapper = function () {
        if (this.currWrapper < 5) {
            this.currWrapper++;
        }
        // console.log(this.newTransferForm.value);
    };
    NewTransferComponent.prototype.isAdmin = function () {
        return this.passService.isAdmin();
    };
    NewTransferComponent.prototype.isActeurOrPorteur = function (event) {
        this.isActeurOrPorteurExists = event.target.checked;
    };
    NewTransferComponent.prototype.isUsersCountExists = function (event) {
        this.usersCountExists = event.target.checked;
        console.log(this.usersCountExists);
    };
    NewTransferComponent.prototype.isDownloadsCountExists = function (event) {
        this.downloadsCountExists = event.target.checked;
        console.log(this.downloadsCountExists);
    };
    NewTransferComponent.prototype.isIndicatorsExperimentationsCountExists = function (event) {
        this.indicatorsExperimentationsCountExists = event.target.checked;
        console.log(this.indicatorsExperimentationsCountExists);
    };
    NewTransferComponent.prototype.isIndicatorsAwardsCountExists = function (event) {
        this.indicatorsAwardsCountExists = event.target.checked;
        console.log(this.indicatorsAwardsCountExists);
    };
    NewTransferComponent.prototype.onTransferFormSubmit = function () {
        var _this = this;
        if (!this.currAsset)
            return;
        this.transferErrorMessage = '';
        var inpTransfert = this.newTransferForm.value;
        var newTransfert = {};
        var currTransferUserId;
        if (Object.keys(this.currTransfert).length > 0) {
            newTransfert.id = this.currTransfert.id;
            newTransfert.assets = this.currTransfert.assets;
            currTransferUserId = this.currTransfert.userId;
        }
        else {
            newTransfert.assets = [this.currAsset];
            console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ', newTransfert.assets);
        }
        this.user = this.storageService.getUser();
        if (!this.user) {
            this.transferErrorMessage = 'Login error. Please, log in';
        }
        newTransfert.userId = currTransferUserId || this.user.id;
        newTransfert.destination = {
            type: this.transferType._id,
            name: inpTransfert.destinationName,
            contact: inpTransfert.destinationContact
        };
        newTransfert.openSource = {
            exists: inpTransfert.openSourceExists,
            hyperlink: inpTransfert.openSourceHyperlink
        };
        newTransfert.sciCommunity = {
            exists: inpTransfert.sciCommunityExists,
            hyperlink: inpTransfert.sciCommunityHyperlink
        };
        newTransfert.open = {
            exists: inpTransfert.openExists,
            hyperlink: inpTransfert.openHyperlink
        };
        newTransfert.indicators = {
            usersCount: this.usersCountExists ? inpTransfert.indicatorsUsersCount : -1,
            downloadsCount: this.downloadsCountExists ? inpTransfert.indicatorsDownloadsCount : -1,
            businessImpact: inpTransfert.indicatorsBusinessImpact,
            licenseCost: inpTransfert.indicatorsIndustrializedPoC ? inpTransfert.indicatorsLicenseCost : -1,
            industrializedPoC: inpTransfert.indicatorsIndustrializedPoC,
            licenseReturn: inpTransfert.indicatorsLicenseReturn,
            experimentationsCount: this.indicatorsExperimentationsCountExists ? inpTransfert.indicatorsExperimentationsCount : -1,
            awardsCount: this.indicatorsAwardsCountExists ? inpTransfert.indicatorsAwardsCount : -1
        };
        newTransfert.status = inpTransfert.status;
        if (Object.keys(this.currTransfert).length === 0) {
            this.requests.addTransfer(newTransfert).then(function (res) {
                // console.log('new transfer res => ', res);
                _this.currWrapper = 2;
                _this.resTransfer = res;
                _this.resStr = '';
                // this.currAsset.assetStatus = 'TRANSFERED';
                // this.requests.editAsset(this.currAsset).then(res => {
                //     console.log(`Created a new transfer from asset ${this.currAsset.id}. Asset is now transfered.`);
                // });
            }).catch(function (err) {
                console.error('new transfer error => ', err);
                if (err.status === 403) {
                    _this.transferErrorMessage = 'Invalid fields';
                }
                else if (err.status == 0) {
                    _this.transferErrorMessage = 'Connection error. Please, check your internet connection';
                }
                else if (err.status === 401) {
                    _this.transferErrorMessage = 'Login error. Please, log in';
                }
            });
        }
        else {
            this.requests.editTransfer(newTransfert).then(function (res) {
                // console.log('new asset res => ', res);
                _this.currWrapper = 2;
                _this.resTransfer = res;
            }).catch(function (err) {
                console.error('new asset error => ', err);
                if (err.status === 403) {
                    _this.transferErrorMessage = 'Invalid fields';
                }
                else if (err.status == 0) {
                    _this.transferErrorMessage = 'Connection error. Please, check your internet connection';
                }
                else if (err.status === 401) {
                    _this.transferErrorMessage = 'Login error. Please, log in';
                }
            });
        }
    };
    Object.defineProperty(NewTransferComponent.prototype, "detailedDescription", {
        get: function () {
            return this.newTransferForm.get('detailedDescription');
        },
        enumerable: true,
        configurable: true
    });
    NewTransferComponent.prototype.getSelectedDestination = function () {
        var _this = this;
        return this.transferTypes.find(function (i) {
            if (i._id === _this.currTransfert.destination.type) {
                return i;
            }
        });
    };
    NewTransferComponent.prototype.getData = function () {
        var _this = this;
        var route = this.router.url.split('/');
        this.requests.getItems('destinationList').then(function (res) {
            _this.transferTypes = Object.values(res).map(function (i) { return i; });
            _this.transferType = _this.transferTypes[0];
            if (route[1] === 'edit_transfer') {
                if (_this.getSelectedDestination()) {
                    _this.transferType = _this.getSelectedDestination();
                }
            }
        }).catch(function (err) {
            console.log(err);
        });
    };
    NewTransferComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (!this.isAdmin()) {
            alert('You are not allowed to see this page');
            this.router.navigate(['main']);
        }
        this.currWrapper = 1;
        var route = this.router.url.split('/');
        this.newTransferForm = this.formBuilder.group({
            destinationType: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', []),
            destinationName: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', []),
            destinationContact: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', []),
            openSourceExists: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](false, []),
            openSourceHyperlink: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', []),
            sciCommunityExists: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](false, []),
            sciCommunityHyperlink: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', []),
            openExists: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](false, []),
            openHyperlink: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', []),
            indicatorsUsersCount: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](0, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].min(0)]),
            indicatorsDownloadsCount: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](0, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].min(0)]),
            indicatorsBusinessImpact: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', []),
            indicatorsLicenseCost: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](0, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].min(0)]),
            indicatorsIndustrializedPoC: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](false, []),
            indicatorsLicenseReturn: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](false, []),
            indicatorsExperimentationsCount: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](0, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].min(0)]),
            indicatorsAwardsCount: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](0, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].min(0)]),
            status: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('PENDING', []),
        });
        if (route[1] === 'edit_transfer') {
            this.requests.getTransfer(route[2]).then(function (res) {
                // console.log('going to edit transfer => ', res);
                _this.currTransfert = res;
                _this.newTransferForm = _this.formBuilder.group({
                    destinationType: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](res.destination.type, []),
                    destinationName: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currTransfert.destination.name, []),
                    destinationContact: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currTransfert.destination.contact, []),
                    openSourceExists: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currTransfert.openSource.exists, []),
                    openSourceHyperlink: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currTransfert.openSource.hyperlink, []),
                    sciCommunityExists: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currTransfert.sciCommunity.exists, []),
                    sciCommunityHyperlink: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currTransfert.sciCommunity.hyperlink, []),
                    openExists: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currTransfert.open.exists, []),
                    openHyperlink: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currTransfert.open.hyperlink, []),
                    indicatorsUsersCount: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currTransfert.indicators.usersCount > 0 ? _this.currTransfert.indicators.usersCount : 0, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].min(0)]),
                    indicatorsDownloadsCount: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currTransfert.indicators.downloadsCount > 0 ? _this.currTransfert.indicators.downloadsCount : 0, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].min(0)]),
                    indicatorsBusinessImpact: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currTransfert.indicators.businessImpact, []),
                    indicatorsLicenseCost: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currTransfert.indicators.licenseCost > 0 ? _this.currTransfert.indicators.licenseCost : 0, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].min(0)]),
                    indicatorsIndustrializedPoC: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currTransfert.indicators.industrializedPoC, []),
                    indicatorsLicenseReturn: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currTransfert.indicators.licenseReturn, []),
                    indicatorsExperimentationsCount: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currTransfert.indicators.experimentationsCount > 0 ? _this.currTransfert.indicators.experimentationsCount : 0, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].min(0)]),
                    indicatorsAwardsCount: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currTransfert.indicators.awardsCount > 0 ? _this.currTransfert.indicators.awardsCount : 0, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].min(0)]),
                    status: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](_this.currTransfert.status, []),
                });
                _this.indicatorsAwardsCountExists = _this.currTransfert.indicators.awardsCount > 0;
                _this.usersCountExists = _this.currTransfert.indicators.usersCount > 0;
                _this.downloadsCountExists = _this.currTransfert.indicators.downloadsCount > 0;
                _this.indicatorsExperimentationsCountExists = _this.currTransfert.indicators.experimentationsCount > 0;
            }).catch(function (err) {
                console.error(err);
            });
        }
        else {
            this.user = this.storageService.getUser();
            this.currAsset = this.storageService.getAsset();
            if (!this.currAsset)
                this.router.navigate(['main']);
        }
        this.getData();
    };
    NewTransferComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-new-transfer',
            template: __webpack_require__(/*! ./new-transfer.component.html */ "./src/app/new-transfer/new-transfer.component.html"),
            styles: [__webpack_require__(/*! ./new-transfer.component.scss */ "./src/app/new-transfer/new-transfer.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _services_requests_service__WEBPACK_IMPORTED_MODULE_4__["RequestsService"],
            _services_local_storage_service__WEBPACK_IMPORTED_MODULE_3__["LocalStorageService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _services_pass_service__WEBPACK_IMPORTED_MODULE_5__["PassService"]])
    ], NewTransferComponent);
    return NewTransferComponent;
}());



/***/ }),

/***/ "./src/app/not-found/not-found.component.html":
/*!****************************************************!*\
  !*** ./src/app/not-found/not-found.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"not-found-container\">\n  <div class=\"error-container\">\n    <h1>404</h1>\n  </div>\n\n  <p class=\"return\">\n    <a href=\"#\"> Return to main</a>\n  </p>\n</div>\n"

/***/ }),

/***/ "./src/app/not-found/not-found.component.scss":
/*!****************************************************!*\
  !*** ./src/app/not-found/not-found.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".not-found-container {\n  height: 100vh;\n  width: 100%;\n  background-color: #2D72D9;\n  color: #fff;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  margin: 0; }\n  .not-found-container .error-container {\n    text-align: center; }\n  .not-found-container .error-container h1 {\n      margin: 0;\n      padding: 0;\n      font-size: 220px; }\n  .not-found-container a {\n    color: #fff; }\n"

/***/ }),

/***/ "./src/app/not-found/not-found.component.ts":
/*!**************************************************!*\
  !*** ./src/app/not-found/not-found.component.ts ***!
  \**************************************************/
/*! exports provided: NotFoundComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotFoundComponent", function() { return NotFoundComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NotFoundComponent = /** @class */ (function () {
    function NotFoundComponent() {
    }
    NotFoundComponent.prototype.ngOnInit = function () {
    };
    NotFoundComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-not-found',
            template: __webpack_require__(/*! ./not-found.component.html */ "./src/app/not-found/not-found.component.html"),
            styles: [__webpack_require__(/*! ./not-found.component.scss */ "./src/app/not-found/not-found.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], NotFoundComponent);
    return NotFoundComponent;
}());



/***/ }),

/***/ "./src/app/project-body/project-body.component.html":
/*!**********************************************************!*\
  !*** ./src/app/project-body/project-body.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-projects-list *ngIf=\"!isEditingAsset\" [changeState]=\"changeEditingState.bind(this)\"></app-projects-list>\n\n<!--<app-new-asset *ngIf=\"isEditingAsset\" [currAsset]=\"changingAsset\" [changeState]=\"changeEditingState.bind(this)\"></app-new-asset>-->\n\n<!--<app-new-transfer></app-new-transfer>-->\n"

/***/ }),

/***/ "./src/app/project-body/project-body.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/project-body/project-body.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/project-body/project-body.component.ts":
/*!********************************************************!*\
  !*** ./src/app/project-body/project-body.component.ts ***!
  \********************************************************/
/*! exports provided: ProjectBodyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectBodyComponent", function() { return ProjectBodyComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ProjectBodyComponent = /** @class */ (function () {
    function ProjectBodyComponent() {
        this.changingAsset = {};
        this.isEditingAsset = false;
    }
    ProjectBodyComponent.prototype.changeEditingState = function (asset) {
        this.changingAsset = asset;
        this.isEditingAsset = !this.isEditingAsset;
        console.log(this.changingAsset, this.isEditingAsset);
    };
    ProjectBodyComponent.prototype.ngOnInit = function () {
        this.isEditingAsset = false;
    };
    ProjectBodyComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-project-body',
            template: __webpack_require__(/*! ./project-body.component.html */ "./src/app/project-body/project-body.component.html"),
            styles: [__webpack_require__(/*! ./project-body.component.scss */ "./src/app/project-body/project-body.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ProjectBodyComponent);
    return ProjectBodyComponent;
}());



/***/ }),

/***/ "./src/app/projects-list/projects-list.component.html":
/*!************************************************************!*\
  !*** ./src/app/projects-list/projects-list.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"projects-main-body\">\n <div class=\"row\">\n        <div class=\"col-md-1 col-lg-1\"></div>\n        <div class=\"col-md-10 col-lg-10 filters-box\">\n            <div class=\"toolbar\" title=\"Cacher / Révéler les filtres\" (click)=\"toggleFilters()\">\n                <span *ngIf=\"filtersFolded\" class=\"fold icon folded\">▼</span>\n                <span *ngIf=\"!filtersFolded\" class=\"fold icon expanded\">▼</span>\n                <div class=\"toolbar-title\">FILTRES</div>\n            </div>\n            <div [ngClass]=\"{'folded': filtersFolded}\" class=\"filter-wrapper row\">\n                <div class=\"select-wrapper col-md-6 col-lg-6\">\n                    <div class=\"card\">\n                        <span>Programme</span>\n                        <select name=\"programmFilterSelect\" [(ngModel)]=\"programmToFilter\" (change)=\"onSelectChange($event)\" #programmFilterSelect>\n                            <option value=\"{{'\\u02B0'}}\">\n                                <span>- Tous -</span>\n                            </option>\n                            <option [ngValue]=\"key\" *ngFor=\"let key of filters.filterProgramsValues\">\n                                <span>{{key.shortName}}</span>\n                            </option>\n                        </select>\n                    </div>\n                    <div class=\"card\">\n                        <span>Projet</span>\n                        <select name=\"projectFilterSelect\" [(ngModel)]=\"projectToFilter\" (change)=\"onSelectChange($event)\" #projectFilterSelect>\n                            <option value=\"{{'\\u02B0'}}\">\n                                <span>- Tous -</span>\n                            </option>\n                            <option [ngValue]=\"key\" *ngFor=\"let key of programmToFilter['projects']\">\n                                <span>{{key.shortName}}</span>\n                            </option>\n\n                        </select>\n                    </div>\n                    <div style=\"margin-top: 0;\" class=\"card\">\n                        <span>Type d'actif</span>\n                        <select name=\"projectFilterType\" (change)=\"onSelectChange($event)\" [ngModel]=\"activityType\" #projectFilterType>\n\n                            <option selected=\"selected\" value=\"{{'\\u02B0'}}\">\n                                <span>- Tous -</span>\n                            </option>\n                            <option *ngFor=\"let status of filterAssetStatusType\" [value]=\"status\">\n                                <span>{{status}}</span>\n                            </option>\n                        </select>\n                    </div>\n\n                    <div style=\"margin-top: 0;\" class=\"card\">\n                        <span>Classification de l'actif</span>\n                        <select name=\"projectFilterClassifType\" (change)=\"onSelectChange($event)\" [(ngModel)]=\"filters.filterAssetClassifType\" #projectFilterClassifType>\n                            <option selected=\"selected\" value=\"{{'\\u02B0'}}\">\n                            <span>- Tous -</span>\n                            </option>\n                            <option *ngFor=\"let classType of filterAssetClassifType\" [value]=\"classType\">\n                            <span>{{classType}}</span>\n                            </option>\n                        </select>\n                    </div>\n\n                    <div style=\"margin-top: 0;\" class=\"card\">\n                        <span>Nombre de transferts liés</span>\n                        <select name=\"assetTransfersNumberFilter\" (change)=\"onSelectChange($event)\" [(ngModel)]=\"filters.filterAssetTransfersNumber\" #assetTransfersNumberFilter>\n                            <option value=\"{{'\\u02B0'}}\">\n                                <span>- Tous -</span>\n                            </option>\n                            <option [value]=\"1\">\n                                <span>1+</span>\n                            </option>\n                            <option [value]=\"3\">\n                                <span>3+</span>\n                            </option>\n                            <option [value]=\"5\">\n                                <span>5+</span>\n                            </option>\n                            <option [value]=\"0\">\n                                <span>Aucun</span>\n                            </option>\n                        </select>\n                    </div>\n                </div>\n                <div class=\"select-wrapper col-md-6 col-lg-6\">\n                    <div>\n                        <span>Granularité de l'actif valorisable</span>\n                        <select name=\"platformType\" (change)=\"onSelectChange($event)\" [(ngModel)]=\"filters.filterPlateformeType\" #platformType>\n                            <option value=\"{{'\\u02B0'}}\">\n                                <span>- Tous -</span>\n                            </option>\n                            <option [value]=\"'Unitaire'\">\n              <span>\n                Unitaire\n              </span>\n                            </option>\n                            <option [value]=\"'Démonstrateur'\">\n              <span>\n                Démonstrateur\n              </span>\n                            </option>\n                        </select>\n                    </div>\n                    <div style=\"margin-top: 0;\" class=\"card\">\n                        <span>Statut de l'actif</span>\n                        <select name=\"assetStatusFilter\" (change)=\"onSelectChange($event)\" [(ngModel)]=\"activityStatus\" #assetStatusFilter>\n                            <option selected=\"selected\" value=\"{{'\\u02B0'}}\">\n                                <span>- Tous -</span>\n                            </option>\n                            <option *ngFor=\"let status of filterAssetStatus\" [value]=\"status\">\n                                <span>{{status}}</span>\n                            </option>\n                        </select>\n                    </div>\n                    <div style=\"margin-top: 0;\" class=\"card\">\n                        <span>Statut de la fiche</span>\n                        <select name=\"assetCardStatusFilter\" (change)=\"onSelectChange($event)\" [ngModel]=\"filters.filterAssetCardStatus\" #assetCardStatusFilter>\n                            <option value=\"{{'\\u02B0'}}\">\n                                <span>- Tous -</span>\n                            </option>\n                            <option [value]=\"'PENDING'\">\n                                <span>En cours de rédaction</span>\n                            </option>\n                            <option [value]=\"'WAITING_VALIDATION'\">\n                                <span>En attente de validation Codir</span>\n                            </option>\n                            <option [value]=\"'VALIDATED'\">\n                                <span>Validé</span>\n                            </option>\n                        </select>\n                    </div>\n                    <div class=\"tag-input\">\n                        <tag-input class=\"keywords\" [(ngModel)]=\"filters.filterChipsArr\" (onAdd)=\"onChipsChange()\" (onRemove)=\"onChipsChange()\"\n                                   [theme]=\"'foundation-theme'\" [secondaryPlaceholder]=\"'Mots-clés'\"></tag-input>\n                        <fa name=\"tags\" class=\"tags\"></fa>\n                    </div>\n                    <div class=\"clear-all-btn row\" (click)=\"clearFilter()\">\n                        <span>Réinitialiser les filtres</span>\n                        <fa name=\"times\" class=\"clear-icon\"></fa>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"col-md-1 col-lg-1\"></div>\n    </div>\n    <div class=\"row\">\n        <div class=\"col-md-1 col-lg-1\"></div>\n        <div class=\"col-md-10 col-lg-10\" style=\"padding: 0\">\n            <div class=\"panel panel-primary table-wrapper\">\n                <h4 class=\"panel-heading\">\n                    Liste des actifs valorisables\n                </h4>\n                <span class=\"results-number\" title=\"Nombre d'entrées de premier niveau\">{{assets?.data?.length}}</span>\n                <p-treeTable [value]=\"assets?.data\">\n                    <p-column class=\"info-td\" [style]=\"{'width':'15%'}\">\n                        <ng-template let-col let-node=\"rowData\">\n                            <div class=\"td-wrapper\" [ngStyle]=\"{'top': '-9px'}\">\n                                <div class=\"badge\" [ngClass]=\"node.data.isPlatformAsset ? 'plateforme' : 'unitaire'\">{{node.data.isPlatformAsset ? 'Démonstrateur' : 'Unitaire'}}</div>\n                                <div class=\"table-id-wrapper\">\n                                    <fa *ngIf=\"node.data.parentAssetId!=0\" class=\"child-icon\" name=\"reply\" [rotate]=\"180\"></fa>\n                                    <span style=\"font-weight: bolder;\">{{node.data.id}}</span>\n                                </div>\n                                <p>{{node.data?.type}}</p>\n                                <div class=\"transfers-number\">\n                                    <a [routerLink]=\"['/asset_info', node.data.id]\" fragment=\"related-transfers\">\n                                        <span *ngIf=\"node.data.transfersNumber && node.data.transfersNumber !== 0\" title=\"Nombre de transferts associés\" class=\"number-tag\">{{node.data.transfersNumber}}</span>\n                                    </a>\n                                </div>\n                            </div>\n                        </ng-template>\n                    </p-column>\n\n                    <p-column [style]=\"{'width':'55%'}\">\n                        <ng-template let-col let-node=\"rowData\">\n                            <div class=\"td-flex-wrapper\">\n                                <div class=\"td-wrapper second-td\" [ngStyle]=\"{'top': '6px'}\">\n                                    <p class=\"asset-description name\">{{node.data.identification.name}}</p>\n                                    <p class=\"asset-description description\">{{node.data.desc}}</p>\n                                    <p class=\"asset-description status\">{{node.data.assetStatus == 'PENDING' ? 'En cours de rédaction' : node.data.assetStatus == 'WAITING_VALIDATION' ? 'Pour validation CODIR' : 'Validé'}}</p>\n                                </div>\n                            </div>\n                        </ng-template>\n                    </p-column>\n\n                    <p-column [style]=\"{'width':'10%'}\">\n                        <ng-template let-col let-node=\"rowData\">\n                            <div class=\"td-wrapper\">\n                                <p>{{node.data.identification.program.shortName}}</p>\n                            </div>\n                        </ng-template>\n                    </p-column>\n\n                    <p-column [style]=\"{'width':'10%'}\">\n                        <ng-template let-col let-node=\"rowData\">\n                            <div class=\"td-wrapper\">\n                                <p>{{node.data.identification.project.shortName}}</p>\n                            </div>\n                        </ng-template>\n                    </p-column>\n\n                    <p-column [style]=\"{'width':'10%'}\">\n                        <ng-template let-col let-node=\"rowData\">\n                            <div class=\"icons-wrapper\">\n                                <fa class=\"user-icon\" name=\"eye\" (click)=\"viewAssetInfo(node.data)\"></fa>\n                                <fa class=\"user-icon\" name=\"pencil\" (click)=\"toEditAsset(node.data)\" [ngClass]=\"{'disabled': !isAdmin() && (!isAssetAuthor(node.data) || node.data.assetStatus === 'VALIDATED')}\"></fa>\n                                <fa class=\"user-icon\" name=\"arrow-right\" (click)=\"toCreateTransfer(node.data)\" [ngClass]=\"{'disabled': !isAdmin()}\"></fa>\n                                <fa class=\"user-icon\" name=\"trash\" (click)=\"deleteAsset(node.data)\" [ngClass]=\"{'disabled': !isAdmin() && !isAssetAuthor(node.data) || node.data.transfersNumber > 0}\"></fa>\n                            </div>\n                        </ng-template>\n                    </p-column>\n                </p-treeTable>\n            </div>\n        </div>\n        <div class=\"col-md-1 col-lg-1\"></div>\n    </div>\n</div>\n"

/***/ }),

/***/ "./src/app/projects-list/projects-list.component.scss":
/*!************************************************************!*\
  !*** ./src/app/projects-list/projects-list.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "* {\n  word-break: break-all; }\n\nh5 {\n  margin: 0; }\n\nselect {\n  background: transparent;\n  border: 1px solid #0062cc;\n  border-radius: 3px; }\n\nselect:hover {\n    outline: transparent;\n    background-color: #0069d9;\n    color: #fff; }\n\nselect:active {\n    background-color: inherit;\n    color: #000; }\n\n.projects-main-body {\n  cursor: default;\n  padding-top: 90px; }\n\n.projects-main-body .panel, .projects-main-body .panel-primary {\n    border-radius: 0; }\n\n.projects-main-body .panel .panel-heading, .projects-main-body .panel-primary .panel-heading {\n      border-top-left-radius: 0;\n      border-top-right-radius: 0; }\n\n.projects-main-body .table-wrapper {\n    position: relative; }\n\n.projects-main-body .table-wrapper .table-header {\n      padding: 10px;\n      padding-right: 0;\n      border-top-left-radius: 3px;\n      border-top-right-radius: 3px;\n      font-size: 15px;\n      text-align: left;\n      background-color: #337ab7;\n      border-color: #337ab7;\n      color: #fff;\n      font-weight: 600;\n      width: 100%;\n      height: 42px; }\n\n.projects-main-body .table-wrapper .td-flex-wrapper {\n      display: flex;\n      justify-content: center;\n      align-items: center; }\n\n.projects-main-body .table-wrapper .td-wrapper .badge {\n      border-radius: 3px;\n      position: absolute;\n      top: 0;\n      left: 50%;\n      -webkit-transform: translate(-50%, -100%);\n              transform: translate(-50%, -100%); }\n\n.projects-main-body .table-wrapper .td-wrapper .plateforme {\n      background-color: #5cb85c; }\n\n.projects-main-body .table-wrapper .td-wrapper .unitaire {\n      background-color: #337ab7; }\n\n.projects-main-body .table-wrapper .td-wrapper .transfers-number {\n      font-size: 12px;\n      position: absolute;\n      top: 0;\n      right: 5px;\n      -webkit-transform: translate(0, -100%);\n              transform: translate(0, -100%); }\n\n.projects-main-body .table-wrapper .td-wrapper .transfers-number .number-tag {\n        padding: 2px 10px;\n        background: #1f8927;\n        color: whitesmoke;\n        font-weight: bolder;\n        border-radius: 3px; }\n\n.projects-main-body .table-wrapper .results-number {\n      position: absolute;\n      right: 20px;\n      top: 15px;\n      font-size: 1.2em;\n      font-weight: bolder;\n      color: white; }\n\n.projects-main-body .table-wrapper ::ng-deep.ui-treetable .ui-treetable-row {\n      width: 100%;\n      height: 90px; }\n\n.projects-main-body .table-wrapper ::ng-deep.ui-treetable .ui-treetable-row .ui-treetable-child-table-container {\n        background-color: #F5F5F5; }\n\n.projects-main-body .table-wrapper ::ng-deep.ui-treetable .ui-treetable-row .info-td {\n        width: 20%;\n        max-width: 20%;\n        overflow: hidden; }\n\n.projects-main-body .table-wrapper ::ng-deep.ui-treetable ::ng-deep tbody {\n      border-right-color: transparent; }\n\n.projects-main-body .table-wrapper ::ng-deep.ui-treetable ::ng-deep td:not(:last-child) {\n      padding: 0 !important;\n      font-size: 15px;\n      border-right: 1px solid #ddd; }\n\n.projects-main-body .table-wrapper ::ng-deep.ui-treetable thead {\n      display: none; }\n\n.projects-main-body .table-wrapper ::ng-deep.ui-treetable table {\n      table-layout: auto; }\n\n.projects-main-body .table-wrapper ::ng-deep.ui-treetable ::ng-deep td {\n      font-size: 15px; }\n\n.projects-main-body .table-wrapper ::ng-deep.ui-treetable .second-td {\n      padding-left: 12px; }\n\n.projects-main-body .table-wrapper ::ng-deep.ui-treetable .second-td p {\n        text-align: left; }\n\n.projects-main-body .table-wrapper ::ng-deep.ui-treetable .td-wrapper {\n      position: relative;\n      top: 30px; }\n\n.projects-main-body .table-wrapper ::ng-deep.ui-treetable .td-wrapper .table-id-wrapper {\n        margin-top: 5px;\n        width: 100%;\n        display: flex;\n        justify-content: center; }\n\n.projects-main-body .table-wrapper ::ng-deep.ui-treetable .td-wrapper .table-id-wrapper span {\n          width: 80%;\n          text-align: center; }\n\n.projects-main-body .table-wrapper ::ng-deep.ui-treetable .icons-wrapper {\n      position: relative;\n      top: 36px; }\n\n.projects-main-body .table-wrapper ::ng-deep.ui-treetable .icons-wrapper fa {\n        width: 25%;\n        display: block;\n        float: left;\n        text-align: center; }\n\n.projects-main-body .table-wrapper ::ng-deep.ui-treetable .icons-wrapper .user-icon:hover {\n        cursor: pointer; }\n\n.projects-main-body .table-wrapper ::ng-deep.ui-treetable .icons-wrapper .user-icon.disabled {\n        opacity: 0.75;\n        /* Opacity (Transparency) */\n        color: rgba(0, 0, 0, 0.75);\n        /* RGBA Color (Alternative Transparency) */\n        -webkit-filter: blur(0.5px);\n        /* Blur */\n        -moz-filter: blur(0.5px);\n        filter: blur(0.5px); }\n\n.projects-main-body .table-wrapper ::ng-deep.ui-treetable .icons-wrapper .user-icon.disabled:hover {\n          cursor: default; }\n\n.projects-main-body .table-wrapper .panel-heading {\n      padding: 15px;\n      margin: 0; }\n\n.projects-main-body p {\n    word-break: break-word;\n    white-space: normal;\n    text-align: center;\n    padding: 0 0.5vw;\n    font-size: 15px; }\n\n.projects-main-body .asset-description {\n    word-break: break-word;\n    white-space: nowrap;\n    padding: 0 0.5vw;\n    font-size: 15px;\n    line-height: 25px;\n    text-overflow: ellipsis;\n    display: block;\n    overflow: hidden;\n    text-align: left;\n    width: 43vw; }\n\n.projects-main-body .asset-description.name {\n      font-weight: bolder;\n      margin-bottom: 0; }\n\n.projects-main-body .asset-description.description {\n      font-style: italic;\n      font-size: .9em;\n      line-height: 1em; }\n\n.projects-main-body select:disabled {\n    opacity: 0.3; }\n\n.projects-main-body .all-btn-wrapper div span {\n    font-size: 15px; }\n\n.projects-main-body .all-btn-wrapper div fa {\n    font-size: 25px; }\n\n.projects-main-body .all-btn-wrapper div:first-of-type {\n    width: 4vw;\n    height: 2vw;\n    border: 0.1vw solid #d3d3d3;\n    margin-right: 1vw; }\n\n.projects-main-body .filters-box {\n    border: 1px solid #337ab7;\n    padding: 0;\n    margin-bottom: 1vw; }\n\n.projects-main-body .filters-box .toolbar {\n      height: 30px;\n      background-color: #337ab7;\n      cursor: pointer;\n      color: white; }\n\n.projects-main-body .filters-box .toolbar .fold.icon {\n        height: 30px;\n        width: 30px;\n        display: flex;\n        justify-content: center;\n        align-items: center; }\n\n.projects-main-body .filters-box .toolbar .fold.icon.folded {\n          -webkit-transform: rotate(-90deg);\n                  transform: rotate(-90deg); }\n\n.projects-main-body .filters-box .toolbar .toolbar-title {\n        position: absolute;\n        top: 0px;\n        left: 50%;\n        -webkit-transform: translate(-50%, 25%);\n                transform: translate(-50%, 25%); }\n\n.projects-main-body .filters-box .filter-wrapper {\n      margin: 10px 0; }\n\n.projects-main-body .filters-box .filter-wrapper.folded {\n        height: 0px;\n        overflow: hidden;\n        margin: 0; }\n\n.projects-main-body .filters-box .filter-wrapper .select-wrapper {\n        height: 100%; }\n\n.projects-main-body .filters-box .filter-wrapper .select-wrapper .half-width {\n          display: inline-block;\n          margin: 10px 10px 5px 20px;\n          width: 45%; }\n\n.projects-main-body .filters-box .filter-wrapper .select-wrapper div {\n          height: 40px;\n          border: 1px solid #d3d3d3;\n          padding: 8px 10px;\n          margin: 10px 20px;\n          border-radius: 2px; }\n\n.projects-main-body .filters-box .filter-wrapper .select-wrapper div span {\n            font-size: 15px;\n            display: inline-block;\n            float: left; }\n\n.projects-main-body .filters-box .filter-wrapper .select-wrapper div fa {\n            font-size: 25px;\n            display: inline-block;\n            float: right; }\n\n.projects-main-body .filters-box .filter-wrapper .select-wrapper div .clear-icon {\n            font-size: 25px;\n            position: relative;\n            bottom: 7px;\n            right: 1px; }\n\n.projects-main-body .filters-box .filter-wrapper .select-wrapper div select {\n            font-size: 15px;\n            height: 26px;\n            border: 1px solid #000;\n            position: relative;\n            bottom: 2px;\n            width: 110px;\n            display: inline-block;\n            float: right; }\n\n.projects-main-body .filters-box .filter-wrapper .select-wrapper div select:hover {\n              cursor: pointer; }\n\n.projects-main-body .filters-box .filter-wrapper .select-wrapper .tag-input {\n          padding: 0; }\n\n.projects-main-body .filters-box .filter-wrapper .select-wrapper .tag-input tag-input.keywords {\n            height: 38px;\n            display: inline-block;\n            /* Details CSS in styles.scss because of precedence over Bootstrap*/ }\n\n.projects-main-body .filters-box .filter-wrapper .select-wrapper .tag-input fa {\n            height: 100%;\n            display: flex;\n            justify-content: center;\n            align-items: center;\n            font-size: 20px;\n            margin-right: 10px; }\n\n.projects-main-body .filters-box .filter-wrapper .select-wrapper .clear-all-btn:hover {\n          cursor: pointer; }\n\n.projects-main-body .filters-box .filter-wrapper .select-wrapper .clear-all-btn:active {\n          background-color: #D3D3D3; }\n"

/***/ }),

/***/ "./src/app/projects-list/projects-list.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/projects-list/projects-list.component.ts ***!
  \**********************************************************/
/*! exports provided: ProjectsListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectsListComponent", function() { return ProjectsListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_requests_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/requests.service */ "./src/app/services/requests.service.ts");
/* harmony import */ var _services_local_storage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/local-storage.service */ "./src/app/services/local-storage.service.ts");
/* harmony import */ var _services_pass_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/pass.service */ "./src/app/services/pass.service.ts");
/* harmony import */ var _services_filters_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/filters.service */ "./src/app/services/filters.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ProjectsListComponent = /** @class */ (function () {
    function ProjectsListComponent(router, requests, storageService, filters, passService) {
        var _this = this;
        this.router = router;
        this.requests = requests;
        this.storageService = storageService;
        this.filters = filters;
        this.passService = passService;
        this.user = {};
        this.programmToFilter = '\u02B0';
        this.projectToFilter = '\u02B0';
        this.activityStatus = '\u02B0';
        this.activityType = '\u02B0';
        this.filtersFolded = false;
        this.assetsCache = [];
        this.assets = {};
        this.assetsObj = {};
        this.filterAssetClassifType = [];
        this.filterAssetStatusType = [];
        this.filterAssetStatus = [];
        this.userSubscription = storageService.getUserObservable().subscribe(function (user) {
            _this.user = user;
            if (!_this.user) {
                _this.router.navigate(['login']);
            }
        });
    }
    ProjectsListComponent.prototype.filterAssets = function () {
        var resObj = {
            tree: [],
            processed: []
        };
        // Filter assets by keeping trace of processed assets and avoid duplicates
        // Storing the result tree in the resObj accumulator
        this.doFilterAsset(this.assetsCache, resObj);
        this.assets.data = Object.values(resObj.tree);
    };
    ProjectsListComponent.prototype.doFilterAsset = function (assets, result) {
        var programF = this.filters.filterProgrammValue === 'Sans nom de programme' ? '' :
            (this.programmToFilter === '\u02B0' ? '\u02B0' : this.programmToFilter['_id']);
        var projectF = this.filters.filterProjectValue === 'Sans nom de projet' ? '' :
            (this.projectToFilter === '\u02B0' ? '\u02B0' : this.projectToFilter['_id']);
        var assetTypeF = this.filters.filterAssetType;
        var assetClassifTypeF = this.filters.filterAssetClassifType;
        var assetCardStatusF = this.filters.filterAssetCardStatus;
        var assetStatusF = this.activityStatus;
        var assetTransfersNumber = this.filters.filterAssetTransfersNumber;
        var typeF = this.filters.filterPlateformeType === "\u02B0" ? "\u02B0" : this.filters.filterPlateformeType === 'Démonstrateur';
        var _loop_1 = function (asset) {
            if (asset.data.identification
                && ((programF === "\u02B0") || asset.data.identification.programId === programF)
                && ((projectF === "\u02B0") || asset.data.identification.projectId === projectF)
                && ((assetTypeF === "\u02B0") || asset.data.type === assetTypeF)
                && ((assetClassifTypeF[0] === "\u02B0") || asset.data.classification.type === assetClassifTypeF)
                && ((assetCardStatusF === "\u02B0") || asset.data.assetStatus === assetCardStatusF)
                && ((assetStatusF === "\u02B0") || asset.data.status === assetStatusF) // TODO
                && ((assetTransfersNumber === "\u02B0") || this_1.filterMatchesNumberOfTransfers(asset.data, parseInt(assetTransfersNumber)))
                && ((typeof typeF === 'string') || asset.data.isPlatformAsset === typeF)) {
                if (this_1.filters.filterChipsArr.length > 0) {
                    var isCompared = true;
                    if (this_1.filters.filterChipsArr.every(function (chip) {
                        return asset.data.desc.toLowerCase().indexOf(chip.value.toLowerCase()) === -1;
                    }) && this_1.filters.filterChipsArr.every(function (chip) {
                        return asset.data.keywords.toLowerCase().indexOf(chip.value.toLowerCase()) === -1;
                    })) {
                        isCompared = false;
                    }
                    // Si asset éligible et non traité
                    // On rajoute l'asset dans l'accumulateur et garde trace de lui et ses enfants de 1er niveau
                    if (isCompared && !result.processed.includes(asset.data.id)) {
                        result.processed.push(asset.data.id);
                        result.tree[asset.data.id] = asset;
                        if (asset.children && asset.children.length > 0) {
                            asset.children.forEach(function (item) { return result.processed.push(item.data.id); });
                        }
                    }
                }
                else if (!result.processed.includes(asset.data.id)) {
                    result.processed.push(asset.data.id);
                    result.tree[asset.data.id] = asset;
                    if (asset.children && asset.children.length > 0) {
                        asset.children.forEach(function (item) { return result.processed.push(item.data.id); });
                    }
                }
            }
            // Si l'asset a des enfants, traitement récursif
            if (asset.children && asset.children.length > 0) {
                this_1.doFilterAsset(asset.children, result);
            }
        };
        var this_1 = this;
        for (var _i = 0, assets_1 = assets; _i < assets_1.length; _i++) {
            var asset = assets_1[_i];
            _loop_1(asset);
        }
    };
    ProjectsListComponent.prototype.filterMatchesNumberOfTransfers = function (asset, nbr) {
        if (nbr === 0) {
            if (!asset.transfersNumber || asset.transfersNumber === 0) {
                return true;
            }
        }
        else {
            if (asset.transfersNumber && asset.transfersNumber >= nbr) {
                return true;
            }
        }
        return false;
    };
    ProjectsListComponent.prototype.updateData = function (nodes) {
        var _this = this;
        return nodes ? nodes.map(function (node) {
            /*
              if (this.filters.filterProgramsValues.indexOf(node.identification.program || 'Sans nom de programme') < 0) {
                this.filters.filterProgramsValues.push(node.identification.program || 'Sans nom de programme');
              }

              if (this.filters.filterProjectValues.indexOf(node.identification.project || 'Sans nom de projet') < 0) {
                this.filters.filterProjectValues.push(node.identification.project || 'Sans nom de projet');
              }


              this.filters.filterProgramsValues.push({
                label: node.identification.program || 'Sans nom de projet',
                value: node.id
              });

              this.filters.filterProjectValues.push({
                label: node.identification.name || 'Sans nom de programme',
                value: node.id
              });

              this.filters.filterProgramsValues[node.identification.program || 'Sans nom de projet'] = '';
              this.filters.filterProjectValues[node.identification.name || 'Sans nom de programme'] = '';
            */
            var asset = {
                'data': node,
                'children': node.children ? _this.updateData(node.children) : []
            };
            _this.assetsObj[asset.data.id] = asset;
            return asset;
        }) : [];
    };
    ProjectsListComponent.prototype.clearFilter = function () {
        this.filters.filterProgramsValues = ["\u02B0"];
        this.programmToFilter = "\u02B0";
        this.filters.filterProjectValues = ["\u02B0"].concat(this.filters.initFilterProjectValues[this.filters.filterProgramsValues[0]]);
        this.projectToFilter = "\u02B0";
        this.filters.filterAssetType = "\u02B0";
        this.filters.filterAssetClassifType = "\u02B0";
        this.filters.filterAssetCardStatus = "\u02B0";
        this.filters.filterAssetTransfersNumber = "\u02B0";
        this.filters.filterAssetStatus = "\u02B0";
        this.filters.filterPlateformeType = "\u02B0";
        this.activityStatus = '\u02B0';
        this.filters.filterChipsArr = [];
        this.programmFilterSelect.nativeElement.selectedIndex = 0;
        this.projectFilterSelect.nativeElement.selectedIndex = 0;
        this.projectFilterType.nativeElement.selectedIndex = 0;
        this.assetCardStatusFilter.nativeElement.selectedIndex = 0;
        this.assetTransfersNumberFilter.nativeElement.selectedIndex = 0;
        this.platformType.nativeElement.selectedIndex = 0;
        this.getAssets();
        this.getData();
    };
    ProjectsListComponent.prototype.onSelectChange = function (event) {
        if (event.target.name === 'programmFilterSelect') {
            this.filters.filterProgrammValue = event.target.value;
            this.filters.filterProjectValue = "\u02B0";
            this.projectToFilter = "\u02B0";
            this.filters.filterProjectValues = ["\u02B0"].concat(this.filters.initFilterProjectValues[this.filters.filterProgrammValue]);
        }
        else if (event.target.name === 'projectFilterSelect') {
            this.filters.filterProjectValue = event.target.value;
        }
        else if (event.target.name === 'platformType') {
            this.filters.filterPlateformeType = event.target.value;
        }
        else if (event.target.name === 'projectFilterType') {
            this.filters.filterAssetType = event.target.value;
        }
        else if (event.target.name === 'projectFilterClassifType') {
            this.filters.filterAssetClassifType = event.target.value;
        }
        else if (event.target.name === 'assetCardStatusFilter') {
            this.filters.filterAssetCardStatus = event.target.value;
        }
        else if (event.target.name === 'assetTransfersNumberFilter') {
            this.filters.filterAssetTransfersNumber = event.target.value;
        }
        else if (event.target.name === 'assetStatusFilter') {
            this.filters.filterAssetStatus = this.activityStatus;
        }
        this.filterAssets();
    };
    ProjectsListComponent.prototype.onChipsChange = function () {
        this.filterAssets();
    };
    ProjectsListComponent.prototype.assetsTree = function (assets) {
        var keysObj = {};
        var resArr = [];
        assets.forEach(function (asset) {
            if (keysObj[asset.id]) {
                var tArr = keysObj[asset.id];
                keysObj[asset.id] = asset;
                asset.children = tArr;
            }
            else {
                keysObj[asset.id] = asset;
                asset.children = [];
            }
            if (asset.parentAssetId) {
                if (Array.isArray(keysObj[asset.parentAssetId])) {
                    keysObj[asset.parentAssetId].push(asset);
                }
                else {
                    keysObj[asset.parentAssetId].children.push(asset);
                }
            }
            else {
                resArr.push(asset);
            }
        });
        return resArr;
    };
    ProjectsListComponent.prototype.getAssets = function () {
        var _this = this;
        this.assetsObj = {};
        this.requests.getAssets().then(function (res) {
            console.log(res);
            _this.getTransfersForAssetRecursive(res);
            _this.assets = {
                'data': _this.updateData(res)
            };
            _this.assetsCache = _this.assets.data;
            _this.filterAssets();
        })
            .catch(function (err) {
            console.log(err);
        });
    };
    ProjectsListComponent.prototype.deleteAsset = function (asset) {
        var _this = this;
        if (asset.transfersNumber && asset.transfersNumber > 0) {
            return;
        }
        if (!this.isAdmin() && !this.isAssetAuthor(asset)) {
            return;
        }
        var deleteAsset = confirm("Delete asset '" + asset.identification.name + "'");
        if (!deleteAsset) {
            return;
        }
        this.requests.deleteAsset(asset.id).then(function (res) {
            _this.getAssets();
        }).catch(function (err) {
            _this.getAssets();
        });
    };
    ProjectsListComponent.prototype.isAdmin = function () {
        return this.passService.isAdmin();
    };
    ProjectsListComponent.prototype.isAssetAuthor = function (asset) {
        return this.passService.isAssetAuthor(asset);
    };
    ProjectsListComponent.prototype.isUserHasAssetRights = function (asset) {
        return this.passService.isUserHasAssetRights(asset);
    };
    ProjectsListComponent.prototype.toNewAsset = function () {
        this.router.navigate(['new_asset']);
    };
    ProjectsListComponent.prototype.toEditAsset = function (nodeData) {
        if (!this.isAdmin() && (this.user.id != nodeData.identification.userId || nodeData.status === 'VALIDATED')) {
            return;
        }
        this.storageService.setAsset(nodeData);
        this.router.navigate(["edit_asset/" + nodeData.id]);
    };
    ProjectsListComponent.prototype.toCreateTransfer = function (nodeData) {
        if (!this.isAdmin()) {
            return;
        }
        this.storageService.setAsset(nodeData);
        this.router.navigate(['new_transfer']);
    };
    ProjectsListComponent.prototype.getDisplayStringForAssetType = function (type) {
        switch (type) {
            case 'METHOD':
                return 'Methode, Processus, Bonnes pratiques, Guidelines, Specifications';
            case 'STATE_OF_ART':
                return 'Etat de l\'art';
            case 'STUDY':
                return 'Etude (exploiter/valider)';
            case 'DATASET':
                return 'Dataset/corpus';
            case 'PATENT':
                return 'Brevet';
            case 'PUBLICATION':
                return 'Publication';
            case 'SOFTWARE':
                return 'Logiciel, Modele, outil, algorithme, code';
            case 'STANDARD':
                return 'Standart';
            case 'OTHER':
                return 'Autre';
        }
    };
    ProjectsListComponent.prototype.getTransfersForAssetRecursive = function (assetsArray) {
        var _this = this;
        assetsArray.forEach(function (item) {
            if (item.children) {
                _this.getTransfersForAssetRecursive(item.children);
            }
            _this.getTransfersForAsset(item.id).subscribe(function (res) {
                item.transfersNumber = res.length;
                item.transfers = res;
            });
        });
    };
    ProjectsListComponent.prototype.getTransfersForAsset = function (assetId) {
        return this.requests.getTransfersForAsset(assetId);
    };
    ProjectsListComponent.prototype.getData = function () {
        var _this = this;
        this.filters.filterProgramsValues = ["\u02B0"];
        this.filters.filterProjectValues = ["\u02B0"];
        this.filters.filterAssetClassifType = "\u02B0";
        this.requests.getProgramsList().then(function (res) {
            _this.filters.filterProgramsValues = res;
        }).catch(function (err) {
            console.log(err);
        });
        this.requests.classificationList().then(function (res) {
            _this.filterAssetClassifType = Object.values(res).map(function (i) { return i.name; });
        }).catch(function (err) {
            console.log(err);
        });
        this.requests.getItems('statusList').then(function (res) {
            _this.filterAssetStatus = Object.values(res).map(function (i) { return i.name; });
        }).catch(function (err) {
            console.log(err);
        });
        this.requests.getItems('typeList').then(function (res) {
            _this.filterAssetStatusType = Object.values(res).map(function (i) { return i.name; });
        }).catch(function (err) {
            console.log(err);
        });
    };
    ProjectsListComponent.prototype.viewAssetInfo = function (asset) {
        this.router.navigate(["/asset_info/" + asset.id]);
    };
    ProjectsListComponent.prototype.toggleFilters = function () {
        this.filtersFolded = !this.filtersFolded;
    };
    ProjectsListComponent.prototype.ngOnInit = function () {
        this.user = this.storageService.getUser();
        this.storageService.setAsset({});
        this.getAssets();
        this.getData();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('changeState'),
        __metadata("design:type", Function)
    ], ProjectsListComponent.prototype, "changeState", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('programmFilterSelect'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], ProjectsListComponent.prototype, "programmFilterSelect", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('projectFilterSelect'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], ProjectsListComponent.prototype, "projectFilterSelect", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('projectFilterType'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], ProjectsListComponent.prototype, "projectFilterType", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('projectFilterClassifType'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], ProjectsListComponent.prototype, "projectFilterClassifType", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('assetCardStatusFilter'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], ProjectsListComponent.prototype, "assetCardStatusFilter", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('assetTransfersNumberFilter'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], ProjectsListComponent.prototype, "assetTransfersNumberFilter", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('platformType'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], ProjectsListComponent.prototype, "platformType", void 0);
    ProjectsListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-projects-list',
            template: __webpack_require__(/*! ./projects-list.component.html */ "./src/app/projects-list/projects-list.component.html"),
            styles: [__webpack_require__(/*! ./projects-list.component.scss */ "./src/app/projects-list/projects-list.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _services_requests_service__WEBPACK_IMPORTED_MODULE_2__["RequestsService"],
            _services_local_storage_service__WEBPACK_IMPORTED_MODULE_3__["LocalStorageService"],
            _services_filters_service__WEBPACK_IMPORTED_MODULE_5__["FiltersService"],
            _services_pass_service__WEBPACK_IMPORTED_MODULE_4__["PassService"]])
    ], ProjectsListComponent);
    return ProjectsListComponent;
}());



/***/ }),

/***/ "./src/app/reference-edit/reference-edit.component.html":
/*!**************************************************************!*\
  !*** ./src/app/reference-edit/reference-edit.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal fade\" id=\"modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"modalTitle\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h5 class=\"modal-title\" id=\"modalTitle\">{{isEditing || isEditingUnit ? 'Editer' : 'Ajouter'}}</h5>\n        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\" #closeAddExpenseModal>\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\">\n        <ng-container *ngIf=\"type !== 'utilisateurs' && type !== 'projets' && type !== 'programmes' && type !== 'classification' && !isAddingUnit && !isEditingUnit\">\n          <input type=\"text\" [placeholder]=\"itemPlaceholder\" class=\"form-control\" [(ngModel)]=\"itemName\">\n        </ng-container>\n\n        <ng-container *ngIf=\"isAddingUnit\">\n          <input type=\"text\" [placeholder]=\"'Nom'\" class=\"form-control\" [(ngModel)]=\"effort.name\">\n        </ng-container>\n\n        <ng-container *ngIf=\"isEditingUnit\">\n          <input type=\"text\" [placeholder]=\"'Nom'\" class=\"form-control\" [(ngModel)]=\"effort.name\">\n        </ng-container>\n\n        <ng-container *ngIf=\"type === 'programmes' || type === 'projets' || type === 'classification'\">\n          <input type=\"text\" [placeholder]=\"'Nom'\" class=\"form-control\" [(ngModel)]=\"itemName\">\n          <input type=\"text\" [placeholder]=\"'Nom court'\" class=\"form-control\" [(ngModel)]=\"itemShortName\">\n        </ng-container>\n\n        <ng-container *ngIf=\"type === 'utilisateurs'\">\n          <ng-container  *ngIf=\"!isEditing\">\n            <input type=\"text\" [placeholder]=\"'Nom'\" class=\"form-control\" [(ngModel)]=\"user.firstName\">\n            <input type=\"text\" [placeholder]=\"'Prénom'\" class=\"form-control\" [(ngModel)]=\"user.lastName\">\n            <input type=\"text\" [placeholder]=\"'Login'\" class=\"form-control\" [(ngModel)]=\"user.login\">\n            <input type=\"password\" [placeholder]=\"'Mot de passe'\" class=\"form-control\" [(ngModel)]=\"user.password\">\n            <input type=\"password\" [placeholder]=\"'Répéter le mot de passe'\" class=\"form-control\" [(ngModel)]=\"passwordCheck\">\n            <div class=\"alert alert-danger\" role=\"alert\" *ngIf=\"user.password !== passwordCheck\">Passwords must match!</div>\n            <label>\n              Choisir le rôle\n              <select [(ngModel)]=\"user.role\">\n                <option name=\"user\" value=\"USER\">Utilisateur</option>\n                <option name=\"super-user\" value=\"SUPER_USER\">Super-utilisateur</option>\n                <option name=\"admin\" value=\"ADMIN\">Administrateur</option>\n              </select>\n            </label>\n            <h5>Sélectionner les programmes</h5>\n\n            <div class=\"creation-rights-wrapper\">\n              <div *ngFor=\"let i of itemsList\" class=\"items-list\" style=\"margin: 15px 0\">\n                <label>\n                  Programme:\n                  <input style=\"margin-left: 15px;\" type=\"checkbox\" (change)=\"creationRightsChangeProgram($event, i, 'user')\">\n                  <span>{{i.name}}</span>\n                  <div style=\"display: flex; flex-direction: column; margin-left: 40px;\">\n                    Projects:\n                    <label *ngFor=\"let project of i['projects']\">\n                      <input style=\"margin-left: 15px;\" [id]=\"i.id + '/' + project.id\" type=\"checkbox\"\n                             [disabled]=\"shouldRadioDisable(i.id, 'user')\"\n                             (change)=\"creationRightsChangeProject($event, i.id, 'user', project)\">\n                      <span>{{project.name}}</span>\n                    </label>\n                  </div>\n                </label>\n              </div>\n            </div>\n          </ng-container>\n\n          <ng-container *ngIf=\"isEditing\">\n            <input type=\"text\" [placeholder]=\"'Nom'\" class=\"form-control\" [(ngModel)]=\"currentUser.firstName\">\n            <input type=\"text\" [placeholder]=\"'Prénom'\" class=\"form-control\" [(ngModel)]=\"currentUser.lastName\">\n            <input type=\"text\" [placeholder]=\"'Login'\" class=\"form-control\" [(ngModel)]=\"currentUser.login\">\n            <button type=\"button\" class=\"btn btn-success\" *ngIf=\"!isPasswordFieldsVisible\" (click)=\"isPasswordFieldsVisible = true\">Changer le mot de passe</button>\n            <ng-container *ngIf=\"isPasswordFieldsVisible\">\n              <input type=\"password\" [placeholder]=\"'Mot de passe'\" class=\"form-control\" [(ngModel)]=\"currentUser.password\">\n              <input type=\"password\" [placeholder]=\"'Répéter le mot de passe'\" class=\"form-control\" [(ngModel)]=\"passwordCheck\">\n              <div class=\"alert alert-danger\" role=\"alert\" *ngIf=\"currentUser.password !== passwordCheck\">Passwords must match!</div>\n            </ng-container>\n            <button type=\"button\" class=\"btn btn-danger\" *ngIf=\"isPasswordFieldsVisible\" (click)=\"clearPasswords()\">Annuler changer le mot de passe</button>\n\n            <div class=\"edit-creation-rights\">\n              <div *ngFor=\"let i of itemsList\" class=\"items-list\" style=\"margin-bottom: 20px\">\n                <label>\n                  Programme:\n                  <input style=\"margin-left: 15px;\" type=\"checkbox\" [checked]=\"doesUserHasProgramRight(i.name, 'currentUser')\" (change)=\"creationRightsChangeProgram($event, i, 'currentUser')\">\n                  <span>{{i.name}}</span>\n                  <div style=\"display: flex; flex-direction: column; margin-left: 40px;\">\n                    Projects:\n                    <label *ngFor=\"let project of i['projects']\">\n                      <input style=\"margin-left: 15px;\" [id]=\"i.id + '/' + project.id\" type=\"checkbox\"\n                             [disabled]=\"shouldRadioDisable(i.id, 'currentUser')\"\n                             [checked]=\"doesUserHasProjectRight(i.id, project.id, 'currentUser')\"\n                             (change)=\"creationRightsChangeProject($event, i.id, 'currentUser', project)\">\n                      <span>{{project.name}}</span>\n                    </label>\n                  </div>\n                </label>\n              </div>\n            </div>\n          </ng-container>\n        </ng-container>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-danger\" data-dismiss=\"modal\"  (click)=\"clear()\">Annuler</button>\n        <button type=\"button\" class=\"btn btn-success\" data-dismiss=\"modal\" (click)=\"addItem()\" [disabled]=\"isFormInvalid()\"\n                *ngIf=\"!isEditing && !isEditingUnit && !isAddingUnit\">\n          Valider\n        </button>\n        <button type=\"button\" class=\"btn btn-success\" data-dismiss=\"modal\" (click)=\"saveUnit()\" [disabled]=\"!effort.name\"\n                *ngIf=\"isAddingUnit\">\n          Valider\n        </button>\n        <button type=\"button\" class=\"btn btn-success\" data-dismiss=\"modal\" (click)=\"editUnit()\" [disabled]=\"!effort.name\"\n                *ngIf=\"isEditingUnit\">\n          Valider\n        </button>\n        <button type=\"button\" class=\"btn btn-success\" data-dismiss=\"modal\" (click)=\"saveChanges()\" [disabled]=\"!itemName || !itemShortName\"\n                *ngIf=\"isEditing && (type === 'projets' || type === 'programmes' || type === 'classification')\">\n          Valider\n        </button>\n        <button type=\"button\" class=\"btn btn-success\" data-dismiss=\"modal\" (click)=\"editUser()\" [disabled]=\"isFormInvalid()\"\n                *ngIf=\"isEditing && type === 'utilisateurs'\">\n          Valider\n        </button>\n        <button type=\"button\" class=\"btn btn-success\" data-dismiss=\"modal\" (click)=\"saveChanges()\" [disabled]=\"!itemName\"\n                *ngIf=\"isEditing && type !== 'utilisateurs' && type !== 'projets' && type !== 'programmes' && type !== 'classification'\">\n          Valider\n        </button>\n      </div>\n    </div>\n  </div>\n</div>\n\n<div class=\"row edit-form-wrapper\">\n  <div class=\"col-md-1 col-lg-1\"></div>\n  <div class=\"panel panel-primary col-md-10 col-lg-10\">\n    <div class=\"panel-heading\">\n      <span class=\"panel-title\">Gestion de la liste \"{{type}}\"</span>\n    </div>\n    <div class=\"panel-body\" *ngIf=\"type !== 'utilisateurs' && type !== 'projets' && type !== 'effort'\">\n      <div class=\"row\">\n        <div class=\"col-md-4 col-lg-4\"></div>\n        <div class=\"col-md-4 col-lg-4\">\n          <div class=\"list-wrapper panel-info\">\n            <div class=\"panel-heading\">\n              {{type[0].toUpperCase() + type.slice(1)}}\n            </div>\n            <div class=\"panel-body\">\n              <ul *ngFor=\"let item of itemsList; let i = index\">\n                <li>\n                  {{item.name}} {{item.shortName ? item.shortName : ''}}\n                  <div class=\"manage-buttons-wrapper\">\n                    <button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#modal\" (click)=\"editItem(i)\">\n                      <i class=\"fas fa-pen\"></i>\n                    </button>\n                    <button type=\"button\" class=\"btn btn-danger\" (click)=\"deleteItem(i)\">\n                      <i class=\"fas fa-trash-alt\"></i>\n                    </button>\n                  </div>\n                </li>\n              </ul>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-md-4 col-lg-4\"></div>\n      </div>\n    </div>\n\n    <div class=\"panel-body\" *ngIf=\"type === 'effort'\">\n      <div class=\"project-wrapper\">\n        <div>\n          <div *ngFor=\"let effort of itemsList; let i = index\" class=\"list-wrapper panel-info\" style=\"width: 19vw\">\n            <div class=\"panel-heading\" style=\"display: flex; align-items: center; justify-content: space-between;\">\n              Effort \"{{effort.type}}\"\n              <div class=\"manage-buttons-wrapper\">\n              <button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#modal\" (click)=\"editItem(i)\">\n              <i class=\"fas fa-pen\"></i>\n              </button>\n              <button type=\"button\" class=\"btn btn-danger\" (click)=\"deleteItem(i)\">\n              <i class=\"fas fa-trash-alt\"></i>\n              </button>\n              </div>\n            </div>\n            <div class=\"panel-body\">\n              <ul class=\"projects-list\" *ngFor=\"let unit of effort.unit; let j = index\">\n                <li>{{unit.name}}</li>\n                <div class=\"manage-buttons-wrapper\">\n                  <button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#modal\" (click)=\"openUnitEdit(unit, j, i)\">\n                    <i class=\"fas fa-pen\"></i>\n                  </button>\n                  <button type=\"button\" class=\"btn btn-danger\" (click)=\"deleteUnit(i, j)\">\n                    <i class=\"fas fa-trash-alt\"></i>\n                  </button>\n                </div>\n              </ul>\n              <div class=\"manage-buttons-wrapper\">\n                <button type=\"button\" class=\"btn btn-success\" data-toggle=\"modal\" data-target=\"#modal\" (click)=\"currentIndex = i; isAddingUnit = true\">\n                  <span>Ajouter</span>\n                  <i class=\"fas fa-plus-square\"></i>\n                </button>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n\n    </div>\n\n    <div class=\"panel-body\" *ngIf=\"type === 'utilisateurs'\">\n      <table class=\"table\">\n        <thead class=\"thead-dark\">\n        <tr>\n          <th scope=\"col\">Prénom</th>\n          <th scope=\"col\">Nom</th>\n          <th scope=\"col\">Accès aux programmes</th>\n          <th scope=\"col\">Utilisateur</th>\n          <th scope=\"col\">Super-utilisateur</th>\n          <th scope=\"col\">Administrateur</th>\n        </tr>\n        </thead>\n        <tbody>\n        <ng-container *ngFor=\"let u of userList let i = index\">\n          <tr>\n            <td>{{u.lastName}}</td>\n            <td>{{u.firstName}}</td>\n            <td>\n              <ng-container *ngFor=\"let right of u.creationRights; let ind = index\">\n                {{ind === u.creationRights.length - 1 ? right.name : right.name + ', '}}\n              </ng-container>\n            </td>\n            <td>\n              <label>\n                <input type=\"radio\" name=\"{{u.id}}\" [checked]=\"u.role === 'USER'\" (change)=\"saveRolesChanges(i, 'USER', u)\">\n              </label>\n            </td>\n            <td>\n              <label>\n                <input type=\"radio\" name=\"{{u.id}}\" [checked]=\"u.role === 'SUPER_USER'\" (change)=\"saveRolesChanges(i, 'SUPER_USER', u)\">\n              </label>\n            </td>\n            <td>\n              <label>\n                <input type=\"radio\" name=\"{{u.id}}\" [checked]=\"u.role === 'ADMIN'\" (change)=\"saveRolesChanges(i, 'ADMIN', u)\">\n              </label>\n            </td>\n            <td style=\"display: flex\">\n              <button type=\"button\" class=\"btn btn-danger\" (click)=\"deleteUser(u.id)\">\n                <i class=\"fas fa-trash-alt\"></i>\n              </button>\n              <button type=\"button\" class=\"btn btn-success\" (click)=\"isEditing = true; currentUser = u\" data-toggle=\"modal\" data-target=\"#modal\">\n                <i class=\"fas fa-pen\"></i>\n              </button>\n            </td>\n          </tr>\n        </ng-container>\n        </tbody>\n      </table>\n    </div>\n\n    <div class=\"panel-body\" *ngIf=\"type === 'projets'\">\n      <div class=\"project-wrapper\">\n        <div>\n          <div *ngFor=\"let program of programList; let i = index\" class=\"list-wrapper panel-info\">\n            <div class=\"panel-heading\">\n              Programme {{program.name}}\n            </div>\n            <div class=\"panel-body\">\n              <ul class=\"projects-list\" *ngFor=\"let item of program.items\">\n                <li>{{item.name}}</li>\n                <div class=\"manage-buttons-wrapper\">\n                  <button type=\"button\" class=\"btn btn-primary\" (click)=\"editItem(i, item)\" data-toggle=\"modal\" data-target=\"#modal\">\n                    <i class=\"fas fa-pen\"></i>\n                  </button>\n                  <button type=\"button\" class=\"btn btn-danger\" (click)=\"deleteItem(i, item)\">\n                    <i class=\"fas fa-trash-alt\"></i>\n                  </button>\n                </div>\n              </ul>\n              <div class=\"manage-buttons-wrapper\">\n                <button type=\"button\" class=\"btn btn-success\" data-toggle=\"modal\" data-target=\"#modal\" (click)=\"currentIndex = i; isEditing = false\">\n                  <span>Ajouter</span>\n                  <i class=\"fas fa-plus-square\"></i>\n                </button>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n\n    <div class=\"row\">\n      <div class=\"col-md-4 col-lg-4\"></div>\n      <div class=\"col-md-4 col-lg-4 bottom-btn-wrapper\" [ngStyle]=\"type === 'projets' ? {'margin-top': '0'} : {'margin-top': '100px'}\">\n        <button type=\"button\" class=\"btn btn-success\" data-toggle=\"modal\" data-target=\"#modal\" (click)=\"isEditing = false; isEditingUnit = false\" *ngIf=\"type !== 'projets'\">\n          <span>Ajouter</span>\n          <i class=\"fas fa-plus-square\"></i>\n        </button>\n        <button type=\"button\" class=\"btn btn-danger\" (click)=\"back()\">Retour</button>\n      </div>\n      <div class=\"col-md-4 col-lg-4\"></div>\n    </div>\n  </div>\n  <div class=\"col-md-1 col-lg-1\"></div>\n</div>\n"

/***/ }),

/***/ "./src/app/reference-edit/reference-edit.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/reference-edit/reference-edit.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".edit-creation-rights {\n  display: flex;\n  flex-direction: column;\n  height: 15vw;\n  overflow: scroll; }\n\n.modal .modal-header {\n  display: flex;\n  align-items: center;\n  justify-content: space-between;\n  border-top-left-radius: 6px;\n  border-top-right-radius: 6px;\n  color: #fff;\n  background-color: #337ab7;\n  border-color: #337ab7; }\n\n.modal .modal-header:before, .modal .modal-header:after {\n    display: none; }\n\n.modal .modal-body {\n  display: flex;\n  flex-direction: column; }\n\n.modal .modal-body .items-list input {\n    margin-right: 5px; }\n\n.modal .modal-body input {\n    margin: 10px 0; }\n\n.modal .modal-body h5 {\n    font-weight: bold; }\n\n.modal .modal-body .creation-rights-wrapper {\n    display: flex;\n    flex-direction: column;\n    height: 15vw;\n    overflow: scroll; }\n\n.modal .modal-body .creation-rights-wrapper label {\n      margin: 0 10px; }\n\n.modal .small-modal {\n  display: flex;\n  align-items: center;\n  flex-direction: column;\n  border-radius: 0;\n  box-shadow: none;\n  border-bottom: none; }\n\n.modal .small-modal label {\n    min-height: 30px;\n    display: flex;\n    align-items: center;\n    justify-content: space-between; }\n\n.modal .small-modal label span {\n      display: block;\n      width: 70px; }\n\n.modal .small-modal label input {\n      height: auto;\n      width: auto;\n      margin: 0 10px; }\n\n.modal .small-modal .items-list-wrapper {\n    height: 150px;\n    overflow-y: scroll; }\n\n.popover-body .modal-header .fa-times-circle {\n  cursor: pointer; }\n\n.popover-body .popover-footer {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  height: 50px;\n  border-bottom-left-radius: 6px;\n  border-bottom-right-radius: 6px;\n  background-color: #fff;\n  background-clip: padding-box;\n  border-top: 1px solid #e5e5e5;\n  border-left: 1px solid rgba(0, 0, 0, 0.2);\n  border-right: 1px solid rgba(0, 0, 0, 0.2); }\n\n.table .programs-list {\n  cursor: pointer; }\n\n.table .programs-list:hover {\n    background: #f3f3f3; }\n\n.table tr td button {\n  margin: 0 5px; }\n\n.edit-form-wrapper {\n  padding-top: 90px; }\n\n.edit-form-wrapper .col-md-10, .edit-form-wrapper .col-lg-10 {\n    padding-left: 0;\n    padding-right: 0; }\n\n.edit-form-wrapper .list-wrapper {\n    display: flex;\n    flex-direction: column; }\n\n.edit-form-wrapper .list-wrapper .panel-body {\n      padding: 5px;\n      border: 1px solid #ddd;\n      border-top: 1px solid transparent; }\n\n.edit-form-wrapper .list-wrapper .panel-body ul {\n        padding: 10px;\n        min-height: 40px;\n        margin-bottom: 0;\n        border-bottom: 1px solid #ddd; }\n\n.edit-form-wrapper .list-wrapper .panel-body ul:last-of-type {\n          margin-bottom: 0;\n          border-bottom: none; }\n\n.edit-form-wrapper .list-wrapper .panel-body ul li {\n          cursor: pointer;\n          list-style: none;\n          display: flex;\n          align-items: center;\n          justify-content: space-between; }\n\n.edit-form-wrapper .list-wrapper .panel-body .projects-list {\n        display: flex;\n        align-items: center;\n        justify-content: space-between; }\n\n.edit-form-wrapper .manage-buttons-wrapper {\n    display: flex;\n    align-items: center;\n    justify-content: center; }\n\n.edit-form-wrapper .manage-buttons-wrapper button {\n      display: flex;\n      align-items: center;\n      justify-content: space-between;\n      margin: 0 5px; }\n\n.edit-form-wrapper .manage-buttons-wrapper button .fa-plus-square {\n        margin-left: 10px; }\n\n.edit-form-wrapper .manage-buttons-wrapper button:first-of-type {\n        margin-left: 0; }\n\n.edit-form-wrapper .manage-buttons-wrapper button:last-of-type {\n        margin-right: 0; }\n\n.edit-form-wrapper .bottom-btn-wrapper {\n    margin-bottom: 15px;\n    display: flex;\n    align-items: center;\n    justify-content: center; }\n\n.edit-form-wrapper .bottom-btn-wrapper button {\n      margin: 0 15px; }\n\n.edit-form-wrapper .bottom-btn-wrapper button .fas {\n        margin-left: 10px; }\n\n.edit-form-wrapper .project-wrapper .panel-info {\n    width: 16vw;\n    display: inline-flex;\n    margin: 0 10px; }\n\n.edit-form-wrapper .project-wrapper .list-wrapper {\n    margin-bottom: 90px; }\n"

/***/ }),

/***/ "./src/app/reference-edit/reference-edit.component.ts":
/*!************************************************************!*\
  !*** ./src/app/reference-edit/reference-edit.component.ts ***!
  \************************************************************/
/*! exports provided: ReferenceEditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReferenceEditComponent", function() { return ReferenceEditComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_requests_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/requests.service */ "./src/app/services/requests.service.ts");
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ReferenceEditComponent = /** @class */ (function () {
    function ReferenceEditComponent(router, route, requests) {
        this.router = router;
        this.route = route;
        this.requests = requests;
        this.isPasswordFieldsVisible = false;
        this.type = '';
        this.itemsList = [];
        this.manageButtonText = '';
        this.itemPlaceholder = '';
        this.userList = [];
        this.programList = [];
        this.isEditing = false;
        this.isAddingUnit = false;
        this.isEditingUnit = false;
        this.itemName = '';
        this.itemShortName = '';
        this.effortIndex = '';
        this.unitIndex = '';
        this.currentUser = {
            firstName: '',
            lastName: '',
            login: '',
            password: '',
            role: '',
            creationRights: []
        };
        this.itemUrl = {
            url: '',
            getUrl: ''
        };
        this.passwordCheck = '';
        this.user = {
            firstName: '',
            lastName: '',
            login: '',
            password: '',
            role: 'USER',
            creationRights: []
        };
        this.userNewName = '';
        this.userNewLastName = '';
        this.effort = {
            name: ''
        };
    }
    ReferenceEditComponent.prototype.init = function () {
        this.currentIndex = -1;
        switch (this.type) {
            case 'Granularité':
                this.type = 'granularité';
                this.itemPlaceholder = 'Granularité';
                this.itemsList = [];
                this.manageButtonText = 'une donnée';
                this.itemUrl.url = 'granularity';
                this.itemUrl.getUrl = 'granularityList';
                this.getItemList(this.itemUrl.getUrl);
                break;
            case 'Statut':
                this.type = 'statut de l\'actif';
                this.itemPlaceholder = 'Statut';
                this.itemsList = [];
                this.manageButtonText = 'un statut';
                this.itemUrl.url = 'status';
                this.itemUrl.getUrl = 'statusList';
                this.getItemList(this.itemUrl.getUrl);
                break;
            case 'Type':
                this.type = 'type d\'actif';
                this.itemPlaceholder = 'Type';
                this.itemsList = [];
                this.manageButtonText = 'un type d\'actif';
                this.itemUrl.url = 'type';
                this.itemUrl.getUrl = 'typeList';
                this.getItemList(this.itemUrl.getUrl);
                break;
            case 'Effort':
                this.type = 'effort';
                this.itemPlaceholder = 'Effort';
                this.itemsList = [];
                this.manageButtonText = 'une donnée';
                this.itemUrl.url = 'effort';
                this.itemUrl.getUrl = 'effortList';
                this.getItemList(this.itemUrl.getUrl);
                break;
            case 'Indicateurs':
                this.type = 'indicateurs';
                this.itemPlaceholder = 'Indicateur';
                this.itemsList = [];
                this.manageButtonText = 'un indicateur';
                this.itemUrl.url = 'indicators';
                this.itemUrl.getUrl = 'indicatorsList';
                this.getItemList(this.itemUrl.getUrl);
                break;
            case 'Classification':
                this.type = 'classification';
                this.itemsList = [];
                this.manageButtonText = 'une donnée';
                this.getClassificationList();
                break;
            case 'Programmes':
                this.type = 'programmes';
                this.manageButtonText = 'un programme';
                this.getProgramList();
                break;
            case 'Projets':
                this.type = 'projets';
                this.manageButtonText = 'un projet';
                this.getProgramList();
                break;
            case 'Utilisateurs':
                this.type = 'utilisateurs';
                this.getProgramList();
                break;
            case 'Destination':
                this.type = 'type de destination de transfert';
                this.itemPlaceholder = 'Destination';
                this.itemUrl.url = 'destination';
                this.itemUrl.getUrl = 'destinationList';
                this.getItemList(this.itemUrl.getUrl);
                break;
        }
    };
    ReferenceEditComponent.prototype.saveUnit = function () {
        var _this = this;
        if (!this.itemsList[this.currentIndex].unit) {
            this.itemsList[this.currentIndex].unit = [];
            this.itemsList[this.currentIndex].unit.push({ name: this.effort.name, id: Date.now() });
        }
        else {
            this.itemsList[this.currentIndex].unit.push({ name: this.effort.name, id: Date.now() });
        }
        this.requests.editItem(this.itemsList[this.currentIndex], this.itemUrl.url, this.itemsList[this.currentIndex]._id)
            .then(function () {
            _this.effort.name = '';
            _this.clear();
            _this.getItemList(_this.itemUrl.getUrl);
        }).catch(function (err) {
            console.log(err);
        });
    };
    ReferenceEditComponent.prototype.deleteUnit = function (effortIndex, unitIndex) {
        var _this = this;
        if (confirm('Etes-vous sûr de vouloir supprimer cet élément?')) {
            this.itemsList[effortIndex].unit.splice(unitIndex, 1);
            this.requests.editItem(this.itemsList[effortIndex], this.itemUrl.url, this.itemsList[effortIndex]._id)
                .then(function () {
                _this.effort.name = '';
                _this.clear();
                _this.getItemList(_this.itemUrl.getUrl);
            }).catch(function (err) {
                console.log(err);
            });
        }
    };
    ReferenceEditComponent.prototype.openUnitEdit = function (unit, unitIndex, effortIndex) {
        this.effortIndex = effortIndex;
        this.unitIndex = unitIndex;
        this.isEditingUnit = true;
        this.isAddingUnit = false;
        this.effort.name = unit.name;
    };
    ReferenceEditComponent.prototype.editUnit = function () {
        var _this = this;
        this.itemsList[this.effortIndex].unit[this.unitIndex].name = this.effort.name;
        this.requests.editItem(this.itemsList[this.effortIndex], this.itemUrl.url, this.itemsList[this.effortIndex]._id)
            .then(function () {
            _this.effort.name = '';
            _this.clear();
            _this.getItemList(_this.itemUrl.getUrl);
        }).catch(function (err) {
            console.log(err);
        });
    };
    ReferenceEditComponent.prototype.mapRightsBeforePost = function (creationRights) {
        var temp = [];
        for (var i = 0; i < creationRights.length; i++) {
            temp.push({
                programId: creationRights[i]['id'],
                projects: creationRights[i].projects.map(function (project) { return project.id; })
            });
        }
        this.user.creationRights = temp;
        return temp;
    };
    ReferenceEditComponent.prototype.addItem = function () {
        var _this = this;
        switch (this.type) {
            case 'projets':
                this.requests.createProject({
                    name: this.itemName,
                    shortName: this.itemShortName,
                    programId: this.programList[this.currentIndex].id
                }).then(function (res) {
                    console.log(res);
                    _this.getProgramList();
                    _this.itemName = '';
                    _this.itemShortName = '';
                }).catch(function (err) {
                    console.log('err', err);
                });
                break;
            case 'programmes':
                this.requests.createProgram({
                    name: this.itemName,
                    shortName: this.itemShortName
                }).then(function () {
                    _this.itemName = '';
                    _this.itemShortName = '';
                    _this.getProgramList();
                }).catch(function (err) {
                    console.log('err', err);
                });
                break;
            case 'utilisateurs':
                this.mapRightsBeforePost(this.user.creationRights);
                this.requests.createUser(this.user).then(function (res) {
                    console.log(res);
                    _this.user = {
                        firstName: '',
                        lastName: '',
                        login: '',
                        password: '',
                        role: 'USER',
                        creationRights: []
                    };
                    _this.passwordCheck = '';
                    _this.getUsersList();
                }).catch(function (err) {
                    console.log(err);
                });
                break;
            case 'classification':
                this.requests.createClassification({ name: this.itemName, shortName: this.itemShortName }).then(function () {
                    _this.itemName = '';
                    _this.itemShortName = '';
                    _this.getClassificationList();
                }).catch(function (err) {
                    console.log(err);
                });
                break;
            case 'effort':
                if (!this.isAddingUnit) {
                    this.requests.createItem({ type: this.itemName }, this.itemUrl.url).then(function () {
                        _this.getItemList(_this.itemUrl.getUrl);
                        _this.itemName = '';
                    }).catch(function (err) {
                        console.log(err);
                    });
                }
                break;
            default:
                this.requests.createItem({ name: this.itemName }, this.itemUrl.url).then(function () {
                    _this.getItemList(_this.itemUrl.getUrl);
                    _this.itemName = '';
                }).catch(function (err) {
                    console.log(err);
                });
        }
        this.closeAddExpenseModal.nativeElement.click();
    };
    ReferenceEditComponent.prototype.saveChanges = function () {
        var _this = this;
        switch (this.type) {
            case 'projets':
                this.requests.editProject({
                    name: this.itemName,
                    shortName: this.itemShortName
                }, this.currentItem['id']).then(function () {
                    _this.getProgramList();
                    _this.clear();
                }).catch(function (err) {
                    console.log(err);
                });
                break;
            case 'programmes':
                this.requests.editProgram({
                    name: this.itemName,
                    shortName: this.itemShortName
                }, this.itemsList[this.currentIndex].id).then(function () {
                    _this.getProgramList();
                    _this.clear();
                }).catch(function (err) {
                    console.log(err);
                });
                break;
            case 'classification':
                this.requests.editClassification({
                    name: this.itemName,
                    shortName: this.itemShortName
                }, this.itemsList[this.currentIndex]._id).then(function () {
                    _this.getClassificationList();
                    _this.clear();
                }).catch(function (err) {
                    console.log(err);
                });
                break;
            case 'effort':
                this.itemsList[this.currentIndex].type = this.itemName;
                this.requests.editItem(this.itemsList[this.currentIndex], this.itemUrl.url, this.itemsList[this.currentIndex]._id)
                    .then(function () {
                    _this.getItemList(_this.itemUrl.getUrl);
                    _this.clear();
                }).catch(function (err) {
                    console.log(err);
                });
                break;
            default:
                this.requests.editItem({ name: this.itemName }, this.itemUrl.url, this.itemsList[this.currentIndex]._id)
                    .then(function () {
                    _this.getItemList(_this.itemUrl.getUrl);
                    _this.clear();
                }).catch(function (err) {
                    console.log(err);
                });
                break;
        }
        this.itemName = '';
        this.itemShortName = '';
        this.closeAddExpenseModal.nativeElement.click();
    };
    ReferenceEditComponent.prototype.deleteItem = function (index, item) {
        var _this = this;
        this.currentItem = item;
        if (confirm('Etes-vous sûr de vouloir supprimer cet élément?')) {
            switch (this.type) {
                case 'projets':
                    this.requests.deleteProject(this.programList[index].id, this.currentItem['id']).then(function () {
                        _this.getProgramList();
                    }).catch(function (err) {
                        console.log(err);
                    });
                    break;
                case 'programmes':
                    this.requests.deleteProgram(this.itemsList[index].id).then(function () {
                        _this.getProgramList();
                    }).catch(function (err) {
                        console.log(err);
                    });
                    break;
                case 'classification':
                    this.requests.deleteClassification(this.itemsList[index]._id).then(function () {
                        _this.getClassificationList();
                    }).catch(function (err) {
                        console.log(err);
                    });
                    break;
                default:
                    this.requests.deleteItem(this.itemsList[index]._id, this.itemUrl.url).then(function () {
                        _this.getItemList(_this.itemUrl.getUrl);
                    }).catch(function (err) {
                        console.log(err);
                    });
                    break;
            }
            this.currentIndex = -1;
        }
    };
    ReferenceEditComponent.prototype.editItem = function (index, item) {
        if (item) {
            this.itemShortName = item.shortName;
            this.itemName = item.name;
            this.currentItem = item;
        }
        else {
            this.itemShortName = this.itemsList[index].shortName;
            this.itemName = this.itemsList[index].name;
        }
        if (this.type === 'effort' && !this.isAddingUnit) {
            this.itemName = this.itemsList[index].type;
            this.currentItem = this.itemsList[index];
        }
        this.isEditing = true;
        this.isEditingUnit = false;
        this.isAddingUnit = false;
        this.currentIndex = index;
    };
    ReferenceEditComponent.prototype.getClassificationList = function () {
        var _this = this;
        this.requests.classificationList().then(function (res) {
            _this.itemsList = res;
            console.log(res);
        }).catch(function (err) {
            console.log(err);
        });
    };
    ReferenceEditComponent.prototype.getProgramList = function () {
        var _this = this;
        this.requests.getProgramsList().then(function (res) {
            if (_this.type === 'projets') {
                _this.programList = res.map(function (i) {
                    return {
                        name: i.name,
                        shortName: i.shortName,
                        id: i.id,
                        items: i.projects || []
                    };
                });
            }
            else if (_this.type === 'utilisateurs') {
                _this.itemsList = res;
                _this.getUsersList();
            }
            else {
                _this.itemsList = res.map(function (i) {
                    return {
                        name: i.name,
                        shortName: i.shortName,
                        id: i.id
                    };
                });
            }
        }).catch(function (err) {
            console.log(err);
        });
    };
    ReferenceEditComponent.prototype.clearPasswords = function () {
        this.currentUser.password = '';
        this.passwordCheck = '';
        this.isPasswordFieldsVisible = false;
    };
    ReferenceEditComponent.prototype.getProgramNames = function (creationRights) {
        var temp = [];
        for (var i = 0; i < this.itemsList.length; i++) {
            var _loop_1 = function (j) {
                if (this_1.itemsList[i].id === creationRights[j].programId) {
                    temp.push(__assign({}, this_1.itemsList[i], { projects: [] }));
                    this_1.itemsList[i].projects.map(function (item) {
                        for (var k = 0; k < creationRights[j].projects.length; k++) {
                            if (item.id === creationRights[j].projects[k]) {
                                temp[temp.length - 1].projects.push(item);
                            }
                        }
                    });
                }
            };
            var this_1 = this;
            for (var j = 0; j < creationRights.length; j++) {
                _loop_1(j);
            }
        }
        return temp;
    };
    ReferenceEditComponent.prototype.getUsersList = function () {
        var _this = this;
        this.requests.getUsers().then(function (res) {
            _this.userList = Object.values(res).map(function (i) {
                return __assign({}, i, { creationRights: _this.getProgramNames(i['creationRights']) });
            });
            // this.userList = res.map(i => {
            //     return {
            //         ...i,
            //         creationRights: this.getProgramNames(i.creationRights)
            //     };
            // });
        }).catch(function (err) {
            console.log(err);
        });
    };
    ReferenceEditComponent.prototype.getItemList = function (type) {
        var _this = this;
        this.requests.getItems(type).then(function (res) {
            console.log(res);
            _this.itemsList = Object.values(res);
        }).catch(function (err) {
            console.log(err);
        });
    };
    ReferenceEditComponent.prototype.back = function () {
        this.router.navigate(['administration']);
    };
    ReferenceEditComponent.prototype.clear = function () {
        this.isEditing = false;
        this.isAddingUnit = false;
        this.isEditingUnit = false;
        this.itemName = '';
        this.itemShortName = '';
        this.effort.name = '';
    };
    ReferenceEditComponent.prototype.editUser = function () {
        var _this = this;
        var temp = Object.assign({}, this.currentUser);
        temp.creationRights = this.mapRightsBeforePost(this.currentUser.creationRights);
        this.requests.editUser(temp).then(function (res) {
            _this.passwordCheck = '';
            _this.currentUser = {
                firstName: '',
                lastName: '',
                login: '',
                password: '',
                role: '',
                creationRights: []
            };
            _this.getProgramList();
        }).catch(function (err) {
            console.log(err);
        });
    };
    ReferenceEditComponent.prototype.deleteUser = function (id) {
        var _this = this;
        if (confirm('Etes-vous sûr de vouloir supprimer cet élément?')) {
            this.requests.deleteUser(id).then(function () {
                _this.getProgramList();
            }).catch(function (err) {
                console.log(err);
            });
        }
    };
    ReferenceEditComponent.prototype.creationRightsChangeProject = function (event, programId, user, project) {
        var isRight = false;
        if (event.target.checked) {
            this[user].creationRights.forEach(function (program) {
                if (program.id === programId) {
                    if (!program.projects.length) {
                        program.projects.push(project);
                    }
                    else {
                        program.projects.forEach(function (proj) {
                            if (proj.id === project.id) {
                                isRight = true;
                            }
                        });
                        if (!isRight) {
                            program.projects.push(project);
                        }
                    }
                }
            });
        }
        else {
            var index_1;
            this[user].creationRights.forEach(function (program) {
                if (program.id === programId) {
                    program.projects.forEach(function (proj, ind) {
                        if (proj.id === project.id) {
                            index_1 = ind;
                            isRight = true;
                        }
                        if (isRight) {
                            program.projects.splice(index_1, 1);
                        }
                    });
                }
            });
        }
    };
    ReferenceEditComponent.prototype.creationRightsChangeProgram = function (event, id, user) {
        var _this = this;
        // if (event.target.checked) {
        //     if (this[user].creationRights.indexOf(id) === -1) {
        //         this[user].creationRights.push(id);
        //     }
        // } else {
        //     if (this[user].creationRights.indexOf(id) !== -1) {
        //         this[user].creationRights.splice(this[user].creationRights.indexOf(id), 1);
        //     }
        // }
        var isRight = false;
        if (event.target.checked) {
            this[user].creationRights.forEach(function (i) {
                if (i.id === id.id) {
                    isRight = true;
                }
            });
            if (!isRight) {
                this[user].creationRights.push(__assign({}, id, { projects: [] }));
            }
        }
        else {
            var index_2;
            this[user].creationRights.forEach(function (i, ind) {
                if (i.id === id.id) {
                    index_2 = ind;
                    isRight = true;
                }
            });
            if (isRight) {
                this[user].creationRights[index_2].projects.forEach(function (i) {
                    document.getElementById(_this[user].creationRights[index_2].id + '/' + i.id)['checked'] = false;
                });
                this[user].creationRights.splice(index_2, 1);
            }
        }
    };
    ReferenceEditComponent.prototype.isFormInvalid = function () {
        switch (this.type) {
            case 'projects':
            case 'progammes':
                return !this.itemName || !this.itemShortName;
            case 'utilisateurs':
                if (!this.isEditing) {
                    return this.user.password !== this.passwordCheck || !this.isUserValid();
                }
                else {
                    return this.currentUser.password !== this.passwordCheck || !this.isUserValid();
                }
            default:
                return !this.itemName;
        }
    };
    ReferenceEditComponent.prototype.shouldRadioDisable = function (programId, user) {
        var r = true;
        this[user].creationRights.forEach(function (i) {
            if (i.id === programId) {
                r = false;
            }
        });
        return r;
    };
    ReferenceEditComponent.prototype.doesUserHasProjectRight = function (programId, id, user) {
        var r = false;
        this[user].creationRights.forEach(function (i) {
            if (i.id === programId) {
                i.projects.forEach(function (proj) {
                    if (proj.id === id) {
                        r = true;
                    }
                });
            }
        });
        return r;
    };
    ReferenceEditComponent.prototype.doesUserHasProgramRight = function (name, user) {
        var r = false;
        this[user].creationRights.forEach(function (i) {
            if (i.name === name) {
                r = true;
            }
        });
        return r;
    };
    ReferenceEditComponent.prototype.isUserValid = function () {
        if (!this.isEditing) {
            return this.user.firstName && this.user.lastName && this.user.login && this.user.password;
        }
        else {
            return this.currentUser.firstName && this.currentUser.lastName && this.currentUser.login;
        }
    };
    ReferenceEditComponent.prototype.mapRights = function (user) {
        var temp = [];
        Object.values(this.itemsList).forEach(function (obj) {
            // if (user.creationRights.indexOf(obj['name']) !== -1) {
            //     temp.push(obj['id']);
            // }
            user.creationRights.forEach(function (item) {
                if (item.name === obj['name']) {
                    temp.push(obj);
                }
            });
            // if (user.creationRights.indexOf(obj['name']) !== -1) {
            //     temp.push(obj['id']);
            // }
        });
        return temp;
    };
    ReferenceEditComponent.prototype.saveRolesChanges = function (index, value, user) {
        this.userList[index].role = value;
        this.saveUserRole(user);
    };
    ReferenceEditComponent.prototype.saveUserRole = function (user) {
        var _this = this;
        var tempUser = Object.assign({}, user);
        tempUser.creationRights = this.mapRightsBeforePost(user.creationRights);
        if (this.userNewName) {
            tempUser.firstName = this.userNewName;
        }
        if (this.userNewLastName) {
            tempUser.lastName = this.userNewLastName;
        }
        this.requests.editUser(tempUser).then(function () {
            _this.userNewName = '';
            _this.userNewLastName = '';
            _this.getProgramList();
        }).catch(function (err) {
            console.log(err);
        });
    };
    ReferenceEditComponent.prototype.ngAfterViewInit = function () {
        // const arr = Array.from(document.getElementsByClassName('list-wrapper'));
        // const maxHeight = Math.max.apply(null, arr.map(i => i['offsetHeight']));
        // arr.map(i => {
        //     i['style'].height = maxHeight + 'px';
        //     i['style'].marginBottom = '20px';
        // });
    };
    ReferenceEditComponent.prototype.ngOnInit = function () {
        this.type = this.route.snapshot.params.type;
        this.init();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('closeAddExpenseModal'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], ReferenceEditComponent.prototype, "closeAddExpenseModal", void 0);
    ReferenceEditComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-reference-edit',
            template: __webpack_require__(/*! ./reference-edit.component.html */ "./src/app/reference-edit/reference-edit.component.html"),
            styles: [__webpack_require__(/*! ./reference-edit.component.scss */ "./src/app/reference-edit/reference-edit.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _services_requests_service__WEBPACK_IMPORTED_MODULE_2__["RequestsService"]])
    ], ReferenceEditComponent);
    return ReferenceEditComponent;
}());



/***/ }),

/***/ "./src/app/services/auth-guard.service.ts":
/*!************************************************!*\
  !*** ./src/app/services/auth-guard.service.ts ***!
  \************************************************/
/*! exports provided: AuthGuardService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuardService", function() { return AuthGuardService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _local_storage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./local-storage.service */ "./src/app/services/local-storage.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthGuardService = /** @class */ (function () {
    function AuthGuardService(storageService, router) {
        this.storageService = storageService;
        this.router = router;
    }
    AuthGuardService.prototype.canActivate = function (route, state) {
        var url = state.url;
        if (url !== '/asset_info' && url !== '/new_transfer') {
            this.storageService.setAsset({});
        }
        else {
            // alert('');
        }
        return this.checkLogin(url);
    };
    AuthGuardService.prototype.checkLogin = function (url) {
        var isUser = this.storageService.getUser();
        if (isUser) {
            return true;
        }
        // Store the attempted URL for redirecting
        // this.authService.redirectUrl = url;
        // Navigate to the login page with extras
        this.router.navigate(['/login']);
        return false;
    };
    AuthGuardService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_local_storage_service__WEBPACK_IMPORTED_MODULE_2__["LocalStorageService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AuthGuardService);
    return AuthGuardService;
}());



/***/ }),

/***/ "./src/app/services/filters.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/filters.service.ts ***!
  \*********************************************/
/*! exports provided: FiltersService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FiltersService", function() { return FiltersService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FiltersService = /** @class */ (function () {
    function FiltersService() {
        // filterProgramsValues = ['\u{02B0}', 'IC', 'IA', 'TI', 'TA', 'DT'];
        this.filterProgramsValues = ["\u02B0"];
        this.filterProgrammValue = "\u02B0";
        this.initFilterProjectValues = {
            "\u02B0": [],
            'IC': ['EIC', 'Io4', 'STC', 'Autre'],
            'IA': ['AMC', 'CDF', 'DSL', 'ISC', 'OAR', 'TOP', 'SIM', 'Autre'],
            'TI': ['BFI', 'BST', 'HCU', 'IMM', 'IVA', 'LCE', 'MIC', 'MSM', 'SCE', 'Autre'],
            'TA': ['API', 'CMI', 'CTI', 'ELA', 'EVA', 'FSF', 'ISE', 'LRA', 'PST', 'SCA', 'SVA', 'TAS', 'Autre'],
            'DT': ['Transverse']
        };
        this.filterProjectValues = ["\u02B0"].concat(this.initFilterProjectValues[this.filterProgramsValues[0]]);
        this.filterProgramsValue = "\u02B0";
        this.filterProjectValue = "\u02B0";
        this.filterAssetType = "\u02B0";
        this.filterAssetClassifType = "\u02B0";
        // filterAssetClassifType = ['\u{02B0}'];
        this.filterAssetCardStatus = "\u02B0";
        this.filterAssetStatus = "\u02B0";
        this.filterPlateformeType = "\u02B0";
        this.filterAssetTransfersNumber = "\u02B0";
        this.filterChipsArr = [];
    }
    FiltersService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], FiltersService);
    return FiltersService;
}());



/***/ }),

/***/ "./src/app/services/local-storage.service.ts":
/*!***************************************************!*\
  !*** ./src/app/services/local-storage.service.ts ***!
  \***************************************************/
/*! exports provided: LocalStorageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocalStorageService", function() { return LocalStorageService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_Subject__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/Subject */ "./node_modules/rxjs-compat/_esm5/Subject.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LocalStorageService = /** @class */ (function () {
    function LocalStorageService() {
        this.user = new rxjs_Subject__WEBPACK_IMPORTED_MODULE_1__["Subject"]();
    }
    LocalStorageService.prototype.updateUser = function (user) {
        localStorage.setItem('user', JSON.stringify(user));
        this.user.next(user);
    };
    LocalStorageService.prototype.deleteUser = function () {
        localStorage.removeItem('user');
        this.user.next();
    };
    LocalStorageService.prototype.getUser = function () {
        var user = JSON.parse(localStorage.getItem('user'));
        return user ? Object.keys(user).length > 0 ? user : undefined : undefined;
    };
    LocalStorageService.prototype.setAsset = function (asset) {
        localStorage.setItem('asset', JSON.stringify(asset));
    };
    LocalStorageService.prototype.getAsset = function () {
        var asset = JSON.parse(localStorage.getItem('asset'));
        return asset ? Object.keys(asset).length > 0 ? asset : undefined : undefined;
    };
    LocalStorageService.prototype.getUserObservable = function () {
        return this.user.asObservable();
    };
    LocalStorageService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], LocalStorageService);
    return LocalStorageService;
}());



/***/ }),

/***/ "./src/app/services/pass.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/pass.service.ts ***!
  \******************************************/
/*! exports provided: PassService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PassService", function() { return PassService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_local_storage_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/local-storage.service */ "./src/app/services/local-storage.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PassService = /** @class */ (function () {
    function PassService(storageService) {
        this.storageService = storageService;
    }
    PassService.prototype.isAdmin = function () {
        var user = this.storageService.getUser();
        if (!user) {
            return false;
        }
        // return user.role === 'ADMIN' || user.role === 'SUPER_USER';
        return user.roles === 'ADMIN';
    };
    PassService.prototype.isSuperUser = function () {
        var user = this.storageService.getUser();
        if (!user) {
            return false;
        }
        return user.roles === 'SUPER_USER';
    };
    PassService.prototype.isPriviledged = function () {
        return this.isAdmin() || this.isSuperUser();
    };
    PassService.prototype.isAssetAuthor = function (asset) {
        if (!asset || Object.keys(asset).length === 0) {
            return false;
        }
        var user = this.storageService.getUser();
        if (!user) {
            return false;
        }
        return user.id == asset.identification.userId;
    };
    PassService.prototype.isAssetStatusEditable = function (asset) {
        return !!asset.label && !!asset.repository && !!asset.detailedDescription;
    };
    PassService.prototype.isUserHasAssetRights = function (asset) {
        if (this.isAdmin()) {
            return true;
        }
        if (!asset || Object.keys(asset).length === 0) {
            return false;
        }
        // console.log('1');
        var user = this.storageService.getUser();
        if (!user) {
            return false;
        }
        var cRights = user.creationRights;
        return cRights[asset.identification.program] ? cRights[asset.identification.program].indexOf(asset.identification.project) >= 0 : false;
    };
    PassService.prototype.getAssetStatus = function (asset) {
        if (!asset || Object.keys(asset).length === 0) {
            return '';
        }
        switch (asset.assetStatus) {
            case 'INITIALIZED':
                return 'Initialisé';
            case 'IN_PROGRESS':
                return 'En cours';
            case 'DONE':
                return 'Terminé';
            case 'TRANSFERED':
                return 'Transféré';
        }
    };
    PassService.prototype.getAssetClassif = function (asset) {
        if (!asset || Object.keys(asset).length === 0) {
            return '';
        }
        switch (asset.classification.type) {
            case 'HIGH_POTENTIAL':
                return 'Fort potentiel';
            case 'FOLLOW':
                return 'Actif a suivre';
            case 'CONFIRM':
                return 'A confirmer';
        }
    };
    PassService.prototype.getAssetType = function (asset) {
        if (!asset || Object.keys(asset).length === 0) {
            return '';
        }
        var types = {
            METHOD: 'Methode, Processus, Bonnes pratiques, Guidelines, Specifications',
            STATE_OF_ART: 'Etat de l\'art',
            STUDY: 'Etude (exploiter/valider)',
            DATASET: 'Dataset/corpus',
            PATENT: 'Brevet',
            PUBLICATION: 'Publication',
            SOFTWARE: 'Logiciel, Modele, outil, algorithme, code',
            STANDARD: 'Standart',
            OTHER: 'Autre'
        };
        return types[asset.type] || asset.type || 'Autre';
    };
    PassService.prototype.getTransfertStatus = function (transfert) {
        if (!transfert || Object.keys(transfert).length === 0) {
            return '';
        }
        return transfert.status === 'PENDING' ? 'En cours de rédaction' : transfert.status === 'WAITING_VALIDATION' ? 'Pour validation CODIR' : 'Validé';
    };
    PassService.prototype.getTransfertType = function (type) {
        var types = {
            PARTNER: "Partenaire",
            PROJECT: "Projet"
        };
        return types[type] || "";
    };
    PassService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_services_local_storage_service__WEBPACK_IMPORTED_MODULE_1__["LocalStorageService"]])
    ], PassService);
    return PassService;
}());



/***/ }),

/***/ "./src/app/services/requests.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/requests.service.ts ***!
  \**********************************************/
/*! exports provided: RequestsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RequestsService", function() { return RequestsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment_prod__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment.prod */ "./src/environments/environment.prod.ts");
/* harmony import */ var _local_storage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./local-storage.service */ "./src/app/services/local-storage.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RequestsService = /** @class */ (function () {
    function RequestsService(http, storageService) {
        this.http = http;
        this.storageService = storageService;
        // url = 'http://192.168.31.118:3001';
        // url = 'http://e24fe050.ngrok.io';
        // url = 'http://192.168.31.35:3001';
        this.url = _environments_environment_prod__WEBPACK_IMPORTED_MODULE_2__["environment"].REST_API_URL;
    }
    RequestsService.prototype.getDestionationName = function (id) {
        var token = 'bearer ' + this.storageService.getUser().token;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.post(this.url + "/destinationName" + ("/" + id), { headers: headers })
            .toPromise()
            .then(function (res) {
            return Promise.resolve(res);
        }).catch(function (err) {
            console.error(err);
            return Promise.reject(err);
        });
    };
    // Granularity, status, type, classification, indicators, effort
    RequestsService.prototype.getItems = function (type) {
        var token = 'bearer ' + this.storageService.getUser().token;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Content-Type': "text/plain",
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.get(this.url + ("/" + type), { headers: headers })
            .toPromise()
            .then(function (res) {
            return Promise.resolve(res);
        })
            .catch(function (err) { return Promise.reject(err); });
    };
    RequestsService.prototype.deleteItem = function (id, type) {
        var token = 'bearer ' + this.storageService.getUser().token;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.delete(this.url + ("/" + type) + ("/" + id), { headers: headers })
            .toPromise()
            .then(function (res) {
            console.log(res);
            return Promise.resolve(res);
        }).catch(function (err) {
            console.error(err);
            return Promise.reject(err);
        });
    };
    RequestsService.prototype.createItem = function (item, type) {
        var token = 'bearer ' + this.storageService.getUser().token;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.post(this.url + ("/" + type), item, { headers: headers })
            .toPromise()
            .then(function (res) {
            console.log('Project created => ', res);
            return Promise.resolve(res);
        })
            .catch(function (err) { return Promise.reject(err); });
    };
    RequestsService.prototype.editItem = function (item, type, id) {
        var token = 'bearer ' + this.storageService.getUser().token;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.put(this.url + ("/" + type) + ("/" + id), item, { headers: headers })
            .toPromise()
            .then(function (res) {
            console.log(res);
            return Promise.resolve(res);
        }).catch(function (err) {
            console.error(err);
            return Promise.reject(err);
        });
    };
    // Projects
    RequestsService.prototype.createProject = function (project) {
        var token = 'bearer ' + this.storageService.getUser().token;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.post(this.url + "/project", project, { headers: headers })
            .toPromise()
            .then(function (res) {
            console.log('Project created => ', res);
            return Promise.resolve(res);
        })
            .catch(function (err) { return Promise.reject(err); });
    };
    RequestsService.prototype.deleteProject = function (programId, projectId) {
        var token = 'bearer ' + this.storageService.getUser().token;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.delete(this.url + ("/project/" + programId + "/" + projectId), { headers: headers })
            .toPromise()
            .then(function (res) {
            console.log(res);
            return Promise.resolve(res);
        }).catch(function (err) {
            console.error(err);
            return Promise.reject(err);
        });
    };
    RequestsService.prototype.editProject = function (project, id) {
        console.log(project, id);
        var token = 'bearer ' + this.storageService.getUser().token;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.put(this.url + ("/project/" + id), project, { headers: headers })
            .toPromise()
            .then(function (res) {
            console.log(res);
            return Promise.resolve(res);
        }).catch(function (err) {
            console.error(err);
            return Promise.reject(err);
        });
    };
    RequestsService.prototype.getProjectsList = function () {
        var token = 'bearer ' + this.storageService.getUser().token;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Content-Type': "text/plain",
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.get(this.url + '/projectsList', { headers: headers })
            .toPromise()
            .then(function (res) {
            return Promise.resolve(res);
        })
            .catch(function (err) { return Promise.reject(err); });
    };
    // Classification
    RequestsService.prototype.classificationList = function () {
        var token = 'bearer ' + this.storageService.getUser().token;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Content-Type': "text/plain",
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.get(this.url + '/classificationList', { headers: headers })
            .toPromise()
            .then(function (res) {
            return Promise.resolve(res);
        })
            .catch(function (err) { return Promise.reject(err); });
    };
    RequestsService.prototype.createClassification = function (classification) {
        console.log('REQUEST => ', classification);
        var token = 'bearer ' + this.storageService.getUser().token;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.post(this.url + "/classification", classification, { headers: headers })
            .toPromise()
            .then(function (res) {
            console.log('RESPONSE ', res);
            return Promise.resolve(res);
        }).catch(function (err) {
            console.error(err);
            return Promise.reject(err);
        });
    };
    RequestsService.prototype.deleteClassification = function (id) {
        var token = 'bearer ' + this.storageService.getUser().token;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.delete(this.url + ("/classification/" + id), { headers: headers })
            .toPromise()
            .then(function (res) {
            console.log(res);
            return Promise.resolve(res);
        }).catch(function (err) {
            console.error(err);
            return Promise.reject(err);
        });
    };
    RequestsService.prototype.editClassification = function (classification, id) {
        var token = 'bearer ' + this.storageService.getUser().token;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.put(this.url + ("/classification/" + id), classification, { headers: headers })
            .toPromise()
            .then(function (res) {
            return Promise.resolve(res);
        }).catch(function (err) {
            console.error(err);
            return Promise.reject(err);
        });
    };
    // Program
    RequestsService.prototype.getUserProgramsList = function () {
        var token = 'bearer ' + this.storageService.getUser().token;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Access-Control-Allow-Origin': '*',
            'Content-Type': "text/plain",
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.get(this.url + '/userProgramsList', { headers: headers })
            .toPromise()
            .then(function (res) {
            return Promise.resolve(res);
        })
            .catch(function (err) { return Promise.reject(err); });
    };
    RequestsService.prototype.getProgramsList = function () {
        var token = 'bearer ' + this.storageService.getUser().token;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Content-Type': "text/plain",
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.get(this.url + '/programsList', { headers: headers })
            .toPromise()
            .then(function (res) {
            return Promise.resolve(res);
        })
            .catch(function (err) { return Promise.reject(err); });
    };
    RequestsService.prototype.createProgram = function (program) {
        // console.log('in service => ', asset);
        var token = 'bearer ' + this.storageService.getUser().token;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.post(this.url + "/program", program, { headers: headers })
            .toPromise()
            .then(function (res) {
            console.log(res);
            return Promise.resolve(res);
        }).catch(function (err) {
            console.error(err);
            return Promise.reject(err);
        });
    };
    RequestsService.prototype.deleteProgram = function (id) {
        var token = 'bearer ' + this.storageService.getUser().token;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.delete(this.url + ("/program/" + id), { headers: headers })
            .toPromise()
            .then(function (res) {
            console.log(res);
            return Promise.resolve(res);
        }).catch(function (err) {
            console.error(err);
            return Promise.reject(err);
        });
    };
    RequestsService.prototype.editProgram = function (program, id) {
        var token = 'bearer ' + this.storageService.getUser().token;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.put(this.url + ("/program/" + id), program, { headers: headers })
            .toPromise()
            .then(function (res) {
            console.log(res);
            return Promise.resolve(res);
        }).catch(function (err) {
            console.error(err);
            return Promise.reject(err);
        });
    };
    // User
    RequestsService.prototype.login = function (val) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Content-Type': 'application/x-www-form-urlencoded',
            'authorization': val.password + '',
            'Accept': 'application/json'
        });
        return this.http.get(this.url + ("/login/" + val.login), { headers: headers })
            .toPromise()
            .then(function (res) {
            console.log('USER ====> ', res);
            return Promise.resolve(res);
        })
            .catch(function (err) { return Promise.reject(err); });
    };
    RequestsService.prototype.logout = function () {
        var token = 'bearer ' + this.storageService.getUser().token;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Content-Type': "text/plain",
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.delete(this.url + "/logout", { headers: headers })
            .toPromise()
            .then(function (res) {
            localStorage.removeItem('access_token');
            localStorage.removeItem('curr_user');
            return Promise.resolve(res);
        })
            .catch(function (err) { return Promise.reject(err); });
    };
    RequestsService.prototype.getUser = function (userId) {
        var token = 'bearer ' + this.storageService.getUser().token;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Content-Type': "text/plain",
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.get(this.url + ("/user/" + userId), { headers: headers })
            .toPromise()
            .then(function (res) {
            return Promise.resolve(res);
        })
            .catch(function (err) { return Promise.reject(err); });
    };
    RequestsService.prototype.getUsers = function () {
        var token = 'bearer ' + this.storageService.getUser().token;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Content-Type': "text/plain",
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.get(this.url + "/usersList", { headers: headers })
            .toPromise()
            .then(function (res) {
            return Promise.resolve(res);
        })
            .catch(function (err) { return Promise.reject(err); });
    };
    RequestsService.prototype.editUser = function (user) {
        var token = 'bearer ' + this.storageService.getUser().token;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.put(this.url + ("/users/" + user.id), user, { headers: headers })
            .toPromise()
            .then(function (res) {
            return Promise.resolve(res);
        }).catch(function (err) {
            console.error(err);
            return Promise.reject(err);
        });
    };
    RequestsService.prototype.createUser = function (user) {
        var token = 'bearer ' + this.storageService.getUser().token;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.post(this.url + '/user', user, { headers: headers })
            .toPromise()
            .then(function (res) {
            return Promise.resolve(res);
        }).catch(function (err) {
            console.error(err);
            return Promise.reject(err);
        });
    };
    RequestsService.prototype.deleteUser = function (userid) {
        var token = 'bearer ' + this.storageService.getUser().token;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.delete(this.url + ("/users/" + userid), { headers: headers })
            .toPromise()
            .then(function (res) {
            console.log(res);
            return Promise.resolve(res);
        }).catch(function (err) {
            console.error(err);
            return Promise.reject(err);
        });
    };
    // Assets
    RequestsService.prototype.addAsset = function (asset) {
        var token = 'bearer ' + this.storageService.getUser().token;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.post(this.url + "/asset", asset, { headers: headers })
            .toPromise()
            .then(function (res) {
            console.log(res);
            return Promise.resolve(res);
        }).catch(function (err) {
            console.error(err);
            return Promise.reject(err);
        });
    };
    RequestsService.prototype.getAssets = function () {
        var token = 'bearer ' + this.storageService.getUser().token;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Content-Type': "text/plain",
            'Authorization': token, 'Accept': 'application/json, text/plain, */*'
        });
        return this.http.get(this.url + "/assetsList", { headers: headers })
            .toPromise()
            .then(function (res) {
            return Promise.resolve(res);
        })
            .catch(function (err) { return Promise.reject(err); });
    };
    RequestsService.prototype.getAssetsChildren = function () {
        var token = 'bearer ' + this.storageService.getUser().token;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Content-Type': "text/plain",
            'Authorization': token, 'Accept': 'application/json, text/plain, */*'
        });
        return this.http.get(this.url + "/assetsChildren", { headers: headers })
            .toPromise()
            .then(function (res) {
            return Promise.resolve(res);
        })
            .catch(function (err) { return Promise.reject(err); });
    };
    RequestsService.prototype.newAsset = function (assetId) {
        var token = 'bearer ' + this.storageService.getUser().token;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Content-Type': "text/plain",
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.get(this.url + ("/newasset/" + assetId), { headers: headers })
            .toPromise()
            .then(function (res) {
            return Promise.resolve(res);
        })
            .catch(function (err) { return Promise.reject(err); });
    };
    RequestsService.prototype.getAsset = function (assetId) {
        var token = 'bearer ' + this.storageService.getUser().token;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Content-Type': "text/plain",
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.get(this.url + ("/asset/" + assetId), { headers: headers })
            .toPromise()
            .then(function (res) {
            return Promise.resolve(res);
        })
            .catch(function (err) { return Promise.reject(err); });
    };
    RequestsService.prototype.deleteAsset = function (assetId) {
        var token = 'bearer ' + this.storageService.getUser().token;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.delete(this.url + ("/asset/" + assetId), { headers: headers })
            .toPromise()
            .then(function (res) {
            console.log(res);
            return Promise.resolve(res);
        }).catch(function (err) {
            console.error(err);
            return Promise.reject(err);
        });
    };
    RequestsService.prototype.editAsset = function (asset) {
        var token = 'bearer ' + this.storageService.getUser().token;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.put(this.url + ("/asset/" + asset.id), asset, { headers: headers })
            .toPromise()
            .then(function (res) {
            console.log(res);
            return Promise.resolve(res);
        }).catch(function (err) {
            console.error(err);
            return Promise.reject(err);
        });
    };
    // Transfers
    RequestsService.prototype.addTransfer = function (transfer) {
        var token = 'bearer ' + this.storageService.getUser().token;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.post(this.url + "/transfer", transfer, { headers: headers })
            .toPromise()
            .then(function (res) {
            console.log(res);
            return Promise.resolve(res);
        }).catch(function (err) {
            console.error(err);
            return Promise.reject(err);
        });
    };
    RequestsService.prototype.getTransfers = function () {
        var token = 'bearer ' + this.storageService.getUser().token;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Content-Type': "text/plain",
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.get(this.url + "/transferList", { headers: headers })
            .toPromise()
            .then(function (res) {
            return Promise.resolve(res);
        })
            .catch(function (err) { return Promise.reject(err); });
    };
    RequestsService.prototype.getTransfersForAsset = function (_assetId) {
        var token = 'bearer ' + this.storageService.getUser().token;
        var _headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Content-Type': "text/plain",
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        var _params = { asset: _assetId };
        return this.http.get(this.url + "/transferList", { headers: _headers, params: _params });
    };
    RequestsService.prototype.getStats = function () {
        var token = 'bearer ' + this.storageService.getUser().token;
        var _headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Content-Type': "text/plain",
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        this.http.get(this.url + "/stats/all", { headers: _headers }).subscribe(function (data) {
            var csvText = String(data.text);
            var date = new Date();
            var filename = "AGAV_stats_" + (date.getFullYear() + date.getMonth() + 1 + date.getDate()) + ".csv";
            var encodedData = encodeURI(csvText);
            var link = document.createElement('a');
            link.setAttribute('href', 'data:text/csv;charset=utf-8,%EF%BB%BF' + encodedData);
            link.setAttribute('download', filename);
            link.click();
        });
    };
    RequestsService.prototype.getActions = function () {
        var token = 'bearer ' + this.storageService.getUser().token;
        var _headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Content-Type': "text/plain",
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.get(this.url + "/actions", { headers: _headers });
    };
    RequestsService.prototype.createAction = function (action) {
        var token = 'bearer ' + this.storageService.getUser().token;
        var _headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.post(this.url + "/actions/", action, { headers: _headers });
    };
    RequestsService.prototype.getActionsForAsset = function (assetId) {
        var token = 'bearer ' + this.storageService.getUser().token;
        var _headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Content-Type': "text/plain",
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.get(this.url + ("/actions/asset/" + assetId), { headers: _headers });
    };
    RequestsService.prototype.getActionsForProject = function (projectId) {
        var token = 'bearer ' + this.storageService.getUser().token;
        var _headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Content-Type': "text/plain",
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.get(this.url + ("/actions/project/" + projectId), { headers: _headers });
    };
    RequestsService.prototype.deleteAction = function (actionId) {
        var token = 'bearer ' + this.storageService.getUser().token;
        var _headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Content-Type': "text/plain",
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.delete(this.url + ("/actions/" + actionId), { headers: _headers })
            .toPromise()
            .then(function (res) {
            console.log(res);
            return Promise.resolve(res);
        }).catch(function (err) {
            console.error(err);
            return Promise.reject(err);
        });
    };
    RequestsService.prototype.updateAction = function (actionId, action) {
        var token = 'bearer ' + this.storageService.getUser().token;
        var _headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.put(this.url + ("/actions/" + actionId), action, { headers: _headers })
            .toPromise()
            .then(function (res) {
            console.log(res);
            return Promise.resolve(res);
        }).catch(function (err) {
            console.error(err);
            return Promise.reject(err);
        });
    };
    RequestsService.prototype.getTransfer = function (transferId) {
        var token = 'bearer ' + this.storageService.getUser().token;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Content-Type': "text/plain",
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.get(this.url + ("/transfer/" + transferId), { headers: headers })
            .toPromise()
            .then(function (res) {
            return Promise.resolve(res);
        })
            .catch(function (err) { return Promise.reject(err); });
    };
    RequestsService.prototype.deleteTransfer = function (transferId) {
        var token = 'bearer ' + this.storageService.getUser().token;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.delete(this.url + ("/transfer/" + transferId), { headers: headers })
            .toPromise()
            .then(function (res) {
            console.log(res);
            return Promise.resolve(res);
        }).catch(function (err) {
            console.error(err);
            return Promise.reject(err);
        });
    };
    RequestsService.prototype.editTransfer = function (transfer) {
        var token = 'bearer ' + this.storageService.getUser().token;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Authorization': token,
            'Accept': 'application/json, text/plain, */*'
        });
        return this.http.put(this.url + ("/transfer/" + transfer.id), transfer, { headers: headers })
            .toPromise()
            .then(function (res) {
            // console.log(res);
            return Promise.resolve(res);
        }).catch(function (err) {
            console.error(err);
            return Promise.reject(err);
        });
    };
    RequestsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _local_storage_service__WEBPACK_IMPORTED_MODULE_3__["LocalStorageService"]])
    ], RequestsService);
    return RequestsService;
}());



/***/ }),

/***/ "./src/app/smite-modals/components/modal/modal.component.html":
/*!********************************************************************!*\
  !*** ./src/app/smite-modals/components/modal/modal.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ng-content></ng-content>"

/***/ }),

/***/ "./src/app/smite-modals/components/modal/modal.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/smite-modals/components/modal/modal.component.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  /* modals are hidden by default */\n  display: none; }\n  :host .smite-modal {\n    /* modal container fixed across whole screen */\n    position: fixed;\n    top: 0;\n    right: 0;\n    bottom: 0;\n    left: 0;\n    /* z-index must be higher than .smite-modal-background */\n    z-index: 1000;\n    /* enables scrolling for tall modals */\n    overflow: auto; }\n  :host .smite-modal .smite-modal-body {\n      padding: 20px;\n      background: #fff;\n      /* margin exposes part of the modal background */\n      margin: 40px; }\n  :host .smite-modal-background {\n    /* modal background fixed across whole screen */\n    position: fixed;\n    top: 0;\n    right: 0;\n    bottom: 0;\n    left: 0;\n    /* semi-transparent black  */\n    background-color: #000;\n    opacity: 0.75;\n    /* z-index must be below .smite-modal and above everything else  */\n    z-index: 900; }\n  body.smite-modal-open {\n  /* body overflow is hidden to hide main scrollbar when modal window is open */\n  overflow: hidden; }\n"

/***/ }),

/***/ "./src/app/smite-modals/components/modal/modal.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/smite-modals/components/modal/modal.component.ts ***!
  \******************************************************************/
/*! exports provided: ModalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalComponent", function() { return ModalComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_modals_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/modals.service */ "./src/app/smite-modals/services/modals.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ModalComponent = /** @class */ (function () {
    function ModalComponent(modals, el) {
        this.modals = modals;
        this.el = el;
        this.element = el.nativeElement;
    }
    ModalComponent.prototype.ngOnInit = function () {
        var modal = this;
        // ensure id attribute exists
        if (!this.id) {
            console.error('modal must have an id');
            return;
        }
        // move element to bottom of page (just before </body>) so it can be displayed above everything else
        document.body.appendChild(this.element);
        // close modal on background click
        this.element.addEventListener('click', function (e) {
            var target = e.target;
            // if modal-background is found, we clicked outside of the modal
            var modalBackground = target.closest('.smite-modal-background');
            // close modal if we clicked on the background
            if (modalBackground) {
                modal.close();
            }
        });
        // add self (this modal instance) to the modal service so it's accessible from controllers
        this.modals.add(this);
    };
    // remove self from modal service when directive is destroyed
    ModalComponent.prototype.ngOnDestroy = function () {
        this.modals.remove(this.id);
        this.element.remove();
    };
    // open modal
    ModalComponent.prototype.open = function () {
        // display element
        this.element.style.display = "block";
        document.body.classList.add('smite-modal-open');
    };
    // close modal
    ModalComponent.prototype.close = function () {
        // hide element
        this.element.style.display = "none";
        document.body.classList.remove('smite-modal-open');
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], ModalComponent.prototype, "id", void 0);
    ModalComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            //moduleId: module.id.toString(),
            selector: 'smite-modal',
            template: __webpack_require__(/*! ./modal.component.html */ "./src/app/smite-modals/components/modal/modal.component.html"),
            styles: [__webpack_require__(/*! ./modal.component.scss */ "./src/app/smite-modals/components/modal/modal.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_modals_service__WEBPACK_IMPORTED_MODULE_1__["ModalsService"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]])
    ], ModalComponent);
    return ModalComponent;
}());



/***/ }),

/***/ "./src/app/smite-modals/index.ts":
/*!***************************************!*\
  !*** ./src/app/smite-modals/index.ts ***!
  \***************************************/
/*! exports provided: ModalComponent, ModalsService, SmiteModalsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _smite_modals_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./smite-modals.module */ "./src/app/smite-modals/smite-modals.module.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "SmiteModalsModule", function() { return _smite_modals_module__WEBPACK_IMPORTED_MODULE_0__["SmiteModalsModule"]; });

/* harmony import */ var _components_modal_modal_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/modal/modal.component */ "./src/app/smite-modals/components/modal/modal.component.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ModalComponent", function() { return _components_modal_modal_component__WEBPACK_IMPORTED_MODULE_1__["ModalComponent"]; });

/* harmony import */ var _services_modals_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./services/modals.service */ "./src/app/smite-modals/services/modals.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ModalsService", function() { return _services_modals_service__WEBPACK_IMPORTED_MODULE_2__["ModalsService"]; });






/***/ }),

/***/ "./src/app/smite-modals/services/modals.service.ts":
/*!*********************************************************!*\
  !*** ./src/app/smite-modals/services/modals.service.ts ***!
  \*********************************************************/
/*! exports provided: ModalsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalsService", function() { return ModalsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var ModalsService = /** @class */ (function () {
    function ModalsService() {
        this.modals = [];
    }
    ModalsService.prototype.add = function (modal) {
        this.modals.push(modal);
    };
    ModalsService.prototype.remove = function (id) {
        var modalToRemove = this.modals.findIndex(function (item) { return item.id == id; });
        if (modalToRemove)
            this.modals.splice(modalToRemove, 1);
    };
    ModalsService.prototype.open = function (id) {
        var modalToOpen = this.modals.find(function (item) { return item.id == id; });
        if (modalToOpen)
            modalToOpen.open();
    };
    ModalsService.prototype.close = function (id) {
        // close modal specified by id
        var modalToClose = this.modals.find(function (item) { return item.id == id; });
        if (modalToClose)
            modalToClose.close();
    };
    ModalsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])()
    ], ModalsService);
    return ModalsService;
}());



/***/ }),

/***/ "./src/app/smite-modals/smite-modals.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/smite-modals/smite-modals.module.ts ***!
  \*****************************************************/
/*! exports provided: SmiteModalsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SmiteModalsModule", function() { return SmiteModalsModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _components_modal_modal_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/modal/modal.component */ "./src/app/smite-modals/components/modal/modal.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SmiteModalsModule = /** @class */ (function () {
    function SmiteModalsModule() {
    }
    SmiteModalsModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"]
            ],
            declarations: [
                _components_modal_modal_component__WEBPACK_IMPORTED_MODULE_2__["ModalComponent"]
            ],
            exports: [
                _components_modal_modal_component__WEBPACK_IMPORTED_MODULE_2__["ModalComponent"]
            ]
        })
    ], SmiteModalsModule);
    return SmiteModalsModule;
}());



/***/ }),

/***/ "./src/app/transfer-info/transfer-info.component.html":
/*!************************************************************!*\
  !*** ./src/app/transfer-info/transfer-info.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n  <div class=\"col-md-3 col-lg-3\"></div>\n  <div class=\"col-md-6 col-lg-6\">\n    <div class=\"return-btn-wrapper\">\n      <a routerLink=\"/transferts\" routerLinkActive=\"active\">\n        <fa name=\"angle-left\"></fa>\n        <span>Retour a la liste des transferts</span>\n      </a>\n    </div>\n  </div>\n  <div class=\"col-md-3 col-lg-3\"></div>\n</div>\n\n<div class=\"row\">\n  <div class=\"col-md-3 col-lg-3\"></div>\n  <div class=\"col-md-6 col-lg-6\">\n    <div class=\"transfer-info-wrapper panel panel-primary\">\n      <div class=\"panel-heading\">\n        <span class=\"panel-title\">Transfert</span>\n      </div>\n      <div class=\"list-group\">\n        <div class=\"list-group-item\">\n          <span>ID:\n            <span class=\"asset-description\">{{currTransfer?.id || \"-\"}}</span>\n          </span>\n        </div>\n        <div class=\"list-group-item\">\n          <span>Status:\n            <span class=\"asset-description\">{{getTransfertStatus(currTransfer)}}</span>\n          </span>\n        </div>\n        <div class=\"list-group-item\">\n          <span>Contact:\n            <span class=\"asset-description\">{{currTransfer?.destination?.contact || '-'}}</span>\n          </span>\n        </div>\n        <div class=\"list-group-item\">\n          <span>Nom:\n            <span class=\"asset-description\">{{currTransfer?.destination?.name || '-'}}</span>\n          </span>\n        </div>\n        <div class=\"list-group-item\">\n          <span>Type de destination:\n            <span class=\"asset-description\">{{transferDestinationType || '-'}}</span>\n          </span>\n        </div>\n        <div class=\"list-group-item\">\n          <span>Opensource:\n            <span class=\"asset-description\">{{currTransfer?.openSource?.exists ? currTransfer?.openSource?.hyperlink : 'Non'}}</span>\n          </span>\n        </div>\n        <div class=\"list-group-item\">\n          <span>Communauté scientifique:\n            <span class=\"asset-description\">{{currTransfer?.sciCommunity?.exists ? currTransfer?.sciCommunity?.hyperlink : 'Non'}}</span>\n          </span>\n        </div>\n        <div class=\"list-group-item\">\n          <span>Ouverte:\n            <span class=\"asset-description\">{{currTransfer?.open?.exists ? currTransfer?.open?.hyperlink : 'Non'}}</span>\n          </span>\n        </div>\n        <div class=\"list-group-item\">\n          <span>Nombre d'Awards:\n            <span class=\"asset-description\">{{currTransfer?.indicators?.awardsCount < 0 ? 'Non' : currTransfer?.indicators?.awardsCount}}</span>\n          </span>\n        </div>\n        <div class=\"list-group-item\">\n          <span>Impact business:\n            <span class=\"asset-description\">{{currTransfer?.indicators?.businessImpact || '-'}}</span>\n          </span>\n        </div>\n        <div class=\"list-group-item\">\n          <span>Nombre de téléchargements:\n            <span class=\"asset-description\">{{currTransfer?.indicators?.downloadsCount < 0 ? 'Non' : currTransfer?.indicators?.downloadsCount}}</span>\n          </span>\n        </div>\n        <div class=\"list-group-item\">\n          <span>Nombre d'expérimentations:\n            <span class=\"asset-description\">{{currTransfer?.indicators?.experimentationsCount < 0 ? 'Non' : currTransfer?.indicators?.experimentationsCount}}</span>\n          </span>\n        </div>\n        <div class=\"list-group-item\">\n          <span>POC industrialisé:\n            <span class=\"asset-description\">{{currTransfer?.indicators?.industrializedPoC ? 'Oui' : 'Non'}}</span>\n          </span>\n        </div>\n        <div class=\"list-group-item\">\n          <span>License coût:\n            <span class=\"asset-description\">{{currTransfer?.indicators?.licenseCost < 0 ? 'Non' : currTransfer?.indicators?.licenseCost}}</span>\n          </span>\n        </div>\n        <div class=\"list-group-item\">\n          <span>License return:\n            <span class=\"asset-description\">{{currTransfer?.indicators?.licenseReturn ? 'Oui' : 'Non'}}</span>\n          </span>\n        </div>\n        <div class=\"list-group-item\">\n          <span>Nombre d'utilisateurs:\n            <span class=\"asset-description\">{{currTransfer?.indicators?.usersCount < 0 ? 'Non' : currTransfer?.indicators?.usersCount}}</span>\n          </span>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"col-md-3 col-lg-3\"></div>\n</div>\n\n<div class=\"row\">\n  <div class=\"col-md-3 col-lg-3\"></div>\n  <div class=\"col-md-6 col-lg-6\">\n    <div class=\"table-wrapper\">\n      <div class=\"table-header\">Actif valorisable transféré</div>\n      <p-treeTable [value]=\"assets?.data\">\n        <p-column [style]=\"{'width':'15%'}\">\n          <ng-template let-col let-node=\"rowData\">\n            <div class=\"td-wrapper\" [ngStyle]=\"{'top': '0'}\">\n              <p>{{node.data.id}}</p>\n              <p>{{node.data.type}}</p>\n            </div>\n          </ng-template>\n        </p-column>\n\n        <p-column  [style]=\"{'width':'55%'}\">\n          <ng-template let-col let-node=\"rowData\">\n            <div class=\"td-wrapper second-td\" [ngStyle]=\"{'top': '6px'}\">\n              <p class=\"asset-description\">{{node.data.identification.name}}\n                <span class=\"badge\" [ngClass]=\"node.data.isPlatformAsset ? 'plateforme' : 'unitaire'\">{{node.data.isPlatformAsset ? 'Démonstrateur' : 'Unitaire'}}</span>\n              </p>\n              <p class=\"asset-description\">{{node.data.desc}}</p>\n              <p class=\"asset-description\">{{node.data.status == 'PENDING' ? 'En cours de rédaction' : node.data.status == 'WAITING_VALIDATION' ? 'Pour validation CODIR' : 'Validé'}}</p>\n            </div>\n          </ng-template>\n        </p-column>\n\n        <p-column [style]=\"{'width':'10%'}\">\n          <ng-template let-col let-node=\"rowData\">\n            <div class=\"td-wrapper\">\n              <p>{{node.data.identification.program.shortName}}</p>\n            </div>\n          </ng-template>\n        </p-column>\n\n        <p-column [style]=\"{'width':'10%'}\">\n          <ng-template let-col let-node=\"rowData\">\n            <div class=\"td-wrapper\">\n              <p>{{node.data.identification.project.shortName}}</p>\n            </div>\n          </ng-template>\n        </p-column>\n\n        <p-column [style]=\"{'width':'10%'}\">\n          <ng-template let-col let-node=\"rowData\">\n            <div class=\"icons-wrapper\">\n              <fa class=\"user-icon\" name=\"eye\" (click)=\"viewAssetInfo(node.data)\" [ngClass]=\"{'disabled': !isAdmin()}\"></fa>\n            </div>\n          </ng-template>\n        </p-column>\n      </p-treeTable>\n    </div>\n  </div>\n  <div class=\"col-md-3 col-lg-3\"></div>\n</div>\n                \n                "

/***/ }),

/***/ "./src/app/transfer-info/transfer-info.component.scss":
/*!************************************************************!*\
  !*** ./src/app/transfer-info/transfer-info.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".table-wrapper .table-header {\n  padding: 10px;\n  padding-right: 0;\n  border-top-left-radius: 3px;\n  border-top-right-radius: 3px;\n  font-size: 15px;\n  text-align: left;\n  background-color: #337ab7;\n  border-color: #337ab7;\n  color: #fff;\n  font-weight: 600;\n  width: 100%;\n  height: 42px; }\n\n.table-wrapper ::ng-deep.ui-treetable .ui-treetable-row {\n  border-bottom: 0.05vw solid #d9d9d9;\n  width: 100%;\n  height: 90px;\n  border-right: 0.05vw solid #d9d9d9;\n  padding: 0 1vw; }\n\n.table-wrapper ::ng-deep.ui-treetable .ui-treetable-row .ui-treetable-child-table-container {\n    background-color: #F5F5F5; }\n\n.table-wrapper ::ng-deep.ui-treetable {\n  margin-bottom: 3vw; }\n\n.table-wrapper ::ng-deep.ui-treetable table {\n    table-layout: fixed !important; }\n\n.table-wrapper ::ng-deep.ui-treetable ::ng-deep td {\n    padding: 0 !important;\n    font-size: 15px;\n    border-right: 1px solid #ddd; }\n\n.table-wrapper ::ng-deep.ui-treetable ::ng-deep td p {\n      text-align: center; }\n\n.table-wrapper ::ng-deep.ui-treetable thead {\n    display: none; }\n\n.table-wrapper ::ng-deep.ui-treetable table {\n    table-layout: auto; }\n\n.table-wrapper ::ng-deep.ui-treetable ::ng-deep td {\n    font-size: 15px; }\n\n.table-wrapper ::ng-deep.ui-treetable .second-td {\n    padding-left: 12px; }\n\n.table-wrapper ::ng-deep.ui-treetable .second-td p {\n      text-align: left; }\n\n.table-wrapper ::ng-deep.ui-treetable .td-wrapper {\n    position: relative;\n    top: 37px; }\n\n.table-wrapper ::ng-deep.ui-treetable .icons-wrapper {\n    text-align: center; }\n\n.table-wrapper ::ng-deep.ui-treetable .icons-wrapper .user-icon:hover {\n      cursor: pointer; }\n\n.table-wrapper ::ng-deep.ui-treetable .icons-wrapper .user-icon.disabled {\n      opacity: 0.75;\n      /* Opacity (Transparency) */\n      color: rgba(0, 0, 0, 0.75);\n      /* RGBA Color (Alternative Transparency) */\n      -webkit-filter: blur(0.5px);\n      /* Blur */\n      -moz-filter: blur(0.5px);\n      filter: blur(0.5px); }\n\n.table-wrapper ::ng-deep.ui-treetable .icons-wrapper .user-icon.disabled:hover {\n        cursor: default; }\n\n.table-wrapper ::ng-deep.ui-treetable .icons-wrapper fa {\n      position: relative;\n      top: 37px; }\n\n.asset-description {\n  word-break: break-word;\n  white-space: nowrap;\n  padding: 0 0.5vw;\n  font-size: 15px;\n  text-overflow: ellipsis;\n  display: block;\n  overflow: hidden;\n  text-align: left; }\n\n.asset-description .plateforme {\n    background-color: #5cb85c; }\n\n.asset-description .unitaire {\n    background-color: #337ab7; }\n\n.row {\n  width: 100%; }\n\n.return-btn-wrapper {\n  height: 140px;\n  padding-top: 62px; }\n\n.return-btn-wrapper fa {\n    font-size: 16px;\n    margin-right: 0.5vw; }\n\n.transfer-info-wrapper {\n  margin: 5vw auto;\n  margin-top: 0; }\n\n.transfer-info-wrapper .statut-de-la-fiche-wrapper {\n    padding: 13px;\n    width: 100%;\n    height: 155px;\n    border: none;\n    margin-bottom: 0; }\n\n.transfer-info-wrapper .statut-de-la-fiche-wrapper label:hover {\n      cursor: pointer; }\n\n.transfer-info-wrapper .statut-de-la-fiche-wrapper div {\n      display: block;\n      margin: 1vw 0 0 0;\n      text-align: center; }\n\n.transfer-info-wrapper .statut-de-la-fiche-wrapper .btn-wrapper {\n      padding: 0;\n      margin: 0;\n      height: 40px; }\n\n.transfer-info-wrapper .statut-de-la-fiche-wrapper .btn-wrapper button {\n        cursor: pointer;\n        margin: 0 auto;\n        display: block; }\n\n.transfer-info-wrapper .statut-de-la-fiche-wrapper span {\n      text-transform: uppercase;\n      font-weight: 600; }\n\n.transfer-info-wrapper span {\n    font-size: 15px; }\n\n.transfer-info-wrapper div span {\n    font-size: 15px;\n    font-weight: 600; }\n"

/***/ }),

/***/ "./src/app/transfer-info/transfer-info.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/transfer-info/transfer-info.component.ts ***!
  \**********************************************************/
/*! exports provided: TransferInfoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransferInfoComponent", function() { return TransferInfoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_requests_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/requests.service */ "./src/app/services/requests.service.ts");
/* harmony import */ var _services_local_storage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/local-storage.service */ "./src/app/services/local-storage.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_pass_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/pass.service */ "./src/app/services/pass.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var TransferInfoComponent = /** @class */ (function () {
    function TransferInfoComponent(router, aRouter, requests, storageService, passService) {
        this.router = router;
        this.aRouter = aRouter;
        this.requests = requests;
        this.storageService = storageService;
        this.passService = passService;
        this.assets = {};
        this.typeTouched = true;
        this.transferType = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('PENDING', []);
        this.programName = '';
    }
    Object.defineProperty(TransferInfoComponent.prototype, "transferDestinationType", {
        get: function () {
            if (this.currTransfer) {
                if (this.currTransfer.destination.type === 'PARTNER') {
                    return 'Partenaire';
                }
                else if (this.currTransfer.destination.type === 'PROJECT') {
                    return 'Projet';
                }
                else {
                    return '-';
                }
            }
            return '-';
        },
        enumerable: true,
        configurable: true
    });
    TransferInfoComponent.prototype.toCloseInfo = function () {
        this.closeInfo();
    };
    TransferInfoComponent.prototype.updateData = function (nodes) {
        var _this = this;
        return nodes ? nodes.map(function (node) {
            var asset = {
                'data': node,
                'children': node.children ? _this.updateData(node.children) : []
            };
            return asset;
        }) : [];
    };
    TransferInfoComponent.prototype.isAdmin = function () {
        return this.passService.isAdmin();
    };
    TransferInfoComponent.prototype.isPriviledged = function () {
        return this.passService.isPriviledged();
    };
    TransferInfoComponent.prototype.getTransfertStatus = function (transfert) {
        return this.passService.getTransfertStatus(transfert);
    };
    TransferInfoComponent.prototype.onValiderBtnClick = function () {
        var _this = this;
        this.currTransfer.status = this.transferType.value;
        this.currTransfer.status = 'VALIDATED';
        this.requests.editTransfer(this.currTransfer).then(function (res) {
            console.log(res);
            _this.ngOnInit();
        }).catch(function (err) {
            alert(err.message);
        });
    };
    TransferInfoComponent.prototype.viewAssetInfo = function (asset) {
        this.storageService.setAsset(asset);
        this.router.navigate(["/asset_info/" + asset.id]);
    };
    TransferInfoComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (!this.isPriviledged()) {
            alert('You are not allowed to see this page');
            this.router.navigate(['main']);
        }
        this.requests.getProgramsList().then(function (res) {
            console.log(res);
        }).catch(function (err) {
            console.log(err);
        });
        this.transferType.valueChanges.subscribe(function (res) {
            _this.typeTouched = _this.currTransfer.status === _this.transferType.value;
            console.log(_this.typeTouched, res, _this.currTransfer.status, _this.transferType);
        });
        this.aRouter.params.subscribe(function (params) {
            var currTranferId = params['id']; // --> Name must match wanted parameter
            _this.requests.getTransfer(currTranferId).then(function (res) {
                _this.currTransfer = res;
                _this.transferType = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](_this.currTransfer.status, []);
                _this.transferType.valueChanges.subscribe(function (res) {
                    _this.typeTouched = _this.currTransfer.status === _this.transferType.value;
                    _this.currTransfer.status = _this.transferType.value;
                    _this.requests.editTransfer(_this.currTransfer).then(function (res) {
                        console.log(res);
                        _this.currTransfer = res;
                    }).catch(function (err) {
                        alert(err.message);
                    });
                    console.log(_this.typeTouched, res, _this.currTransfer.status, _this.transferType);
                });
                _this.assets = {
                    'data': _this.updateData(_this.currTransfer.assets)
                };
            }).catch(function (err) {
                console.error(err);
            });
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('closeInfo'),
        __metadata("design:type", Function)
    ], TransferInfoComponent.prototype, "closeInfo", void 0);
    TransferInfoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-transfer-info',
            template: __webpack_require__(/*! ./transfer-info.component.html */ "./src/app/transfer-info/transfer-info.component.html"),
            styles: [__webpack_require__(/*! ./transfer-info.component.scss */ "./src/app/transfer-info/transfer-info.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _services_requests_service__WEBPACK_IMPORTED_MODULE_2__["RequestsService"],
            _services_local_storage_service__WEBPACK_IMPORTED_MODULE_3__["LocalStorageService"],
            _services_pass_service__WEBPACK_IMPORTED_MODULE_5__["PassService"]])
    ], TransferInfoComponent);
    return TransferInfoComponent;
}());



/***/ }),

/***/ "./src/app/transfers-list/transfers-list.component.html":
/*!**************************************************************!*\
  !*** ./src/app/transfers-list/transfers-list.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n  <div class=\"col-md-1 col-lg-1\"></div>\n  <div class=\"col-md-10 col-lg-10\">\n    <div class=\"transfer-list-wrapper\">\n      <div class=\"table-header\">Liste des transferts</div>\n      <span class=\"results-number\" title=\"Nombre de transferts\">{{transfers?.data?.length}}</span>\n      <p-treeTable [value]=\"transfers?.data\">\n        <p-column>\n          <ng-template let-col let-node=\"rowData\">\n            <div class=\"td-wrapper\" [ngStyle]=\"{'top': '-10px'}\">\n              <p>{{node.data.destination.name}}</p>\n              <p>{{node.data.id}}</p>\n              <p>{{getTransfertType(node.data.destination.type)}}</p>\n            </div>\n          </ng-template>\n        </p-column>\n\n        <p-column [style]=\"{'width':'50%'}\">\n          <ng-template let-col let-node=\"rowData\">\n            <div class=\"td-wrapper\">\n              <p *ngFor=\"let asset of node?.data?.assets\">\n                <strong>{{asset?.id}}:</strong><span style=\"margin-left: 1rem\">{{asset?.identification.name}}</span>\n              </p>\n            </div>\n          </ng-template>\n        </p-column>\n\n        <p-column>\n          <ng-template let-col let-node=\"rowData\">\n            <div class=\"td-wrapper\">\n              <p>{{node.data.detailedDescription}}</p>\n              <p>{{node.data.label}}</p>\n              <p>{{getTransfertStatus(node.data)}}</p>\n            </div>\n          </ng-template>\n        </p-column>\n\n        <!--<p-column>-->\n        <!--<ng-template let-col let-node=\"rowData\">-->\n        <!--<div class=\"td-wrapper\">-->\n        <!--<p></p>-->\n        <!--</div>-->\n        <!--</ng-template>-->\n        <!--</p-column>-->\n\n        <p-column [style]=\"{'width':'15%'}\">\n          <ng-template let-col let-node=\"rowData\">\n            <div class=\"icons-wrapper\">\n              <fa class=\"user-icon\" name=\"eye\" (click)=\"viewTransferInfo(node.data)\"></fa>\n              <!--<fa class=\"user-icon\" name=\"pencil\" (click)=\"editAsset(node.data)\" [ngClass]=\"{'disabled': user.role!=='ADMIN' }\"></fa>-->\n              <fa class=\"user-icon\" name=\"pencil\" (click)=\"toEditTransfer(node.data)\"></fa>\n              <!--<fa class=\"user-icon\" name=\"plus\" (click)=\"toCreateTransfer(node.data)\" [ngClass]=\"{'disabled': user.role!=='ADMIN' && user.role!=='SUPER_USER'}\"></fa>-->\n              <fa class=\"user-icon\" name=\"trash\" (click)=\"deleteTransfer(node.data)\"></fa>\n            </div>\n          </ng-template>\n        </p-column>\n      </p-treeTable>\n\n      <app-transfer-info *ngIf=\"isTransferInfoVisible\" [closeInfo]=\"closeInfo.bind(this)\"></app-transfer-info>\n\n    </div>\n  </div>\n  <div class=\"col-md-1 col-lg-1\"></div>\n</div>\n"

/***/ }),

/***/ "./src/app/transfers-list/transfers-list.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/transfers-list/transfers-list.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host .row {\n  padding-top: 90px;\n  margin-bottom: 20px; }\n\n.transfer-list-wrapper {\n  position: relative; }\n\n.transfer-list-wrapper .results-number {\n    position: absolute;\n    right: 20px;\n    top: 10px;\n    font-size: 1.2em;\n    font-weight: bolder;\n    color: white; }\n\n.transfer-list-wrapper .table-header {\n    padding: 10px;\n    padding-right: 0;\n    font-size: 15px;\n    text-align: left;\n    background-color: #337ab7;\n    border-color: #337ab7;\n    color: #fff;\n    font-weight: 600;\n    width: 100%;\n    height: 42px; }\n\n.transfer-list-wrapper ::ng-deep.ui-treetable .ui-treetable-row {\n    width: 100%;\n    height: 90px; }\n\n.transfer-list-wrapper ::ng-deep.ui-treetable .ui-treetable-row .ui-treetable-child-table-container {\n      background-color: #F5F5F5; }\n\n.transfer-list-wrapper ::ng-deep.ui-treetable table {\n    table-layout: fixed !important; }\n\n.transfer-list-wrapper ::ng-deep.ui-treetable ::ng-deep td {\n    padding: 0 !important;\n    font-size: 15px;\n    border-right: 1px solid #ddd; }\n\n.transfer-list-wrapper ::ng-deep.ui-treetable ::ng-deep td p {\n      text-align: center; }\n\n.transfer-list-wrapper ::ng-deep.ui-treetable thead {\n    display: none; }\n\n.transfer-list-wrapper ::ng-deep.ui-treetable table {\n    table-layout: auto; }\n\n.transfer-list-wrapper ::ng-deep.ui-treetable ::ng-deep td {\n    font-size: 15px; }\n\n.transfer-list-wrapper ::ng-deep.ui-treetable .second-td {\n    padding-left: 12px; }\n\n.transfer-list-wrapper ::ng-deep.ui-treetable .second-td p {\n      text-align: left; }\n\n.transfer-list-wrapper ::ng-deep.ui-treetable .td-wrapper {\n    position: relative;\n    top: 35px; }\n\n.transfer-list-wrapper ::ng-deep.ui-treetable .icons-wrapper {\n    position: relative;\n    top: 40px; }\n\n.transfer-list-wrapper ::ng-deep.ui-treetable .icons-wrapper fa {\n      width: 33.3%;\n      display: block;\n      float: left;\n      text-align: center; }\n\n.transfer-list-wrapper ::ng-deep.ui-treetable .icons-wrapper .user-icon:hover {\n      cursor: pointer; }\n\n.transfer-list-wrapper ::ng-deep.ui-treetable .icons-wrapper .user-icon.disabled {\n      opacity: 0.75;\n      /* Opacity (Transparency) */\n      color: rgba(0, 0, 0, 0.75);\n      /* RGBA Color (Alternative Transparency) */\n      -webkit-filter: blur(0.5px);\n      /* Blur */\n      -moz-filter: blur(0.5px);\n      filter: blur(0.5px); }\n\n.transfer-list-wrapper ::ng-deep.ui-treetable .icons-wrapper .user-icon.disabled:hover {\n        cursor: default; }\n\n.transfer-list-wrapper .panel-heading {\n    padding: 15px;\n    margin: 0; }\n"

/***/ }),

/***/ "./src/app/transfers-list/transfers-list.component.ts":
/*!************************************************************!*\
  !*** ./src/app/transfers-list/transfers-list.component.ts ***!
  \************************************************************/
/*! exports provided: TransfersListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransfersListComponent", function() { return TransfersListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_requests_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/requests.service */ "./src/app/services/requests.service.ts");
/* harmony import */ var _services_local_storage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/local-storage.service */ "./src/app/services/local-storage.service.ts");
/* harmony import */ var _services_pass_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/pass.service */ "./src/app/services/pass.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TransfersListComponent = /** @class */ (function () {
    function TransfersListComponent(router, requests, storageService, passService) {
        var _this = this;
        this.router = router;
        this.requests = requests;
        this.storageService = storageService;
        this.passService = passService;
        this.transfersObj = {};
        this.transfers = {};
        this.isTransferInfoVisible = false;
        this.user = {};
        this.userSubscription = storageService.getUserObservable().subscribe(function (user) {
            // console.log('user updated => ', user);
            _this.user = user;
            if (!_this.user) {
                _this.router.navigate(['login']);
            }
        });
    }
    TransfersListComponent.prototype.getTransfertStatus = function (transferData) {
        return this.passService.getTransfertStatus(transferData);
    };
    TransfersListComponent.prototype.getTransfertType = function (type) {
        return this.passService.getTransfertType(type);
    };
    TransfersListComponent.prototype.deleteTransfer = function (transferData) {
        var _this = this;
        console.log('deleting transfer => ', transferData);
        // if (this.user.role !== 'ADMIN' && this.user.role !== 'SUPER_USER') { return; }
        var deleteTransfer = confirm("Delete asset \"" + transferData.label + "\"");
        if (!deleteTransfer) {
            return;
        }
        this.requests.deleteTransfer(transferData.id).then(function (res) {
            console.log(res);
            _this.getTransfers();
        }).catch(function (err) {
            console.error(err);
            _this.getTransfers();
        });
    };
    TransfersListComponent.prototype.toEditTransfer = function (transferData) {
        console.log('editing transfer => ', transferData);
        this.router.navigate(["edit_transfer/" + transferData.id]);
    };
    TransfersListComponent.prototype.viewTransferInfo = function (transferData) {
        // console.log('info transfer => ', transferData);
        // localStorage.setItem('currTransfer', JSON.stringify(transferData));
        // this.isTransferInfoVisible = true;
        this.router.navigate(["transfer_info/" + transferData.id]);
    };
    TransfersListComponent.prototype.closeInfo = function () {
        this.isTransferInfoVisible = false;
    };
    TransfersListComponent.prototype.updateData = function (nodes) {
        var _this = this;
        // console.log(nodes);
        return nodes ? nodes.map(function (node) {
            var tranfer = {
                'data': node,
            };
            _this.transfersObj[tranfer.data.id] = tranfer;
            return tranfer;
        }) : [];
    };
    TransfersListComponent.prototype.getTransfers = function () {
        // console.log('getting transfers');
        var _this = this;
        this.requests.getTransfers().then(function (res) {
            console.log('asdasda => ', res);
            _this.transfers = {
                'data': _this.updateData(res)
            };
        }).catch(function (err) {
            if (err.status === 403) {
                alert('You are not allowed to see this page');
                _this.router.navigate(['main']);
            }
            console.error(err);
        });
    };
    TransfersListComponent.prototype.ngOnInit = function () {
        this.getTransfers();
    };
    TransfersListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-transfers-list',
            template: __webpack_require__(/*! ./transfers-list.component.html */ "./src/app/transfers-list/transfers-list.component.html"),
            styles: [__webpack_require__(/*! ./transfers-list.component.scss */ "./src/app/transfers-list/transfers-list.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _services_requests_service__WEBPACK_IMPORTED_MODULE_2__["RequestsService"],
            _services_local_storage_service__WEBPACK_IMPORTED_MODULE_3__["LocalStorageService"],
            _services_pass_service__WEBPACK_IMPORTED_MODULE_4__["PassService"]])
    ], TransfersListComponent);
    return TransfersListComponent;
}());



/***/ }),

/***/ "./src/environments/environment.prod.ts":
/*!**********************************************!*\
  !*** ./src/environments/environment.prod.ts ***!
  \**********************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
var environment = {
    production: true,
    REST_API_URL: 'http://35.180.162.253:3000'
    // REST_API_URL: 'http://192.168.31.213:3005'
    // REST_API_URL: 'https://agav-backend.irtsystemx.org'
};


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/geek/Projects/frontend/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map